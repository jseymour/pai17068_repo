/**************************************************************************** 
*                    B & R   P O S I T I O N I N G                          * 
***************************************************************************** 
*                                                                           * 
*                  Header File for Library ACP10MAN                         * 
*                                                                           * 
**************************** COPYRIGHT (C) ********************************** 
*     THIS SOFTWARE IS THE PROPERTY OF B&R AUSTRIA: ALL RIGHTS RESERVED.    * 
*     NO PART OF THIS SOFTWARE MAY BE USED OR COPIED IN ANY WAY WITHOUT     * 
*              THE PRIOR WRITTEN PERMISSION OF B&R AUSTRIA.                 * 
****************************************************************************/ 

#ifndef ACP10MAN_H_VERSION 
#define ACP10MAN_H_VERSION 0x0544 

#include <ncglobal.h>
#include <acp10par.h>


/*** DATA TYPES *************************************************************/ 

typedef struct ACP10SWVER_typ {           /* SW-Versions-ID [hexadezimal] */
   UINT nc_manager;                       /* NC-Manager */
   UINT nc_system;                        /* NC-Betriebssystem */
} ACP10SWVER_typ;

typedef struct ACP10SIM_typ {            /* Simulationsmodus */
   USINT status;                         /* Status */
   USINT reserve1;                       /* Reserviert */
   UINT  reserve2;                       /* Reserviert */
} ACP10SIM_typ;

typedef struct ACP10GLINI_typ {            /* Globale Initialisierung */
   USINT init;                             /* Globale Initialisierung abgeschlossen */
   USINT reserve1;                         /* Reserviert */
   UINT  reserve2;                         /* Reserviert */
} ACP10GLINI_typ;

typedef struct ACP10NSVRQ_typ {           /* Request (Anforderung an den Antrieb) */
   UINT par_id;                           /* Parameter-ID */
   UINT reserve;                          /* Reserviert */
} ACP10NSVRQ_typ;

typedef struct ACP10NSVRS_typ {           /* Response (Antwort vom Antrieb) */
   UINT par_id;                           /* Parameter-ID */
   UINT reserve;                          /* Reserviert */
} ACP10NSVRS_typ;

typedef struct ACP10NETSV_typ {                     /* Service-Schnittstelle */
   UDINT          daten_adr;                        /* Adresse der Daten */
   USINT          daten_text[32];                   /* Daten im Textformat */
   ACP10NSVRQ_typ request;                          /* Request (Anforderung an den Antrieb) */
   ACP10NSVRS_typ response;                         /* Response (Antwort vom Antrieb) */
} ACP10NETSV_typ;

typedef struct ACP10NET_typ {                     /* Netzwerk */
   USINT          init;                           /* Netzwerk initialisiert */
   USINT          reserve1;                       /* Reserviert */
   UINT           reserve2;                       /* Reserviert */
   ACP10NETSV_typ service;                        /* Service-Schnittstelle */
} ACP10NET_typ;

typedef struct ACP10DISTA_typ {            /* Status */
   USINT referenz;                         /* Referenzschalter */
   USINT pos_hw_end;                       /* Positiver HW-Endschalter */
   USINT neg_hw_end;                       /* Negativer HW-Endschalter */
   USINT trigger1;                         /* Trigger1 */
   USINT trigger2;                         /* Trigger2 */
   USINT reserve1;                         /* Reserviert */
   UINT  reserve2;                         /* Reserviert */
} ACP10DISTA_typ;

typedef struct ACP10DILEV_typ {           /* Aktive Eingangspegel */
   UINT referenz;                         /* Referenzschalter */
   UINT pos_hw_end;                       /* Positiver HW-Endschalter */
   UINT neg_hw_end;                       /* Negativer HW-Endschalter */
   UINT trigger1;                         /* Trigger1 */
   UINT trigger2;                         /* Trigger2 */
   UINT reserve;                          /* Reserviert */
} ACP10DILEV_typ;

typedef struct ACP10DIFRC_typ {            /* Status der digitalen Eing�nge �ber Force-Funktion setzen */
   USINT referenz;                         /* Referenzschalter */
   USINT pos_hw_end;                       /* Positiver HW-Endschalter */
   USINT neg_hw_end;                       /* Negativer HW-Endschalter */
   USINT trigger1;                         /* Trigger1 */
   USINT trigger2;                         /* Trigger2 */
   USINT reserve1;                         /* Reserviert */
   UINT  reserve2;                         /* Reserviert */
} ACP10DIFRC_typ;

typedef struct ACP10DIGIN_typ {                     /* Digitale Eing�nge */
   USINT          init;                             /* Digitale Eing�nge initialisiert */
   USINT          reserve1;                         /* Reserviert */
   UINT           reserve2;                         /* Reserviert */
   ACP10DISTA_typ status;                           /* Status */
   ACP10DILEV_typ pegel;                            /* Aktive Eingangspegel */
   ACP10DIFRC_typ force;                            /* Status der digitalen Eing�nge �ber Force-Funktion setzen */
} ACP10DIGIN_typ;

typedef struct ACP10ENCSL_typ {            /* Last */
   UDINT einheiten;                        /* Einheiten an der Last */
   UDINT umdr_motor;                       /* Motorumdrehungen */
} ACP10ENCSL_typ;

typedef struct ACP10ENCSC_typ {                     /* Ma�stab */
   ACP10ENCSL_typ last;                             /* Last [einheiten/umdr_motor] */
} ACP10ENCSC_typ;

typedef struct ACP10ENCPA_typ {                     /* Parameter */
   USINT          zaehlrchtg;                       /* Z�hlrichtung */
   USINT          reserve1;                         /* Reserviert */
   UINT           reserve2;                         /* Reserviert */
   ACP10ENCSC_typ maszstab;                         /* Ma�stab */
} ACP10ENCPA_typ;

typedef struct ACP10ENCIF_typ {                     /* Geber-Interface */
   USINT          init;                             /* Geber-Interface initialisiert */
   USINT          reserve1;                         /* Reserviert */
   UINT           reserve2;                         /* Reserviert */
   ACP10ENCPA_typ parameter;                        /* Parameter */
} ACP10ENCIF_typ;

typedef struct ACP10AXLPA_typ {           /* Parameter */
   REAL v_pos;                            /* Geschwindigkeit in positive Richtung */
   REAL v_neg;                            /* Geschwindigkeit in negative Richtung */
   REAL a1_pos;                           /* Beschleunigung in positive Richtung */
   REAL a2_pos;                           /* Verz�gerung in positive Richtung */
   REAL a1_neg;                           /* Beschleunigung in negative Richtung */
   REAL a2_neg;                           /* Verz�gerung in negative Richtung */
   REAL t_ruck;                           /* Ruckzeit */
   REAL t_in_pos;                         /* Einschwingzeit vor Meldung "In Position" */
   DINT pos_sw_end;                       /* Positive SW-Endlage */
   DINT neg_sw_end;                       /* Negative SW-Endlage */
   REAL ds_warnung;                       /* Schleppfehlergrenzwert f�r Ausgabe einer Warnung */
   REAL ds_abbruch;                       /* Schleppfehlergrenzwert f�r Abbruch einer Bewegung */
} ACP10AXLPA_typ;

typedef struct ACP10AXLIM_typ {                     /* Grenzwerte */
   USINT          init;                             /* Achsgrenzwerte initialisiert */
   USINT          reserve1;                         /* Reserviert */
   UINT           reserve2;                         /* Reserviert */
   ACP10AXLPA_typ parameter;                        /* Parameter */
} ACP10AXLIM_typ;

typedef struct ACP10CTRPO_typ {           /* Lageregler */
   REAL kv;                               /* Proportionalverst�rkung */
   REAL tn;                               /* Nachstellzeit des I-Anteils */
   REAL t_voraus;                         /* Vorausschauzeit */
   REAL t_gesamt;                         /* Gesamtverz�gerung */
   REAL p_max;                            /* Maximaler Proportionaleingriff */
   REAL i_max;                            /* Maximaler Integraleingriff */
} ACP10CTRPO_typ;

typedef struct ACP10CTRSP_typ {           /* Drehzahlregler */
   REAL kv;                               /* Proportionalverst�rkung */
   REAL tn;                               /* Nachstellzeit des I-Anteils */
} ACP10CTRSP_typ;

typedef struct ACP10CTRL_typ {                     /* Regler */
   USINT          init;                            /* Regler initialisiert */
   USINT          bereit;                          /* Bereit */
   USINT          status;                          /* Status */
   USINT          reserve;                         /* Reserviert */
   ACP10CTRPO_typ lage;                            /* Lageregler */
   ACP10CTRSP_typ drehzahl;                        /* Drehzahlregler */
} ACP10CTRL_typ;

typedef struct ACP10AXSTI_typ {            /* Index des Parametersatzes */
   USINT befehl;                           /* f�r den n�chsten Abbruchbefehl */
   USINT reserve1;                         /* Reserviert */
   UINT  reserve2;                         /* Reserviert */
} ACP10AXSTI_typ;

typedef struct ACP10AXSTP_typ {            /* Parameters�tze */
   USINT bremsrampe;                       /* Bremsrampe */
   USINT regler;                           /* Reglerzustand nach Abbruch der Bewegung */
   UINT  reserve;                          /* Reserviert */
} ACP10AXSTP_typ;

typedef struct ACP10AXSTO_typ {                     /* Abbruch einer Bewegung */
   USINT          init;                             /* Abbruch initialisiert */
   USINT          reserve1;                         /* Reserviert */
   UINT           reserve2;                         /* Reserviert */
   ACP10AXSTI_typ index;                            /* Index */
   ACP10AXSTP_typ parameter[4];                     /* Parametersatz */
} ACP10AXSTO_typ;

typedef struct ACP10HOMST_typ {            /* Status */
   USINT ok;                               /* Referenzposition g�ltig */
   USINT reserve1;                         /* Reserviert */
   UINT  reserve2;                         /* Reserviert */
   REAL  tr_s_rel;                         /* Distanz zwischen der Aktivierung von "Triggern auf Referenzimpuls" und dem Auftreten des Referenzimpulses */
   DINT  offset;                           /* Referenzier-Offset nach Abschlu� des Referenzierens */
} ACP10HOMST_typ;

typedef struct ACP10HOMPA_typ {            /* Parameter */
   DINT  s;                                /* Referenzposition */
   REAL  v_schalter;                       /* Geschwindigkeit f�r die Referenzschaltersuche */
   REAL  v_trigger;                        /* Triggergeschwindigkeit */
   REAL  a;                                /* Beschleunigung */
   USINT modus;                            /* Modus */
   USINT sch_flanke;                       /* Referenzschalter-Flanke */
   USINT startrchtg;                       /* Startrichtung */
   USINT triggrchtg;                       /* Triggerrichtung */
   USINT refimpuls;                        /* Referenzimpuls */
   USINT reserve1;                         /* Reserviert */
   UINT  reserve2;                         /* Reserviert */
   REAL  tr_s_block;                       /* Distanz zur Blockierung der Aktivierung von "Triggern auf Referenzimpuls" */
} ACP10HOMPA_typ;

typedef struct ACP10HOME_typ {                     /* Referenzieren */
   USINT          init;                            /* Referenzieren initialisiert */
   USINT          reserve1;                        /* Reserviert */
   UINT           reserve2;                        /* Reserviert */
   ACP10HOMST_typ status;                          /* Status */
   ACP10HOMPA_typ parameter;                       /* Parameter */
} ACP10HOME_typ;

typedef struct ACP10BMVST_typ {            /* Status */
   USINT in_pos;                           /* "In Position" (Zielposition erreicht) */
   USINT reserve1;                         /* Reserviert */
   UINT  reserve2;                         /* Reserviert */
} ACP10BMVST_typ;

typedef struct ACP10BMVOV_typ {           /* Override */
   UINT v;                                /* Geschwindigkeits-Override */
   UINT a;                                /* Beschleunigungs-Override */
} ACP10BMVOV_typ;

typedef struct ACP10BMVPA_typ {           /* Parameter */
   DINT s;                                /* Zielposition oder relative Verfahrdistanz */
   REAL v_pos;                            /* Geschwindigkeit in positive Richtung */
   REAL v_neg;                            /* Geschwindigkeit in negative Richtung */
   REAL a1_pos;                           /* Beschleunigung in positive Richtung */
   REAL a2_pos;                           /* Verz�gerung in positive Richtung */
   REAL a1_neg;                           /* Beschleunigung in negative Richtung */
   REAL a2_neg;                           /* Verz�gerung in negative Richtung */
} ACP10BMVPA_typ;

typedef struct ACP10TRSTP_typ {            /* Modus "Stop nach Trigger" */
   USINT init;                             /* Initialisiert */
   USINT ereignis;                         /* Ereignis */
   UINT  reserve;                          /* Reserviert */
   DINT  s_rest;                           /* Restweg nach Trigger */
} ACP10TRSTP_typ;

typedef struct ACP10BAMOV_typ {                     /* Basis-Bewegungen */
   USINT          init;                             /* Basis-Bewegungen initialisiert */
   USINT          reserve1;                         /* Reserviert */
   UINT           reserve2;                         /* Reserviert */
   ACP10BMVST_typ status;                           /* Status */
   ACP10BMVOV_typ override;                         /* Override */
   ACP10BMVPA_typ parameter;                        /* Parameter */
   ACP10TRSTP_typ trg_stop;                         /* Modus "Stop nach Trigger" */
} ACP10BAMOV_typ;

typedef struct ACP10AXMOV_typ {                     /* Achs-Bewegung */
   UINT           modus;                            /* Modus */
   UINT           detail;                           /* Detail */
   ACP10AXSTO_typ abbruch;                          /* Abbruch einer Bewegung */
   ACP10HOME_typ  referenz;                         /* Referenzieren */
   ACP10BAMOV_typ basis;                            /* Basis-Bewegungen */
} ACP10AXMOV_typ;

typedef struct ACP10AXMOS_typ {            /* Status-Bits */
   USINT fehler;                           /* Fehler */
   USINT warnung;                          /* Warnung aufgetreten */
   USINT ds_warnung;                       /* Schleppfehlergrenzwert f�r Ausgabe einer Warnung */
   USINT reserve;                          /* Reserviert */
} ACP10AXMOS_typ;

typedef struct ACP10AXMON_typ {                     /* Monitor */
   DINT           s;                                /* Position */
   REAL           v;                                /* Geschwindigkeit */
   ACP10AXMOS_typ status;                           /* Status-Bits */
} ACP10AXMON_typ;

typedef struct ACP10MSCNT_typ {            /* Anzahl der nicht quittierten Meldungen */
   USINT fehler;                           /* Anzahl der nicht quittierten Fehler */
   USINT warnung;                          /* Anzahl der nicht quittierten Warnungen */
   UINT  reserve;                          /* Reserviert */
} ACP10MSCNT_typ;

typedef struct ACP10MSREC_typ {            /* Aktueller Meldungssatz */
   UINT  par_id;                           /* Parameter-ID */
   UINT  nummer;                           /* Fehlernummer */
   UDINT info;                             /* Zusatz-Info */
} ACP10MSREC_typ;

typedef struct ACP10MTXST_typ {           /* Status der Textermittlung */
   UINT zeilen;                           /* Zeilen des ermittelten Textes */
   UINT fehler;                           /* Fehler */
} ACP10MTXST_typ;

typedef struct ACP10MTXPA_typ {            /* Parameter f�r Textermittlung */
   UINT  format;                           /* Ausgabeformat */
   UINT  spalten;                          /* Anzahl der Spalten einer Zeile */
   USINT datenmodul[10];                   /* Name des Datenmodules */
   UINT  daten_len;                        /* L�nge des Datenpuffers im Anwenderprogramm */
   UDINT daten_adr;                        /* Adresse der Daten */
} ACP10MTXPA_typ;

typedef struct ACP10MSTXT_typ {                     /* Textermittlung f�r aktuellen Meldungssatz */
   ACP10MTXST_typ status;                           /* Status */
   ACP10MTXPA_typ parameter;                        /* Parameter */
} ACP10MSTXT_typ;

typedef struct ACP10MSG_typ {                     /* Meldungen */
   ACP10MSCNT_typ anzahl;                         /* Anzahl der nicht quittierten Meldungen */
   ACP10MSREC_typ satz;                           /* Fehlersatz */
   ACP10MSTXT_typ text;                           /* Textermittlung f�r aktuellen Meldungssatz */
} ACP10MSG_typ;

typedef struct ACP10ACHSE_typ {                     /* ACP10 - Reelle Achse */
   USINT          NOT_USE_1[4];
   UINT           size;                             /* Gr��e des zugeh�rigen NC-Manager-Datentyps */
   UINT           reserve;                          /* Reserviert */
   ACP10SWVER_typ sw_version;                       /* SW-Versions-ID [hexadezimal] */
   ACP10SIM_typ   simulation;                       /* Simulationsmodus */
   ACP10GLINI_typ global;                           /* Globale Initialisierung */
   ACP10NET_typ   netzwerk;                         /* Netzwerk */
   ACP10DIGIN_typ dig_e;                            /* Digitale Eing�nge */
   ACP10ENCIF_typ geber_if;                         /* Geber Interface */
   ACP10AXLIM_typ grenzwert;                        /* Grenzwert */
   ACP10CTRL_typ  regler;                           /* Regler */
   ACP10AXMOV_typ bewegung;                         /* Achs-Bewegung */
   ACP10AXMON_typ monitor;                          /* Monitor */
   ACP10MSG_typ   meldung;                          /* Meldungen (Fehler, Warnungen) */
   USINT          NOT_USE_2[48];
} ACP10ACHSE_typ;

typedef struct ACP10MONET_typ {            /* Netzwerk */
   USINT init;                             /* Initialisiert */
   USINT reserve1;                         /* Reserviert */
   UINT  reserve2;                         /* Reserviert */
} ACP10MONET_typ;

typedef struct ACP10TRCTR_typ {            /* Trigger */
   UINT  par_id;                           /* Parameter-ID */
   USINT ereignis;                         /* Ereignis */
   USINT reserve;                          /* Reserviert */
   REAL  schwelle;                         /* Schwellwert */
   REAL  fenster;                          /* Fenster */
} ACP10TRCTR_typ;

typedef struct ACP10TRCDA_typ {           /* Datenpunkte f�r Test */
   UINT par_id;                           /* Parameter-ID */
} ACP10TRCDA_typ;

typedef struct ACP10TRCSV_typ {            /* Betriebssystem-Variable */
   USINT daten_typ;                        /* Datentyp */
   USINT reserve1;                         /* Reserviert */
   UINT  reserve2;                         /* Reserviert */
   UDINT adresse;                          /* Adresse */
} ACP10TRCSV_typ;

typedef struct ACP10TRCSY_typ {                     /* Konfiguration der Betriebssystem-Variablen */
   ACP10TRCSV_typ trigger;                          /* Trigger */
   ACP10TRCSV_typ test_dat[10];                     /* Datenpunkt f�r Test */
} ACP10TRCSY_typ;

typedef struct ACP10TRC_typ {                     /* Trace */
   USINT          status;                         /* Status */
   USINT          reserve1;                       /* Reserviert */
   UINT           reserve2;                       /* Reserviert */
   UDINT          buf_size;                       /* Gr��e des Trace-Daten-Puffers auf dem Antrieb */
   USINT          NOT_USE_1[4];
   REAL           t_trace;                        /* Aufzeichnungsdauer */
   REAL           t_abtast;                       /* Abtastzeit */
   REAL           t_verzoeg;                      /* Startverz�gerung */
   ACP10TRCTR_typ trigger;                        /* Trigger */
   ACP10TRCDA_typ test_dat[10];                   /* Datenpunkt f�r Test */
   ACP10TRCSY_typ system_var;                     /* Konfiguration der Betriebssystem-Variablen */
} ACP10TRC_typ;

typedef struct ACP10MODUL_typ {                     /* ACP10 - NC-Modul */
   USINT          NOT_USE_1[4];
   UINT           size;                             /* Gr��e des zugeh�rigen NC-Manager-Datentyps */
   UINT           reserve;                          /* Reserviert */
   ACP10SWVER_typ sw_version;                       /* SW-Versions-ID [hexadezimal] */
   ACP10MONET_typ netzwerk;                         /* Netzwerk */
   ACP10TRC_typ   trace;                            /* Trace */
   ACP10MSG_typ   meldung;                          /* Meldungen (Fehler, Warnungen) */
   USINT          NOT_USE_2[48];
} ACP10MODUL_typ;

typedef struct ACP10DBLST_typ {            /* Status */
   UDINT daten_len;                        /* L�nge des zum Antrieb �bertragenen Datenblockes */
   USINT fehler;                           /* Fehler */
   USINT reserve1;                         /* Reserviert */
   UINT  reserve2;                         /* Reserviert */
} ACP10DBLST_typ;

typedef struct ACP10DBLPA_typ {            /* Parameter */
   UDINT daten_len;                        /* L�nge des Datenblockes im Anwenderprogramm */
   UDINT daten_adr;                        /* Adresse der Daten */
   USINT datenmodul[10];                   /* Name des Datenmodules */
   UINT  index;                            /* Index */
   UINT  modus;                            /* Modus */
   UINT  format;                           /* Ausgabeformat */
} ACP10DBLPA_typ;

typedef struct ACP10DATBL_typ {                     /* Datenblock-Operation */
   ACP10DBLST_typ status;                           /* Status */
   ACP10DBLPA_typ parameter;                        /* Parameter */
} ACP10DATBL_typ;

typedef struct ACP10PRADR_typ {            /* Parameter-Satz f�r Format "ncFORMAT_ADR" */
   UINT  par_id;                           /* Parameter-ID */
   UINT  reserve;                          /* Reserviert */
   UDINT daten_adr;                        /* Adresse der Daten */
} ACP10PRADR_typ;

typedef struct ACP10PRB06_typ {            /* Parameter-Satz f�r Format "ncFORMAT_B06" */
   UINT  par_id;                           /* Parameter-ID */
   USINT daten_byte[6];                    /* Daten-Bytes */
} ACP10PRB06_typ;

typedef struct ACP10PRT10_typ {            /* Parameter-Satz f�r Format "ncFORMAT_T10" */
   UINT  par_id;                           /* Parameter-ID */
   USINT daten_text[10];                   /* Daten im Textformat */
} ACP10PRT10_typ;

typedef struct ACP10PRECS_typ {                     /* Parameter-S�tze */
   ACP10PRADR_typ format_adr;                       /* Parameter-Satz f�r Format "ncFORMAT_ADR" */
   ACP10PRB06_typ format_b06;                       /* Parameter-Satz f�r Format "ncFORMAT_B06" */
   ACP10PRT10_typ format_t10;                       /* Parameter-Satz f�r Format "ncFORMAT_T10" */
} ACP10PRECS_typ;

typedef struct ACP10TYPES_typ {                     /* Datenstruktur f�r zus�tzliche Datentypen */
   ACP10DATBL_typ datenblock;                       /* Datenblock-Operation */
   ACP10PRECS_typ par_saetze;                       /* Parameter-S�tze */
} ACP10TYPES_typ;

typedef struct ACP10SWVMA_typ {           /* SW-Version (nur NC-Manager) */
   UINT nc_manager;                       /* NC-Manager */
   UINT reserve;                          /* Reserviert */
} ACP10SWVMA_typ;

typedef struct ACP10USBID_typ {           /* Basis-CAN-IDs f�r User-CAN-Objekte */
   UINT write_cob;                        /* Write-CAN-Objekt */
   UINT read_cob;                         /* Read-CAN-Objekt */
} ACP10USBID_typ;

typedef struct ACP10WRCOB_typ {            /* Write-CAN-Objekt */
   UDINT can_obj;                          /* CAN-Objekt (COB-Handle) von CAN_defineCOB() */
   UINT  status;                           /* Status */
   UINT  reserve;                          /* Reserviert */
} ACP10WRCOB_typ;

typedef struct ACP10USCOB_typ {                     /* ACP10 - User-CAN-Objekte */
   USINT          NOT_USE_1[4];
   UINT           size;                             /* Gr��e des zugeh�rigen NC-Manager-Datentyps */
   UINT           reserve;                          /* Reserviert */
   USINT          if_name[32];                      /* Interface-Name */
   ACP10SWVMA_typ sw_version;                       /* SW-Versions-ID [hexadezimal] */
   ACP10USBID_typ basis_id;                         /* Basis-CAN-IDs */
   ACP10WRCOB_typ write_cob[8];                     /* Write-CAN-Objekt */
} ACP10USCOB_typ;


#endif /* ACP10MAN_H_VERSION */ 

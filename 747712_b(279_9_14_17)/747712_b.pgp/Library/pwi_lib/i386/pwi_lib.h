/* Automation Studio Generated Header File, Format Version 1.00 */
/* do not change */
#ifndef PWI_LIB_H_
#define PWI_LIB_H_
#define _WEAK	__attribute__((__weak__))

#include <bur/plctypes.h>

#include <DVFrame.h>
#include <standard.h>
#include <CAN_LIB.h>


/* Constants */


/* Datatypes */
typedef struct PWI_root
{
	unsigned long adrText;
	unsigned short maxSpeech;
	unsigned short ogSpeech;
	unsigned short cntSpeech;
	unsigned long adrPage;
	unsigned short maxPage;
	unsigned short ogPage;
	unsigned short cntPage;
	unsigned long adrList;
	unsigned short maxObj;
	unsigned short ogObj;
	unsigned short cntObj;
	unsigned long adrGrph;
	unsigned short maxGrph;
	unsigned short ogGrph;
	unsigned short cntGrph;
	unsigned long adrTrend;
	unsigned short maxTrend;
	unsigned short ogTrend;
	unsigned short TableauTyp;
	unsigned long CCVT100Adr;
	unsigned short CCVT100Len;
	unsigned long CCP121Adr;
	unsigned short CCP121Len;
	unsigned long adrTCode;
	unsigned short lenTCode;
	unsigned long adrTFunc;
	unsigned short lenTFunc;
	unsigned long adrTHandy;
	unsigned short lenTHandy;
	unsigned long adrTMode;
	unsigned short lenTMode;
	unsigned long adrScale;
	unsigned short cntScale;
	unsigned long adrCharSet;
	unsigned short lenCharSet;
	unsigned short maxPrior;
	unsigned long adrIF_Par;
	unsigned long adrIF_Buf;
	unsigned long adrSt_Drv;
	unsigned long adrSt_Wrk;
	unsigned long adrSt_Inp;
	unsigned long adrSt_Env;
	unsigned long adrSt_Par;
} PWI_root;

typedef struct PWI_Trend
{
	unsigned long adrTrend;
} PWI_Trend;

typedef struct trend_ctrl
{
	unsigned long adrInit;
	unsigned short wrTrend;
	unsigned short rdTrend;
	unsigned short fullTrend;
	unsigned short szTrend;
	unsigned short clearTrend;
} trend_ctrl;

typedef struct CtrlCode
{
	unsigned char Status[6];
	unsigned char ClrScr[6];
	unsigned char AllLedAus[6];
	unsigned char TModus[6];
	unsigned char TZeit[6];
	unsigned char TFrequenz[6];
	unsigned char LedEin[6];
	unsigned char LedAus[6];
	unsigned char LedBlnkL[6];
	unsigned char LedBlnkS[6];
	unsigned char LedEcho[6];
	unsigned char CLinks[6];
	unsigned char CRechts[6];
	unsigned char COben[6];
	unsigned char CUnten[6];
	unsigned char CHome[6];
	unsigned char CEnde[6];
	unsigned char CPos1[6];
	unsigned char CPos2[6];
	unsigned char CPos3[6];
	unsigned char CR[6];
	unsigned char LF[6];
	unsigned char CEin[6];
	unsigned char CAus[6];
	unsigned char CBlnk[6];
	unsigned char CForm[6];
	unsigned char ZIns[6];
	unsigned char ZLoesch[6];
	unsigned char ABlnkEin[6];
	unsigned char AInvEin[6];
	unsigned char AttrAus[6];
	unsigned char Schrift1[6];
	unsigned char Schrift2[6];
	unsigned char Schrift4[6];
	unsigned char setPixel[6];
	unsigned char clrPixel[6];
	unsigned char LinieW[6];
	unsigned char LinieWC[6];
	unsigned char LinieS[6];
	unsigned char LinieSC[6];
	unsigned char Rahmen[6];
	unsigned char RahmenC[6];
	unsigned char Rechteck[6];
	unsigned char RechteckC[6];
	unsigned char Balken[6];
	unsigned char Trend[6];
	unsigned char addTrend[6];
	unsigned char scTrend[6];
	unsigned char def11Char[6];
	unsigned char def22Char[6];
	unsigned char ld11Char[6];
	unsigned char ld22Char[6];
	unsigned char chBright[6];
	unsigned char tmBright[6];
	unsigned char defBright[6];
	unsigned char TextNr[6];
	unsigned char ChNorm[6];
	unsigned char ChInv[6];
	unsigned char ChBlnk[6];
	unsigned char BitMapDef[6];
	unsigned char BitMapSend[6];
	unsigned char XCharOn[6];
	unsigned char XCharOff[6];
	unsigned char XPos[6];
	unsigned char XInvOn[6];
	unsigned char XInvOff[6];
} CtrlCode;

typedef struct PWI_ifpar
{
	unsigned long idIF;
	unsigned long idQueue;
	unsigned long CANidRQST;
	unsigned long CANidREMO;
	unsigned char TabCheck;
	unsigned char noKey;
	unsigned char semaphor[4];
	unsigned char semMax;
	unsigned short stateIF;
	unsigned short stateQueue;
} PWI_ifpar;

typedef struct pwi_ifbuf
{
	unsigned char clrTxQueue;
	unsigned char clrRxQueue;
	unsigned long adrTxQueue;
	unsigned short szTxQueue;
	unsigned short wpTxQueue;
	unsigned short rpTxQueue;
	unsigned long adrT1Queue;
	unsigned short szT1Queue;
	unsigned short wpT1Queue;
	unsigned short rpT1Queue;
	unsigned long adrT2Queue;
	unsigned short szT2Queue;
	unsigned short wpT2Queue;
	unsigned short rpT2Queue;
	unsigned long adrT3Queue;
	unsigned short szT3Queue;
	unsigned short wpT3Queue;
	unsigned short rpT3Queue;
	unsigned long adrT4Queue;
	unsigned short szT4Queue;
	unsigned short wpT4Queue;
	unsigned short rpT4Queue;
	unsigned long adrRxQueue;
	unsigned short szRxQueue;
	unsigned short wpRxQueue;
	unsigned short rpRxQueue;
	unsigned short sendBuffer;
	unsigned short maxBuffer;
	unsigned short sendActiv;
	unsigned short sendLen;
} pwi_ifbuf;

typedef struct PWI_smbuf
{
	unsigned long adrTxQueue;
	unsigned short szTxQueue;
	unsigned short wpTxQueue;
	unsigned short rpTxQueue;
} PWI_smbuf;

typedef struct PWI_alloc
{
	unsigned long adrAlloc;
	unsigned long sizeAlloc;
} PWI_alloc;

typedef struct sysx_info
{
	unsigned char aws_name[6];
	unsigned char aws_typ[2];
	unsigned long cpu_info;
	unsigned short ma_globl;
	unsigned short md_globl;
	unsigned long os_len;
	unsigned long user_len;
	unsigned long tmp_len;
	unsigned long eprom;
	unsigned long fix_ram;
} sysx_info;

typedef struct PWI_obj
{
	unsigned long adrDM;
	unsigned char cmd[4];
	unsigned char init;
	unsigned char start;
	unsigned char step;
	unsigned char chgCursor;
	unsigned char attr1;
	unsigned char attr2;
	unsigned char attr3;
	unsigned char attr4;
	unsigned long adrAdd1;
	unsigned short lenAdd1;
	unsigned long valAdd1;
	unsigned long adrAdd2;
	unsigned short lenAdd2;
	unsigned long valAdd2;
	unsigned long adrPV;
	unsigned short lenPV;
	unsigned char modePV;
	unsigned char res;
	unsigned long valPV;
	unsigned long adrEnable;
	unsigned short lenEnable;
	unsigned long valEnable;
	unsigned long adrActiv;
	unsigned short lenActiv;
	unsigned short cntPrior;
	unsigned short stateObj;
} PWI_obj;

typedef struct PWI_out
{
	unsigned long adrDM;
	unsigned char cmd[4];
	unsigned char init;
	unsigned char start;
	unsigned char step;
	unsigned char chgCursor;
	unsigned char attr1;
	unsigned char attr2;
	unsigned long adrAdd1;
	unsigned short lenAdd1;
	unsigned long valAdd1;
	unsigned long adrAdd2;
	unsigned short lenAdd2;
	unsigned long valAdd2;
	unsigned long adrPV;
	unsigned short lenPV;
	unsigned char modePV;
	unsigned char res;
	unsigned long valPV;
	unsigned long adrEnable;
	unsigned short lenEnable;
	unsigned long valEnable;
	unsigned long adrActiv;
	unsigned short lenActiv;
	unsigned short cntPrior;
	unsigned long valINT;
	unsigned short stateOut;
} PWI_out;

typedef struct pwi_stdrv
{
	unsigned short SpeechAct;
	unsigned short PageAct;
	unsigned long adrPage;
	unsigned long adrPageOld;
	unsigned long adrAddMSK;
	unsigned long adrAddNAT;
	unsigned long adrAddOUT;
	unsigned long adrAddINP;
	unsigned long adrAddKEY;
	unsigned long adrAddKEYO;
	unsigned char PageReady;
	unsigned char attr1;
	unsigned char attr2;
	unsigned char TableauOK;
	unsigned char CurOn;
	unsigned char CurChange;
	unsigned char KeyWait;
	unsigned char KeyRepeat;
	unsigned char inpEnable;
	unsigned char inpFirst;
} pwi_stdrv;

typedef struct pwi_stinp
{
	signed short No;
	signed short ONo;
	unsigned char cmd[3];
	unsigned char cmdO[3];
	signed char nextcur;
	unsigned char ownPage;
	unsigned long lVal;
	signed long iVal;
	signed char pos;
	signed char posMax;
	unsigned char Buffer[64];
	unsigned char BufferChg;
	unsigned char dpExist;
	unsigned long adrPV;
	unsigned short lenPV;
	unsigned char modePV;
	unsigned char res;
	unsigned long valPV;
	unsigned long adrEnable;
	unsigned short lenEnable;
	unsigned long valEnable;
	unsigned long adrActiv;
	unsigned short lenActiv;
	unsigned long adrOPV;
	unsigned short lenOPV;
	unsigned char modeOPV;
	unsigned char res2;
	unsigned long valOPV;
	unsigned long adrOEnable;
	unsigned short lenOEnable;
	unsigned long valOEnable;
	unsigned long adrOActiv;
	unsigned short lenOActiv;
	unsigned long adrInit;
	unsigned long adrOInit;
	unsigned long adrvalAdd1;
	unsigned long adrvalAdd2;
	unsigned char newCursor;
	unsigned char inpEnable;
	unsigned char inpFirst;
	unsigned char inpEnter;
	unsigned char inpKey;
} pwi_stinp;

typedef struct PWI_page
{
	unsigned long adrPMask;
	unsigned short szPMask;
	unsigned long adrPInfo;
	unsigned long adrNDat;
	unsigned short countNDat;
	unsigned long adrODat;
	unsigned short countODat;
	unsigned long adrOCtrl;
	unsigned short actODat;
	unsigned long adrIDat;
	unsigned short countIDat;
	unsigned long adrICtrl;
	unsigned short actIDat;
	unsigned long adrKDat;
	unsigned short countKDat;
	unsigned long adrKCtrl;
	unsigned short initPage;
	unsigned short codePage;
	unsigned short statePage;
	signed short cmdNo;
} PWI_page;

typedef struct PWI_env
{
	unsigned short LifeCheck;
	unsigned short TimeOut;
	signed short DefcurNo;
	signed short DefcmdNo;
	signed short NewcurNo;
	signed short NewcmdNo;
	unsigned char DefownPage;
	unsigned char NewownPage;
	signed char DefcurPage;
	signed char NewcurPage;
	unsigned char triggerCur;
	unsigned char disCursor;
	unsigned char disCurLFRG;
	signed char nextInpENo;
	signed char nextInpESt;
	unsigned char loopInput;
	unsigned char disInput;
	unsigned char disKey;
	unsigned char signKey;
	unsigned char dpChar;
	unsigned char dpFix;
	unsigned char hiddenChar;
	unsigned char strInsert;
	unsigned char srcKeyLim;
	unsigned char curChar;
	unsigned char curInput;
	signed char curPosObj;
	unsigned char signFormat;
	unsigned char optNewPage;
	unsigned char nNCnewPage;
	unsigned char nNXnewPage;
} PWI_env;

typedef struct PWI_list
{
	unsigned long adrOBJDat;
	unsigned short countOBJDa;
	unsigned long adrOBJCtrl;
	unsigned short actOBJDat;
	unsigned short initObj;
	unsigned short codeObj;
	unsigned short stateObj;
} PWI_list;

typedef struct PWI_pInfo
{
	unsigned char addMaskH;
	unsigned char addMaskL;
	unsigned char addNatH;
	unsigned char addNatL;
	unsigned char addOutH;
	unsigned char addOutL;
	unsigned char addInH;
	unsigned char addInL;
	unsigned char addKeyH;
	unsigned char addKeyL;
	unsigned char NeXTPageH;
	unsigned char NeXTPageL;
	unsigned char PReVPageH;
	unsigned char PReVPageL;
	unsigned char info1;
	unsigned char info2;
	unsigned char info3;
	unsigned char info4;
} PWI_pInfo;

typedef struct pwi_key
{
	unsigned long adrDM;
	unsigned char cmd[3];
	unsigned char init;
	unsigned char start;
	unsigned char stept;
	unsigned long adrAdd1;
	unsigned short lenAdd1;
	unsigned long valAdd1;
	unsigned long adrAdd2;
	unsigned short lenAdd2;
	unsigned long valAdd2;
	unsigned long adrPV;
	unsigned short lenPV;
	unsigned long valPV;
	unsigned long adrEnable;
	unsigned short lenEnable;
	unsigned long adrActiv;
	unsigned short lenActiv;
	unsigned short stateKey;
} pwi_key;

typedef struct out_ctrl
{
	unsigned char posx;
	unsigned char posy;
	unsigned char size;
	unsigned char attr1;
	unsigned char attr2;
	unsigned char mode1;
	unsigned char mode2;
	unsigned char b1;
	unsigned char b2;
	unsigned char b3;
	unsigned short prior;
} out_ctrl;

typedef struct out_dm
{
	unsigned char cmd[4];
	out_ctrl dCTRL;
} out_dm;

typedef struct key_ctrl
{
	unsigned char code;
	unsigned char mode;
	unsigned short val1;
} key_ctrl;

typedef struct key_dm
{
	unsigned char cmd[4];
	key_ctrl dCTRL;
} key_dm;

typedef struct PWI_nat
{
	unsigned long adrDM;
} PWI_nat;

typedef struct nat_dm
{
	unsigned char cmd[4];
	unsigned char posx;
	unsigned char posy;
	unsigned char size;
	unsigned char attr1;
	unsigned char attr2;
	unsigned char mode1;
	unsigned char b1;
	unsigned char b2;
} nat_dm;

typedef struct PWI_Grph
{
	unsigned long adrGrph;
	unsigned short sizeGrph;
} PWI_Grph;

typedef struct obj_ctrl
{
	unsigned char relx1;
	unsigned char size1;
	unsigned char attr1;
	unsigned char mode1;
	unsigned short text1;
	unsigned char relx2;
	unsigned char size2;
	unsigned char attr2;
	unsigned char mode2;
	unsigned short text2;
	unsigned char relx3;
	unsigned char size3;
	unsigned char attr3;
	unsigned char mode3;
	unsigned short text3;
	unsigned char relx4;
	unsigned char size4;
	unsigned char attr4;
	unsigned char mode4;
	unsigned char mode5;
	unsigned char b1;
	unsigned char b2;
	unsigned char b3;
	unsigned short prior;
} obj_ctrl;

typedef struct obj_dm
{
	unsigned char cmd[4];
	obj_ctrl dCTRL;
} obj_dm;

typedef struct PWI_text
{
	unsigned long adrText;
	unsigned short countText;
	unsigned short stateText;
} PWI_text;

typedef struct PWI_aText
{
	unsigned long adrText;
} PWI_aText;

typedef struct PWI_Para
{
	unsigned short memCheckD;
	unsigned short memTypeD;
	unsigned short TableauTyp;
	unsigned short charSetTyp;
	unsigned char nameCCVT10[10];
	unsigned char nameCCP121[10];
	unsigned char nameTCode[10];
	unsigned char nameTFunc[10];
	unsigned char nameTHandy[10];
	unsigned char nameScale[10];
	unsigned char nameCharS[10];
	unsigned short memCheckT;
	unsigned short memTypeT;
	unsigned short maxSpeech;
	unsigned char nameText[10];
	unsigned short memCheckP;
	unsigned short memTypeP;
	unsigned short maxPage;
	unsigned short maxObj;
	unsigned short maxGrph;
	unsigned char nameMask[10];
	unsigned char namePage[10];
	unsigned char nameObj[10];
	unsigned char nameGrph[10];
	unsigned short memCheckIF;
	unsigned short memTypeIF;
	unsigned short TxWaitCycl;
	unsigned short szTxQueue;
	unsigned short szRxQueue;
	unsigned short szT1Queue;
	unsigned short szT2Queue;
	unsigned short szT3Queue;
	unsigned short szT4Queue;
	unsigned short comType;
	unsigned char nameCanPar[10];
	unsigned char nameRSPar[10];
} PWI_Para;

typedef struct ctrlCp121
{
	unsigned char curLeft[4];
	unsigned char curRight[4];
	unsigned char curDown[4];
	unsigned char curUp[4];
	unsigned char curHome[4];
	unsigned char carrReturn[4];
	unsigned char curLineCol[4];
	unsigned char curCol[4];
	unsigned char curLine[4];
	unsigned char attrBlkOn[4];
	unsigned char attrBlkOff[4];
	unsigned char charBlkOn[4];
	unsigned char charBlkOff[4];
	unsigned char keyCodeReq[4];
	unsigned char disLight[4];
	unsigned char ledOn[4];
	unsigned char ledOff[4];
	unsigned char allLedOn[4];
	unsigned char allLedOff[4];
	unsigned char ledBlkSlow[4];
	unsigned char ledBlkFast[4];
	unsigned char resetPanel[4];
	unsigned char clearPanel[4];
	unsigned char delLineCur[4];
	unsigned char delLine[4];
} ctrlCp121;

typedef struct PWI_exCAN
{
	unsigned char device[56];
	unsigned short node;
	unsigned short queueSize;
	unsigned short baud;
	unsigned short cob_cnt;
	unsigned short info;
} PWI_exCAN;

typedef struct PWI_ScalDm
{
	unsigned long x1;
	unsigned long y1;
	unsigned long x2;
	unsigned long y2;
} PWI_ScalDm;

typedef struct PWI_ScalFc
{
	float m;
	float t;
} PWI_ScalFc;

typedef struct pwi_stwrk
{
	unsigned char posx;
	unsigned char posy;
	unsigned char size;
	unsigned char attr1;
	unsigned char attr2;
	unsigned char mode1;
	unsigned char mode2;
	unsigned char b1;
	unsigned char b2;
	unsigned char b3;
	unsigned short prior;
} pwi_stwrk;

typedef struct RSconfig
{
	unsigned short idle;
	unsigned short delime;
	unsigned char delim[2];
	unsigned short tx_cnt;
	unsigned short rx_cnt;
	unsigned short tx_len;
	unsigned short rx_len;
	unsigned short argc;
	unsigned long argv;
} RSconfig;

typedef struct PWI_exRS
{
	unsigned char device[56];
	unsigned char mode[56];
	unsigned char rts;
	unsigned char config;
	unsigned short idle;
	unsigned short delime;
	unsigned char delim[2];
	unsigned short tx_cnt;
	unsigned short rx_cnt;
	unsigned short tx_len;
	unsigned short rx_len;
} PWI_exRS;

typedef struct obj_dat
{
	unsigned char relx1;
	unsigned char size1;
	unsigned char attr1;
	unsigned char mode1;
	unsigned short text1;
} obj_dat;



/* Datatypes of function blocks */
typedef struct alloc
{
	/* VAR_INPUT (analogous) */
	struct PWI_alloc* allocDat;
	unsigned long sizeAlloc;
	unsigned long adrAlloc;
	/* VAR_OUTPUT (analogous) */
	unsigned short stateFree;
	unsigned short stateAlloc;
	/* VAR (analogous) */
	unsigned long* adradrAllo;
	/* VAR_INPUT (digital) */
	plcbit allocTyp;
} alloc_typ;

typedef struct tmBright
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned char timebright;
	/* VAR_OUTPUT (analogous) */
	unsigned short codeBright;
	/* VAR (analogous) */
	unsigned short i;
	unsigned long sendAdr;
	unsigned long nextAdr;
	struct PWI_smbuf* dSM_BUF;
	unsigned char* semaphor;
	struct pwi_ifbuf* dIF_BUF;
	struct PWI_ifpar* dIF_PAR;
	unsigned char* ch;
	struct CtrlCode* dCCVT100;
	struct PWI_root* dPWI;
	/* VAR_INPUT (digital) */
	plcbit enable;
} tmBright_typ;

typedef struct str_cut
{
	/* VAR_INPUT (analogous) */
	unsigned long adrString;
	/* VAR (analogous) */
	signed short LEN;
	unsigned short offString;
	unsigned char* ch;
} str_cut_typ;

typedef struct setPoint
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned char mode;
	unsigned char posx;
	unsigned char posy;
	/* VAR_OUTPUT (analogous) */
	unsigned short codePoint;
	/* VAR (analogous) */
	unsigned short i;
	unsigned long nextAdr;
	struct PWI_smbuf* dSM_BUF;
	unsigned char* semaphor;
	struct pwi_ifbuf* dIF_BUF;
	struct PWI_ifpar* dIF_PAR;
	unsigned char* ch;
	struct CtrlCode* dCCVT100;
	struct PWI_root* dPWI;
	/* VAR_INPUT (digital) */
	plcbit enable;
} setPoint_typ;

typedef struct rsTrend
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned short trendNo;
	/* VAR_OUTPUT (analogous) */
	unsigned short codeTrend;
	/* VAR (analogous) */
	unsigned long adrTrend;
	struct trend_ctrl* dTREND_CTR;
	struct PWI_Trend* dTREND;
	unsigned char* ch;
	struct PWI_root* dPWI;
	/* VAR_INPUT (digital) */
	plcbit enable;
} rsTrend_typ;

typedef struct get_valu
{
	/* VAR_INPUT (analogous) */
	unsigned long adrPV;
	unsigned short lenPV;
	/* VAR_OUTPUT (analogous) */
	unsigned long val;
	/* VAR (analogous) */
	unsigned long* lPV;
	unsigned short* wPV;
	unsigned char* bPV;
} get_valu_typ;

typedef struct pwi_obje
{
	/* VAR_INPUT (analogous) */
	struct PWI_obj* dPWI_OBJ;
	/* VAR_OUTPUT (analogous) */
	unsigned long valEnable;
	/* VAR (analogous) */
	struct get_valu zzget_valu00000;
	/* VAR_OUTPUT (digital) */
	plcbit chgEnable;
} pwi_obje_typ;

typedef struct pvt_trd
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	struct PWI_out* dPWI_OUT;
	struct out_ctrl* dOUT_CTRL;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	unsigned long adrTrend;
	signed short i;
	signed short size;
	unsigned char* trendVar;
	struct trend_ctrl* dTREND_CTR;
	struct PWI_Trend* dTREND;
	unsigned char* ch;
	struct CtrlCode* dCCVT100;
} pvt_trd_typ;

typedef struct pvt_grp
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char posx;
	unsigned char posy;
	unsigned char sizex;
	unsigned char sizey;
	unsigned char attr1;
	unsigned long adrGrph;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	unsigned char bVal;
	unsigned short i;
	unsigned short n;
	unsigned short countBytes;
	unsigned short count;
	unsigned short m;
	unsigned short pixels;
	unsigned char* source;
	struct CtrlCode* dCCVT100;
	unsigned char* ch;
} pvt_grp_typ;

typedef struct pvt_cps
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char posx;
	unsigned char posy;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct pwi_stdrv* dST_DRV;
	struct CtrlCode* dCCVT100;
	unsigned char* ch;
} pvt_cps_typ;

typedef struct pvt_cur
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char mode;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct CtrlCode* dCCVT100;
} pvt_cur_typ;

typedef struct get_vals
{
	/* VAR_INPUT (analogous) */
	unsigned long adrPV;
	unsigned short lenPV;
	/* VAR_OUTPUT (analogous) */
	signed long val;
	/* VAR (analogous) */
	signed long* i32PV;
	signed short* i16PV;
	signed char* i8PV;
} get_vals_typ;

typedef struct pwi_objc
{
	/* VAR_INPUT (analogous) */
	struct PWI_obj* dPWI_OBJ;
	struct obj_ctrl* dOBJ_CTRL;
	unsigned short maxPrior;
	/* VAR_OUTPUT (analogous) */
	unsigned long valPV;
	unsigned long valEnable;
	/* VAR (analogous) */
	unsigned short i;
	unsigned char* ch;
	struct get_vals zzget_vals00000;
	struct get_valu zzget_valu00001;
	struct get_vals zzget_vals00002;
	struct get_valu zzget_valu00003;
	struct get_valu zzget_valu00004;
	struct get_vals zzget_vals00005;
	struct get_valu zzget_valu00006;
	/* VAR_OUTPUT (digital) */
	plcbit chgOut;
	plcbit chgPV;
	plcbit chgEnable;
	plcbit chgAttr;
	plcbit chgCursor;
} pwi_objc_typ;

typedef struct pwi_objm
{
	/* VAR_INPUT (analogous) */
	struct PWI_obj* dPWI_OBJ;
	struct obj_ctrl* dOBJ_CTRL;
	unsigned long valPV;
	unsigned long valEnable;
	unsigned short stateOBJ;
} pwi_objm_typ;

typedef struct pvt_cha
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char posx;
	unsigned char posy;
	unsigned char attr;
	unsigned char size;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct CtrlCode* dCCVT100;
	unsigned char* ch;
} pvt_cha_typ;

typedef struct pvt_bar
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char posx;
	unsigned char posy;
	unsigned char breit;
	unsigned char lang;
	unsigned char mode;
	unsigned long val;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	unsigned char* ch;
	struct CtrlCode* dCCVT100;
} pvt_bar_typ;

typedef struct pvt_led
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char code;
	unsigned char mode;
	unsigned char attr;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct CtrlCode* dCCVT100;
	unsigned char* ch;
} pvt_led_typ;

typedef struct pvt_line
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char posx;
	unsigned char posy;
	unsigned char size;
	unsigned char mode;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct CtrlCode* dCCVT100;
	unsigned char* ch;
} pvt_line_typ;

typedef struct pvt_attr
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char attr1;
	unsigned char attr2;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct CtrlCode* dCCVT100;
} pvt_attr_typ;

typedef struct pvt_str
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char posx;
	unsigned char posy;
	unsigned char size;
	unsigned char mode1;
	unsigned long adrSrc;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	unsigned short i;
	unsigned char length;
	unsigned char start;
	unsigned char* dch;
	struct PWI_Para* dST_PAR;
	unsigned char* chSet;
	unsigned char* sch;
	struct CtrlCode* dCCVT100;
	unsigned char* ch;
} pvt_str_typ;

typedef struct pwi_oclc
{
	/* VAR_INPUT (analogous) */
	unsigned long adrScale;
	unsigned long vali;
	unsigned char mode;
	unsigned char scale;
	/* VAR_OUTPUT (analogous) */
	unsigned long valo;
	float valf;
	/* VAR (analogous) */
	struct PWI_ScalFc* dPWI_ScalF;
} pwi_oclc_typ;

typedef struct intasci
{
	/* VAR_INPUT (analogous) */
	unsigned char mode1;
	unsigned char mode2;
	unsigned char length;
	unsigned char postdigits;
	unsigned char dpChar;
	unsigned long valPV;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	signed char rest;
	unsigned short base;
	signed char firstNum;
	signed char position;
	signed char sign;
	signed char lenDisplay;
	unsigned long value;
	signed char i;
	unsigned char* ch;
	/* VAR (digital) */
	plcbit predigits;
} intasci_typ;

typedef struct pvt_frm
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char posx;
	unsigned char posy;
	unsigned char sizex;
	unsigned char sizey;
	unsigned char line;
	unsigned char mode;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct CtrlCode* dCCVT100;
	unsigned char* ch;
} pvt_frm_typ;

typedef struct pwi_ochk
{
	/* VAR_INPUT (analogous) */
	struct PWI_out* dPWI_OUT;
	struct out_ctrl* dOUT_CTRL;
	unsigned short maxPrior;
	/* VAR_OUTPUT (analogous) */
	unsigned long valPV;
	unsigned long valEnable;
	/* VAR (analogous) */
	unsigned short i;
	unsigned char* ch;
	struct get_vals zzget_vals00000;
	struct get_valu zzget_valu00001;
	struct get_vals zzget_vals00002;
	struct get_valu zzget_valu00003;
	struct get_valu zzget_valu00004;
	struct get_vals zzget_vals00005;
	struct get_valu zzget_valu00006;
	/* VAR_OUTPUT (digital) */
	plcbit chgOut;
	plcbit chgPV;
	plcbit chgEnable;
	plcbit chgAttr;
	plcbit chgCur;
} pwi_ochk_typ;

typedef struct pwi_tadr
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned short Speech;
	unsigned short noText;
	/* VAR_OUTPUT (analogous) */
	unsigned long adrText;
	unsigned short stateText;
	/* VAR (analogous) */
	struct PWI_aText* dPWI_aTEXT;
	struct PWI_text* dPWI_TEXT;
	struct PWI_root* dPWI;
} pwi_tadr_typ;

typedef struct pwi_omrk
{
	/* VAR_INPUT (analogous) */
	struct PWI_out* dPWI_OUT;
	struct out_ctrl* dOUT_CTRL;
	unsigned long valPV;
	unsigned long valEnable;
	unsigned short stateOut;
} pwi_omrk_typ;

typedef struct pwi_xvt1
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned char OutInMode;
	unsigned char ownPage;
	unsigned long adrPage;
	unsigned short initPage;
	unsigned long SendAdrIn;
	/* VAR_OUTPUT (analogous) */
	unsigned short statePage;
	unsigned long SendAdrOut;
	/* VAR (analogous) */
	unsigned short wb;
	unsigned short wb4;
	unsigned short wb5;
	struct pwi_obje OBJE_CHK;
	unsigned char Buffer[32];
	struct pvt_trd TRD_OUT;
	struct pvt_grp GRP_OUT;
	struct pvt_cps CPS_OUT;
	struct pvt_cur CUR_OUT;
	unsigned short stEnable;
	unsigned short objChgPos;
	unsigned short cmdNo;
	unsigned short wb3;
	struct pwi_objc OBJ_CHK;
	unsigned short offset;
	struct pwi_objm OBJ_MRK;
	unsigned short index;
	unsigned long nextAdr;
	unsigned short n;
	struct pvt_cha CH_CHAN;
	unsigned short wb2;
	struct pvt_bar BAR_OUT;
	struct pvt_led LED_OUT;
	struct pvt_line LIN_OUT;
	struct pvt_attr ATT_OUT;
	struct pvt_str STR_OUT;
	struct pwi_oclc VAL_CLC;
	float fMin;
	struct intasci INT_OUT;
	float fMax;
	float fVal;
	struct pvt_frm FRA_OUT;
	unsigned short TextNo;
	unsigned short wb1;
	struct pwi_ochk OUT_CHK;
	struct pwi_tadr TADR;
	struct pwi_omrk OUT_MRK;
	struct pwi_stwrk* dST_WRK;
	struct PWI_env* dST_ENV;
	struct pwi_stinp* dST_INP;
	struct pwi_stdrv* dST_DRV;
	struct PWI_list* dLIST;
	struct PWI_page* dPAGE;
	struct PWI_Grph* dGRPH;
	struct obj_ctrl* dOBJ_CTRL;
	struct obj_dat* dOBJ_DAT;
	struct out_dm* dOUT_DM;
	struct PWI_root* dPWI;
	struct PWI_out* dPWI_OUT;
	struct PWI_obj* dPWI_OBJ;
	struct out_ctrl* dOUT_CTRL;
	unsigned char* ch;
	/* VAR (digital) */
	plcbit chgAttr;
	plcbit objNew;
	plcbit objChg;
	plcbit out;
	plcbit mrkout;
	plcbit objout;
	plcbit flag;
} pwi_xvt1_typ;

typedef struct page_txt
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned long pradrText;
	unsigned short noText;
	unsigned long nameText;
	/* VAR_OUTPUT (analogous) */
	unsigned short codeText;
	unsigned short infoTextNo;
	unsigned short infoTextLn;
	unsigned short stateText;
	unsigned long sizeText;
	unsigned long nxadrText;
	/* VAR (analogous) */
	unsigned short countText;
	unsigned long nextAdr;
	unsigned char Buffer[16];
	unsigned long adr_pt;
	unsigned long dm_data;
	unsigned long dm_ident;
	unsigned short i;
	unsigned char mem_type;
	unsigned long data_len;
	struct PWI_aText* dPWI_aTEXT;
	struct PWI_text* dPWI_TEXT;
	struct PWI_root* dPWI;
	unsigned char* ch;
	/* VAR_INPUT (digital) */
	plcbit memCheck;
} page_txt_typ;

typedef struct mem_szem
{
	/* VAR_OUTPUT (analogous) */
	unsigned long sizeMem;
	unsigned short stateMem;
	/* VAR (analogous) */
	struct sysx_info SYSx_info;
	/* VAR_INPUT (digital) */
	plcbit memCheck;
	plcbit memType;
} mem_szem_typ;

typedef struct PWI_Tini
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	struct PWI_alloc* allocDat;
	unsigned short maxSpeech;
	unsigned long nameText;
	unsigned long textPVName;
	/* VAR_OUTPUT (analogous) */
	unsigned short cntSpeech;
	unsigned short codeText;
	unsigned short infoTextNo;
	unsigned short infoTextLn;
	unsigned short stateText;
	unsigned short stateFree;
	unsigned long sizeText;
	/* VAR (analogous) */
	unsigned long nxadrText;
	struct page_txt page_txt;
	unsigned short m;
	unsigned long textPVsize;
	unsigned long sizeThis;
	unsigned long sizeRPS;
	unsigned long textPVAdr;
	unsigned long sizeInfoTe;
	unsigned char* textPVarry;
	struct PWI_text* dPWI_TEXT;
	struct PWI_root* dPWI;
	struct mem_szem zzmem_szem00000;
	struct alloc zzalloc00001;
	struct alloc zzalloc00002;
	/* VAR_INPUT (digital) */
	plcbit enable;
	plcbit memCheck;
	plcbit memType;
	/* VAR_OUTPUT (digital) */
	plcbit okPWI;
	/* VAR (digital) */
	plcbit textPVen;
} PWI_Tini_typ;

typedef struct pwi_rini
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	struct PWI_alloc* allocDat;
	unsigned long idRSIn;
	unsigned short szTxQueue;
	unsigned short szRxQueue;
	unsigned short szT1Queue;
	unsigned short szT2Queue;
	unsigned short szT3Queue;
	unsigned short szT4Queue;
	unsigned long nameRSPar;
	unsigned short argc;
	unsigned long argv;
	/* VAR_OUTPUT (analogous) */
	unsigned long idRSOut;
	unsigned short codeRS;
	unsigned long infoRS1;
	unsigned long infoRS2;
	unsigned short stateRS;
	unsigned short stateClose;
	unsigned short stateFree;
	unsigned long sizeIF;
	unsigned long sizeRPS;
	/* VAR (analogous) */
	unsigned long outarg;
	unsigned char Buffer[64];
	unsigned long DMlen;
	unsigned long adrConfig;
	unsigned long inarg;
	unsigned short ioctrl;
	struct RSconfig RSconfig;
	unsigned char DMtyp;
	unsigned long DMadr;
	unsigned long DMid;
	struct PWI_ifpar* dIF_PAR;
	struct pwi_ifbuf* dIF_BUF;
	struct PWI_exRS* dPWI_exRS;
	struct PWI_root* dPWI;
	struct FRM_close zzFRM_close00000;
	struct mem_szem zzmem_szem00001;
	struct alloc zzalloc00002;
	struct str_cut zzstr_cut00003;
	struct FRM_xopen zzFRM_xopen00004;
	struct FRM_ctrl zzFRM_ctrl00005;
	/* VAR_INPUT (digital) */
	plcbit enable;
	plcbit memCheck;
	plcbit memType;
	/* VAR_OUTPUT (digital) */
	plcbit okPWI;
} pwi_rini_typ;

typedef struct pwi_rcom
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned char* KeyFieldT;
	unsigned char* KeyField;
	unsigned char* KeyFieldP;
	unsigned char* KeyFieldO;
	/* VAR_OUTPUT (analogous) */
	unsigned char keycode;
	unsigned char keycodeTab;
	unsigned char messageTab;
	unsigned short stateRsTX;
	unsigned short stateRsRX;
	/* VAR (analogous) */
	unsigned long Timer_Rep;
	unsigned long Timer_Wait;
	unsigned long Timer_LChe;
	unsigned long Timer_Tout;
	unsigned short m;
	unsigned short szRxBuffer;
	unsigned long adrRxBuffe;
	unsigned long Timer_Foll;
	unsigned short szTxData;
	unsigned short szTxBuffer;
	unsigned long adrTxBuffe;
	unsigned char keyold;
	unsigned short i;
	unsigned short rp;
	struct PWI_smbuf* dSM_BUF;
	struct ctrlCp121* dCCP121;
	struct CtrlCode* dCCVT100;
	struct PWI_env* dST_ENV;
	struct pwi_ifbuf* dIF_BUF;
	struct PWI_ifpar* dIF_PAR;
	struct pwi_stdrv* dST_DRV;
	struct PWI_root* dPWI;
	unsigned char* ch;
	struct TON_10ms zzTON_10ms00000;
	struct FRM_gbuf zzFRM_gbuf00001;
	struct FRM_write zzFRM_write00002;
	struct FRM_robuf zzFRM_robuf00003;
	struct TON_10ms zzTON_10ms00004;
	struct FRM_read zzFRM_read00005;
	struct TON_10ms zzTON_10ms00006;
	struct TON_10ms zzTON_10ms00007;
	struct TON_10ms zzTON_10ms00008;
	struct FRM_rbuf zzFRM_rbuf00009;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR_OUTPUT (digital) */
	plcbit TableauOK;
	plcbit keypress;
	plcbit keypuls;
	/* VAR (digital) */
	plcbit now_Rep;
	plcbit now_LCheck;
	plcbit now_Tout;
	plcbit now_Wait;
	plcbit now_Foll;
	plcbit now_Wait_o;
	plcbit TableauChe;
} pwi_rcom_typ;

typedef struct pwi_prio
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned long adrPage;
	/* VAR (analogous) */
	unsigned short curNo;
	unsigned short cmdNo;
	struct PWI_list* dLIST;
	struct PWI_page* dPAGE;
	struct obj_ctrl* dOBJ_CTRL;
	struct PWI_obj* dPWI_OBJ;
	struct PWI_out* dPWI_OUT;
	struct out_ctrl* dOUT_CTRL;
} pwi_prio_typ;

typedef struct page_msk
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned short noMask;
	unsigned long nameMask;
	/* VAR_OUTPUT (analogous) */
	unsigned short codeMask;
	unsigned short infoMaskNo;
	unsigned short infoMaskLn;
	unsigned short stateMask;
	/* VAR (analogous) */
	unsigned char mem_type;
	unsigned long data_len;
	unsigned long dm_data;
	unsigned long dm_ident;
	unsigned long nextadr;
	unsigned char Buffer[16];
	struct PWI_page* dPAGE;
	struct PWI_root* dPWI;
} page_msk_typ;

typedef struct mem_cmp
{
	/* VAR_INPUT (analogous) */
	unsigned char* mem1;
	unsigned char* mem2;
	unsigned short LEN;
	/* VAR (analogous) */
	unsigned short i;
	/* VAR_OUTPUT (digital) */
	plcbit eq;
} mem_cmp_typ;

typedef struct asctint
{
	/* VAR_INPUT (analogous) */
	unsigned long adrBuf;
	unsigned char dpChar;
	unsigned char postdigits;
	unsigned char mode2;
	/* VAR_OUTPUT (analogous) */
	unsigned long valPV;
	/* VAR (analogous) */
	unsigned short base;
	unsigned short length;
	unsigned char digit;
	unsigned short k;
	unsigned short j;
	unsigned short i;
	unsigned char* ch;
	/* VAR (digital) */
	plcbit sign;
} asctint_typ;

typedef struct put_valu
{
	/* VAR_INPUT (analogous) */
	unsigned long adrPV;
	unsigned short lenPV;
	unsigned long val;
	/* VAR (analogous) */
	unsigned long* lPV;
	unsigned short* wPV;
	unsigned char* bPV;
} put_valu_typ;

typedef struct page_dat
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned long pradrPage;
	unsigned short noPage;
	unsigned long noDMIn;
	unsigned long offDMIn;
	unsigned long noDMFIn;
	unsigned long namePage;
	unsigned char* szTrends;
	/* VAR_OUTPUT (analogous) */
	unsigned short codePage;
	unsigned short infoPageNo;
	unsigned short infoPageLn;
	unsigned short statePage;
	unsigned long sizePage;
	unsigned long noDMOut;
	unsigned long offDMOut;
	unsigned long noDMFOut;
	unsigned long nxadrPage;
	/* VAR (analogous) */
	unsigned long rest;
	unsigned long valAdd1;
	unsigned short countKDat;
	unsigned short countIDat;
	unsigned short countODat;
	unsigned short countNDat;
	unsigned short countKCtrl;
	unsigned short countICtrl;
	unsigned short countOCtrl;
	unsigned short actKCtrl;
	unsigned short actICtrl;
	unsigned short actOCtrl;
	unsigned char BufferNo[16];
	unsigned short noLine;
	unsigned short noDM;
	unsigned long offDM;
	unsigned long sizeNAT;
	unsigned char Buffer[16];
	unsigned short m;
	unsigned long adrKCtrl;
	unsigned long adrOCtrl;
	unsigned long sizeOUT;
	unsigned long sizeOCT;
	unsigned long sizeKEY;
	unsigned long sizeKCT;
	unsigned short LEN;
	unsigned short n;
	unsigned long lenPV;
	unsigned long adrPV;
	unsigned long size;
	unsigned long adr_pt;
	unsigned char mem_type;
	unsigned long data_len;
	unsigned long dm_data;
	unsigned long dm_ident;
	unsigned long nextadr;
	struct trend_ctrl* dTREND_CTR;
	struct PWI_Trend* dTREND;
	struct PWI_page* dPAGE;
	struct PWI_pInfo* dPAGE_INFO;
	struct nat_dm* dNAT_DM;
	struct PWI_nat* dPWI_NAT;
	struct key_dm* dKEY_DM;
	struct out_dm* dOUT_DM;
	struct PWI_out* dPWI_OUT;
	struct pwi_key* dPWI_KEY;
	struct PWI_root* dPWI;
	unsigned char* cmd;
	unsigned char* ch;
	struct mem_cmp zzmem_cmp00000;
	struct mem_cmp zzmem_cmp00001;
	struct mem_cmp zzmem_cmp00002;
	struct mem_cmp zzmem_cmp00003;
	struct mem_cmp zzmem_cmp00004;
	struct mem_cmp zzmem_cmp00005;
	struct mem_cmp zzmem_cmp00006;
	struct mem_cmp zzmem_cmp00007;
	struct mem_cmp zzmem_cmp00008;
	struct mem_cmp zzmem_cmp00009;
	struct mem_cmp zzmem_cmp00010;
	struct mem_cmp zzmem_cmp00011;
	struct mem_cmp zzmem_cmp00012;
	struct mem_cmp zzmem_cmp00013;
	struct mem_cmp zzmem_cmp00014;
	struct mem_cmp zzmem_cmp00015;
	struct mem_cmp zzmem_cmp00016;
	struct mem_cmp zzmem_cmp00017;
	struct mem_cmp zzmem_cmp00018;
	struct mem_cmp zzmem_cmp00019;
	struct mem_cmp zzmem_cmp00020;
	struct mem_cmp zzmem_cmp00021;
	struct mem_cmp zzmem_cmp00022;
	struct mem_cmp zzmem_cmp00023;
	struct mem_cmp zzmem_cmp00024;
	struct mem_cmp zzmem_cmp00025;
	struct mem_cmp zzmem_cmp00026;
	struct mem_cmp zzmem_cmp00027;
	struct asctint zzasctint00028;
	struct mem_cmp zzmem_cmp00029;
	struct mem_cmp zzmem_cmp00030;
	struct mem_cmp zzmem_cmp00031;
	struct mem_cmp zzmem_cmp00032;
	struct mem_cmp zzmem_cmp00033;
	struct mem_cmp zzmem_cmp00034;
	struct mem_cmp zzmem_cmp00035;
	struct mem_cmp zzmem_cmp00036;
	struct mem_cmp zzmem_cmp00037;
	struct mem_cmp zzmem_cmp00038;
	struct mem_cmp zzmem_cmp00039;
	struct mem_cmp zzmem_cmp00040;
	struct mem_cmp zzmem_cmp00041;
	struct mem_cmp zzmem_cmp00042;
	struct mem_cmp zzmem_cmp00043;
	struct mem_cmp zzmem_cmp00044;
	struct mem_cmp zzmem_cmp00045;
	struct asctint zzasctint00046;
	struct asctint zzasctint00047;
	struct asctint zzasctint00048;
	struct get_valu zzget_valu00049;
	struct get_valu zzget_valu00050;
	struct put_valu zzput_valu00051;
	struct asctint zzasctint00052;
	struct asctint zzasctint00053;
	struct get_valu zzget_valu00054;
	struct get_valu zzget_valu00055;
	struct put_valu zzput_valu00056;
	struct asctint zzasctint00057;
	struct asctint zzasctint00058;
	struct get_valu zzget_valu00059;
	struct get_valu zzget_valu00060;
	struct mem_cmp zzmem_cmp00061;
	struct asctint zzasctint00062;
	/* VAR_INPUT (digital) */
	plcbit memCheck;
	/* VAR (digital) */
	plcbit var;
	plcbit cnst;
	plcbit eq;
	plcbit ok;
} page_dat_typ;

typedef struct page_obj
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned long pradrObj;
	unsigned short noObj;
	unsigned long noDMIn;
	unsigned long offDMIn;
	unsigned long noDMFIn;
	unsigned long nameObj;
	/* VAR_OUTPUT (analogous) */
	unsigned short codeObj;
	unsigned short infoObjNo;
	unsigned short infoObjLn;
	unsigned short stateObj;
	unsigned long sizeObj;
	unsigned long noDMOut;
	unsigned long offDMOut;
	unsigned long noDMFOut;
	unsigned long nxadrObj;
	/* VAR (analogous) */
	unsigned long rest;
	unsigned long adrOBJ_CTR;
	unsigned long sizeOBJ;
	unsigned char Buffer[16];
	unsigned short m;
	unsigned char cmd;
	unsigned short countOBJCt;
	unsigned short countOBJDa;
	unsigned short actOBJCtrl;
	unsigned short noLine;
	unsigned char BufferNo[16];
	unsigned short noDM;
	unsigned long offDM;
	unsigned long sizeOBJCtr;
	unsigned short LEN;
	unsigned short n;
	unsigned long lenPV;
	unsigned long adrPV;
	unsigned long size;
	unsigned long adr_pt;
	unsigned char mem_type;
	unsigned long data_len;
	unsigned long dm_data;
	unsigned long dm_ident;
	unsigned long nextadr;
	struct PWI_list* dLIST;
	struct obj_ctrl* dOBJ_CTRL;
	struct obj_dm* dOBJ_DM;
	struct PWI_obj* dPWI_OBJ;
	struct PWI_root* dPWI;
	unsigned char* ch;
	struct mem_cmp zzmem_cmp00000;
	struct mem_cmp zzmem_cmp00001;
	struct mem_cmp zzmem_cmp00002;
	struct mem_cmp zzmem_cmp00003;
	struct mem_cmp zzmem_cmp00004;
	struct mem_cmp zzmem_cmp00005;
	struct mem_cmp zzmem_cmp00006;
	struct mem_cmp zzmem_cmp00007;
	struct mem_cmp zzmem_cmp00008;
	struct mem_cmp zzmem_cmp00009;
	struct mem_cmp zzmem_cmp00010;
	struct mem_cmp zzmem_cmp00011;
	struct mem_cmp zzmem_cmp00012;
	struct mem_cmp zzmem_cmp00013;
	struct mem_cmp zzmem_cmp00014;
	struct mem_cmp zzmem_cmp00015;
	struct mem_cmp zzmem_cmp00016;
	struct mem_cmp zzmem_cmp00017;
	struct asctint zzasctint00018;
	struct asctint zzasctint00019;
	struct asctint zzasctint00020;
	struct get_valu zzget_valu00021;
	struct get_valu zzget_valu00022;
	struct get_valu zzget_valu00023;
	struct put_valu zzput_valu00024;
	struct mem_cmp zzmem_cmp00025;
	struct asctint zzasctint00026;
	/* VAR_INPUT (digital) */
	plcbit memCheck;
	/* VAR (digital) */
	plcbit cnst;
	plcbit eq;
	plcbit var;
	plcbit ok;
} page_obj_typ;

typedef struct page_grp
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned short noGrph;
	unsigned long nameGrph;
	/* VAR_OUTPUT (analogous) */
	unsigned short codeGrph;
	unsigned short infoGrphNo;
	unsigned short infoGrphLn;
	unsigned short stateGrph;
	/* VAR (analogous) */
	unsigned char mem_type;
	unsigned long data_len;
	unsigned long dm_data;
	unsigned long dm_ident;
	unsigned long nextadr;
	unsigned char Buffer[16];
	struct PWI_Grph* dGRPH;
	struct PWI_root* dPWI;
	/* VAR_INPUT (digital) */
	plcbit memCheck;
} page_grp_typ;

typedef struct PWI_Pini
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	struct PWI_alloc* allocDat;
	unsigned short maxPage;
	unsigned short maxObj;
	unsigned short maxGrph;
	unsigned long nameMask;
	unsigned long namePage;
	unsigned long nameObj;
	unsigned long nameGrph;
	unsigned char* szTrends;
	/* VAR_OUTPUT (analogous) */
	unsigned short cntPage;
	unsigned short cntObj;
	unsigned short cntGrph;
	unsigned short codeMask;
	unsigned short infoMaskNo;
	unsigned short infoMaskLn;
	unsigned short codePage;
	unsigned short infoPageNo;
	unsigned short infoPageLn;
	unsigned short codeObj;
	unsigned short infoObjNo;
	unsigned short infoObjLn;
	unsigned short codeGrph;
	unsigned short infoGrphNo;
	unsigned short infoGrphLn;
	unsigned short codeTrend;
	unsigned short infoTrendN;
	unsigned short infoTrendL;
	unsigned short statePage;
	unsigned short stateFree;
	unsigned long sizeInfoPa;
	unsigned long sizeInfoLi;
	unsigned long sizeInfoGr;
	unsigned long sizeInfoTr;
	unsigned long sizePage;
	unsigned long sizeObj;
	unsigned long sizeGrph;
	unsigned long sizeTrend;
	unsigned long sizeRPS;
	/* VAR (analogous) */
	struct page_msk page_msk;
	struct page_dat page_dat;
	struct page_obj page_obj;
	struct page_grp page_grp;
	unsigned long adrTrend;
	unsigned long nxadrPage;
	unsigned long noFDM;
	unsigned long noDM;
	unsigned long nxadrObj;
	unsigned long sizeThis;
	unsigned long offDM;
	unsigned short m;
	struct trend_ctrl* dTREND_CTR;
	struct PWI_Trend* dTREND;
	struct PWI_Grph* dGRPH;
	struct PWI_list* dLIST;
	struct PWI_page* dPAGE;
	struct PWI_root* dPWI;
	struct mem_szem zzmem_szem00000;
	struct alloc zzalloc00001;
	struct alloc zzalloc00002;
	struct alloc zzalloc00003;
	struct alloc zzalloc00004;
	struct alloc zzalloc00005;
	struct alloc zzalloc00006;
	struct alloc zzalloc00007;
	/* VAR_INPUT (digital) */
	plcbit enable;
	plcbit memCheck;
	plcbit memType;
	/* VAR_OUTPUT (digital) */
	plcbit okPWI;
} PWI_Pini_typ;

typedef struct pwi_pclr
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned long adrPage;
	/* VAR (analogous) */
	unsigned short curNo;
	unsigned short cmdNo;
	struct obj_ctrl* dOBJ_CTRL;
	struct PWI_obj* dPWI_OBJ;
	struct out_ctrl* dOUT_CTRL;
	struct PWI_out* dPWI_OUT;
	struct PWI_page* dPAGE;
	struct PWI_list* dLIST;
} pwi_pclr_typ;

typedef struct pvt_kwt
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char mode;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct CtrlCode* dCCVT100;
	unsigned char* ch;
} pvt_kwt_typ;

typedef struct pvt_krep
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char mode;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct CtrlCode* dCCVT100;
	unsigned char* ch;
} pvt_krep_typ;

typedef struct pvt_clr
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct CtrlCode* dCCVT100;
} pvt_clr_typ;

typedef struct pvt_itm
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char mode;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct CtrlCode* dCCVT100;
	unsigned char* ch;
} pvt_itm_typ;

typedef struct pvt_kmod
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char code;
	unsigned char mode;
	unsigned long adrBuf;
	/* VAR_OUTPUT (analogous) */
	unsigned long nextAdr;
	/* VAR (analogous) */
	struct CtrlCode* dCCVT100;
	unsigned char* ch;
} pvt_kmod_typ;

typedef struct pwi_nvt1
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned long adrPage;
	unsigned long SendAdrIn;
	/* VAR_OUTPUT (analogous) */
	unsigned short statePage;
	unsigned long SendAdrOut;
	/* VAR (analogous) */
	unsigned short offGraph;
	struct pvt_kwt KWT_OUT;
	struct pvt_grp GRP_OUT;
	struct pvt_krep REP_OUT;
	unsigned long adrPWI_NAT;
	unsigned long nextAdr;
	struct pvt_clr CLR_OUT;
	struct pvt_itm ITM_OUT;
	struct pvt_kmod KEY_OUT;
	unsigned char orgcode;
	struct pvt_led LED_OUT;
	struct pvt_frm FRA_OUT;
	struct pvt_line LIN_OUT;
	struct pwi_tadr TADR;
	unsigned short cmdNo;
	struct pvt_attr ATT_OUT;
	struct pvt_str STR_OUT;
	struct PWI_env* dST_ENV;
	struct PWI_page* dPAGE;
	struct pwi_stdrv* dST_DRV;
	struct nat_dm* dNAT_DM;
	struct PWI_nat* dPWI_NAT;
	struct PWI_root* dPWI;
	unsigned char* ch;
	struct PWI_Grph* dGRPH;
} pwi_nvt1_typ;

typedef struct nextcur
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	signed char NeXTcurIn;
	/* VAR_OUTPUT (analogous) */
	unsigned char newCursor;
	signed char NeXTcurOut;
	/* VAR (analogous) */
	unsigned long adrOActiv;
	unsigned short lenOActiv;
	unsigned long adrActiv;
	unsigned long adrLIST;
	unsigned long szSTRCData;
	signed short ONoOld;
	signed short NoOld;
	unsigned char ownPageOld;
	unsigned short lenActiv;
	unsigned long objValue;
	signed short newOUT;
	signed short newOBJ;
	unsigned long objEnable;
	unsigned long outEnable;
	unsigned short PReVPage;
	unsigned short NeXTPage;
	unsigned short curCount;
	unsigned short pageCount;
	unsigned char ownPage;
	unsigned long adrPage[2];
	signed short i;
	signed short maxOBJ;
	signed short maxOUT;
	unsigned char* ch;
	struct PWI_pInfo* dPAGE_INFO;
	struct PWI_list* dLIST;
	struct PWI_env* dST_ENV;
	struct PWI_page* dPAGE;
	struct pwi_stinp* dST_INP;
	struct pwi_stdrv* dST_DRV;
	struct out_ctrl* dOUT_CTRL;
	struct PWI_out* dPWI_OUT;
	struct PWI_obj* dPWI_OBJ;
	struct get_valu zzget_valu00000;
	struct get_valu zzget_valu00001;
	struct put_valu zzput_valu00002;
	struct get_valu zzget_valu00003;
	struct put_valu zzput_valu00004;
	struct get_valu zzget_valu00005;
	struct put_valu zzput_valu00006;
	struct put_valu zzput_valu00007;
	struct put_valu zzput_valu00008;
	struct put_valu zzput_valu00009;
	/* VAR_INPUT (digital) */
	plcbit capCursor;
	/* VAR (digital) */
	plcbit nloopInput;
	plcbit searchPage;
	plcbit searchCur;
	plcbit searchCmd;
} nextcur_typ;

typedef struct pwi_ovt1
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned short SpeechAct;
	unsigned short PageAct;
	/* VAR_OUTPUT (analogous) */
	unsigned short PageNew;
	signed short ActcurNo;
	signed short ActcmdNo;
	unsigned char ActownPage;
	unsigned short stateOut;
	/* VAR (analogous) */
	struct pwi_xvt1 pwi_xvt1;
	struct pwi_nvt1 pwi_nvt1;
	struct nextcur NEXT_CUR;
	unsigned long adrOInitOl;
	unsigned long objValue;
	unsigned long adrInitOld;
	unsigned short addMsk;
	unsigned short addInp;
	struct pwi_pclr pwi_pclr;
	unsigned short prevPage;
	unsigned short nextPage;
	unsigned short addKey;
	unsigned short addOut;
	unsigned short addNat;
	struct pvt_cur CUR_OUT;
	unsigned short stateXOut;
	struct pwi_prio pwi_prio;
	unsigned long SendAdr;
	unsigned short stateNOut;
	unsigned long adrPage;
	signed char NeXTpage;
	struct PWI_env* dST_ENV;
	unsigned char* ch;
	struct pwi_ifbuf* dIF_BUF;
	struct pwi_stinp* dST_INP;
	struct pwi_stdrv* dST_DRV;
	struct PWI_page* dPAGE;
	struct PWI_pInfo* dPAGE_INFO;
	struct PWI_root* dPWI;
	struct get_valu zzget_valu00000;
	/* VAR_INPUT (digital) */
	plcbit enable;
	plcbit PageAgain;
	/* VAR_OUTPUT (digital) */
	plcbit PageReady;
} pwi_ovt1_typ;

typedef struct PWI_KAsk
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned char keycode;
	unsigned long SendAdrIn;
	/* VAR_OUTPUT (analogous) */
	unsigned short stateAsk;
	unsigned long SendAdrOut;
	/* VAR (analogous) */
	unsigned long nextAdr;
	struct ctrlCp121* dCCP121;
} PWI_KAsk_typ;

typedef struct pwi_hkey
{
	/* VAR_INPUT (analogous) */
	struct PWI_root* dPWI;
	unsigned long adrPage;
	unsigned char* keyField;
	unsigned char* keyFieldP;
	/* VAR_OUTPUT (analogous) */
	unsigned short PageNew;
	unsigned short SpeechNew;
	/* VAR (analogous) */
	unsigned long valAdd2;
	unsigned char mode;
	signed long iVALold;
	unsigned long lVALold;
	unsigned long lStepint;
	signed long iStepint;
	signed short keytime;
	unsigned long adrPVold;
	unsigned long lStep;
	signed long iStep;
	unsigned short lenPV;
	unsigned short cmdNo;
	unsigned long adrPV;
	unsigned long eVAL;
	unsigned long Tm_now;
	unsigned long lVAL;
	signed long iVAL;
	unsigned long valAdd1;
	unsigned long* dvalAdd1;
	unsigned long* dvalAdd2;
	struct PWI_env* dST_ENV;
	struct pwi_stinp* dST_INP;
	struct pwi_stdrv* dST_DRV;
	struct PWI_page* dPAGE;
	struct pwi_key* dPWI_KEY;
	struct key_ctrl* dKEY_CTRL;
	struct TON_10ms zzTON_10ms00000;
	struct get_vals zzget_vals00001;
	struct get_valu zzget_valu00002;
	struct get_vals zzget_vals00003;
	struct get_valu zzget_valu00004;
	struct get_valu zzget_valu00005;
	struct get_vals zzget_vals00006;
	struct put_valu zzput_valu00007;
	struct get_valu zzget_valu00008;
	struct put_valu zzput_valu00009;
	struct get_valu zzget_valu00010;
	struct get_vals zzget_vals00011;
	struct put_valu zzput_valu00012;
	struct get_valu zzget_valu00013;
	struct put_valu zzput_valu00014;
	struct get_valu zzget_valu00015;
	struct get_valu zzget_valu00016;
	struct put_valu zzput_valu00017;
	struct get_valu zzget_valu00018;
	struct get_valu zzget_valu00019;
	struct put_valu zzput_valu00020;
	struct get_valu zzget_valu00021;
	struct get_valu zzget_valu00022;
	struct get_valu zzget_valu00023;
	struct get_valu zzget_valu00024;
	/* VAR_INPUT (digital) */
	plcbit keypress;
	/* VAR_OUTPUT (digital) */
	plcbit changePuls;
	/* VAR (digital) */
	plcbit now;
	plcbit ok;
} pwi_hkey_typ;

typedef struct pwi_iclc
{
	/* VAR_INPUT (analogous) */
	unsigned long vali;
	unsigned char mode;
	unsigned char scale;
	unsigned long ScaleAdr;
	/* VAR_OUTPUT (analogous) */
	unsigned long valo;
	float valf;
	/* VAR (analogous) */
	struct PWI_ScalFc* dPWI_ScalF;
} pwi_iclc_typ;

typedef struct pwi_inp
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned char keyUserIn;
	unsigned char ASCIIUsrI;
	unsigned char keycode;
	unsigned long aKeyField;
	unsigned long aKeyFieldP;
	/* VAR_OUTPUT (analogous) */
	unsigned short PageNew;
	unsigned short SpeechNew;
	unsigned char keyUserOut;
	unsigned char ASCIIUsrO;
	unsigned short stateInp;
	/* VAR (analogous) */
	signed short pos;
	signed char nextInpESt;
	unsigned short m;
	unsigned short n;
	unsigned char keyFieldZ[16];
	unsigned char keyField[16];
	unsigned char keyFieldP[16];
	unsigned short base;
	unsigned char keyvalue;
	unsigned short length;
	unsigned long objEnable;
	unsigned long outEnable;
	unsigned char size;
	unsigned long valPV;
	unsigned long lVALold;
	unsigned short lenPV;
	struct pwi_hkey PWI_KEY;
	unsigned long lVAL;
	unsigned long adrPV;
	struct pwi_iclc VAL_CLC;
	signed short i;
	struct intasci INT_OUT;
	unsigned char new;
	unsigned char* dm;
	unsigned long* dvalAdd2;
	unsigned long* dvalAdd1;
	unsigned char* keyfunc;
	struct pwi_stwrk* dST_WRK;
	struct PWI_env* dST_ENV;
	struct pwi_stinp* dST_INP;
	struct pwi_stdrv* dST_DRV;
	struct PWI_root* dPWI;
	unsigned char* ch;
	struct asctint zzasctint00000;
	struct get_valu zzget_valu00001;
	struct put_valu zzput_valu00002;
	struct mem_cmp zzmem_cmp00003;
	struct get_valu zzget_valu00004;
	struct get_valu zzget_valu00005;
	/* VAR_INPUT (digital) */
	plcbit enable;
	plcbit keypuls;
	plcbit keypress;
	/* VAR_OUTPUT (digital) */
	plcbit inputOpen;
	plcbit enterPuls;
	plcbit changePuls;
	/* VAR (digital) */
	plcbit flag;
	plcbit eq;
} pwi_inp_typ;

typedef struct pwi_dini
{
	/* VAR_INPUT (analogous) */
	struct PWI_alloc* allocDat;
	unsigned long nameCCVT10;
	unsigned long nameCCP121;
	unsigned long nameTCode;
	unsigned long nameTFunc;
	unsigned long nameTHandy;
	unsigned long nameScale;
	unsigned long nameCharS;
	unsigned long adrEnv;
	unsigned long adrPar;
	/* VAR_OUTPUT (analogous) */
	unsigned long idPWI;
	unsigned short codePWI;
	unsigned short infoPWI1;
	unsigned short infoPWI2;
	unsigned short statePWI;
	unsigned short stateFree;
	unsigned long sizeRPS;
	/* VAR (analogous) */
	unsigned long DMlen;
	unsigned long sizeScale;
	unsigned long sizePWI;
	unsigned long DMid;
	unsigned long DMadr;
	unsigned char DMtyp;
	unsigned short i;
	unsigned long lZwi1;
	unsigned long lZwi2;
	struct pwi_stwrk* dST_WRK;
	struct PWI_env* dPWI_ENV;
	struct pwi_stinp* dST_INP;
	struct pwi_stdrv* dST_DRV;
	struct PWI_ScalFc* dPWI_ScalF;
	struct PWI_ScalDm* dPWI_ScalD;
	unsigned char* ch;
	struct PWI_root* dPWI;
	struct mem_szem zzmem_szem00000;
	struct alloc zzalloc00001;
	struct mem_szem zzmem_szem00002;
	struct alloc zzalloc00003;
	struct alloc zzalloc00004;
	/* VAR_INPUT (digital) */
	plcbit enable;
	plcbit memCheck;
	plcbit memType;
	plcbit TableauTyp;
	/* VAR_OUTPUT (digital) */
	plcbit okPWI;
} pwi_dini_typ;

typedef struct pwi_cini
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	struct PWI_alloc* allocDat;
	unsigned long idCANIn;
	unsigned short szTxQueue;
	unsigned short szRxQueue;
	unsigned short szT1Queue;
	unsigned short szT2Queue;
	unsigned short szT3Queue;
	unsigned short szT4Queue;
	unsigned long nameCANPar;
	unsigned long adrErrCAN;
	unsigned short nodeCAN;
	/* VAR_OUTPUT (analogous) */
	unsigned long idCANOut;
	unsigned short codeCAN;
	unsigned long infoCAN1;
	unsigned long infoCAN2;
	unsigned short stateCAN;
	unsigned short stateFree;
	unsigned long sizeIF;
	unsigned long sizeRPS;
	/* VAR (analogous) */
	unsigned short stateQueue;
	unsigned long idQueue;
	unsigned char Buffer[64];
	unsigned long DMlen;
	unsigned char DMtyp;
	unsigned long DMadr;
	unsigned long DMid;
	struct PWI_ifpar* dIF_PAR;
	struct pwi_ifbuf* dIF_BUF;
	struct PWI_exCAN* dPWI_exCAN;
	struct PWI_root* dPWI;
	struct mem_szem zzmem_szem00000;
	struct alloc zzalloc00001;
	struct str_cut zzstr_cut00002;
	struct CANopen zzCANopen00003;
	struct CANqueue zzCANqueue00004;
	/* VAR_INPUT (digital) */
	plcbit enable;
	plcbit memCheck;
	plcbit memType;
	/* VAR_OUTPUT (digital) */
	plcbit okPWI;
} pwi_cini_typ;

typedef struct pwi_ccom
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned char* KeyFieldT;
	unsigned char* KeyField;
	unsigned char* KeyFieldP;
	unsigned char* KeyFieldO;
	/* VAR_OUTPUT (analogous) */
	unsigned char keycode;
	unsigned char keycodeTab;
	unsigned char messageTab;
	unsigned short stateCanTX;
	unsigned short stateCanRX;
	unsigned short receiveLen;
	/* VAR (analogous) */
	unsigned char keyold;
	unsigned long Tm_LCheck;
	unsigned long Tm_Tout;
	unsigned long Tm_Wait;
	unsigned long Tm_Foll;
	unsigned long Tm_Rep;
	unsigned short m;
	unsigned char RxBuffer[16];
	unsigned char TxBuffer[16];
	unsigned short i;
	unsigned short rp;
	struct PWI_smbuf* dSM_BUF;
	struct ctrlCp121* dCCP121;
	struct CtrlCode* dCCVT100;
	struct PWI_env* dST_ENV;
	struct PWI_ifpar* dIF_PAR;
	struct pwi_ifbuf* dIF_BUF;
	struct PWI_root* dPWI;
	unsigned char* ch;
	struct pwi_stdrv* dST_DRV;
	struct TON_10ms zzTON_10ms00000;
	struct CANwrite zzCANwrite00001;
	struct TON_10ms zzTON_10ms00002;
	struct CANread zzCANread00003;
	struct CANrd zzCANrd00004;
	struct TON_10ms zzTON_10ms00005;
	struct TON_10ms zzTON_10ms00006;
	struct TON_10ms zzTON_10ms00007;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR_OUTPUT (digital) */
	plcbit TableauOK;
	plcbit keypress;
	plcbit keypuls;
	/* VAR (digital) */
	plcbit now_LCheck;
	plcbit now_Wait;
	plcbit now_Foll;
	plcbit now_Rep;
	plcbit now_Wait_o;
	plcbit TableauCh;
	plcbit now_Tout;
} pwi_ccom_typ;

typedef struct ld22Char
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	/* VAR_OUTPUT (analogous) */
	unsigned short codeld22;
	/* VAR (analogous) */
	unsigned short i;
	unsigned long sendAdr;
	unsigned long nextAdr;
	struct PWI_smbuf* dSM_BUF;
	unsigned char* semaphor;
	struct pwi_ifbuf* dIF_BUF;
	struct PWI_ifpar* dIF_PAR;
	struct CtrlCode* dCCVT100;
	struct PWI_root* dPWI;
	/* VAR_INPUT (digital) */
	plcbit enable;
} ld22Char_typ;

typedef struct ld11Char
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	/* VAR_OUTPUT (analogous) */
	unsigned short codeld11;
	/* VAR (analogous) */
	unsigned short i;
	unsigned long sendAdr;
	unsigned long nextAdr;
	struct PWI_smbuf* dSM_BUF;
	unsigned char* semaphor;
	struct pwi_ifbuf* dIF_BUF;
	struct PWI_ifpar* dIF_PAR;
	struct CtrlCode* dCCVT100;
	struct PWI_root* dPWI;
	/* VAR_INPUT (digital) */
	plcbit enable;
} ld11Char_typ;

typedef struct free
{
	/* VAR_INPUT (analogous) */
	struct PWI_alloc* allocDat;
	unsigned short count;
	/* VAR_OUTPUT (analogous) */
	unsigned short stateFree;
	/* VAR (analogous) */
	unsigned long adrsize;
	unsigned long size;
	unsigned short i;
} free_typ;

typedef struct df22Char
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned char bVal;
	unsigned char* pattern;
	/* VAR_OUTPUT (analogous) */
	unsigned short codedf22;
	/* VAR (analogous) */
	unsigned short m;
	unsigned short n;
	unsigned short i;
	unsigned long sendAdr;
	unsigned long nextAdr;
	struct PWI_smbuf* dSM_BUF;
	unsigned char* semaphor;
	struct pwi_ifbuf* dIF_BUF;
	struct PWI_ifpar* dIF_PAR;
	unsigned char* ch;
	struct CtrlCode* dCCVT100;
	struct PWI_root* dPWI;
	/* VAR_INPUT (digital) */
	plcbit enable;
} df22Char_typ;

typedef struct df11Char
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned char bVal;
	unsigned char* pattern;
	/* VAR_OUTPUT (analogous) */
	unsigned short codedf11;
	/* VAR (analogous) */
	unsigned short m;
	unsigned short n;
	unsigned short i;
	unsigned long sendAdr;
	unsigned long nextAdr;
	struct PWI_smbuf* dSM_BUF;
	unsigned char* semaphor;
	struct pwi_ifbuf* dIF_BUF;
	struct PWI_ifpar* dIF_PAR;
	unsigned char* ch;
	struct CtrlCode* dCCVT100;
	struct PWI_root* dPWI;
	/* VAR_INPUT (digital) */
	plcbit enable;
} df11Char_typ;

typedef struct chBright
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned char bright;
	/* VAR_OUTPUT (analogous) */
	unsigned short codechBrig;
	/* VAR (analogous) */
	unsigned short i;
	unsigned long sendAdr;
	unsigned long nextAdr;
	struct PWI_smbuf* dSM_BUF;
	unsigned char* semaphor;
	struct pwi_ifbuf* dIF_BUF;
	struct PWI_ifpar* dIF_PAR;
	unsigned char* ch;
	struct CtrlCode* dCCVT100;
	struct PWI_root* dPWI;
	/* VAR_INPUT (digital) */
	plcbit enable;
} chBright_typ;

typedef struct wrTrend
{
	/* VAR_INPUT (analogous) */
	unsigned long idPWI;
	unsigned short trendNo;
	unsigned char pVar;
	/* VAR_OUTPUT (analogous) */
	unsigned short codeTrend;
	/* VAR (analogous) */
	unsigned long adrTrend;
	struct trend_ctrl* dTREND_CTR;
	struct PWI_Trend* dTREND;
	unsigned char* ch;
	struct PWI_root* dPWI;
	/* VAR_INPUT (digital) */
	plcbit enable;
	plcbit over;
	/* VAR_OUTPUT (digital) */
	plcbit fullTrend;
} wrTrend_typ;



/* Prototyping of functions and function blocks */
void alloc(alloc_typ* inst);
void tmBright(tmBright_typ* inst);
void str_cut(str_cut_typ* inst);
void setPoint(setPoint_typ* inst);
void rsTrend(rsTrend_typ* inst);
void pwi_xvt1(pwi_xvt1_typ* inst);
void PWI_Tini(PWI_Tini_typ* inst);
void pwi_tadr(pwi_tadr_typ* inst);
void pwi_rini(pwi_rini_typ* inst);
void pwi_rcom(pwi_rcom_typ* inst);
void pwi_prio(pwi_prio_typ* inst);
void PWI_Pini(PWI_Pini_typ* inst);
void pwi_pclr(pwi_pclr_typ* inst);
void pwi_ovt1(pwi_ovt1_typ* inst);
void pwi_omrk(pwi_omrk_typ* inst);
void pwi_oclc(pwi_oclc_typ* inst);
void pwi_ochk(pwi_ochk_typ* inst);
void pwi_objm(pwi_objm_typ* inst);
void pwi_obje(pwi_obje_typ* inst);
void pwi_objc(pwi_objc_typ* inst);
void pwi_nvt1(pwi_nvt1_typ* inst);
void PWI_KAsk(PWI_KAsk_typ* inst);
void pwi_inp(pwi_inp_typ* inst);
void pwi_iclc(pwi_iclc_typ* inst);
void pwi_hkey(pwi_hkey_typ* inst);
void pwi_dini(pwi_dini_typ* inst);
void pwi_cini(pwi_cini_typ* inst);
void pwi_ccom(pwi_ccom_typ* inst);
void pvt_trd(pvt_trd_typ* inst);
void pvt_str(pvt_str_typ* inst);
void pvt_line(pvt_line_typ* inst);
void pvt_led(pvt_led_typ* inst);
void pvt_kwt(pvt_kwt_typ* inst);
void pvt_krep(pvt_krep_typ* inst);
void pvt_kmod(pvt_kmod_typ* inst);
void pvt_itm(pvt_itm_typ* inst);
void pvt_grp(pvt_grp_typ* inst);
void pvt_frm(pvt_frm_typ* inst);
void pvt_cur(pvt_cur_typ* inst);
void pvt_cps(pvt_cps_typ* inst);
void pvt_clr(pvt_clr_typ* inst);
void pvt_cha(pvt_cha_typ* inst);
void pvt_bar(pvt_bar_typ* inst);
void pvt_attr(pvt_attr_typ* inst);
void get_valu(get_valu_typ* inst);
void page_txt(page_txt_typ* inst);
void page_obj(page_obj_typ* inst);
void page_msk(page_msk_typ* inst);
void page_grp(page_grp_typ* inst);
void page_dat(page_dat_typ* inst);
void nextcur(nextcur_typ* inst);
void mem_szem(mem_szem_typ* inst);
void mem_cmp(mem_cmp_typ* inst);
void ld22Char(ld22Char_typ* inst);
void ld11Char(ld11Char_typ* inst);
void intasci(intasci_typ* inst);
void put_valu(put_valu_typ* inst);
void get_vals(get_vals_typ* inst);
void free(free_typ* inst);
void df22Char(df22Char_typ* inst);
void df11Char(df11Char_typ* inst);
void chBright(chBright_typ* inst);
void asctint(asctint_typ* inst);
void wrTrend(wrTrend_typ* inst);



#endif /* PWI_LIB_H_ */
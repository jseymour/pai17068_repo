/****************************************************************************/
/*                                                                          */
/*  FRAMELIB.H                                                              */
/*  Automation Studio Frame-Driver-Library Include File                     */
/*                                                                          */
/*      Automation Studio                                                   */
/*  Copyright Bernecker&Rainer 2000                                         */
/*                                                                          */
/****************************************************************************/

#ifndef _DVFRAME_H_
#ifdef __cplusplus
extern "C" {
#endif
#define _DVFRAME_H_

/*
Includes
*/

#include        <bur/plctypes.h>            /* Data type definitions to IEC-1131 */

/*
Type Definiton
*/

/* Definition der Bit-Struktur */
typedef struct FRM_xopen
{
/* Non boolean Input Parameter */
UDINT   device;
UDINT   mode;
UDINT   config;

/* Non boolean Output Parameter */
UINT    status;
UDINT   ident;

/* Non boolean Static Local */
USINT   InternalData[28];

/* Boolean Input Parameter */
BOOL    enable;

/* Boolean Output Parameter */

/* Boolean Static Local */

} FRM_xopen_typ;

/* Definition der Bit-Struktur */
typedef struct FRM_close
{
/* Non boolean Input Parameter */
UDINT   ident;

/* Non boolean Output Parameter */
UINT    status;

/* Non boolean Static Local */
USINT   InternalData[28];

/* Boolean Input Parameter */
BOOL    enable;

/* Boolean Output Parameter */

/* Boolean Static Local */

} FRM_close_typ;

typedef struct FRM_read
{
/* Non boolean Input Parameter */
UDINT   ident;

/* Non boolean Output Parameter */
UINT    status;
UDINT   buffer;
UINT    buflng;

/* Non boolean Static Local */
USINT   InternalData[28];

/* Boolean Input Parameter */
BOOL    enable;

/* Boolean Output Parameter */

/* Boolean Static Local */

} FRM_read_typ;

typedef struct FRM_write
{
/* Non boolean Input Parameter */
UDINT   ident;
UDINT   buffer;
UINT    buflng;

/* Non boolean Output Parameter */
UINT    status;

/* Non boolean Static Local */
USINT   InternalData[28];

/* Boolean Input Parameter */
BOOL    enable;

/* Boolean Output Parameter */

/* Boolean Static Local */

} FRM_write_typ;

typedef struct FRM_gbuf
{
/* Non boolean Input Parameter */
UDINT   ident;

/* Non boolean Output Parameter */
UINT    status;
UDINT   buffer;
UINT    buflng;

/* Non boolean Static Local */
USINT   InternalData[28];

/* Boolean Input Parameter */
BOOL    enable;

/* Boolean Output Parameter */

/* Boolean Static Local */

} FRM_gbuf_typ;

typedef struct FRM_robuf
{
/* Non boolean Input Parameter */
UDINT   ident;
UDINT   buffer;
UINT    buflng;

/* Non boolean Output Parameter */
UINT    status;

/* Non boolean Static Local */
USINT   InternalData[28];

/* Boolean Input Parameter */
BOOL    enable;

/* Boolean Output Parameter */

/* Boolean Static Local */

} FRM_robuf_typ;

typedef struct FRM_rbuf
{
/* Non boolean Input Parameter */
UDINT   ident;
UDINT   buffer;
UINT    buflng;

/* Non boolean Output Parameter */
UINT    status;

/* Non boolean Static Local */
USINT   InternalData[28];

/* Boolean Input Parameter */
BOOL    enable;

/* Boolean Output Parameter */

/* Boolean Static Local */

} FRM_rbuf_typ;

typedef struct FRM_mode
{
/* Non boolean Input Parameter */
UDINT   ident;
UDINT   mode;

/* Non boolean Output Parameter */
UINT    status;

/* Non boolean Static Local */
USINT   InternalData[28];

/* Boolean Input Parameter */
BOOL    enable;

/* Boolean Output Parameter */

/* Boolean Static Local */

} FRM_mode_typ;

typedef struct FRM_ctrl
{
/* Non boolean Input Parameter */
UDINT   ident;
UINT    ioctrl;
UDINT   inarg;

/* Non boolean Output Parameter */
UINT    status;
UDINT   outarg;

/* Non boolean Static Local */
USINT   InternalData[44];

/* Boolean Input Parameter */
BOOL    enable;

/* Boolean Output Parameter */

/* Boolean Static Local */

} FRM_ctrl_typ;

/*
Additonal Typedefinitions for FRM_xopen (Input "config")
*/

typedef
struct
{
UINT    idle;       /* idle */
UINT    delimc;     /* number of delimiter */
USINT   delim[2];   /* delimiter */
UINT    tx_cnt;     /* number of tx buffer */
UINT    rx_cnt;     /* number of rx buffer */
UINT    tx_lng;     /* length of tx buffer */
UINT    rx_lng;     /* length of rx buffer */
UINT    argc;       /* number of arguments */
UDINT   *argv;      /* array of arguments */
} XOPENCONFIG;

/* ----------------------------------------------------------------------- */
#define     DVARG_BAUD             (1<< 0)  /* baudrate in multiples of 100 */
#define     ARG_BAUD                    0   /* */
/* definition of atypical baudrates */
/* ----------------------------------------------------------------------- */
#define     DVARG_EVSEND           (1<<14)  /* Values for taskID and events for static usage: */
#define     ARG_EVSEND                 14   /* HIGHWORD = HIWORD(taskID), LOWWORD = event mask */
/* !!! to not use in cyclic applications !!! */
/* ----------------------------------------------------------------------- */
#define     DVARG_PVPOLLADR        (1<<15)  /* Frame received counter for polling reasons */
#define     ARG_PVPOLLADR              15   /* */
/* contents of address (PV/unsigned char) is incremented! */
/* ----------------------------------------------------------------------- */
#define     DVARG_TXPVPOLLADR      (1<<17)  /* frame transmit counter for polling reasons */
#define     ARG_TXPVPOLLADR            17   /* */
/* contents of address (PV/unsigned char) is incremented! */
/* ----------------------------------------------------------------------- */



/*
Prototyping
*/

void FRM_xopen  (FRM_xopen_typ   *lpFrmXopen);
void FRM_close  (FRM_close_typ   *lpFrmClose);
void FRM_read   (FRM_read_typ    *lpFrmRead);
void FRM_write  (FRM_write_typ   *lpFrmWrite);
void FRM_gbuf   (FRM_gbuf_typ    *lpFrmGetOutputBuffer);
void FRM_robuf  (FRM_robuf_typ   *lpFrmReleaseOutputBuffer);
void FRM_rbuf   (FRM_rbuf_typ    *lpFrmReleaseInputBuffer);
void FRM_mode   (FRM_mode_typ    *lpFrmMode);
void FRM_ctrl   (FRM_ctrl_typ    *lpFrmControl);

#ifdef __cplusplus
}
#endif
#endif /* _DVFRAME_H_ */

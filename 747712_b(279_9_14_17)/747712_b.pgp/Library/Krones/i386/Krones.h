/* Automation Studio Generated Header File, Format Version 1.00 */
/* do not change */
#ifndef KRONES_H_
#define KRONES_H_
#define _WEAK	__attribute__((__weak__))

#include <bur/plctypes.h>



/* Constants */
_WEAK const signed char MAXVERWEILZEIT = 50;
_WEAK const signed char MAX_VERWEILZEIT = 50;


/* Datatypes */


/* Datatypes of function blocks */
typedef struct String_Move
{
	/* VAR_INPUT (analogous) */
	unsigned long adrQuellString;
	unsigned long adrZielString;
	unsigned short ZielPos;
	/* VAR (analogous) */
	unsigned short aktPos;
	unsigned short Verweilzeit;
	signed short lenZielString;
	signed short lenQuellString;
	plcstring Leerzeichen[5+1];
	/* VAR_INPUT (digital) */
	plcbit StartCopy;
	/* VAR_OUTPUT (digital) */
	plcbit Fertig;
	/* VAR (digital) */
	plcbit SchiebeRechts;
	plcbit SchiebeLinks;
	plcbit zzEdge00000;
} String_Move_typ;



/* Prototyping of functions and function blocks */
signed short INT_MOVE(plcbit Enable, signed short Input);
unsigned char Wert_Schieberegister(unsigned char* Schieberegister, unsigned char Auslesezeiger);
signed short INT_ADD(plcbit Enable, signed short Input_1, signed short Input_2);
void String_Move(String_Move_typ* inst);



#endif /* KRONES_H_ */
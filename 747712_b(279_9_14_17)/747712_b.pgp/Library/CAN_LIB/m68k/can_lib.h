/* Automation Studio Generated Header File, Format Version 1.00 */
/* do not change */
#ifndef CAN_LIB_H_
#define CAN_LIB_H_
#define _WEAK	__attribute__((__weak__))

#include <bur/plc.h>



/* Constants */


/* Datatypes */
typedef struct DevParam
{
	unsigned char baudrate;
	unsigned short cob_nr;
	unsigned long pError;
	unsigned long pDevice;
	unsigned short info;
} DevParam;



/* Datatypes of function blocks */
typedef struct CANbtreg
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long treg0;
	unsigned long treg1;
	/* VAR_OUTPUT (analogous) */
	unsigned short status;
	/* VAR_INPUT (digital) */
	plcbit enable;
} CANbtreg_typ;

typedef struct CANdftab
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long table_adr;
	unsigned short tab_num;
	/* VAR_OUTPUT (analogous) */
	unsigned long tab_ident;
	unsigned short status;
	/* VAR (analogous) */
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CANdftab_typ;

typedef struct CANexcep
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long can_id;
	unsigned char exc_nr;
	/* VAR_OUTPUT (analogous) */
	unsigned short status;
	/* VAR (analogous) */
	unsigned long init_cnt;
	unsigned long cob_handle;
	unsigned long ex_p;
	unsigned long ex_maxtask;
	unsigned long ss_irqvw_p;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CANexcep_typ;

typedef struct CANgetid
{
	/* VAR_INPUT (analogous) */
	unsigned long data_adr;
	/* VAR_OUTPUT (analogous) */
	unsigned char data_lng;
	unsigned long can_id;
	unsigned short status;
	/* VAR (analogous) */
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
} CANgetid_typ;

typedef struct CANnode
{
	/* VAR_OUTPUT (analogous) */
	unsigned char node_nr;
	unsigned short status;
	/* VAR_INPUT (digital) */
	plcbit enable;
} CANnode_typ;

typedef struct CANopen
{
	/* VAR_INPUT (analogous) */
	unsigned char baud_rate;
	unsigned short cob_anz;
	unsigned long error_adr;
	unsigned long device;
	unsigned short info;
	/* VAR_OUTPUT (analogous) */
	unsigned long us_ident;
	unsigned short status;
	/* VAR (analogous) */
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CANopen_typ;

typedef struct CANqueue
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long can_id;
	unsigned short size;
	/* VAR_OUTPUT (analogous) */
	unsigned long q_ident;
	unsigned short status;
	/* VAR (analogous) */
	unsigned long cob_handle;
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CANqueue_typ;

typedef struct CANrd
{
	/* VAR_INPUT (analogous) */
	unsigned long q_ident;
	unsigned long data_adr;
	/* VAR_OUTPUT (analogous) */
	unsigned short size;
	unsigned short status;
	/* VAR_INPUT (digital) */
	plcbit enable;
} CANrd_typ;

typedef struct CANread
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long can_id;
	unsigned long data_adr;
	/* VAR_OUTPUT (analogous) */
	unsigned char data_lng;
	unsigned short status;
	/* VAR (analogous) */
	unsigned long cob_handle;
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CANread_typ;

typedef struct CANrtr
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long can_id;
	unsigned long data_adr;
	/* VAR_OUTPUT (analogous) */
	unsigned char data_lng;
	unsigned short status;
	/* VAR (analogous) */
	unsigned long cob_handle;
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	plcbit request;
	/* VAR (digital) */
	plcbit old_enable;
} CANrtr_typ;

typedef struct CANrwtab
{
	/* VAR_INPUT (analogous) */
	unsigned long tab_ident;
	/* VAR_OUTPUT (analogous) */
	unsigned short status;
	/* VAR_INPUT (digital) */
	plcbit enable;
} CANrwtab_typ;

typedef struct CANngp
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long can_id;
	unsigned char nd_state;
	/* VAR_OUTPUT (analogous) */
	unsigned short status;
	/* VAR (analogous) */
	unsigned long old_id;
	unsigned long cob_handle;
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CANngp_typ;

typedef struct CANupd
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long can_id;
	unsigned long data_adr;
	unsigned char data_lng;
	/* VAR_OUTPUT (analogous) */
	unsigned short status;
	/* VAR (analogous) */
	unsigned long old_id;
	unsigned long cob_handle;
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CANupd_typ;

typedef struct CANwrite
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long can_id;
	unsigned long data_adr;
	unsigned char data_lng;
	/* VAR_OUTPUT (analogous) */
	unsigned short status;
	/* VAR (analogous) */
	unsigned long old_id;
	unsigned long cob_handle;
	unsigned long init_cnt;
	unsigned char wr_order;
	unsigned char align_byte;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CANwrite_typ;

typedef struct CANxnode
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	/* VAR_OUTPUT (analogous) */
	unsigned char node_nr;
	unsigned short status;
	/* VAR_INPUT (digital) */
	plcbit enable;
} CANxnode_typ;

typedef struct CMSdlcon
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long client_id;
	unsigned long server_id;
	unsigned long data_adr;
	unsigned long data_len;
	/* VAR_OUTPUT (analogous) */
	unsigned short add_code;
	unsigned char err_class;
	unsigned char err_code;
	unsigned long mult;
	unsigned long dl_len;
	unsigned short status;
	/* VAR (analogous) */
	unsigned long cob_handle;
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	plcbit initiate;
	/* VAR (digital) */
	plcbit old_enable;
} CMSdlcon_typ;

typedef struct CMSdlreq
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long client_id;
	unsigned long server_id;
	unsigned long mult;
	unsigned long data_adr;
	unsigned long data_len;
	/* VAR_OUTPUT (analogous) */
	unsigned short add_code;
	unsigned char err_class;
	unsigned char err_code;
	unsigned long dl_len;
	unsigned short status;
	/* VAR (analogous) */
	unsigned long cob_handle;
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	plcbit initiate;
	/* VAR (digital) */
	plcbit old_enable;
} CMSdlreq_typ;

typedef struct CMSinit
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned char user_type;
	unsigned short queue_cnt;
	unsigned long modul_name;
	/* VAR_OUTPUT (analogous) */
	unsigned short cms_entry;
	unsigned long cms_ident;
	unsigned short status;
	/* VAR (analogous) */
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CMSinit_typ;

typedef struct CMSmain
{
	/* VAR_INPUT (analogous) */
	unsigned long cms_ident;
	unsigned long cms_mode;
	/* VAR_OUTPUT (analogous) */
	unsigned short status;
	/* VAR (analogous) */
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
} CMSmain_typ;

typedef struct GetNdNr
{
	/* VAR_INPUT (analogous) */
	unsigned long dev_adr;
	/* VAR_OUTPUT (analogous) */
	unsigned char nodenr;
	unsigned short status;
	/* VAR_INPUT (digital) */
	plcbit enable;
} GetNdNr_typ;

typedef struct SetNdNr
{
	/* VAR_INPUT (analogous) */
	unsigned long device;
	unsigned char nodenr;
	/* VAR_OUTPUT (analogous) */
	unsigned short status;
	/* VAR_INPUT (digital) */
	plcbit enable;
} SetNdNr_typ;

typedef struct CANquwr
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long can_id;
	unsigned long data_adr;
	unsigned char data_lng;
	/* VAR_OUTPUT (analogous) */
	unsigned short status;
	/* VAR (analogous) */
	unsigned long cob_handle;
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CANquwr_typ;

typedef struct CANMulrd
{
	/* VAR_INPUT (analogous) */
	unsigned long q_ident;
	unsigned long data_adr;
	/* VAR_OUTPUT (analogous) */
	unsigned short size;
	unsigned long ID;
	unsigned short status;
	/* VAR_INPUT (digital) */
	plcbit enable;
} CANMulrd_typ;

typedef struct CANMulOpen
{
	/* VAR_INPUT (analogous) */
	unsigned long pDevParam;
	unsigned char dev_nr;
	/* VAR_OUTPUT (analogous) */
	unsigned long us_ident;
	unsigned short status;
	/* VAR (analogous) */
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CANMulOpen_typ;

typedef struct CANMulQueue
{
	/* VAR_INPUT (analogous) */
	unsigned long us_ident;
	unsigned long pCAN_id;
	unsigned char ID_nr;
	unsigned short size;
	/* VAR_OUTPUT (analogous) */
	unsigned long q_ident;
	unsigned short status;
	/* VAR (analogous) */
	unsigned long cob_handle;
	unsigned long init_cnt;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit old_enable;
} CANMulQueue_typ;



/* Prototyping of functions and function blocks */
void CANbtreg(CANbtreg_typ* inst);
void CANdftab(CANdftab_typ* inst);
void CANexcep(CANexcep_typ* inst);
void CANgetid(CANgetid_typ* inst);
void CANnode(CANnode_typ* inst);
void CANopen(CANopen_typ* inst);
void CANqueue(CANqueue_typ* inst);
void CANrd(CANrd_typ* inst);
void CANread(CANread_typ* inst);
void CANrtr(CANrtr_typ* inst);
void CANrwtab(CANrwtab_typ* inst);
void CANngp(CANngp_typ* inst);
void CANupd(CANupd_typ* inst);
void CANwrite(CANwrite_typ* inst);
void CANxnode(CANxnode_typ* inst);
void CMSdlcon(CMSdlcon_typ* inst);
void CMSdlreq(CMSdlreq_typ* inst);
void CMSinit(CMSinit_typ* inst);
void CMSmain(CMSmain_typ* inst);
void GetNdNr(GetNdNr_typ* inst);
void SetNdNr(SetNdNr_typ* inst);
void CANquwr(CANquwr_typ* inst);
void CANMulrd(CANMulrd_typ* inst);
void CANMulOpen(CANMulOpen_typ* inst);
void CANMulQueue(CANMulQueue_typ* inst);



#endif /* CAN_LIB_H_ */
/****************************************************************************
*                    B & R   P O S I T I O N I N G                          *
*****************************************************************************
*                                                                           *
*            Header File for Library NCGLOBAL (Version 0270)                * 
*                                                                           *
**************************** COPYRIGHT (C) **********************************
*     THIS SOFTWARE IS THE PROPERTY OF B&R AUSTRIA: ALL RIGHTS RESERVED.    *
*     NO PART OF THIS SOFTWARE MAY BE USED OR COPIED IN ANY WAY WITHOUT     *
*              THE PRIOR WRITTEN PERMISSION OF B&R AUSTRIA.                 *
****************************************************************************/

#ifndef NCGLOBAL_H_ 

#define NCGLOBAL_H_ 

#include <bur/plctypes.h>


/*** FUNCTIONS **************************************************************/ 

UINT ncaccess (UINT nc_sw_id, char *access_name, UDINT *p_nc_object);
UINT ncaction (UDINT nc_object, UINT subject, UINT action);
UINT ncalloc  (UINT bus_typ, UINT modul_adr, UINT object_typ, UINT channel, UDINT object_adr);
UINT nccnccom (UDINT nc_object);
UINT ncda_cr  (USINT mo_typ, USINT *name_p, UDINT dat_len, void *dat_p, void **mo_dat_p_p, void *mo_ident_p);
UINT ncda_dir (USINT mo_typ, USINT *name_p, void **mo_dat_p_p, void *mo_ident_p);
UINT ncda_id  (USINT mo_typ, USINT *name_p, void *mo_ident_p);
UINT ncda_inf (UDINT mo_ident, void **mo_dat_p_p, void *dat_len_p, void *mem_typ_p);
UINT ncda_rd  (UDINT mo_ident, void *dat_p, UDINT dat_len, UDINT mo_dat_off);
UINT ncda_wr  (UDINT mo_ident, void *dat_p, UDINT dat_len, UDINT mo_dat_off);
UINT ncdm_rec (USINT rec_index, UINT dm455_adr, UDINT nc_obj_ax1, UDINT nc_obj_ax2, UDINT pp_cam_dat);
UINT ncirqbuf (UDINT p_input, UDINT pp_buffer);


/*** CONSTANTS **************************************************************/ 

#define ncTRG_STOP        0x2000 /* Modus "Stop nach Trigger" */
#define ncBASIS_TRG_STOP  137    /* Modus "Stop nach Trigger" f�r Basisbewegungen */
#define ncT_GESAMT        128    /* Um "t_gesamt" verz�gert */
#define ncEINTRITT        4      /* Eintritt in den definierten Bereich */
#define ncSERCOS_LIN      0x80   /* Lineargeber */
#define ncMELDUNG         1      /* Meldung */
#define ncETEL            2      /* Etel */
#define ncPACSI           1      /* Pacsi */
#define ncINDRAMAT        0      /* Indramat */
#define ncKORREKTUR       0x80   /* Z�hlbereichs-Korrektur */
#define ncREF_OFFSET      5      /* Referenzier-Offset */
#define ncEREIGNIS        136    /* Ereignis */
#define ncMA_V_KOMP       0x8000 /* Master-Geschwindigkeits-Kompensation */
#define ncPARID_TRACE     10     /* Parid-Trace Modul */
#define nc157CAN          9      /* CAN f�r NC157 */
#define ncDATEN_TEXT      0x0400 /* Datentext */
#define ncBRENNEN         0x0154 /* Brennen */
#define ncBREMS           4      /* Bremsen */
#define ncBINAER          1      /* Bin�r-Code */
#define ncBEW_ENDE        2      /* Bewegungs-Ende */
#define ncBEW_ANF         1      /* Bewegungs-Anfang */
#define ncBEWEGUNG        3      /* Bewegung */
#define ncBESCHL          1      /* Beschleunigung */
#define ncBELEGT          1      /* Belegt */
#define ncBEGRENZ         114    /* Begrenzer */
#define ncBASIS           1      /* Basis-Zustand */
#define ncAUT_ENDE        10     /* Automat-Ende */
#define ncAUTOMAT         122    /* Automat */
#define ncAUSSCH          0x0103 /* Ausschalten */
#define ncAUS             0      /* Aus */
#define ncANZAHL          11     /* Anzahl */
#define ncANTR_IF         104    /* Antriebs-Interface */
#define ncANTRIEB_BEREIT  255    /* Antrieb bereit */
#define RIO4_2005         0x1180 /* Remote I/O im System 2005 (Master-Nr.4) */
#define RIO3_2005         0x0D80 /* Remote I/O im System 2005 (Master-Nr.3) */
#define RIO2_2005         0x0980 /* Remote I/O im System 2005 (Master-Nr.2) */
#define RIO1_2005         0x0580 /* Remote I/O im System 2005 (Master-Nr.1) */
#define MOD_UPDATE        0x8000 /* Update der SW-Module auf der DM455 */
#define IO_2010           0x0400 /* I/O-Modul im System 2010 */
#define IO_2005           0x0480 /* I/O-Modul im System 2005 */
#define FAST_BOOT         0x4000 /* Schnelles Hochfahren der DM455 */
#define ncSEND            0x0170 /* Senden */
#define ncWRITE_COB       320    /* Write-CAN-Objekt */
#define ncACP10USCOB      0x00FF /* ACP10-SW: User-CAN-Objekte */
#define ncMA_S_START_I32  0x0800 /* INT32-Modus f�r "ma_s_start" */
#define ncTRIGGER_2       6      /* Aktuelle Achse: Trigger 2 */
#define ncTRG_STOP_S_REST 2      /* Modus "Stop nach Trigger" (Variante "+s_rest") */
#define ncTRG_STOP_NORMAL 1      /* Modus "Stop nach Trigger" (normal) */
#define ncEINSCH          0x0102 /* Einschalten */
#define ncEIN             1      /* Ein */
#define ncD_EINZSZ        207    /* Decoder-Einzelsatzbetrieb */
#define ncDPR_OVR         217    /* DPR-Override */
#define ncDOWNLOAD        0x0153 /* Download */
#define ncDM16BIT         0x2000 /* Normierung auf 16-Bit-Wert f�r DM455 */
#define ncDIREKT          0      /* Direkt */
#define ncDIG_E           102    /* Digitale Eing�nge */
#define ncDECODER         206    /* Decoder */
#define ncDAT_MOD         301    /* Daten-Modul */
#define ncDATEN           0      /* Daten */
#define ncCNC_SPS         212    /* CNC-SPS-Daten */
#define ncCNC_PROG        20     /* CNC-Programm */
#define ncCNC_K_AX        199    /* CNC-System als Kopplungsachse */
#define ncCNCSYS          2      /* CNC-System */
#define ncCNC             0      /* CNC-Achse (Kartesische Bahnachse) */
#define ncPOWERLINK_IF    0x0001 /* PowerLink Interface */
#define ncCAN_IF          0x0000 /* CAN Interface */
#define ncARNC0MAN        0x9000 /* ARNC0-Manager */
#define ncOHNE_INDEX      1      /* Datenblock-�bertragung ohne Index */
#define ncMIT_INDEX       0      /* Datenblock-�bertragung mit Index */
#define ncFORMAT_B06      2      /* Parameter-Format: Bin�rdaten, 6 Bytes */
#define ncFORMAT_T10      1      /* Parameter-Format: Datentext, 10 Bytes */
#define ncFORMAT_ADR      0      /* Parameter-Format: Datenadresse */
#define ncPAR_SEQU        400    /* Parameter-Sequenz */
#define ncQUICKSTOP       2      /* Quickstop-Funktion */
#define ncSTART_T         0x0113 /* Starten (nach dem Start an die Zeit gekoppelt) */
#define ncINIT_SOLL_POS   6      /* Initialisierung der Sollposition */
#define ncSSI             6      /* SSI Absolutwertgeber */
#define ncRESOLVER        5      /* Resolver */
#define nc20KHZ           0      /* 20 kHz */
#define nc100KHZ          1      /* 100 kHz */
#define ncG00             0      /* G00 */
#define ncFU              1      /* Frequenzumrichter */
#define ncFREI            0      /* Frei */
#define ncFORTSETZ        0x0107 /* Fortsetzen */
#define ncFLIEGEND        3      /* Fliegend */
#define ncFLANKE2         12     /* Zweite Flanke */
#define ncFIXRAM          5      /* FIX-RAM */
#define ncFENSTER         1      /* Fenster */
#define ncFEHL_KL0        215    /* Fehlermeldungen mit Klasse 0 */
#define ncFEHLER          1      /* Fehler */
#define ncFALSCH          0      /* Falsch */
#define ncEX_PARAM        214    /* Verriegelung f�r EX(terne)-Parameter */
#define ncEXTERN          128    /* Extern */
#define ncEVEN            3      /* Gerade */
#define ncEPROM           2      /* Eprom */
#define ncEINZSZ          204    /* Einzelsatzbetrieb */
#define ncSATZ            202    /* CNC-Satz */
#define ncPAR_LIST        402    /* Parameter-Liste */
#define ncPAR_ID          16     /* Parameter-ID */
#define ncSL_LATCHPOS     34     /* Slave-Latch-Position */
#define ncMA_LATCHPOS     32     /* Master-Latch-Position */
#define ncACP_PAR         401    /* ACOPOS-Parameter-Daten */
#define ncLADEN           0x0114 /* Laden */
#define ncPOLAR_PHI       8      /* CNC-Achse (Zirkulare Polarkoordinaten Bahnachse) */
#define ncPOLAR_RAD       4      /* CNC-Achse (Radiale Polarkoordinaten Bahnachse) */
#define ncOBJ_PTR         0x0040 /* NC-Objekt-Pointer */
#define ncAND_N2E         15     /* AND-Operation der n�chsten 2 Ereignisse */
#define ncCYCL_USER_FRDRV 0x0101 /* Zyklische Anwender-Daten vom Antrieb */
#define ncCYCL_USER_TODRV 0x0100 /* Zyklische Anwender-Daten zum Antrieb */
#define ncSL_ABS          33     /* Absolut auf Slave */
#define ncINK_AUSGABE     7      /* Inkrementalgeber-Ausgabe (Emulation) */
#define ncVIRTUELL_IF     0x0080 /* Virtuelles Interface */
#define ncHALT_RST        16     /* Halt nach Restart eines NC-Programmes */
#define ncHALT_PRG        2      /* Halt durch NC-Programm (M00 oder M01) */
#define ncHALT_POS        8      /* Halt nach Positionierung in einem NC-Programm */
#define ncHALT_ESZ        1      /* Halt durch Betriebsart "Einzelsatz" */
#define ncHALT_ERR        32     /* Halt nach Fehler */
#define ncHALT            219    /* Halt */
#define ncGRENZEN         2      /* Grenzwerte */
#define ncGRAY            0      /* Gray-Code */
#define ncGLOBAL          4      /* Global */
#define ncGETR_V          116    /* Geschwindigkeits-Getriebe */
#define ncGETR_ABS        124    /* Absolutes Getriebe */
#define ncGETRIEBE        111    /* Getriebe */
#define ncGEBER_IF        103    /* Geber-Interface */
#define ncG03             3      /* G03 */
#define ncG02             2      /* G02 */
#define ncG01             1      /* G01 */
#define ncKURVEPOL        25     /* Kurvenscheibe-Daten (Polynom) */
#define ncKURVEDAT        24     /* Kurvenscheibe-Daten (Tabelle) */
#define ncKURVE           117    /* Kurvenscheibe */
#define ncKOPPLUNG        0x0112 /* Kopplungs-Konfiguration */
#define ncKOMPAR          115    /* Komparator */
#define ncIST_POS         120    /* Ist-Positionen f�r Kopplung */
#define ncINVERS          255    /* Invers */
#define ncINK_KOMP        252    /* Inkrementalgeber und Komparatorfunktion */
#define ncINK             2      /* Inkrementalgeber */
#define ncINIT            0x0200 /* Initialisieren */
#define ncINFO            0x010C /* Info-Funktion */
#define ncINDIREKT        255    /* Indirekt */
#define ncHW_ENDL         0x1000 /* Hardware-Endlage */
#define ncHIGH            1      /* High */
#define ncHAUPTPRG        -1     /* Beginn des Hauptprogrammes */
#define ncHALT_SPS        4      /* Halt durch SPS-Programm */
#define ncMOD_SYNC        303    /* Modul-Synchronisation */
#define ncMODUL           3      /* NC-Modul */
#define ncMITKURVE        31     /* Mit Kurvenscheibe */
#define ncMEMCARD         4      /* MEM-Card */
#define ncMA_ZU_SL        0x4000 /* Wechsel "Master zu Slave" */
#define ncM1_HALT         203    /* Betriebsart "Halt bei M1" */
#define ncLOW             0      /* Low */
#define ncLOESCHEN        0x0156 /* L�schen */
#define ncLINKS           2      /* Links */
#define ncLINK            0x0157 /* Verbindung (Link) herstellen */
#define ncLINEAR          1      /* Lineare Mitschleppachse */
#define ncLESEN           0x0109 /* Lesen */
#define ncLEER            0      /* Leerzeichen */
#define ncLATCHPOS        32     /* Latch-Position */
#define ncLATCH2          109    /* Positionslatch 2 */
#define ncLATCH1          108    /* Positionslatch 1 */
#define ncOK              0      /* OK */
#define ncODD             2      /* Ungerade */
#define ncN_SCHW          4      /* Unter der Schwelle (negativ) */
#define ncN_PERIOD        1      /* N�chste Masterperiode */
#define ncN_FLANKE        1      /* Negative Flanke */
#define ncNURAUSGL        30     /* Nur Ausgleichsgetriebe */
#define ncNULL_PKT        21     /* Nullpunktverschiebe-Tabelle */
#define ncNULLPTAB        208    /* Nullpunktverschiebe-Tabelle */
#define ncNULLPMON        216    /* Modus "Nullpunktverschiebung von Positionen subtrahieren" f�r CNC-Monitor */
#define ncNULL            1      /* Null-Bytes */
#define ncNPFLANKE        11     /* Zwei-Flanken-Modus (negativ/positiv) */
#define ncNOTHALT         0x0106 /* Nothalt */
#define ncNEGATIV         1      /* Negativ */
#define ncNCSATZNR        22     /* NC-Satznummer */
#define ncNCPR_POS        218    /* Positionierung im NC-Programm */
#define ncMOD_VERZ        128    /* Modul-Verzeichnis */
#define ncREGLER          105    /* Regler */
#define ncREFFAHRT        106    /* Referenzfahrt */
#define ncRECHTS          3      /* Rechts */
#define ncRAM             1      /* RAM */
#define ncQUIT            0x0101 /* Quittieren */
#define ncP_SCHW          3      /* �ber der Schwelle (positiv) */
#define ncP_LATCH2        2      /* Positionslatch 2 */
#define ncP_LATCH1        1      /* Positionslatch 1 */
#define ncP_FLANKE        0      /* Positive Flanke */
#define ncPROGRAMM        201    /* NC-Programm */
#define ncPOSITIV         0      /* Positiv */
#define ncPOSFEHL         4      /* Positions-Fehler (Schleppfehler im Stillstand) */
#define ncPNFLANKE        10     /* Zwei-Flanken-Modus (positiv/negativ) */
#define ncPI_PRAED        0      /* Praediktiver PI-Regler */
#define ncONL_V           113    /* Online-Geschwindigkeitssteuerung */
#define ncONL_POS         107    /* Online-Positionierung */
#define ncSIGNAL2         92     /* Signal2 vom RPS-Programm */
#define ncSIGNAL1         91     /* Signal1 vom RPS-Programm */
#define ncSIGNAL          4      /* Signalleitung */
#define ncSICHERN         0x0155 /* Sichern */
#define ncSETZEN          0x0108 /* Setzen */
#define ncSCHWELLE        0      /* Schwelle */
#define ncSCHLFEHL        2      /* Schleppfehler */
#define ncSATZ            202    /* CNC-Satz */
#define ncR_PARTAB        210    /* R-Parameter-Tabelle */
#define ncR_PARAM         211    /* R-Parameter */
#define ncR_PAR           23     /* R-Parameter-Tabelle */
#define ncR_IMPULS        2      /* Referenzimpuls */
#define ncRSCH_TOR        1      /* Mit Referenzschaltertor */
#define ncRESTART         0x0111 /* Restart */
#define ncREL_POS         119    /* Relativ-Positionierung */
#define ncRELATIV         1      /* Relativ */
#define ncSW_ENDL         1      /* Software-Endlage */
#define ncSTART_IV        0x010E /* Mit Intervall starten */
#define ncSTART_AG        0x010D /* Mit Ausgleichsgetriebe starten */
#define ncSTARTDIR        0x010B /* Direkt starten */
#define ncSTARTABS        0x0110 /* Absolut starten */
#define ncSTART           0x0104 /* Starten */
#define ncSTANDARD        0      /* Standard */
#define ncSSI_SEND        3      /* SSI-Sender */
#define ncSPS_ITR         302    /* SPS-Interruptroutine f�r externe Kopplung */
#define ncSPOSNORM        160    /* Sollpositions-Normierung */
#define ncSOFORT          0      /* Sofort */
#define ncSKIP_FKT        205    /* Betriebsart "SKIP-Funktion" */
#define ncSIMULAT         150    /* Simulationsmodus */
#define ncSIGN_LTG        110    /* Signalleitung */
#define ncSIGNAL4         94     /* Signal4 vom RPS-Programm */
#define ncSIGNAL3         93     /* Signal3 vom RPS-Programm */
#define ncTR_END          1      /* Trace durch NC-Modul beendet */
#define ncTRIGGPOS        112    /* Triggerpositionierung */
#define ncTRIGGER3        24     /* Trigger3 */
#define ncTRIGGER2        22     /* Trigger2 */
#define ncTRIGGER1        20     /* Trigger1 */
#define ncTRIGGER         0      /* Aktuelle Achse: Trigger(1) */
#define ncTRGPOS_S        118    /* Triggerpositionierung mit Zielposition */
#define ncTRACE           304    /* Trace */
#define ncTEXT            0x010F /* Text */
#define ncTANGENT         2      /* Tangentiale Mitschleppachse */
#define ncS_START         10     /* Startposition der Masterachse */
#define ncS_SOLL          10     /* Sollposition */
#define ncS_REST          0x1000 /* Modus "+ s_rest" */
#define ncS_NCPROG        21     /* Bahnposition */
#define ncS_IST           11     /* Istposition */
#define ncSYNC            0x010A /* Synchronisation */
#define ncWAHR            1      /* Wahr */
#define ncV_SOLL          12     /* Sollgeschwindigkeit */
#define ncV_KONST         2      /* Konstante Geschwindigkeit */
#define ncV_IST           14     /* Istgeschwindigkeit */
#define ncV_BAHN          20     /* Bahngeschwindigkeit */
#define ncV_ACHSE         4      /* Virtuelle Achse */
#define ncU_SOLL          13     /* Sollspannung */
#define ncUPLOAD          0x0152 /* Upload */
#define ncUNLINK          0x0158 /* Verbindung (Link) beenden */
#define ncUMBRUCH         2      /* Wort-Umbruch */
#define ncTR_VERZ         6      /* Warten auf Ende der Startverz�gerung */
#define ncTR_TRIGG        3      /* Warten auf Starttrigger */
#define ncTR_TRACE        2      /* Aufzeichnen bis Ende der Aufzeichnungsdauer */
#define ncTR_START        20     /* Trace durch Anwenderprogramm gestartet */
#define ncTR_RING         4      /* Aufzeichnen in Ringpuffer */
#define ncTR_REST         5      /* Aufzeichnen bis Ende der Restzeit */
#define ncKURVEANF        6      /* Beginn einer Kurvenscheibe */
#define ncBEW_CMD         5      /* Bewegungskommando */
#define ncV_SPR_T         4      /* Halt bei �berschreitung von "v_sprung_t" */
#define ncV_SPRUNG        1      /* Halt bei �berschreitung von "v_sprung" */
#define ncZ_KURVE         2      /* Kurvenscheibe des Zustands */
#define ncZ_ENDE          12     /* Ende des Zustands */
#define ncZ_AUSGL         4      /* Ausgleichsgetriebe des Zustands */
#define ncZX              1      /* XZ-Ebene */
#define ncZUSTAND         128    /* Zustand */
#define ncYZ              2      /* YZ-Ebene */
#define ncXY              0      /* XY-Ebene */
#define ncWRK             22     /* Werkzeugdaten-Tabelle */
#define ncWERKZTAB        209    /* Werkzeugdaten-Tabelle */
#define ncABS_BEW         132    /* Bewegung mit absoluter Zielposition */
#define ncREL_BEW         131    /* Bewegung mit relativer Verfahrdistanz */
#define ncU_GRENZ         40     /* An der Spannungsgrenze */
#define ncI_GRENZ         30     /* An der Stromgrenze */
#define ncA_GRENZ         20     /* Mit der in den Achsgrenzwerten definierten Verz�gerung */
#define ncT_RUCK          1      /* Und der eingestellten Ruckzeit */
#define ncA_BEW           10     /* Mit der in der aktuellen Bewegung definierten Verz�gerung */
#define ncEND_SCHALTER    4      /* Mit HW-Endschalter */
#define ncSCHALTER_TOR    1      /* Mit Referenzschaltertor */
#define ncABS_SCHALTER    2      /* Mit absolutem Referenzschalter */
#define ncFORCE           0x0120 /* Force-Funktion */
#define ncENDAT           4      /* Endat */
#define ncREFERENZ        106    /* Referenzieren */
#define ncBASIS_BEW       130    /* Basisbewegung */
#define ncHALT_OK         135    /* Bewegung ist angehalten */
#define ncPOS_BEW         134    /* Bewegung in positiver Richtung */
#define ncNEG_BEW         133    /* Bewegung in negativer Richtung */
#define ncTRIGGER4        26     /* Trigger4 */
#define ncTRIGGER6        30     /* Trigger6 */
#define ncTRIGGER5        28     /* Trigger5 */
#define ncGEBER_IF        103    /* Geber-Interface */
#define ncQUITTIEREN      0x0101 /* Quittieren */
#define ncFORTSETZEN      0x0107 /* Fortsetzen */
#define ncSIMULATION      150    /* Simulationsmodus */
#define ncAUSSCHALTEN     0x0103 /* Ausschalten */
#define ncEINSCHALTEN     0x0102 /* Einschalten */
#define ncRESET           0x0140 /* Reset */
#define ncSERVICE         165    /* Service-Schnittstelle */
#define ncNEG_SCHWELLE    2      /* Mit Schwelle in negativer Richtung */
#define ncPOS_SCHWELLE    1      /* Bereich "Mit Schwelle in positiver Richtung" */
#define ncUNTER_FENSTER   50     /* Bereich "Unter dem Fenster" */
#define ncUEBER_FENSTER   40     /* Bereich "�ber dem Fenster" */
#define ncAUS_FENSTER     30     /* Bereich "Au�erhalb des Fensters" */
#define ncIN_FENSTER      20     /* Innerhalb des Fensters */
#define ncS_MOTOR         1      /* Schrittmotor */
#define ncMASZSTAB        180    /* Ma�stab */
#define ncFREIGABE        170    /* Freigabe */
#define ncD_POS           13     /* Delta-Position */
#define ncBREMSE          171    /* Bremse */
#define ncSERCOSIF        310    /* SERCOS-Interface */
#define ncSERCOS          2      /* SERCOS */
#define ncEXTGEBER        8      /* Externer Geber */
#define ncDIAGNOSE        0x0141 /* Diagnose-INFO ermitteln */
#define ncENGLISCH        1      /* Englisch */
#define ncDEUTSCH         0      /* Deutsch */
#define ncANTRIEB         166    /* Antrieb */
#define ncACP10MAN        0x8000 /* ACP10-Manager */
#define ncREGLER          105    /* Regler */
#define ncTRIGGER8        34     /* Trigger8 */
#define ncTRIGGER7        32     /* Trigger7 */
#define ncANTR_ID         123    /* Antriebs-Identifizierung */
#define ncANALOG          0      /* Analog */
#define ncAKTIV_LO        0      /* Aktiv low */
#define ncAKTIV_HI        1      /* Aktiv high */
#define ncAKTIV           1      /* Aktiv */
#define ncAGL_FEHL        14     /* Ausgleichsgetriebe-Fehler */
#define ncACHSEN          213    /* Achseinstellungen */
#define ncACHSE           1      /* Achse */
#define ncABS_RSCH        2      /* Mit absolutem Referenzschalter */
#define ncABSOLUT         0      /* Absolut */
#define ncABS             1      /* Absolut */
#define ncABBRUCH         0x0105 /* Abbrechen */
#define nc158CAN          7      /* CAN f�r NC158 */
#define nc156CAN          6      /* CAN f�r NC156 */
#define nc154CAN          5      /* CAN f�r NC154 */
#define BASIS2005         1      /* System 2005 Basisrack */


/*** DATA TYPES *************************************************************/ 

#ifndef NC15xMAN_GLOBAL_TYP

#define NC15xMAN_GLOBAL_TYP

/*** NCDA_NPTAB_typ ("ncda_nptab" in PL2000) ***/

typedef struct NCDA_NPSZ_typ {
  REAL           offset[3];
} NCDA_NPSZ_typ;

typedef struct NCDA_NPTAB_typ {
  UINT           anz_saetze;
  UINT           startindex;
  NCDA_NPSZ_typ  satz[200];
} NCDA_NPTAB_typ;


/*** NCDA_RPTAB_typ ("ncda_rptab" in PL2000) ***/

typedef struct NCDA_RPTAB_typ {
  UINT           anz_r_par;
  UINT           startindex;
  REAL           r_par[1000];
} NCDA_RPTAB_typ;


/*** NCDA_WZTAB_typ ("ncda_wztab" in PL2000) ***/

typedef struct NCDA_WZSZ_typ {
  REAL           laenge;
  REAL           radius;
  REAL           offset[3];
} NCDA_WZSZ_typ;

typedef struct NCDA_WZTAB_typ {
  UINT           anz_saetze;
  UINT           startindex;
  NCDA_WZSZ_typ  satz[255];
} NCDA_WZTAB_typ;


/*** NCDA_MODVZ_typ ("ncda_modvz" in PL2000) ***/

typedef struct NCDA_DATMO_typ {
  USINT          name[12];
  USINT          typ;
  USINT          mem_typ;
} NCDA_DATMO_typ;

typedef struct NCDA_MODVZ_typ {
  UINT           anz_datmod;
  UDINT          ram_frei;
  UDINT          prom_frei;
  NCDA_DATMO_typ datenmodul[100];
} NCDA_MODVZ_typ;


/*** DM455NSWTR_typ ("dm455nswtr" in PL2000) ***/

typedef struct DM455NSWTR_typ {
  USINT          copy_itr;
  USINT          schritt;
  UDINT          anz_bytes;
  UDINT          quell_adr;
  UDINT          ziel_adr;
} DM455NSWTR_typ;


/*** NCIRQNPINP_typ ("ncirqnpinp" in PL2000) ***/

typedef struct NCIRQNPINP_typ {
  UDINT          nc_object;
  UINT           subject;
} NCIRQNPINP_typ;

#endif NC15xMAN_GLOBAL_TYP

#endif /* NCGLOBAL_H_ */ 

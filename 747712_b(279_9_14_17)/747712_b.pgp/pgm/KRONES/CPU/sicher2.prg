PROGRAM sicher2 5 (* Ladder Diagram *)
	CODE_INIT
		<?xml version="1.0"?>
		<?ladder Version = 1.0?>
		<!-- Automation Studio Generated XML Section -->
		<networks xmlns:dt="urn:schemas-microsoft-com:datatypes" maxcolumn="2">
			<network label="" comment="Variablen mit '0' initialisieren - ausser Si-Kreis Fehler!" row="17" column="2">
				<row><contact type="open" name="True"/><coil type="reset" name="NA_SU_Freigabe"/></row>
				<row><empty or="true"/><coil type="reset" name="NA_SU_Freigabe_unverz"/></row>
				<row><empty or="true"/><coil type="reset" name="NA_SU_Freigabe_Verkl"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[48]"/></row>
				<row><empty or="true"/><coil type="reset" name="Fehler_Zeitrelais"/></row>
				<row><empty or="true"/><coil type="reset" name="SU_Geoeffnet"/></row>
				<row><empty or="true"/><coil type="reset" name="HM_KL_Quit_Allg"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[6]"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[7]"/></row>
				<row><empty or="true"/><coil type="reset" name="SU_Geoeffn_Vk1"/></row>
				<row><empty or="true"/><coil type="reset" name="SU_Geoeffn_Vk2"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[39] "/></row>
				<row><empty or="true"/><coil type="reset" name="Netzschuetz_Fehlerhaft"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[27]"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[28]"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[29]"/></row>
				<row><empty or="true"/><coil type="reset" name="HM_KL_Quit_Si"/></row>
			</network>
		</networks>
	END_CODE_INIT
	CODE_CYCLIC
		<?xml version="1.0"?>
		<?ladder Version = 1.0?>
		<!-- Automation Studio Generated XML Section -->
		<networks xmlns:dt="urn:schemas-microsoft-com:datatypes" maxcolumn="5">
			<network label="" comment="Änderungen: 
		****************	
		Datum    	Vers.  	Bemerkung                               										Name
		
		28.05.03		2.57	Notausüberwachung (Plausibilitätskontrolle) geändert (Anordnung der Eingänge und Zeit)			SNH
		02.07.03		2.61	Zeiten für Notaus-/Schutzüberwachung verlängert						PUA
		05.08.03		2.63	Globale Variable APS2_Verklebung_vorhanden benutzt, lokale Variable aps2_verklebung_vorhanden	SNH 
					entfernt.
		13.08.03		2.66	Notausüberwachung (Plausibilitätskontrolle) entfernt.						SNH
		 " row="1" column="2">
				<row><contact type="open" name="TRUE"/><line/><line/><line/><coil type="open" name="Versionsgeschichte"/></row>
			</network>
			<network label="" comment="Si_Kreis_Modul_Etima = 0 --&gt; Baustein überspringen" row="1" column="2">
				<row><contact type="closed" name="Si_Kreis_Modul_Etima"/><line/><line/><line/><coil type="jump" name="Ende"/></row>
			</network>
			<network label="" comment="Freigabe Notaus/Schutz ZEITVERZÖGERT
		I_NA_Aggr = zeitverzögertes NA Signal von der ETIMA" row="1" column="2">
				<row><contact type="open" name="I_NA_Aggr"/><line/><line/><line/><coil type="open" name="NA_SU_Freigabe"/></row>
			</network>
			<network label="" comment="Freigabe Notaus/Schutz UNVERZÖGERT
		I_NA_SU_Etima = unverzögertes NA Signal von der ETIMA 
		Die NA/SU Freigabe für die Verklebungen muss unverzögert erfolgen." row="2" column="2">
				<row><contact type="open" name="I_NA_SU_Etima_betaetigt"/><line/><line/><line/><coil type="open" name="NA_SU_Freigabe_unverz"/></row>
				<row><empty/><empty/><empty/><empty or="true"/><coil type="open" name="NA_SU_Freigabe_Verkl"/></row>
			</network>
			<network label="" comment="Anzeige Notaus von Etima" row="2" column="2">
				<row><contact type="closed" name="I_NA_Aggr"/><line/><line/><line/><coil type="open" name="Meldung[48]"/></row>
				<row><contact type="closed" name="I_NA_SU_Etima_betaetigt"/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="1.) Notaus-, Schutzkette in dieser Konstellation nicht vorhanden. Für die Funktion der aggregat.src, dauerhaft auf 1 = i.O.
		2.) Fehler Zeitrelais in dieser Konstellation nicht vorhanden. Für die Funktion der aggregat.src, dauerhaft auf 0 = i.O." row="2" column="2">
				<row><contact type="open" name="True"/><line/><line/><line/><coil type="open" name="NA_SU_Aggr"/></row>
				<row><empty/><empty/><empty/><empty or="true"/><coil type="negated" name="Fehler_Zeitrelais"/></row>
			</network>
			<network label="" comment="Schutzdeckel geoeffent" row="2" column="4">
				<row><contact type="open" name="I_SU_Aggr"/><line/><line/><line/><coil type="open" name="SU_Geoeffnet"/></row>
				<row><contact type="open" name="SU_Geoeffnet"/><contact type="closed" name="I_MMI_Quit_Allg_Taste"/><contact type="closed" name="I_Bus_Quit_Allg"/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="HM-KL Quittieren Allgemein" row="1" column="3">
				<row><contact type="closed" name="I_SU_Aggr"/><contact type="open" name="SU_Geoeffnet"/><line/><line/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Anzeige Schutztuer offen" row="1" column="3">
				<row><contact type="open" name="I_SU_Aggr"/><contact type="open" name="SU_Geoeffnet"/><line/><line/><coil type="open" name="Meldung[6]"/></row>
			</network>
			<network label="" comment="Anzeige Schutztuer war offen" row="1" column="3">
				<row><contact type="closed" name="I_SU_Aggr"/><contact type="open" name="SU_Geoeffnet"/><line/><line/><coil type="open" name="Meldung[7]"/></row>
			</network>
			<network label="" comment="Schutzdeckel an der APS1 Verklebung geoeffnet" row="2" column="5">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="EQ"><input name="IN1"/><output name=""/></function><contact type="open" name="I_SU_Verklebung"/><coil type="open" name="SU_Geoeffn_Vk1"/></row>
				<row><analog type="input" name="2"/><function position="bottom"><input name="IN2"/></function><empty/><empty/></row>
			</network>
			<network label="" comment="Schutzdeckel an der APS2 Verklebung geoeffnet" row="2" column="5">
				<row><contact type="open" name="APS2_Verklebung_vorhanden"/><contact type="open" name="I_SU_Verkl_2"/><line/><line/><coil type="open" name="SU_Geoeffn_Vk2"/></row>
				<row><empty or="true"/><contact type="open" name="SU_Geoeffn_Vk2"/><contact type="closed" name="I_MMI_Quit_Allg_Taste"/><contact type="closed" name="I_Bus_Quit_Allg" or="true"/><empty/></row>
			</network>
			<network label="" comment="Meldung Schutz an Verklebung 1 oder 2 geoeffnet" row="2" column="2">
				<row><contact type="open" name="SU_Geoeffn_Vk1"/><line/><line/><line/><coil type="open" name="Meldung[39] "/></row>
				<row><contact type="open" name="SU_Geoeffn_Vk2"/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="HM-KL Quittieren Allgemein" row="1" column="3">
				<row><contact type="closed" name="I_SU_Verkl_2"/><contact type="open" name="SU_Geoeffn_Vk2"/><line/><line/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Fehlerhafte Zustaende Netzschuetz
		1. Fall: Netzschütz angesteuert aber abgefallen
		2. Fall: Netzschütz nicht angesteuert aber angezogen" row="2" column="3">
				<row><contact type="open" name="O_Netz_Aggr"/><contact type="open" name="I_Rueck_Netz_Aggr"/><line/><line/><coil type="open" name="Netzschuetz_Fehlerhaft"/></row>
				<row><contact type="closed" name="O_Netz_Aggr"/><contact type="closed" name="I_Rueck_Netz_Aggr"/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Kontaktverzoegerung Netzschuetzueberwachung" row="4" column="5">
				<row><empty/><functionblock position="header" name="tON_Netz_laeuft"/><empty/><empty/></row>
				<row><contact type="open" name="Netzschuetz_Fehlerhaft"/><functionblock position="top" type="TON"><input name="IN"/><output name="Q"/></functionblock><line/><coil type="open" name="Fehler_Netzschuetz"/></row>
				<row><analog type="input" name="T#1.5s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty or="true"/><coil type="open" name="Meldung[27]"/></row>
				<row><contact type="open" name="Fehler_Netzschuetz"/><contact type="closed" name="I_MMI_Quit_Allg_Taste"/><contact type="closed" name="I_Bus_Quit_Allg"/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Netzschuetz  -----  Netzschuetz  angesteuert Rückmeldung abgefallen" row="1" column="4">
				<row><contact type="open" name="Fehler_Netzschuetz"/><contact type="open" name="O_Netz_Aggr"/><contact type="open" name="I_Rueck_Netz_Aggr"/><line/><coil type="set" name="Meldung[28]"/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Netzschuetz  ------- Netzschuetz  nicht angesteuert Rückmeldung angesteuert" row="1" column="4">
				<row><contact type="open" name="Fehler_Netzschuetz"/><contact type="closed" name="O_Netz_Aggr"/><contact type="closed" name="I_Rueck_Netz_Aggr"/><line/><coil type="set" name="Meldung[29]"/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Netzschuetz ----- Quit" row="2" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><coil type="reset" name="Meldung[28]"/></row>
				<row><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Meldung[29]"/></row>
			</network>
			<network label="" comment="HM Anzeige Quittieren Sicherheitskreis" row="1" column="3">
				<row><contact type="closed" name="tON_Netz_laeuft.Q"/><contact type="open" name="Fehler_Netzschuetz"/><line/><line/><coil type="set" name="HM_KL_Quit_Si"/></row>
			</network>
			<network label="" comment="KL Quittieren Sicherheitskreis" row="3" column="2">
				<row><contact type="open" name="HM_KL_Quit_Si"/><line/><line/><line/><coil type="open" name="HM_LED_Quit_Si_Kreis"/></row>
				<row><contact type="open" name="Fehler_Netzschuetz"/><line/><line/><line or="true"/><coil type="reset" name="HM_KL_Quit_Si"/></row>
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="Ende" comment="" row="1" column="2">
				<row><contact type="open" name="sicher2_ende"/><line/><line/><line/><coil type="open" name="sicher2_ende"/></row>
			</network>
		</networks>
	END_CODE_CYCLIC
END_PROGRAM

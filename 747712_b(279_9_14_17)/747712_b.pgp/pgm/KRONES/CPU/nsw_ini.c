/************************************************************************************/
/*	Programm zur	   																*/
/*	Initialisierung und zum Einschalten des NSW     								*/
/*																					*/
/* �nderungen:																		*/
/* ***********																		*/	
/* Datum     Vers.  Bemerkung                                                Name	*/
/* 																					*/	
/* 01.01.00  00.00  neu erstellt                                             PET	*/
/* 13.02.01  00.01  Startposition auf die Mitte des Masterausgleiches		 SNH/PET*/
/* 04.05.01	 01.28	Zus�tzl. NSW Spuren f�r Schalten w�hrend der Sync.phase	 SNH/PET*/
/* 05.07.01	 01.37	Laserdatierung waehrend Stillstand (Dat.Typ 2)			 SNH/PET*/
/* 07.11.01	 01.54	Dat.Typ 3/4/5											 SNH/PET*/
/************************************************************************************/

#include <bur/plc.h>				/* Definitionen f�r RPS Programmierung	*/
#include <bur/plctypes.h>			/* Definitionen f�r RPS Programmierung	*/
#include <string.h>                 /* String-Funktionen */

#include "acp10man.h"  				/* Funktionen, Datentypen und 			*/
									/* Konstanten f�r ACP10MAN.BR 			*/
#include "global.h"
#include "slave_ax.h"

/* Defines */
#define ACP10_NODE         1   /* Knotennummer des Antriebes */

#define	MAX_SPUREN	4
#define	MAX_NOCKEN	5

#define W_NSW					0
#define	IO_CONFIG				2
#define	W_IO_CONFIG				3
#define NSW_PARAMETER			5
#define NSW_INIT_ALLGEMEIN		10
#define W_NSW_INIT_ALLGEMEIN	30
#define NSW_INIT_SPUR			40
#define	W_NSW_INIT_SPUR			50
#define	NSW_INIT_NOCKE			60
#define	W_NSW_INIT_NOCKE		70
#define	NSW_INIT_ENDE			80
#define	NSW_START				90
#define	W_NSW_START				100

/* Datentypen f�r Nockenschaltwerk */
typedef struct {
	DINT	ein;
	DINT	aus;
} NOCKE_typ;

typedef struct {
	UINT		do_kanal;
	UINT		spur_enable_id;
	REAL		do_verz_zeit;
	NOCKE_typ	nocke[10];
} SPUR_typ;

typedef struct {
	UINT		master;
	DINT		ma_s_start;
	DINT		intervall;
	SPUR_typ	spur[4];
} NSW_typ;


/* lokale Variablen f�r NSW */
_LOCAL  ACP10ACHSE_typ 	*p_ax_dat;      /* Pointer auf die Achs-Objekt-Daten */ 

_LOCAL  UDINT          	ax_obj;         /* Achs-Objekt (NC-Objekt-Pointer) */

_LOCAL	UINT           	alloc_status;   /* Status von ncalloc() */
_LOCAL	UINT           	action_status;  /* Status von ncaction() */
_LOCAL	UINT           	dattyp_error;   /* Datentypfehler (Gr��e des NC-Datentyps in "acp10man.h") */
_LOCAL	UINT           	version_error;  /* Versionsfehler (Version aus "acp10man.h") */
_LOCAL	plcbit			quittieren;
_LOCAL	UINT			kommando;

_LOCAL	UI4_TYP			dio_config;
_LOCAL	NSW_typ			nsw;
_LOCAL	UINT			nsw_init_par_nr;
_LOCAL	UINT			spur_init_par_nr;
_LOCAL	UINT			nocke_init_step;
_LOCAL	UINT			init_zaehl_spur;
_LOCAL	UINT			init_zaehl_nocke;
_LOCAL	plcbit			keine_AC130_karte_vorhanden;
_LOCAL	DINT			pos_datierung_ein;		/* NSW Spur 0 Ein - Kompensationsphase */
_LOCAL	DINT			pos_datierung_aus;		/* NSW Spur 0 Aus - Kompensationsphase */
_LOCAL	DINT			Laenge_Maschinenteilung_ber;
_LOCAL 	DINT			ma_s_sync;
_LOCAL 	DINT			ma_s_comp; 
_LOCAL	DINT			pos_sync1_ein;			/* NSW Spur 1 Ein - Synchronphase */
_LOCAL	DINT			pos_sync1_aus;			/* NSW Spur 1 Aus - Synchronphase */
_LOCAL	DINT			pos_sync2_ein;			/* NSW Spur 2 Ein - Synchronphase */
_LOCAL	DINT			pos_sync2_aus;			/* NSW Spur 2 Aus - Synchronphase */
_LOCAL	DINT			pos_sync3_ein;			/* NSW Spur 3 Ein - Synchronphase */
_LOCAL	DINT			pos_sync3_aus;			/* NSW Spur 3 Aus - Synchronphase */

_LOCAL	UINT			step_alt;				/* Letzter Schritt im Schrittschaltwerk */ 
_LOCAL	UINT			step_debug_lfd_nr;		/* Aktueller Zaehler im Schrittfeld */
_LOCAL	UINT			step_debug[100];		/* Feld mit den letzten 40 Schritten */ 


/****************************************************************************/
/*		Initialisierungs-UP													*/
/****************************************************************************/
void _INIT ax_init(void)
{
    alloc_status = ncalloc(ncACP10MAN,ACP10_NODE,ncACHSE,1,(UDINT)&ax_obj);

    if ( ( alloc_status != ncOK ) && ( alloc_status != 10600 ) )
    {
    	/* Der NC-Objekt-Pointer ist ung�ltig */
		return;
	}

	p_ax_dat = (ACP10ACHSE_typ*)ax_obj;

    if ( p_ax_dat->size != sizeof(ACP10ACHSE_typ) )
    {
    	/* Der NC-Datentyp ist nicht kompatibel zum NC-Manager */
		dattyp_error = sizeof(ACP10ACHSE_typ);
		return;
	}
	else
	{
		dattyp_error = 0;
	}
		
    if ( ( p_ax_dat->sw_version.nc_manager&0xFFF0 ) != ( ACP10MAN_H_VERSION&0xFFF0 ) )
    {
    	/* Die Version ist nicht kompatibel zum NC-Manager */
		version_error = ACP10MAN_H_VERSION;
		return;
	}
	else
	{ 
		version_error = 0;
	}

	dio_config.id = ACP10PAR_DIO_CONFIG;
	
	nsw_init_par_nr = 0;
	spur_init_par_nr = 0;
	nocke_init_step = 0;
	init_zaehl_spur = 1;
	init_zaehl_nocke = 1;

	nsw_init = 0;
	nsw_start = 0;	
	
    step_nsw_init = W_NSW;

	keine_AC130_karte_vorhanden = 0;
}

void _CYCLIC ax_cyclic(void)
{
	if ( dattyp_error || version_error )
	{
    	/* "acp10man.h" ist nicht kompatibel zum NC-Manager */
		return;
	}
    if ( alloc_status != ncOK )
	{
        if ( alloc_status == 10600 )
		{
        	/* Wenn die Funktion ncalloc() den Wert 10600 liefert, dann ist das */
			/* Ergebnis in "nc_object" g�ltig und in den Fehlers�tzen des NC-Objekts */
        	/* wurden zus�tzliche Fehler eingetragen. Die einzig erlaubte NC-Aktion ist */
        	/* nun "ncaction(nc_object,ncMELDUNG,ncQUITTIEREN)" (zum Lesen der Meldungen). */
			if ( quittieren == 1 )
			{
            	/* Aktuellen Meldungssatz quittieren */
				if ( ( action_status = ncaction(ax_obj,ncMELDUNG,ncQUITTIEREN) ) == ncOK )
				{
					quittieren = 0;
				}
			}
		}
		return;
	}


	/************************************************************************************/
	/* Sequenz zur �bergabe von Befehlen an das NSW		 								*/
	/************************************************************************************/
	switch ( step_nsw_init )
	{
		case W_NSW:
			if (nsw_io_config == 1)
			{
	        	step_nsw_init = IO_CONFIG;
			}
		
			if (nsw_init == 1)
			{
	        	step_nsw_init = NSW_PARAMETER;
			}
				
			if (nsw_start == 1)
			{
				step_nsw_init = NSW_START;
			}
			break;

		case IO_CONFIG:

			/* ENDAT-Geber und kein NSW, da keine Datierung keine Walzenverriegelung */
			if ( (Akt_Maschinen_Param.Maschine_Gebertyp == 0) && (Akt_Produkt_Param.Typ_Datierung == 0) )
			{
				nsw_io_config = 0;
				keine_AC130_karte_vorhanden = 1;
				step_nsw_init = W_NSW;
				break;
			}


			if (Akt_Maschinen_Param.Maschine_Gebertyp == 1)	/* Inkrementalgeber */
			{
				dio_config.data = 0x000003F0;	/* 4 DIs, 6 DOs, Karte in Steckplatz 3 */
			}
			else if (Akt_Maschinen_Param.Maschine_Gebertyp == 0)	/* ENDAT-Geber */
			{
				dio_config.data = 0x03F00000;	 /* 4 DIs, 6 DOs, Karte in Steckplatz 4 */
			}

			p_ax_dat->netzwerk.service.request.par_id = dio_config.id;
   			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&dio_config.data;

		    if ((action_status=ncaction(ax_obj,ncSERVICE,ncSETZEN))==ncOK)
		    {
		    	step_nsw_init = W_IO_CONFIG;
		    }
	    	break;
				    			
	    case W_IO_CONFIG:
	    	if (p_ax_dat->netzwerk.service.response.par_id == 
	        	p_ax_dat->netzwerk.service.request.par_id)
	        {
	      		/* Operation erfolgreich abgeschlossen */
      			nsw_io_config = 0;
				step_nsw_init = W_NSW;
	        }
	        break;
			
				
		/* Parameter initialisieren */		
		case NSW_PARAMETER:
		
			/* L�nge einer Maschinenteilung an der Flaschenauskante aus den Parametern berechnen
							                                2 * Pi * ( Entfernung_SK_Tellermitte + Maschinenradius )
			  Laenge_einer_Teilung_an_der_Aussenkante   =  ----------------------------------------------------------
			 														        Anzahl_Teilung 
			*/
			
			/* L�nge einer Maschinenteilung an der Flaschenaussenkante aus den Parametern berechnen */
			if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen > 1) /* Rundl�ufer */
			{
				Laenge_Maschinenteilung_ber = ( (  Akt_Produkt_Param.SK_Tellermitte + ( Akt_Maschinen_Param.Laenge_Maschinenteilung_allg *  Akt_Maschinen_Param.Anzahl_Maschinenteilungen / 6.2831853) ) * 6.2831853) / Akt_Maschinen_Param.Anzahl_Maschinenteilungen;
			}
			if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen == 1) /* Geradl�ufer */
			{
				Laenge_Maschinenteilung_ber = Akt_Maschinen_Param.Laenge_Maschinenteilung_allg;
			}

			nsw.master 		= ncEXTGEBER;
			nsw.ma_s_start	= ma_s_start_NSW; /* Startposition aus slave_ax.c */
			nsw.intervall 	= Laenge_Maschinenteilung_ber;
			
			/****************************************************************************/
			/* NSW Spur 0 f�r Schaltsignal waehrend der Kompenastionsphase = Stillstand */
			/****************************************************************************/
			
			if (Akt_Maschinen_Param.Maschine_Gebertyp == 1)	/* Inkrementalgeber */
			{
				nsw.spur[0].do_kanal = 5; 		/* IO-Karte im Steckplatz 3  - 5.dig. E/A */
			}
			else if (Akt_Maschinen_Param.Maschine_Gebertyp == 0)	/* ENDAT-Geber */
			{
				nsw.spur[0].do_kanal = 21;		/* IO-Karte im Steckplatz 4  - 5.dig. E/A */
			}
			
			
			/* mit der "spur_enable_id" wird eine zus�tzliche UND-Verkn�pfung zum Schalten des Ausganges eingef�gt */
			nsw.spur[0].spur_enable_id	= ACP10PAR_CAM_DRUMSEQ_ENABLE;
			
			/* Bei Datierungstyp "1" wirkt der Parameter "Akt_Produkt_Param.Startposition_NSW" als Totzeitkompensation*/
			
			if (Akt_Produkt_Param.Typ_Datierung == 1)	/* Heisspraegedatierung */
			{
				nsw.spur[0].do_verz_zeit = ((float)Akt_Produkt_Param.Startposition_NSW) / 1000.0;
			}
			else
			{
				nsw.spur[0].do_verz_zeit 	= 0.0;
			}
			
			ma_s_sync = (Akt_Produkt_Param.Laenge_Slave_Intervall - Akt_Produkt_Param.Laenge_Comp_weg_Slave) * Akt_Produkt_Param.Faktor_Synchronweg_Master / 100; 	/* Mastersynchronweg bezogen auf den Maschinengeber (Master) */
			ma_s_comp = Laenge_Maschinenteilung_ber - ma_s_sync; /* Ausgleichsweg Master = Maschinenteilung - Mastersynchronweg */


			if (Akt_Produkt_Param.Typ_Datierung == 1)	/* Heisspraegedatierung */
			{

				/* Die Datierung wird eingeschaltet bei "Start_Pos_Synchron" + "pos_datierung_ein" 		*/
				/* Die "Start_Pos_Synchron" ist in "nsw.ma_s_start	= ma_s_start_neu" enthalten 		*/
				/* Bei (0.45 * Masterausgleich) einschalten, bei (0.55 * Masterausgleich) ausschalten 	*/

				pos_datierung_ein = ma_s_sync + (0.45 * ma_s_comp);

				pos_datierung_aus = ma_s_sync + (0.55 * ma_s_comp);
			}
			
			else if (Akt_Produkt_Param.Typ_Datierung == 2)	/* Laserstillstandsdatierung */
			{
			
				/* Einschalten: 0-90% des Ausgleichsweges (ma_s_comp)                                               		*/
				/* Ausschalten: Einschaltpunkt + 40% des Ausgleichsweges 		                                            */
				/* Achtung: Ist der Einschaltpunkt > 60 %, so liegt der Ausschaltpunkt schon im n�chsten Synchronintervall.	*/

				pos_datierung_ein = ma_s_sync + (Akt_Produkt_Param.Startposition_NSW / 100.0 * ma_s_comp);
				pos_datierung_aus = ma_s_sync + ((Akt_Produkt_Param.Startposition_NSW + 40) / 100.0 * ma_s_comp);
			}
			else if (Akt_Produkt_Param.Typ_Datierung == 3)	/* Laserdatierung */
			{

				/* Einschalten: 0-90% des Etikettenintervalls (Synchronweg + Ausgleichsweges, ma_s_sync + ma_s_comp)                                               		*/
				/* Ausschalten: Einschaltpunkt + 10% des Ausgleichsweges 		                                            */
				/* Achtung: Ist der Einschaltpunkt > 90 %, so liegt der Ausschaltpunkt schon im n�chsten Synchronintervall.	*/
			
				pos_datierung_ein = (ma_s_sync + ma_s_comp) * Akt_Produkt_Param.Startposition_NSW / 100.0;
				pos_datierung_aus = (ma_s_sync + ma_s_comp) * (Akt_Produkt_Param.Startposition_NSW + 10) / 100.0;
 			}
								
			else if (Akt_Produkt_Param.Typ_Datierung == 4)	/* Heisspraegedatierung - Kurvenscheibe mit Stillstand */
			{

				/* Die Datierung wird eingeschaltet bei "Start_Pos_Synchron" + "pos_datierung_ein" 		*/
				/* Die "Start_Pos_Synchron" ist in "nsw.ma_s_start	= ma_s_start_neu" enthalten 		*/
				/* Bei (0.45 * Masterausgleich) einschalten, bei (0.55 * Masterausgleich) ausschalten 	*/

				pos_datierung_ein = ma_s_sync + (0.45 * ma_s_comp);

				pos_datierung_aus = ma_s_sync + (0.55 * ma_s_comp);
			}
			
			else if (Akt_Produkt_Param.Typ_Datierung == 5)	/* Laserstillstandsdatierung  - Kurvenscheibe mit Stillstand */
			{
			
				/* Einschalten: 0-90% des Ausgleichsweges (ma_s_comp)                                               		*/
				/* Ausschalten: Am Ende des Etikettenintervalls		                                            */
				/* Achtung: Hier wird abgepr�ft, ob der Einschaltpunkt <90% des Ausgleichsweges ist, falls nicht wird der	*/
				/* Einschaltpunkt auf 90% des Ausgleichsweges begrenzt */
			
				if (Akt_Produkt_Param.Startposition_NSW < 90)
				{
					pos_datierung_ein = ma_s_sync + (Akt_Produkt_Param.Startposition_NSW / 100.0 * ma_s_comp);
				}
				else
				{
					pos_datierung_ein = ma_s_sync + (90 / 100.0 * ma_s_comp);
				}		
				pos_datierung_aus = ma_s_sync + ma_s_comp;

			}
									
			if (pos_datierung_ein <= Laenge_Maschinenteilung_ber)
			{
				nsw.spur[0].nocke[0].ein = pos_datierung_ein;
			}
			else
			{
				nsw.spur[0].nocke[0].ein = pos_datierung_ein - Laenge_Maschinenteilung_ber;
			}
			
			if (pos_datierung_aus <= Laenge_Maschinenteilung_ber)
			{
				nsw.spur[0].nocke[0].aus = pos_datierung_aus;
			}
			else
			{
				nsw.spur[0].nocke[0].aus = pos_datierung_aus - Laenge_Maschinenteilung_ber;
			}

			/* weitere Nocken der Spur 0 deaktivieren */
			nsw.spur[0].nocke[1].ein 	= 0;
			nsw.spur[0].nocke[1].aus 	= 0;

			nsw.spur[1].do_kanal 		= 0;	/* 7; */
			/* mit der "spur_enable_id" wird eine zus�tzliche UND-Verkn�pfung zum Schalten des Ausganges eingef�gt */
			nsw.spur[1].spur_enable_id	= ACP10PAR_CAM_DRUMSEQ_ENABLE;
			nsw.spur[1].do_verz_zeit 	= 0.0;
			/*Barring good info from Steve should try triggering just before the printer head would otherwise run, maybe more consistent?*/
			nsw.spur[1].nocke[0].ein 	= 0; /*ma_s_sync - 10;*/
			nsw.spur[1].nocke[0].aus 	= 0; /*nsw.spur[1].nocke[0].ein + Akt_Produkt_Param.Laenge_Slave_Intervall;*/
			nsw.spur[1].nocke[1].ein 	= 0;
			nsw.spur[1].nocke[1].aus 	= 0;
			nsw.spur[1].nocke[2].ein 	= 0;
			nsw.spur[1].nocke[2].aus 	= 0;
			nsw.spur[1].nocke[3].ein 	= 0;
			nsw.spur[1].nocke[3].aus 	= 0;
			nsw.spur[1].nocke[4].ein 	= 0;
			nsw.spur[1].nocke[4].aus 	= 0;
			nsw.spur[1].nocke[5].ein 	= 0;
			nsw.spur[1].nocke[5].aus 	= 0;

			nsw.spur[2].do_kanal		= 0;

			step_nsw_init = NSW_INIT_ALLGEMEIN;
			break;
				
		/* allgemeine NSW - Parameter �bergeben */
		case NSW_INIT_ALLGEMEIN:
			nsw_init_par_nr = nsw_init_par_nr + 1;
			if (nsw_init_par_nr > 4)
			{	
				nsw_init_par_nr = 0;
				step_nsw_init = NSW_INIT_SPUR;
				break;
			}

			switch (nsw_init_par_nr)
			{
				case 1:
	    			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CMD_DRUMSEQ;
					kommando = ncAUSSCH;
	    			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&kommando;
	    			break;
				case 2:	
	    			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_DRUMSEQ_MA_AXIS;
	    			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&nsw.master;
	    			break;
	    		case 3:
	    			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_DRUMSEQ_S_START;
	    			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&nsw.ma_s_start;
	    			break;
	    		case 4:
	    			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_DRUMSEQ_S_IV;
	    			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&nsw.intervall;
	    			break;
			}
		    if ((action_status=ncaction(ax_obj,ncSERVICE,ncSETZEN))==ncOK)
		    {
		    	step_nsw_init = W_NSW_INIT_ALLGEMEIN;
		    }
	    	break;
				    			
	    case W_NSW_INIT_ALLGEMEIN:
	    	if (p_ax_dat->netzwerk.service.response.par_id == 
	        	p_ax_dat->netzwerk.service.request.par_id)
	        {
	      		/* Operation erfolgreich abgeschlossen */
	        	step_nsw_init = NSW_INIT_ALLGEMEIN;
	        }
	        break;
	    
		/* Spurparameter einer Nocke �bergeben */
	    case NSW_INIT_SPUR:
			spur_init_par_nr = spur_init_par_nr + 1;
	    	if (spur_init_par_nr > 4)
	    	{
	    		step_nsw_init = NSW_INIT_NOCKE;
	    		spur_init_par_nr = 0;
	    		break;
	    	}
	    
	    	switch (spur_init_par_nr)
	    	{
	    		case 1:
	    			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_DRUMSEQ_TRACK_INDEX;
	    			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&init_zaehl_spur;
	    			break;
	    		case 2:
	    			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_DRUMSEQ_DO_CHAN;
	    			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&nsw.spur[init_zaehl_spur-1].do_kanal;
	    			break;
	    		case 3:
	    			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_DRUMSEQ_TRACK_ENABLE_ID;
	    			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&nsw.spur[init_zaehl_spur-1].spur_enable_id;
	    			break;
	    		case 4:
	    			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_DRUMSEQ_DO_DELAY;
	    			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&nsw.spur[init_zaehl_spur-1].do_verz_zeit;
	    			break;
	    	}
		    if ((action_status=ncaction(ax_obj,ncSERVICE,ncSETZEN))==ncOK)
		    {
		    	step_nsw_init = W_NSW_INIT_SPUR;
		    }
	    	break;
				    			
	    case W_NSW_INIT_SPUR:
	    	if (p_ax_dat->netzwerk.service.response.par_id == 
	        	p_ax_dat->netzwerk.service.request.par_id)
	        {
	      		/* Operation erfolgreich abgeschlossen */
	        	step_nsw_init = NSW_INIT_SPUR;
	        }
	        break;
	    	

		/* Schaltpunkte pro Spur �bergeben */
	    case NSW_INIT_NOCKE:
	    	nocke_init_step = nocke_init_step + 1;
	    	if (nocke_init_step > 3)
	    	{
	    		if (nsw.spur[init_zaehl_spur-1].do_kanal == 0)	
	    		{
	    			/* letzte definierte Spur */
	    			step_nsw_init = NSW_INIT_ENDE;
	    			nocke_init_step = 0;
	    			break;
	    		}
	    		else if (nsw.spur[init_zaehl_spur - 1].nocke[init_zaehl_nocke - 1].aus == 0) 
	    		/* letzte definierte Nocke dieser Spur => n�chste Spur */
	    		{
					init_zaehl_spur++;
					if (init_zaehl_spur >= MAX_SPUREN + 1)	/* max. Anzahl Spuren initialisiert */
					{
						step_nsw_init = NSW_INIT_ENDE;
					}
					else
					{
						step_nsw_init = NSW_INIT_SPUR;
					}
	
					init_zaehl_nocke = 1;
					nocke_init_step = 0;
					break;
				}
				else	
				/* n�chste Nocke initialisieren ? */
				{
					init_zaehl_nocke++;
					if (init_zaehl_nocke >= MAX_NOCKEN + 1) /* max. Anzahl Nocken initialisiert */
					{
						init_zaehl_spur++;
						if (init_zaehl_spur >= MAX_SPUREN + 1)	/* max. Anzahl Spuren initialisiert */
						{
							step_nsw_init = NSW_INIT_ENDE;
						}
						else
						{
							step_nsw_init = NSW_INIT_SPUR;
						}

						init_zaehl_nocke = 1;
						nocke_init_step = 0;
						break;
					}
					else /* n�chste Nocke initialisieren */
					{
						nocke_init_step = 0;
						break;
					}
				}
			}
						
	    				    
	    	switch (nocke_init_step)	
	    	{
	    		case 1:
	    			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_DRUMSEQ_CAM_INDEX;
	    			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&init_zaehl_nocke;
	    			break;
	    		case 2:
	    			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_DRUMSEQ_S_DO_ON;
	    			p_ax_dat->netzwerk.service.daten_adr = 
	    			(UDINT)&nsw.spur[init_zaehl_spur-1].nocke[init_zaehl_nocke-1].ein;
	    			break;
	    		case 3:
	    			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_DRUMSEQ_S_DO_OFF;
	    			p_ax_dat->netzwerk.service.daten_adr = 
	    			(UDINT)&nsw.spur[init_zaehl_spur-1].nocke[init_zaehl_nocke-1].aus;
	    			break;
	    	}	
		    if ((action_status=ncaction(ax_obj,ncSERVICE,ncSETZEN))==ncOK)
		    {
		    	step_nsw_init = W_NSW_INIT_NOCKE;
		    }
	    	break;
	
	  	case W_NSW_INIT_NOCKE:
	    	if (p_ax_dat->netzwerk.service.response.par_id == 
	        	p_ax_dat->netzwerk.service.request.par_id)
	        {
	      		/* Operation erfolgreich abgeschlossen */
	        	step_nsw_init = NSW_INIT_NOCKE;
	       	}
	  		break;
					
		case NSW_INIT_ENDE:
			init_zaehl_spur = 1;
			nsw_init = 0;
			step_nsw_init = W_NSW;
			break;
	
   		/* NSW einschalten */
		case NSW_START:
		
			if (keine_AC130_karte_vorhanden == 1)
			{
	    		nsw_start = 0;
				step_nsw_init = W_NSW;
				break;
			}
			
	   		p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CMD_DRUMSEQ;
			kommando = ncEINSCH;
	    	p_ax_dat->netzwerk.service.daten_adr = (UDINT)&kommando;
		
	   		if ((action_status=ncaction(ax_obj,ncSERVICE,ncSETZEN))==ncOK)
	     		step_nsw_init = W_NSW_START;
	     	break;
	
	  case W_NSW_START:
	  	if (p_ax_dat->netzwerk.service.response.par_id == 
	        p_ax_dat->netzwerk.service.request.par_id)
	    {
	        /* Operation erfolgreich abgeschlossen */
	    	nsw_start = 0;
			step_nsw_init = W_NSW;
		}
		break;
			
    }
    
	/* Schrittnummernaufzeichnung */
	if (step_nsw_init != step_alt)
	{		
    	step_debug[step_debug_lfd_nr] = step_nsw_init;
    	step_debug_lfd_nr = step_debug_lfd_nr + 1;
    }
    if (step_debug_lfd_nr >=100)
    {
        step_debug_lfd_nr = 0;
    }
    
    step_alt = step_nsw_init;
    
}

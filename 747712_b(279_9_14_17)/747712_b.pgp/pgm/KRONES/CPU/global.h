/************************************************************************************/
/* �nderungen:																		*/
/* ***********																		*/	
/* Datum     Vers.  Bemerkung                                                Name	*/
/* 																					*/	
/* 01.01.00  00.00  neu erstellt                                             PET	*/
/* -------------------------------------------------------------------------------- */
/* 05.10.00  00.01  Datentypen der Maschinen und Sortenparameter an die 			*/
/*					tats�chlichen Datenl�ngen angepasst. (neue Reserven)	PET		*/
/* -------------------------------------------------------------------------------- */
/* 03.04.03  02.55  Folgefehler Maschinenparameter eingef�gt				SNH		*/
/* 01.07.04  05.06	Geschwindigkeit Handspenden eingef�gt					HOG		*/
/* ---------------------------------------------------------------------------------*/
/*																					*/
/************************************************************************************/

/***********************************************************************/
/* Datentypdeklaration                                                 */
/***********************************************************************/

typedef struct {
	unsigned short	Typ_Aggregat;				
	unsigned char	Folgefehler;
	unsigned char	Servo_Drehrichtung;
	unsigned short	User_Password;				/* bei Bedarf umbenennen */
	unsigned char	Reserve_4;					/* bei Bedarf umbenennen */
	unsigned char	Maschine_Drehrichtung;
	long			Laenge_Maschinenteilung_allg;
	unsigned short	Geschwindigkeit_Handspenden;				
	unsigned char	Reserve_6;					/* bei Bedarf umbenennen */
	unsigned char	Anzahl_Maschinenteilungen;
	unsigned short	Reserve_7;					/* bei Bedarf umbenennen */
	unsigned char	Reserve_8;					/* bei Bedarf umbenennen */
	unsigned char	Maschine_Gebertyp;
	long			Reserve_9;					/* bei Bedarf umbenennen */
	unsigned short	Reserve_10;					/* bei Bedarf umbenennen */
	unsigned short	Typ_Kommunikation;
	unsigned short	Reserve_11;					/* bei Bedarf umbenennen */
	unsigned char	Reserve_12;					/* bei Bedarf umbenennen */
	unsigned char	Reserve_13;					/* bei Bedarf umbenennen */
	unsigned short	Reserve_14;					/* bei Bedarf umbenennen */
	unsigned char	Produkt_Nr;
	unsigned char	Sprache;
	long			Abstand_Aggregat; 
	long			Reserve_16;					/* bei Bedarf umbenennen */
	long			Reserve_17;					/* bei Bedarf umbenennen */
	long			Reserve_18;					/* bei Bedarf umbenennen */
	long			Reserve_19;					/* bei Bedarf umbenennen */
	long			Reserve_20;					/* bei Bedarf umbenennen */
	long			Reserve_21;					/* bei Bedarf umbenennen */
	long			Reserve_22;					/* bei Bedarf umbenennen */
} Maschine_Param_TYP;
   
typedef struct {
	unsigned short	Vorbereitung_Eti_Verkl_2;
	unsigned short	Vorbereitung_Eti_Verkl;
	unsigned short	Anz_Eti_Vmin;
	unsigned short	Anz_Eti_Schnitt_1;
	long			SK_Tellermitte;
	long			Laenge_Maschinen_SR;
	long			Weg_LS_Spendekante;
	long			Steigung_Vorhalt_V_abhg;	/* bei Bedarf umbenennen */
	long			Start_Pos_Synchron;
	long			Faktor_Synchronweg_Master;
	long 			Laenge_Slave_Intervall; 
	long			Laenge_Comp_weg_Slave;
	unsigned short	Leistung_Fl_h;
	unsigned char	Typ_Verklebung;
	unsigned char	Typ_Datierung;
	long			Startposition_NSW;
	long			Weg_Ma_Trig_Spendekante;
	unsigned short	Anz_Eti_Schnitt_2;
	unsigned short	Geschw_Abroller_Verkl;
	unsigned short	Geschw_Verkl;
	unsigned short	Pos_Vak_Aus_Verkl;
	unsigned short  Pos_Weiche;
	unsigned short	Pos_Abroller;
	long			V_Untergr_Vorhalt;	/* bei Bedarf umbenennen */
	unsigned short	Druck_Schubwalze ;
	unsigned short	Druck_Zugwalze;
	unsigned short	Anz_Eti_Aufroller;
	unsigned short	Pos_Datierung;
	unsigned short	Pos_Eti_Sensor;
	unsigned short	Pos_Achse_A;
	unsigned short	Pos_Achse_B;
	unsigned short	Pos_Achse_C;
	unsigned short	Pos_Achse_D;
	unsigned short	Pos_Achse_E;
	unsigned short	Pos_Achse_F;
	unsigned short	Pos_Verklebung;

} Produkt_Param_TYP;



typedef struct {
	USINT	zeile1[20];
	USINT	zeile2[20];
	USINT	zeile3[20];
	USINT	zeile4[20];	
	USINT	zeile5[20];
	USINT	zeile6[20];
	USINT	zeile7[20];
	USINT	zeile8[20];	
	USINT	zeile9[20];
	USINT	zeile10[20];
	USINT	zeile11[20];
	USINT	zeile12[20];	
	USINT	zeile13[20];
	USINT	zeile14[20];
	USINT	zeile15[20];
	USINT	zeile16[20];	
} FEHLER_TEXT_typ;

typedef struct {
	UINT	Nummer;
	USINT	Anzahl_Zeilen;
	USINT	Fehler_Warnung;
} FEHLER_INFO_typ;


/***********************************************************************/
/* globale Variablendeklaration                                        */
/***********************************************************************/

_GLOBAL	plcbit		Grobtakt;					/* Grobtakt */
_GLOBAL	plcbit		Spender_Ein;				/* Freigabe */
_GLOBAL unsigned short 		Produkt_Nr;			/* Produkt-Nummer */
_GLOBAL plcbit		I_Moschu_Aggr;				/* Motorschutz�berwachung */
_GLOBAL plcbit		I_Rueck_Zeit_Netz;			/* R�ckmeldung Zeitrelais Netzsch�tz Aggregat */
_GLOBAL plcbit		I_Rueck_Netz_Aggr;			/* R�ckmeldung Netzsch�tz Aggregat */
_GLOBAL	plcbit		BA_Auto;					/* Betriebsart Automatik */
_GLOBAL	plcbit		Spenden_Manuell;			/* Manuell Etikett spenden */
_GLOBAL unsigned short	MMI_Var_Puffer_bis_Rollenende;	/* keine Ahnung */
_GLOBAL unsigned int Fehler_ACOPOS_Anzahl;		/* Anzahl der Fehlertexte die in Fehlerarray geschrieben wurden. */
_GLOBAL FEHLER_TEXT_typ Fehler_ACOPOS[11];		/* Fehlertextfeld zur Anzeige des Klartextes */
_GLOBAL FEHLER_INFO_typ Fehler_Info_ACOPOS[11];	/* Fehlernummer erg�nzend zum Klartext */
_GLOBAL int 		Sprache;					/* Sprache des PWI */
_GLOBAL unsigned char	MMI_Betriebsart;		/* Betriebsart Aus - Hand - Auto */
_GLOBAL plcbit		Eti_Trigger_fehlt;			/* Anzeige kein Etikettentrigger w�hrend Referenzfahrt */
_GLOBAL	plcbit		ACOPOS_Fehler;				/* Fehler vom ACOPOS vorhanden */
_GLOBAL	plcbit		ACOPOS_Warnung;				/* Warnung vom ACOPOS vorhanden */
_GLOBAL	plcbit		ACOPOS_Aus_laeuft;			/* Ausschalten der Kurvenscheibenkopplung laeuft */
_GLOBAL	plcbit		Zuviele_Warnungen_ACOPOS;	/* zuviele Warnungen vom ACOPOS vorhanden */
_GLOBAL	plcbit		Parameterwerte_falsch;		/* Parameter falsch oder nich vollst�ndig */
_GLOBAL	plcbit		ACOPOS_Neustart_noetig;		/* Verbindung zum ACOPOS kann nicht initialisiert werden */
_GLOBAL	plcbit		Ref_nicht_mgl;				/* Referenzfahrt nicht m�glich kein Signal vom Etikettensensor */
_GLOBAL plcbit		I_LS_Durchhang_unten;       /* Eingang Lichtschranke Durchhang unten */
_GLOBAL plcbit		I_LS_Durchhang_oben;        /* Eingang Lichtschranke Durchhang oben */
_GLOBAL plcbit		I_Trigger_1;        		/* Eingang Trigger 1 */
_GLOBAL plcbit		I_Trigger_2;        		/* Eingang Trigger 2 */
_GLOBAL plcbit		I_Referenz;        			/* Eingang Referenzschalter */
_GLOBAL plcbit		I_MMI_Quit_Allg_Taste;      /* Quittietaste am Display */
_GLOBAL Maschine_Param_TYP	Akt_Maschinen_Param;	/* Maschinen-Parameter */
_GLOBAL Produkt_Param_TYP	Akt_Produkt_Param;	/* Produkt-Parameter */
_GLOBAL	plcbit		nsw_init;					/* Nockenschaltwerk initialisieren */
_GLOBAL	plcbit		nsw_start;					/* Nockenschaltwerk starten */
_GLOBAL	plcbit		read_diag;					/* Diagnosedaten f�r Display �ber Parameterkanal lesen - Startsignal */
_GLOBAL	plcbit		bus_read_diag;				/* Diagnosedaten f�r Busansteuerung �ber Parameterkanal lesen - Startsignal */
_GLOBAL int			read_diag_Parameter;		/* Diagnosedaten �ber Parameterkanal lesen - Parameternummer */
_GLOBAL UDINT 		read_diag_Datenwert;		/* Wert der angeforderten Diagnosedaten */
_GLOBAL	plcbit		En_Testbetrieb;				/* Testbetrieb ohne Etikettenband - Trigger ausgeschaltet */
_GLOBAL	plcbit		Parameter_init;				/* Parameter in menue.src haben sich ge�ndert. */
_GLOBAL	plcbit		Enc2_Ref_Fahrt;				/* Warten auf Ende Referenzfahrt f�r Inkrementalgeber */
_GLOBAL	plcbit		Enc1_Ref_ok;				/* Referenzfahrt f�r Servo- EN-Dat Geber abgeschlossen */
_GLOBAL	plcbit		SU_Geoeffnet;				/* Aggregatschutz ist ge�ffnet */
_GLOBAL	plcbit		Fehler_Etikettenbandriss;	/* Etikettenband ist gerissen */
_GLOBAL	DINT		ma_s_start_add_el;			/* Masterstartposition-Offset f�r Kurvenscheibe */
_GLOBAL	DINT		ma_s_start_alt;				/* letzte Masterstartposition f�r Kurvenscheibe zur Online�nderung */
_GLOBAL	DINT		ma_s_start_NSW;				/* Masterstartposition f�r Kurvenscheibe und NSW */
_GLOBAL	plcbit		Startpos_geaendert;			/* neues additives Element zum ACOPOS schicken */
_GLOBAL	UINT		step_slave_ax;				/* Aktueller Schritt im Schrittschaltwerk */
_GLOBAL	UINT		step_nsw_init;				/* Aktueller Schritt im Schrittschaltwerk */ 	
_GLOBAL	DINT		Pos_Servo;					/* Geberwert des Motorgebers */
_GLOBAL	DINT		V_Servo;					/* Geschwindigkeit des Servomotors */
_GLOBAL DINT		V_Servo_old;
_GLOBAL USINT		servo_stop;
_GLOBAL USINT		vision_off_counts;
_GLOBAL	plcbit		APS_1;						/* Aggregattyp alter Spender */
_GLOBAL	plcbit		APS_2;						/* Aggregattyp neuer Spender */
_GLOBAL	plcbit		kein_Flada_Signal;			/* Messemaschine ohne Flada */
_GLOBAL	plcbit		I_Inhibit;					/* Messemaschine ohne Flada */
_GLOBAL	plcbit		Aggregat_standby;			/* Inhibit - Bit gesetzt */ 
_GLOBAL	plcbit		ein_Etikett_man_gespendet;	/* ein Etiket wurde manuell gespendet */
_GLOBAL	plcbit		Rollenende;					/* Rollenende erreicht */
_GLOBAL	plcbit		Rollenende_Sperre;			/* Rollenende erreicht - Sperre zu */
_GLOBAL	plcbit		check_Etikett; 				/* Etikettenl�ckenpr�fung selektiert */
_GLOBAL	plcbit		I_Eti_Trigger; 				/* Eingang Etikettensensor */
_GLOBAL	plcbit		Etikettenluecke; 			/* Fehler Etikettenluecke aufgetreten */
_GLOBAL	plcbit		vorwaerts_referenzieren;	/* Etikettenband vorwaerts referenzieren */
_GLOBAL	plcbit		Etiband_ref_ok;				/* R�ckmeldung Achse ist bereit zum Spenden, evtl. mu� noch eine Teilung gefahren werden um Enc2 zu ref. */
_GLOBAL	plcbit		Walzen_verriegelt;			/* Antriebswalzen sind verriegelt */
_GLOBAL	plcbit		HM_Walzen_verriegeln;		/* Antriebswalzen verriegeln */ 
_GLOBAL	plcbit		man_Eti_spenden_laeuft;		/* manuelles Spenden von Etiketten l�uft */ 
_GLOBAL	DINT		Akt_Anz_Eti_Rolle_Vk2;		/* Diagnosewert von Verklebung 2 f�r menue */
_GLOBAL	DINT		Akt_Anz_Eti_Vmin_Vk2;		/* Diagnosewert von Verklebung 2 f�r menue */
_GLOBAL INT			Ana_O_Stellw_Aufroller;		/* Stellwert Aufroller */
_GLOBAL INT			Ana_O_Stellw_Aufroller_2;		/* Stellwert Aufroller bei Verklebung mit Aufrollerumschaltung */
_GLOBAL	plcbit		Stoerung_Bandauslauf;		/* St�rung Web-Jam Bandauslauf */
_GLOBAL	plcbit		ACOPOS_Ein_laeuft;			/* Einschalten der Kurvenscheibenkopplung laeuft */
_GLOBAL	plcbit		APS_1_iGetriebe2;			/* Getriebe�bersetzung Sonderausstattung 2:1 */

_GLOBAL	plcbit		Reset_comp_gear_run;		/* R�cksetzen des aller Fifos und des Ku.-Automaten */
_GLOBAL	plcbit		nsw_io_config;				/* IO-Karte f�r NSW konfigurieren */

_GLOBAL	plcbit		I_Rollenanwahl_Rolle_2;		/* Anwahl Rollenendeauswertung -> Parameterauswahl */
_GLOBAL	plcbit		V_Aufsch_Neu;				/* Neue Geschwindigkeitsaufschaltung f�r Auf-/Abroller selektiert */

_GLOBAL	plcbit		Haecksler_Aufroller;		/* H�cksleraufroller als Aggregattyp angew�hlt */
_GLOBAL	long		V_Durchschnitt;				/* durchschnittliche Tr�gerbandgeschwindigkeit */
_GLOBAL	plcbit		Schleifenauslauf;			/* Konfigbit Schleifenauslauf zus�tzliche Mechanik am APS2 Kopf */
_GLOBAL	plcbit		Fehler_Geberueberwachung;	/* Geberueberwachung hat angesprochen */
_GLOBAL	plcbit		Rollenanwahl_Rolle_2;		/* Rolle 1 bei Verklebungstyp 7 (Aggr. mit Vk, Vk wird aber nicht betrieben) */
_GLOBAL	plcbit		Geberueberw_Gebrueckt;		/*Geberfehler wird w�hrend der ersten 4 Teilungen �berbr�ckt, da St�rungen auf der Geberleitung diese beeinflussen k�nnen*/
_GLOBAL BOOL		DO_PLC_VISION;
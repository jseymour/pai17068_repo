PROGRAM sicher 5 (* Ladder Diagram *)
	CODE_INIT
		<?xml version="1.0"?>
		<?ladder Version = 1.0?>
		<!-- Automation Studio Generated XML Section -->
		<networks xmlns:dt="urn:schemas-microsoft-com:datatypes" maxcolumn="2">
			<network label="" comment="Feldbus Fehler mit '0' initialisieren, da Busanbindung noch nicht progammiert" row="1" column="2">
				<row><contact type="open" name="True"/><coil type="reset" name="Fehler_Feldbus"/></row>
			</network>
			<network label="" comment="Variablen mit '0' initialisieren - ausser Si-Kreis Fehler!" row="15" column="2">
				<row><contact type="open" name="True"/><coil type="reset" name="NA_SU_Gesamt"/></row>
				<row><empty or="true"/><coil type="reset" name="NA_SU_Aggr"/></row>
				<row><empty or="true"/><coil type="reset" name="NA_SU_Fehlerhaft"/></row>
				<row><empty or="true"/><coil type="reset" name="NA_SU_Freigabe"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[4]"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[3]"/></row>
				<row><empty or="true"/><coil type="reset" name="SU_Geoeffnet"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[6]"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[7]"/></row>
				<row><empty or="true"/><coil type="reset" name="Netzschuetz_Fehlerhaft"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[10]"/></row>
				<row><empty or="true"/><coil type="reset" name="HM_LED_Quit_Si_Kreis"/></row>
				<row><empty or="true"/><coil type="reset" name="HM_KL_Quit_Si"/></row>
				<row><empty or="true"/><coil type="reset" name="HM_KL_Quit_Allg"/></row>
				<row><empty or="true"/><coil type="reset" name="Meldung[39] "/></row>
			</network>
		</networks>
	END_CODE_INIT
	CODE_CYCLIC
		<?xml version="1.0"?>
		<?ladder Version = 1.0?>
		<!-- Automation Studio Generated XML Section -->
		<networks xmlns:dt="urn:schemas-microsoft-com:datatypes" maxcolumn="7">
			<network label="" comment="Änderungen: 
		****************	
		Datum    	Vers.  	Bemerkung                               										Name
		
		05.08.03		2.63	Verklebungsschutzauswertung, wenn APS2 Verklebungs Hardware vorhanden (Verklebungstyp 3, 4, 5, 6, und 7)	SNH
		 " row="1" column="2">
				<row><contact type="open" name="TRUE"/><line/><line/><line/><line/><line/><coil type="open" name="Versionsgeschichte"/></row>
			</network>
			<network label="" comment="Si_Kreis_Modul_Etima = 1 --&gt; Baustein überspringen" row="1" column="2">
				<row><contact type="open" name="Si_Kreis_Modul_Etima"/><line/><line/><line/><line/><line/><coil type="jump" name="Ende"/></row>
			</network>
			<network label="" comment="Notaus-, Schutzkette gesamt" row="2" column="4">
				<row><contact type="open" name="I_NA_Aggr"/><contact type="closed" name="I_SU_Aggr"/><contact type="open" name="I_NA_SU_Etima_betaetigt"/><line/><line/><line/><coil type="open" name="NA_SU_Gesamt"/></row>
				<row><empty or="true"/><contact name="NA_SU_getrennt" or="true"/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Notaus-, Schutzkette Aggregat" row="2" column="3">
				<row><contact type="open" name="I_NA_Aggr"/><contact type="closed" name="I_SU_Aggr"/><line/><line/><line/><line/><coil type="open" name="NA_SU_Aggr"/></row>
				<row><empty or="true"/><contact type="open" name="NA_SU_getrennt"/><line/><line/><line/><line or="true"/><coil type="open" name="O_NA_SU_an_Etima"/></row>
			</network>
			<network label="" comment="Fehlerhafte Zustaende Notaus-, Schutzkette" row="4" column="3">
				<row><contact type="closed" name="NA_SU_Gesamt"/><contact type="closed" name="I_Rueck_NA_SU_ges"/><line/><line/><line/><line/><coil type="open" name="NA_SU_Fehlerhaft"/></row>
				<row><contact type="open" name="NA_SU_Gesamt"/><contact type="open" name="I_Rueck_NA_SU_ges"/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="closed" name="NA_SU_Aggr"/><contact type="closed" name="I_Rueck_NA_SU_an_Etima"/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="NA_SU_Aggr"/><contact type="open" name="I_Rueck_NA_SU_an_Etima"/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Kontaktverzoegerung Notaus- Schutzkreis" row="4" column="5">
				<row><empty/><functionblock position="header" name="Zeit_NA_SU_laeuft"/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="NA_SU_Fehlerhaft"/><functionblock position="top" type="TON"><input name="IN"/><output name="Q"/></functionblock><line/><line/><line/><coil type="open" name="Fehler_NA_SU_Kreis"/></row>
				<row><analog type="input" name="T#1s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty or="true"/><empty/></row>
				<row><contact type="open" name="Fehler_NA_SU_Kreis"/><contact type="closed" name="I_MMI_Quit_Allg_Taste"/><contact type="closed" name="I_Bus_Quit_Allg"/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Notaus / Schutz  ------- Notausschütz &quot;Gesamt&quot;  angesteuert Rückmeldung abgefallen" row="2" column="4">
				<row><contact type="open" name="Fehler_NA_SU_Kreis"/><contact type="closed" name="NA_SU_Gesamt"/><contact type="closed" name="I_Rueck_NA_SU_ges"/><line/><line/><line/><coil type="set" name="Meldung[30]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="Meldung[31]"/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Notaus / Schutz  ------- Notausschütz &quot;Gesamt&quot; nicht angesteuert Rückmeldung angesteuert " row="2" column="4">
				<row><contact type="open" name="Fehler_NA_SU_Kreis"/><contact type="open" name="NA_SU_Gesamt"/><contact type="open" name="I_Rueck_NA_SU_ges"/><line/><line/><line/><coil type="set" name="Meldung[30]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="Meldung[32]"/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Notaus / Schutz  ------- Notausschütz &quot;Aggregat&quot; angesteuert Rückmeldung abgefallen" row="2" column="4">
				<row><contact type="open" name="Fehler_NA_SU_Kreis"/><contact type="closed" name="NA_SU_Aggr"/><contact type="closed" name="I_Rueck_NA_SU_an_Etima"/><line/><line/><line/><coil type="set" name="Meldung[33]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="Meldung[34]"/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Notaus / Schutz  -------  Notausschütz &quot;Aggregat&quot; nicht angesteuert Rückmeldung angesteuert " row="2" column="4">
				<row><contact type="open" name="Fehler_NA_SU_Kreis"/><contact type="open" name="NA_SU_Aggr"/><contact type="open" name="I_Rueck_NA_SU_an_Etima"/><line/><line/><line/><coil type="set" name="Meldung[33]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="Meldung[35]"/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Notaus / Schutz ------- Detailanzeigen rücksetzen" row="6" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><coil type="reset" name="Meldung[30]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Meldung[31]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Meldung[32]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Meldung[33]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Meldung[34]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Meldung[35]"/></row>
			</network>
			<network label="" comment="HM Anzeige Quittieren Sicherheitskreis" row="1" column="3">
				<row><contact type="closed" name="Zeit_NA_SU_laeuft.Q"/><contact type="open" name="Fehler_NA_SU_Kreis"/><line/><line/><line/><line/><coil type="set" name="HM_KL_Quit_Si"/></row>
			</network>
			<network label="" comment="Freigabe Notaus-, Schutzkreis
		Die NA/SU Freigabe für die Verklebungen muss unverzögert erfolgen." row="2" column="4">
				<row><contact type="open" name="NA_SU_Gesamt"/><contact type="closed" name="Fehler_NA_SU_Kreis"/><contact type="closed" name="Fehler_Feldbus"/><line/><line/><line/><coil type="open" name="NA_SU_Freigabe"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="open" name="NA_SU_Freigabe_Verkl"/></row>
			</network>
			<network label="" comment="Anzeige Notaus betaetigt" row="1" column="2">
				<row><contact type="closed" name="I_NA_Aggr"/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[4]"/></row>
			</network>
			<network label="" comment="Anzeige Notaus- , Schutz Etima betaetigt - Unterdrücken wenn am Aggr. NA betätigt" row="1" column="3">
				<row><contact type="closed" name="I_NA_SU_Etima_betaetigt"/><contact type="open" name="I_NA_Aggr"/><line/><line/><line/><line/><coil type="open" name="Meldung[3]"/></row>
			</network>
			<network label="" comment="Schutzdeckel geoeffent" row="2" column="4">
				<row><contact type="open" name="I_SU_Aggr"/><line/><line/><line/><line/><line/><coil type="open" name="SU_Geoeffnet"/></row>
				<row><contact type="open" name="SU_Geoeffnet"/><contact type="closed" name="I_MMI_Quit_Allg_Taste"/><contact type="closed" name="I_Bus_Quit_Allg"/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="HM-KL Quittieren Allgemein" row="1" column="3">
				<row><contact type="closed" name="I_SU_Aggr"/><contact type="open" name="SU_Geoeffnet"/><line/><line/><line/><line/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Anzeige Schutztuer offen" row="1" column="3">
				<row><contact type="open" name="I_SU_Aggr"/><contact type="open" name="SU_Geoeffnet"/><line/><line/><line/><line/><coil type="open" name="Meldung[6]"/></row>
			</network>
			<network label="" comment="Anzeige Schutztuer war offen" row="1" column="3">
				<row><contact type="closed" name="I_SU_Aggr"/><contact type="open" name="SU_Geoeffnet"/><line/><line/><line/><line/><coil type="open" name="Meldung[7]"/></row>
			</network>
			<network label="" comment="Schutzdeckel an der APS1 Verklebung geoeffnet" row="2" column="5">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="EQ"><input name="IN1"/><output name=""/></function><contact type="open" name="I_SU_Verklebung"/><line/><line/><coil type="open" name="SU_Geoeffn_Vk1"/></row>
				<row><analog type="input" name="2"/><function position="bottom"><input name="IN2"/></function><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Schutzdeckel an der APS2 Verklebung geoeffnet (V2.63)" row="2" column="5">
				<row><contact type="open" name="APS2_Verklebung_vorhanden"/><contact type="open" name="I_SU_Verkl_2"/><line/><line/><line/><line/><coil type="open" name="SU_Geoeffn_Vk2"/></row>
				<row><empty or="true"/><contact type="open" name="SU_Geoeffn_Vk2"/><contact type="closed" name="I_MMI_Quit_Allg_Taste"/><contact type="closed" name="I_Bus_Quit_Allg"/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Meldung Schutz an Verklebung 1 oder 2 geoeffnet" row="2" column="2">
				<row><contact type="open" name="SU_Geoeffn_Vk1"/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[39] "/></row>
				<row><contact type="open" name="SU_Geoeffn_Vk2"/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="HM-KL Quittieren Allgemein" row="1" column="3">
				<row><contact type="closed" name="I_SU_Verkl_2"/><contact type="open" name="SU_Geoeffn_Vk2"/><line/><line/><line/><line/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Fehlerhafte Zustaende Zeitrelais" row="4" column="7">
				<row><empty/><empty/><empty/><functionblock position="header" name="Zeit_Zeitrelais_laeuft"/><empty/><empty/></row>
				<row><contact type="open" name="Ansteuerung_Netzschuetz"/><contact type="closed" name="I_Rueck_Zeit_Netz"/><line/><functionblock position="top" type="TON"><input name="IN"/><output name="Q"/></functionblock><line/><coil type="open" name="Fehler_Zeitrelais"/></row>
				<row><contact type="closed" name="Ansteuerung_Netzschuetz"/><contact name="I_Rueck_Zeit_Netz" or="true"/><analog type="input" name="T#2.5s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty or="true"/><empty/></row>
				<row><contact type="open" name="Fehler_Zeitrelais"/><contact type="closed" name="I_MMI_Quit_Allg_Taste"/><contact type="closed" name="I_Bus_Quit_Allg"/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Zeitrelais -----  Zeitrelais angesteuert Rückmeldung abgefallen" row="2" column="4">
				<row><contact type="open" name="Fehler_Zeitrelais"/><contact type="open" name="O_Netz_Aggr"/><contact type="closed" name="I_Rueck_Zeit_Netz"/><line/><line/><line/><coil type="set" name="Meldung[24]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="Meldung[25]"/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Zeitrelais  ------- Zeitrelais nicht angesteuert Rückmeldung angesteuert" row="2" column="4">
				<row><contact type="open" name="Fehler_Zeitrelais"/><contact type="closed" name="O_Netz_Aggr"/><contact type="open" name="I_Rueck_Zeit_Netz"/><line/><line/><line/><coil type="set" name="Meldung[24]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="Meldung[26]"/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Zeitrelais ----- Quit" row="3" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><coil type="reset" name="Meldung[24]"/></row>
				<row><contact type="open" name="I_Bus_Quit_Allg"/><line/><line/><line/><line/><line or="true"/><coil type="reset" name="Meldung[25]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Meldung[26]"/></row>
			</network>
			<network label="" comment="HM Anzeige Quittieren Sicherheitskreis" row="1" column="3">
				<row><contact type="closed" name="Zeit_Zeitrelais_laeuft.Q"/><contact type="open" name="Fehler_Zeitrelais"/><line/><line/><line/><line/><coil type="set" name="HM_KL_Quit_Si"/></row>
			</network>
			<network label="" comment="Fehlerhafte Zustaende Netzschuetz" row="2" column="3">
				<row><contact type="closed" name="I_Rueck_Zeit_Netz"/><contact type="closed" name="I_Rueck_Netz_Aggr"/><line/><line/><line/><line/><coil type="open" name="Netzschuetz_Fehlerhaft"/></row>
				<row><contact type="open" name="I_Rueck_Zeit_Netz"/><contact type="open" name="I_Rueck_Netz_Aggr"/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Kontaktverzoegerung Netzschuetzueberwachung" row="4" column="5">
				<row><empty/><functionblock position="header" name="Zeit_Netz_laeuft"/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="Netzschuetz_Fehlerhaft"/><functionblock position="top" type="TON"><input name="IN"/><output name="Q"/></functionblock><line/><line/><line/><coil type="open" name="Fehler_Netzschuetz"/></row>
				<row><analog type="input" name="T#2s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty or="true"/><empty/></row>
				<row><contact type="open" name="Fehler_Netzschuetz"/><contact type="closed" name="I_MMI_Quit_Allg_Taste"/><contact type="closed" name="I_Bus_Quit_Allg"/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Netzschuetz  -----  Netzschuetz  angesteuert Rückmeldung abgefallen" row="2" column="4">
				<row><contact type="open" name="Fehler_Netzschuetz"/><contact type="open" name="I_Rueck_Zeit_Netz"/><contact type="open" name="I_Rueck_Netz_Aggr"/><line/><line/><line/><coil type="set" name="Meldung[27]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="Meldung[28]"/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Netzschuetz  ------- Netzschuetz  nicht angesteuert Rückmeldung angesteuert" row="2" column="4">
				<row><contact type="open" name="Fehler_Netzschuetz"/><contact type="closed" name="I_Rueck_Zeit_Netz"/><contact type="closed" name="I_Rueck_Netz_Aggr"/><line/><line/><line/><coil type="set" name="Meldung[27]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="Meldung[29]"/></row>
			</network>
			<network label="" comment="Anzeige Stoerung Si-Kreis Netzschuetz ----- Quit" row="3" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><coil type="reset" name="Meldung[27]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Meldung[28]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Meldung[29]"/></row>
			</network>
			<network label="" comment="HM Anzeige Quittieren Sicherheitskreis" row="1" column="3">
				<row><contact type="closed" name="Zeit_Netz_laeuft.Q"/><contact type="open" name="Fehler_Netzschuetz"/><line/><line/><line/><line/><coil type="set" name="HM_KL_Quit_Si"/></row>
			</network>
			<network label="" comment="KL Quittieren Sicherheitskreis" row="5" column="2">
				<row><contact type="open" name="HM_KL_Quit_Si"/><line/><line/><line/><line/><line/><coil type="open" name="HM_LED_Quit_Si_Kreis"/></row>
				<row><contact type="open" name="Fehler_NA_SU_Kreis"/><line/><line/><line/><line/><line or="true"/><coil type="reset" name="HM_KL_Quit_Si"/></row>
				<row><contact type="open" name="Fehler_Netzschuetz"/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="Fehler_Zeitrelais"/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="Ende" comment="" row="1" column="2">
				<row><contact type="open" name="sicher_ende"/><line/><line/><line/><line/><line/><coil type="open" name="sicher_ende"/></row>
			</network>
		</networks>
	END_CODE_CYCLIC
END_PROGRAM

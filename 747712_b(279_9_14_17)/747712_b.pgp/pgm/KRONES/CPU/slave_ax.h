typedef struct {
	unsigned short	id;
	unsigned char	data;
	}UI1_TYP;
typedef struct {
	unsigned short	id;
	unsigned short	data;
	}UI2_TYP;
typedef struct {
	unsigned short	id;
	unsigned long	data;
	}UI4_TYP;
typedef struct {
	unsigned short	id;
	char	data;
	}I1_TYP;
typedef struct {
	unsigned short	id;
	short	data;
	}I2_TYP;
typedef struct {
	unsigned short	id;
	long	data;
	}I4_TYP;
typedef struct	{
	unsigned short	id;
	float data;
	}R4_TYP;
typedef struct  {
	unsigned short 	id;
	unsigned char	data[6];
	}K6_TYP;
typedef	struct	{
	UI2_TYP cmd_start;
	UI2_TYP ma_axis;
	UI1_TYP ma_trig_mode;
	UI1_TYP sl_trig_mode;
	UI1_TYP comp_gear_type;
	R4_TYP ma_v_1;
	R4_TYP ma_v_max;
	I4_TYP ma_s_start;
	I4_TYP ma_s_sync;
	I4_TYP ma_s_comp;
	I4_TYP ma_s_trig;	
	UI1_TYP ma_comp_trig_mode;
	I4_TYP ma_s_comp_trig;
	I4_TYP ma_comp_trig_window;
	I4_TYP ma_add_el;
	I4_TYP ma_trig_window;
	I4_TYP sl_s_sync;
	I4_TYP sl_s_comp;
	I4_TYP sl_s_trig;
	I4_TYP sl_s_comp_min;
	I4_TYP sl_s_comp_max;
	I4_TYP sl_trig_window;
	I4_TYP sl_trig_iv_min;
	I4_TYP sl_trig_iv_max;
} CAM_DATA_typ;

/****************************************************************************************/
/*	Programm zur	   																	*/
/*	Steuerung, Initialisierung und Fehlerbehandlung 									*/
/*	einer ACOPOS Achse																	*/
/*																						*/
/* �nderungen:																			*/
/* ***********																			*/	
/* Datum     Vers.  Bemerkung                                                	Name	*/
/* 																						*/	
/* 01.01.00  00.00  neu erstellt                                             	SRO-B&R */
/* 08.10.00  00.01  Eti-ref. warten vor erneutem Auslesen der Triggeranzahl  		PET */
/* 18.11.00  00.02  Masterposition bei Auto aus zyklisch lesen und                  	*/
/*					Kurvenscheibenkopplung bei �nderung ausschalten						*/
/* 08.01.01  00.03  Abfrage und Auswahl der Sprache f�r Fehlermeldungen korr.		PET */
/* 10.01.01  00.04  Referenzbewegung korrigiert								 		PET */
/* 31.01.01  00.05  Etikettenl�ngeanzeige und Istdrehzahlanzeige korrigiert. 	PET/SNH */
/* 06.02.01  00.06	�berpr�fungsfenster f�r FlaDa Signal "cam.ma_trig_window"	PET/SNH */
/* 14.03.01  00.06  Position zum Einkuppeln der Kurvenscheibe erst ganz am 				*/
/*					Ende festlegen											 		PET */
/* 27.03.01  00.07  Fehler MasterFIFO behoben, Inhibit-Bit eingebaut	 	SNH/PET/MOL */
/* 02.04.01  00.08  Slavetriggerfenster ver�ndert->Fehler:Slavetrg.behoben 	 			*/
/* 					Kurvenscheibenzustand beim Einschalten pr�fen ->					*/
/*					Etikettenband referenzieren, wenn n�tig   				 	SNH/PET */
/* 26.04.01  01.27  Etikettenl�cken�berwachung, Weitergabe des Rollenende-	 	PET/SNH */
/* 					signals parametrierbar.												*/
/* 22.08.01  01.42  ACOPOS_Aus_laeuft in den Schritten stop_bewegung nicht          	*/
/*					ausgeben - da diese im Handstart auch durchlaufen werden    	PET	*/
/* 06.09.01  01.47  Nach Handstart pr�fen des Ausschaltstatus des Kurvenautomaten   	*/
/*					unterbinden												    	PET */
/* 10.10.01  01.51  bei "Vorw�rts referenzieren" mu� vor dem Referenzieren datiert  	*/
/*					werden. Sprung von Schritt: BASIS_INIT nach MS_DATIERUNG_EIN 		*/
/*					und zur�ck von MS_W_DATIERUNG_AUS nach ABS_BEW1			    	PET */
/* 19.10.01  01.52  l�sst man die Man_Spend Taste nach dem Datieren los, so         	*/
/* 					wird die Bewegung abgebrochen, Folge ist das beim n�chsten			*/
/*					Man_Spend Vorgang nochmal datiert wurde.--> Variable				*/
/*					ein_Etikett_man_gespendet reset in Schritt MS_DATIERUNG_AUS     	*/ 
/*					und Abfrage der Var bei Manuell Spenden aus.                	SNH */
/* 30.10.01	 01.53  Handstart korrigiert, vor Bewegung pr�fen ob Regler 				*/
/*					eingeschaltet ist - wenn nicht einschalten						PET	*/
/* 06.11.01	 01.54  bei Datierungstyp 4 / 5 Kurvenscheibe mit Stillstand fahren SNH/PET */
/* 07.11.01	 01.54  Parameter�bergabe StartNSW (Fehler!) nach InitNSW verschoben 	PET */
/* 01.02.02	 01.71  Reglereinstellung in Abh�ngigkeit vom APS-Typ initialisieren     	*/
/*                  �berwachung ob Abroller l�uft wenn Tr�gerband gef�rdert wird 	PET */
/* 29.04.02  01.79  Spannungsversorgung an der Geberkarte AC130 fehlerhaft    	SNH/PET */
/*                  - dadurch koennen Gebertakte verloren gehen --> Unsynchronitaet  	*/
/*                  von Warnung auf Fehlerausgabe geaendert                          	*/
/* 15.05.02  01.81  "if"-Abfrage zur �berpr�fung ob Tr�gerband gef�rdert wird korr.  	*/
/* 					"=" -> "=="														PET */
/* 10.09.02	 01.94  Masterfifo wird nicht mehr gel�scht                          	PET */
/* 14.10.02	 02.50  Geberauswertung eingebaut			                         	SIS */
/* 22.10.02	 02.50  Ausschalten aktive bleibt h�ngen wenn Servowarnung kommt	 	PET */
/* 25.10.02	 02.50  Sicherheitsabfrage bei Schreiben �ber Service Kanal entferntPET/SNH */
/* 30.10.02	 02.50  Bearbeitung des Inhibit-Signals ge�ndert				 	PET/SNH */ 
/* 19.11.02  02.51  ACOPOS_Neustart_noetig Meldung f�r Fehlermeldung wegen			 	*/
/*					ACOPOS Fehler FIFO-�berlauf (Fehler Nr. 32037) entfernt. 		 	*/
/* 07.01.03  02.51	Neue Geschwindigkeitsaufschaltung f�r Auf-/Abroller.	    SNH/PET */
/* 30.01.03  02.52	Geschwindigkeitsaufschaltung in eigenes Task �bernommen    		PET */
/*                  Automatenabbruch im Fehlerfall durchf�hren, damit Automat keine		*/
/*                  weiteren Fehler produziert										PET */
/* 02.04.03  02.55	Autochange: cmd_inhibit_aus und cmd_inhibit_ein zur�cksetzen		*/
/*                  bei der Inhibit Auswertung.                                     SNH */
/* 03.04.03	 02.55	Die Anzahl der ACOPOS Warnungen, die zu einem Fehler f�hren,		*/
/*					kann nun mit dem Maschinenparameter Folgefehler eingestellt			*/
/*					werden. Bleibt weiterhin 0 oder 1 eingestellt, wird automatisch		*/
/*					der Defaultwert 8 angew�hlt.									SNH	*/
/* 03.04.03	02.55	Stoerung_Bandauslauf nicht ausgewertet, wenn Testbetrieb aktiv  SNH */
/* 08.04.03	02.55	Umschalten des Inhibit-Kommandos �berarbeitet                 	PET */
/* 08.04.03 02.56	Wenn Testbetrieb aktiv (PW = 999), dann wird beim Handspenden mit   */
/*					der in den Produktparametern eingestellten Leistung gefahren.   SNH */
/* 28.05.03	02.57	"Fehler_Geberueberwachung" eingef�gt, ETIMA soll sofort         SNH */
/*					stehenbleiben.														*/
/* 04.06.03 02.58	APS2 Antrieb schwingt im Stillstand:							SNH */
/*					-> Proportionalverst�rkung des Drehzahlreglers neu eingestellt 		*/
/*					p_ax_dat->regler.drehzahl.kv von 2 auf 1.5 ge�ndert.				*/
/*					Schleppfehlergrenzwert f�r Abbruch einer Bewegung von 1mm -> 2mm	*/
/*					Schleppfehlergrenzwert f�r Warnung von 0,5mm -> 1mm ge�ndert.		*/
/* 02.07.03 02.61	Statusbits beim Ein-/Ausschalten ge�ndert + Regler immer ein	PUA */
/* 02.08.03 02.62   Anzeige der Motordrehzahl korrigiert                            SNH */
/* 04.08.03 02.63	Anzahl Eti bis Rollenende Signal weitergegeben wird, umgestellt SNH */
/* 					auf Auswertung von Verklebungstyp 0 und 7. Bei beiden Typen         */
/*                  wird nicht verklebt. Bei Typ 7 ist zwar eine Verklebung vor-        */
/*					handen, diese wird aber nicht benutzt.								*/
/* 21.08.03 02.67	Diagnosewerte angepasst											PUA */
/*					Fehlernummer wird im ACOPOS-St�rbild mit angezeigt				PUA */
/* 12.09.03	02.71	�berbr�ckung Geberfehler w�hrend erster Teilung eingef�gt		HOG */
/* 19.01.04 02.75	Hand-Modus ge�ndert												HOG */
/* 30.01.04 02.76	Korrektur auf der AC130 f�r Leitgeber eingef�gt					PUA */
/* 22.03.04	02.78	Referenzieren der Masterachse: Ersten Vorgang ge�ndert, zweiten	HOG	*/
/*					eingef�gt. Vor dem Starten des zweiten wird eine definierte 		*/
/* 					Strecke gefahren (Problematik Zur�ckschwingen der Masch.)			*/
/* 01.07.04	02.79	Geschwindigkeit_Handspenden bei den Maschinenparametern hinzu-	HOG	*/
/*					gef�gt. Bei der Berechnung wird auf die H�lfte des maximal 			*/
/*					m�glichen begrenzt.												HOG	*/
/****************************************************************************************/

#include <bur/plc.h>				/*	Definitionen f�r RPS Programmierung	*/
#include <bur/plctypes.h>			/*	Definitionen f�r RPS Programmierung	*/ 

#include "acp10man.h"  				/* Funktionen, Datentypen und 			*/
									/* Konstanten f�r ACP10MAN.BR 			*/
#include "global.h"					/* Definition der globalen Variablen */
#include "slave_ax.h"

#include "parid.h"

/****************************************************************************/
/*		Defines																*/
/****************************************************************************/
#define ACP10_NODE 		1   /* Knotennummer des Antriebes */

/* Schrittschaltwerk Defines - Schrittnummern festlegen */	

#define ALLOC_STATUS_FEHLER 0

#define COMMAND			10

#define REGLER_AUSSCH 	20
#define W_REGLER_AUS	25
#define GLOBAL_INIT		30
#define W_GLOBAL_INIT	31
#define	NSW_IO_CONFIG	32
#define	W_NSW_IO_CONFIG	33
#define	NSW_INIT		34
#define NSW_READ_MA_POS	35
#define	W_NSW_INIT		36
#define	NSW_START		38
#define	W_NSW_START		39
#define SIM_MODUS		40
#define W_SIM_STATUS	50
#define	HW_LIM_IGNORE	52
#define W_HW_LIM_IGNORE	53
#define REGL_INIT		54
#define W_REGL_INIT		55
#define	REGL_MODE		56
#define	W_REGL_MODE		58
#define CHECK_PARAMETER	60
#define DIG_E_INIT		70
#define W_DIG_E_INIT	75
#define REGLER_EINSCH	80
#define W_REGLER_EIN	85
#define REF_START		90
#define W_REF_OK		95
#define READ_EDGE_1		100
#define	W_READ_EDGE_1	105
#define	BASIS_INIT		110
#define	ABS_BEW1		115
#define	READ_EDGE_2		120
#define	W_READ_EDGE_2	125
#define	READ_EDGE_POS	130
#define	W_READ_EDGE_POS	135
#define	W_POS_END		140

#define CAM_CTRL_AUS 	160
#define W_CAM_CTRL_AUS 	165

#define STOP_BEWEGUNG				170
#define W_STOP_BEWEGUNG				175
#define STOP_BEW_LESE_CAM_STATE 	180
#define W_STOP_BEW_LESE_CAM_STATE 	185
#define STOP_BEW_NSW_AUS 			190
#define W_STOP_BEW_NSW_AUS 			195
#define STOP_BEW_REGLER_AUS 		200
#define W_STOP_BEW_REGLER_AUS 		205
#define STOP_CAM_AUT_ABBRUCH		210
#define W_STOP_CAM_AUT_ABBRUCH		215

#define	ENCODER2_INIT		300

#define	MASTER_REF_LESE_POS		303		/*V02.78*/
#define	W_MASTER_REF_LESE_POS	304		/*V02.78*/
#define	MASTER_REF_LESE_POS2	305		/*V02.78*/
#define	W_MASTER_REF_LESE_POS2	306		/*V02.78*/
#define	MASTER_REF_INIT			307		/*V02.78*/
#define	W_MASTER_REF_INIT		308		/*V02.78*/

#define	ENCODER2_PAR		310
#define	W_ENCODER2_PAR		320
#define	READ_ENC2_HOME_OK	325
#define	W_READ_ENC2_HOME_OK	330
#define	ENCODER1_INIT		335
#define	W_ENCODER1_INIT		340

#define CHECK_MASTER_POS		400
#define W_CHECK_MASTER_POS		405

#define	C_GEAR_RESTART				410
#define	W_CLEAR_SL_TRIG_FIFO 		415
#define RESTART_LESE_MASTER_POS		420
#define W_RESTART_LESE_MASTER_POS	425
#define RESTART_LESE_CAM_STATE 		430
#define W_RESTART_LESE_CAM_STATE 	435
#define	CAM_CTRL_EIN 				440
#define	W_CAM_CTRL_EIN 				445

#define	COMP_GEAR_INIT	500
#define	COMP_GEAR_PAR	510
#define	W_COMP_GEAR_PAR	515

#define	C_GEAR_START			520
#define	C_GEAR_START_W			525
#define	C_GEAR_START_1			530
#define	C_GEAR_START_SND_MA_POS	535
#define	C_GEAR_START_SIGNAL		540
#define	W_C_GEAR_START			545

#define SEND_CAM_SL_ADD_EL 		560
#define W_SEND_CAM_SL_ADD_EL 	565

#define	CAM_INHIBIT_EIN 	570
#define	W_CAM_INHIBIT_EIN 	575

#define	CAM_INHIBIT_AUS 	580
#define	W_CAM_INHIBIT_AUS 	585

#define	MANUELL_SPENDEN			600
#define	MS_DATIERUNG_ZEIT_EIN	605
#define	MS_DATIERUNG_EIN		610
#define	MS_W_DATIERUNG_EIN		620
#define	MS_W_ZEIT				630
#define	MS_DATIERUNG_AUS		640
#define	MS_W_DATIERUNG_AUS		650
#define	MS_INIT_BEWEG			660
#define	MS_READ_TRIGGER_POS 	670
#define	MS_W_READ_EDGE_POS 		680
#define	MS_W_REL_POS			690
#define CHECK_VISION_OUT		695

#define WALZENVERRIEGELUNG_ZU		700
#define W_WALZENVERRIEGELUNG_ZU		710
#define WALZENVERRIEGELUNG_AUF		720
#define W_WALZENVERRIEGELUNG_AUF	730
#define VISION_OUT_ON				740
#define VISION_OUT_OFF				745

#define	FEHLER				900
#define	FEHLER_W_ABBRUCH	910
#define	FEHLER_TEXT			920
#define FEHLER_INIT			930
#define W_FEHLER_TEXT		940
#define FEHLER_QUIT			950
#define NEUSTART			960

#define READ_DIAG_INIT		1000
#define READ_DIAG_DATA		1010
#define W_READ_DIAG_DATA	1020

/****************************************************************************/
/*		lokale Typdefinitionen												*/
/****************************************************************************/
_LOCAL	struct {
	UI4_TYP	encoder_type;
	UI4_TYP	scale_inc;
	UI4_TYP scale_units;
	UI4_TYP scale_rev;
	I4_TYP s_act;
	UI1_TYP count_dir;
	K6_TYP homing;
	UI4_TYP homing_ok;
	UI4_TYP	ref_chk_window;
	UI4_TYP	ref_width;
	UI4_TYP	ref_interval;
	UI2_TYP	ref_check_mode;
} encoder2;			
_LOCAL struct {
	I4_TYP rise_edge_cnt;
	I4_TYP fall_edge_cnt;
	I4_TYP rise_edge_s_act;
	I4_TYP fall_edge_s_act;
} trigger1;
_LOCAL struct {
	I4_TYP rise_edge_cnt;
	I4_TYP fall_edge_cnt;
	I4_TYP rise_edge_s_act;
	I4_TYP fall_edge_s_act;
} trigger2;


/****************************************************************************/
/*		Variablendeklaration												*/
/****************************************************************************/
_LOCAL	I4_TYP pctrl_mode_switch;

_LOCAL	UI1_TYP hw_lim_ignore;
			
_LOCAL  ACP10ACHSE_typ 	*p_ax_dat;      /* Pointer auf die Achs-Objekt-Daten */ 

_LOCAL  UDINT          	ax_obj;         /* Achs-Objekt (NC-Objekt-Pointer) */

_LOCAL	UINT           	alloc_status;   /* Status von ncalloc() */
_LOCAL	UINT           	action_status;  /* Status von ncaction() */
_LOCAL	UINT           	dattyp_error;   /* Datentypfehler (Gr��e des NC-Datentyps in "acp10man.h") */
_LOCAL	UINT 			errorstep;		/* Fehlerschritt */
_LOCAL	UINT			step_alt;			/* Letzter Schritt im Schrittschaltwerk */ 
_LOCAL	UINT			step_debug_lfd_nr;	/* Aktueller Zaehler im Schrittfeld */
_LOCAL	UINT			step_debug[100];		/* Feld mit den letzten Schritten */ 
 	
_LOCAL	UINT           	version_error;  /* Versionsfehler (Version aus "acp10man.h") */

_LOCAL	USINT			fehler_halt;	/* Vorwahl: Bewegung Abbruch bei Fehler */
_LOCAL	USINT			regler_aus;		/* Vorwahl: Regler ausschalten bei Fehler */ 
_LOCAL	USINT			simulation;		/* Vorwahl: Simulation ein/aus */
_LOCAL  USINT			vision_out_time;
_LOCAL	USINT			record_index;
_LOCAL	DINT			stop_pos_record[10];

_LOCAL 	plcbit			stop_bewegung; 	/* Abbrechen einer aktiven Bewegung */
_LOCAL	USINT			parameter;
_LOCAL	FEHLER_TEXT_typ	meldung_text;
_LOCAL	CAM_DATA_typ	cam;

_LOCAL	plcbit			global_init;
_LOCAL	plcbit			comp_gear_init;
_LOCAL	plcbit			comp_gear_init_ok;
_LOCAL	plcbit			comp_gear_start;
_LOCAL	plcbit			comp_gear_restart;
_LOCAL	plcbit			comp_gear_run;
_LOCAL	plcbit			comp_gear_stop;
_LOCAL	plcbit			enc2_init;
_LOCAL	plcbit			enc2_init_ok;
_LOCAL	plcbit			chk_enc2_home;
_LOCAL	plcbit			enc2_home_ok;
_LOCAL	plcbit			enc1_init;
_LOCAL	plcbit			enc1_init_ok;
_LOCAL	plcbit			ref_start;
_LOCAL	plcbit			nsw_start_cmd;
_LOCAL	plcbit			nsw_start_ok;
_LOCAL	plcbit			nsw_init_cmd;
_LOCAL	plcbit			nsw_init_ok;
_LOCAL	plcbit			nsw_io_config_cmd;
_LOCAL	plcbit			nsw_io_config_ok;
_LOCAL	plcbit			service_setzen;
_LOCAL	plcbit			service_lesen;

_LOCAL	plcbit			Geberueberw_Gebrueckt_old_lokal;
_LOCAL	DINT			geberueberw_gebrueckt_ax_pos;
_LOCAL  DINT			servo_travel;

_LOCAL	DINT			master_pos_ref_start;		/*Zwischenspeichern der aktuellen Masterposition*/			/*V02.78*/
_LOCAL	DINT			master_pos_ref_akt;			/*Aktuelle Masterposition*/									/*V02.78*/

_LOCAL 	float			handspenden_Teiler;		/*Zur Berechnung des Zwischenergebnisses beim Handspenden*/		/*V05.06*/

_LOCAL	UDINT			Grobtakt_alt;
_LOCAL	UDINT			akt_et_cnt;
_LOCAL	UDINT			letzter_et_cnt;
_LOCAL	UDINT			ZeitBasisAufsch;
_LOCAL	plcbit			BA_Auto_old_lokal;
_LOCAL	plcbit			etikett_ref;
_LOCAL	plcbit			etikett_ref_ok;
_LOCAL	plcbit			dig_e_init;
_LOCAL	plcbit			dig_e_init_ok;
_LOCAL	plcbit			regler_ein;
_LOCAL	plcbit			Spenden_Manuell_old_lokal;
_LOCAL	plcbit			spenden_manuell_cmd;
_LOCAL	plcbit			achse_ref;
_LOCAL	plcbit			achse_ref_ok;
_LOCAL	DINT			et_anfang;
_LOCAL	DINT			et_ende;
_LOCAL	DINT			lue_ende;
_LOCAL	DINT			et_laenge;
_LOCAL	DINT			lue_laenge;
_LOCAL	DINT			ma_s_start_allg;
_LOCAL	DINT			sl_s_trig_allg;
_LOCAL	DINT			trigger2_r_e_c_n;
_LOCAL	DINT			trigger2_f_e_c_n;
_LOCAL	DINT			trigger_pos;
_LOCAL	DINT			ma_s_act;
_LOCAL	DINT			ma_s_act_check;
_LOCAL	DINT			ma_s_act_start_1;
_LOCAL	DINT			ma_s_act_start_2;
_LOCAL	DINT			ma_s_act_start;
_LOCAL	DINT			ma_s_act_restart;
_LOCAL	DINT			ma_s_act_nsw;
_LOCAL  UINT			warte_diag;
_LOCAL  UINT			Zeit_Ref_nicht_mgl;
_LOCAL  UINT			Zeit_Stop_Bew_fertig;
_LOCAL  UINT			Zeit_Eti_Trigger;
_LOCAL  UINT			Zeit_Datierung_Ein;
_LOCAL  UINT			Zeit_Datierung_Aus;
_LOCAL  UINT			Zeit_lese_ma_s_act;
_LOCAL  float 			float_diag_daten;
_LOCAL  long 			long_diag_daten;
_LOCAL  float			Max_Bandgeschw;
_LOCAL  UINT			lfd_Nr_Bandgeschw;
_LOCAL	DINT			encoder2_homing_s;
_LOCAL	USINT			encoder2_homing_mode;
_LOCAL  long			daten_ausgaenge;
_LOCAL  long			visionioadr;
_LOCAL	plcbit			datierung_aus;
_LOCAL  UINT			anz_man_gespendet;
_LOCAL	plcbit			read_diag_init;
_LOCAL	plcbit			read_diag_cmd;
_LOCAL	DINT			ma_s_act_letzter_stop;		/* Masterposition beim Ausschalten des Aggregates */
_LOCAL	unsigned short	read_diag_id;
_LOCAL	unsigned short	letzte_read_diag_id;
_LOCAL	DINT			read_diag_gelesen;			/* letzter gelesener Wert */
_LOCAL	DINT			read_diag_datentyp;
_LOCAL	plcbit			Nachtrigger_war_da;
_LOCAL	plcbit			HM_ZW_Nachtrigger_war_da;
_LOCAL	plcbit			Flada_war_da;
_LOCAL	plcbit			HM_ZW_Flada_war_da;
_LOCAL  UINT			Anz_ACOPOS_Warnung;
_LOCAL  UINT			Anz_ACOPOS_Warnung_trig_missing;
_LOCAL  UINT			Anz_ACOPOS_Warnung_trig_out_of_w;
_LOCAL	DINT			Pos_ACOPOS_Warnung; 
_LOCAL	DINT			Laenge_Maschinenteilung_ber;
_LOCAL	plcbit			check_parameter;
_LOCAL	UINT			cmd_cam_ctrl_data;
_LOCAL	plcbit			cam_ma_add_el;
_LOCAL	UINT			anz_versuche_cam_ctrl_aus;
_LOCAL 	DINT			Pos_letzter_Flada;
_LOCAL 	DINT			Pos_letztes_Etikett;
_LOCAL	UINT			anz_eti; 
_LOCAL	DINT			pos_ref_servo;
_LOCAL  DINT			Max_Drehzahl;
_LOCAL  UINT			lfd_Nr_Drehzahl;
_LOCAL	plcbit			pruefe_master_pos;
_LOCAL  UINT			zeit_pruefe_master_pos;
_LOCAL  UINT			zeit_read_diag;
_LOCAL	UDINT			cmd_inhibit_data;
_LOCAL	plcbit			cmd_inhibit_ein;
_LOCAL	plcbit			cmd_inhibit_aus;
_LOCAL	plcbit			inhibit_ist_ein;
_LOCAL  UINT			cmd_NSW_data;
_LOCAL	USINT			cam_state_data;
_LOCAL	plcbit			cmd_check_restart;
_LOCAL	DINT			pos_servo_rollenende;
_LOCAL	DINT			pos_servo_ende_etikett; 
_LOCAL	plcbit			eti_ref_ausgefuehrt;
_LOCAL	plcbit			ref_im_handstart_ausgefuehrt;
_LOCAL	plcbit			HM_Walzen_verriegeln_old_lokal;
_LOCAL	plcbit			walzen_zu_cmd;
_LOCAL	plcbit			walzen_auf_cmd;
_LOCAL	plcbit			etikett_ref_aktiv;
_LOCAL	DINT			pos_stop_aufroller;
_LOCAL	DINT			pos_rollenwechsel;
_LOCAL	long			dio_state_data;
_LOCAL	DINT			servo_stop_pos;

_LOCAL	USINT			cmd_clear_trig_fifo_data;
_LOCAL  USINT			cam_state_letzter_stop;
_LOCAL	DINT			pos_stop_aufroller;

_LOCAL  UINT			zeit_c_gear_start;
_LOCAL  UINT			zeit_c_gear_start_diag;
_LOCAL  UINT			par_zeit_c_gear_start;
_LOCAL	plcbit			cmd_cam_aut_abbruch;

_LOCAL	DINT			test1;
_LOCAL	DINT			test2;
_LOCAL  float			v_max_etikettenband;
_LOCAL	plcbit			ba_auto_aus_laeuft;

_LOCAL STRING			info_string[20];
_LOCAL STRING			nummer_string[5];

/****************************************************************************/
/*		Initialisierungs-UP													*/
/****************************************************************************/
void _INIT ax_init(void)
{
	ACOPOS_Neustart_noetig = 0;
	
    alloc_status = ncalloc(ncACP10MAN,ACP10_NODE,ncACHSE,1,(UDINT)&ax_obj);

    if ( ( alloc_status != ncOK ) && ( alloc_status != 10600 ) )
    {
    	/* Der NC-Objekt-Pointer ist ung�ltig */
		return;
	}

	p_ax_dat = (ACP10ACHSE_typ*)ax_obj;

    if ( p_ax_dat->size != sizeof(ACP10ACHSE_typ) )
    {
    	/* Der NC-Datentyp ist nicht kompatibel zum NC-Manager */
		dattyp_error = sizeof(ACP10ACHSE_typ);
		return;
	}
	else
	{
		dattyp_error = 0;
	}
		
    if ( ( p_ax_dat->sw_version.nc_manager&0xFFF0 ) != ( ACP10MAN_H_VERSION&0xFFF0 ) )
    {
    	/* Die Version ist nicht kompatibel zum NC-Manager */
		version_error = ACP10MAN_H_VERSION;
		return;
	}
	else
	{ 
		version_error = 0;
	}

	/***************************************************/
	/* Zuweisen der PAR-ID�s f�r Kurvenscheibenautomat */
	/***************************************************/
	pctrl_mode_switch.id	= ACP10PAR_PCTRL_MODE_SWITCH;
	
	hw_lim_ignore.id		= ACP10PAR_LIMIT_SWITCH_IGNORE;
	
	cam.cmd_start.id		= ACP10PAR_CMD_CAM_START;
	cam.ma_axis.id			= ACP10PAR_CAM_MA_AXIS;
	
	cam.ma_trig_mode.id		= ACP10PAR_CAM_MA_TRIG_MODE;
	cam.sl_trig_mode.id		= ACP10PAR_CAM_SL_TRIG_MODE;
	cam.comp_gear_type.id	= ACP10PAR_CAM_COMP_GEAR_TYPE;
	cam.ma_v_max.id			= ACP10PAR_CAM_MA_V_MAX; 
	cam.ma_s_start.id		= ACP10PAR_CAM_MA_S_START;
	cam.ma_s_sync.id		= ACP10PAR_CAM_MA_S_SYNC;
	cam.ma_s_comp.id		= ACP10PAR_CAM_MA_S_COMP;
	cam.ma_s_trig.id		= ACP10PAR_CAM_MA_S_TRIG;
	cam.ma_comp_trig_mode.id= ACP10PAR_CAM_MA_COMP_TRIG_MODE;
	cam.ma_s_comp_trig.id	= ACP10PAR_CAM_MA_S_COMP_TRIG;
	cam.ma_comp_trig_window.id	= ACP10PAR_CAM_MA_COMP_TRIG_WINDOW;
	cam.ma_trig_window.id	= 493;
	cam.sl_s_sync.id		= ACP10PAR_CAM_SL_S_SYNC;
	cam.sl_s_comp.id		= ACP10PAR_CAM_SL_S_COMP;
	cam.sl_s_trig.id		= ACP10PAR_CAM_SL_S_TRIG;
	cam.sl_s_comp_min.id	= ACP10PAR_CAM_SL_S_COMP_MIN;
	cam.sl_s_comp_max.id	= ACP10PAR_CAM_SL_S_COMP_MAX;
	cam.sl_trig_window.id	= ACP10PAR_CAM_SL_TRIG_WINDOW;
	cam.sl_trig_iv_min.id	= ACP10PAR_CAM_SL_TRIG_IV_MIN;
	cam.sl_trig_iv_max.id	= ACP10PAR_CAM_SL_TRIG_IV_MAX;
	cam.ma_add_el.id		= ACP10PAR_CAM_MA_ADD_EL;

	encoder2.encoder_type.id	= ACP10PAR_ENCOD2_TYPE;	
	encoder2.scale_inc.id		= ACP10PAR_SCALE_ENCOD2_INCR;	/* Inkremente pro 1 Umdr. */
	encoder2.scale_units.id		= ACP10PAR_SCALE_ENCOD2_UNITS;
	encoder2.scale_rev.id		= ACP10PAR_SCALE_ENCOD2_REV;
	encoder2.count_dir.id		= ACP10PAR_ENCOD2_COUNT_DIR;
	encoder2.s_act.id			= ACP10PAR_ENCOD2_S_ACT;
	encoder2.homing.id			= ACP10PAR_CMD_ENCOD2_HOMING;
	encoder2.homing_ok.id		= ACP10PAR_STAT_ENC2_HOMING_OK;
	encoder2.ref_chk_window.id	= ACP10PAR_ENCOD2_REF_CHK_WINDOW;
	encoder2.ref_width.id		= ACP10PAR_ENCOD2_REF_WIDTH;
	encoder2.ref_interval.id	= ACP10PAR_ENCOD2_REF_INTERVAL;
	encoder2.ref_check_mode.id	= ACP10PAR_ENCOD2_REF_CHK_MODE;
	
	trigger1.rise_edge_cnt.id 	= PARID_TRIG1_RISE_EDGE_COUNT;
	trigger1.fall_edge_cnt.id 	= PARID_TRIG1_FALL_EDGE_COUNT;
	trigger1.rise_edge_s_act.id = PARID_TRIG1_RISE_EDGE_S_ACT;
	trigger1.fall_edge_s_act.id = PARID_TRIG1_FALL_EDGE_S_ACT;
	trigger2.rise_edge_cnt.id 	= PARID_TRIG2_RISE_EDGE_COUNT;
	trigger2.fall_edge_cnt.id 	= PARID_TRIG2_FALL_EDGE_COUNT;
	trigger2.rise_edge_s_act.id = PARID_TRIG2_RISE_EDGE_S_ACT;
	trigger2.fall_edge_s_act.id = PARID_TRIG2_FALL_EDGE_S_ACT;
	

	/*	Setzen der notwendigen Initialisierungsdaten	*/
	pctrl_mode_switch.data	= 1;	/* differenzielle Aufschaltung	*/
	hw_lim_ignore.data = 1;			/* ignoriere Hardwareendschalter */

	/* Initialisierungswerte der einzelnen Variablen */
	simulation 	= ncAUS;				/* Simulationsmodus ein oder ausschalten */
	step_slave_ax 		= REGLER_AUSSCH;		/* Initialisierung Starten */
	errorstep 	= 0;					/* Fehlerschritt zur�cksetzen */
	stop_bewegung	= 0; 				/* Abbruch einer aktiven Bewegung zur�cksetzen */
	
	etikett_ref = 0;
	etikett_ref_ok = 0;
	achse_ref = 0;
	achse_ref_ok = 0;
	dig_e_init = 0;
	dig_e_init_ok = 0;
 	comp_gear_init = 0;
 	comp_gear_init_ok = 0;
 	nsw_start_cmd = 0;
 	nsw_start_ok = 0;
 	nsw_init_cmd = 0;
 	nsw_init_ok = 0;
 	nsw_io_config_cmd = 0;
 	nsw_io_config_ok = 0;
	comp_gear_start = 0;
	comp_gear_restart = 0;  
	comp_gear_stop = 0;
	comp_gear_run = 0;
	enc1_init = 0;
	enc1_init_ok = 0;
	enc2_init = 0;
	enc2_init_ok = 0;
	chk_enc2_home = 0;
	enc2_home_ok = 0;
	ref_start = 0;	
	global_init = 1;
	check_parameter = 0;
	Parameter_init = 1;
	Enc2_Ref_Fahrt = 0;
	cam_ma_add_el = 0;
	servo_stop = 0;
	servo_stop_pos = 0;
	vision_off_counts = 0;
	Geberueberw_Gebrueckt_old_lokal = 0;
	geberueberw_gebrueckt_ax_pos = 0;
	
	letzter_et_cnt = Grobtakt_alt;
	BA_Auto_old_lokal = 0;
	Spenden_Manuell_old_lokal = 0;
	spenden_manuell_cmd = 0;
	datierung_aus = 0;
	anz_versuche_cam_ctrl_aus = 0;

	
	ACOPOS_Fehler = 0;
	ACOPOS_Warnung = 0;
	ACOPOS_Aus_laeuft = 0;
	Anz_ACOPOS_Warnung = 0;
	Pos_ACOPOS_Warnung = 0;
	Parameterwerte_falsch = 0;
	cmd_inhibit_ein = 0;
	inhibit_ist_ein = 0;
	ref_im_handstart_ausgefuehrt = 0;
	HM_Walzen_verriegeln_old_lokal = 0;
	walzen_zu_cmd = 0;
	walzen_auf_cmd = 0;
	etikett_ref_aktiv = 0;
	eti_ref_ausgefuehrt = 0;
	zeit_c_gear_start = 0;
	zeit_c_gear_start_diag = 0;
	par_zeit_c_gear_start = 4;
	cmd_cam_aut_abbruch = 0;
	Fehler_Geberueberwachung = 0;
	visionioadr = 0x00000040;
	
}

/****************************************************************************/
/*		zyklischer Teil      												*/
/****************************************************************************/
void _CYCLIC ax_cyclic(void)
{

    /* "acp10man.h" ist nicht kompatibel zum NC-Manager */
	if ( dattyp_error || version_error )
		return;
	
    if ( alloc_status != ncOK )
	{
        if ( ( alloc_status == 10600 ) && ( ACOPOS_Neustart_noetig == 0 ) )
		{
        	/* Wenn die Funktion ncalloc() den Wert 10600 liefert, dann ist das */
			/* Ergebnis in "nc_object" g�ltig und in den Fehlers�tzen des NC-Objekts */
        	/* wurden zus�tzliche Fehler eingetragen. Die einzig erlaubte NC-Aktion ist */
        	/* nun "ncaction(nc_object,ncFEHLER,ncQUITTIEREN)" (zum Lesen der Fehler). */

			errorstep = step_slave_ax;
			step_slave_ax = FEHLER_TEXT;
			ACOPOS_Fehler = 1;
			ACOPOS_Neustart_noetig = 1;  /* Neustart erforderlich!!!!!!! */
		}
	}

	/***************************************************************************************************************************/
	/* Geberfehler wird w�hrend der ersten 4 Teilungen �berbr�ckt, da St�rungen auf der Geberleitung diese beeinflussen k�nnen */
	/* "Geberueberw_Gebrueckt" wird auch im Aggregat-Task gesetzt, damit es bei Netz-Ein bereits aktiv ist 					   */
	/***************************************************************************************************************************/
	if ( Geberueberw_Gebrueckt == 1 )
	{
		if ( Geberueberw_Gebrueckt_old_lokal == 0 ) 
		{
			geberueberw_gebrueckt_ax_pos = p_ax_dat->monitor.s;	/* Zwischenspeicherung der Axposition beim Einschalten zur Deaktivierung Geber�berwachung */
			Geberueberw_Gebrueckt_old_lokal = 1;
		}
		if ( ( (p_ax_dat->monitor.s - geberueberw_gebrueckt_ax_pos) > (4 * Akt_Produkt_Param.Laenge_Slave_Intervall) ) || ( (p_ax_dat->monitor.s - geberueberw_gebrueckt_ax_pos) < (-4 * Akt_Produkt_Param.Laenge_Slave_Intervall) ) ) 
		{
			Geberueberw_Gebrueckt = 0;
			geberueberw_gebrueckt_ax_pos = 0;
		}
	}
	
	/********************/
	/*	Automatik ein	*/
	/********************/

	if ((BA_Auto == 1) && (BA_Auto_old_lokal == 0))
	{
		if (ba_auto_aus_laeuft == 1) 	/* Ausschalten ist gerade aktiv und kann abgebrochen werden */
		{
			ba_auto_aus_laeuft = 0;
			ACOPOS_Aus_laeuft = 0;
		}
		else
		{
			ACOPOS_Ein_laeuft = 1;
	
			regler_ein = 1;		/* Regler einschalten, wenn noch nicht ein. (Im Schritt) */

			ref_im_handstart_ausgefuehrt = 0;
			etikett_ref = !etikett_ref_ok;	/* Etikettenreferenzfahrt kann immer ausgef�hrt werden */
			etikett_ref_aktiv = 0;
	
			if (comp_gear_run == 1) 
			{
				if (ref_im_handstart_ausgefuehrt == 1) 
				{
					cmd_check_restart = 0;		/* Pr�fung nicht notwendig, da als letztes Handstart ausgef�hrt wurde. - Etikett referenziert */
				}
				else
				{
					cmd_check_restart = 1; 		/* Pr�fen in welchem Zustand der Kurvenscheibenautomat verlassen wurde. - Evtl. Eti. referezieren */
				}
				comp_gear_restart = 1;  /* Bei aktiver Kurvenscheibenkopplung Regler wieder einschalten. */		
			}
			else
			{
				/* Kurvenscheibenkopplung neu einschalten */
				cmd_cam_aut_abbruch = 1;
				check_parameter = 1;
				dig_e_init = !dig_e_init_ok;
				achse_ref = !achse_ref_ok;
				enc1_init = !enc1_init_ok; 
				enc2_init = !enc2_init_ok; 
				nsw_io_config_cmd = 1;
				nsw_init_cmd = 1;
				nsw_start_cmd = 1;
				comp_gear_init = 1;
				comp_gear_start = 1;
				if (enc2_home_ok == 0)	/*Beim Handspenden wird chk_enc2_home zur�ckgesetzt. Muss hier manuell wieder gesetzt werden.	V02.78*/
				{
					chk_enc2_home = 1;	
				}
			}
			Anz_ACOPOS_Warnung_trig_missing = 0;
			Anz_ACOPOS_Warnung_trig_out_of_w = 0;
			BA_Auto_old_lokal = 1;
			Anz_ACOPOS_Warnung = 0;
		}
	}	

	/********************/
	/*	Automatik aus	*/
	/********************/

	if ( (BA_Auto == 0) && (BA_Auto_old_lokal == 1) )
	{
		ba_auto_aus_laeuft = 1;		
		comp_gear_stop = 1;
		comp_gear_restart = 0;
		cmd_check_restart = 0;
		nsw_start_cmd = 0;
		nsw_init_cmd = 0;
		comp_gear_start = 0;
		achse_ref = 0;
		etikett_ref = 0;
		eti_ref_ausgefuehrt = 0;
		regler_ein = 0;
		enc1_init = 0; 
		enc2_init = 0; 
		comp_gear_init = 0;
		comp_gear_start = 0;
		Zeit_lese_ma_s_act = 0;
		BA_Auto_old_lokal = 0;
		ACOPOS_Ein_laeuft = 0;
	}

	/**********************/
	/*	Manuelles Spenden */
	/**********************/

	if ( (Spenden_Manuell == 1) && (Spenden_Manuell_old_lokal == 0) )
	{
		regler_ein = 1;		/* Regler einschalten, wenn noch nicht ein. (Im Schritt) */

		check_parameter = 1;	
		spenden_manuell_cmd = 1;
		dig_e_init = !dig_e_init_ok;
		achse_ref = !achse_ref_ok;
		etikett_ref = !etikett_ref_ok; 
		etikett_ref_aktiv = 0;
		if ( comp_gear_run == 0 )
		{
			enc1_init = !enc1_init_ok; 
			enc2_init = !enc2_init_ok; 
		}
		anz_man_gespendet = 0;
		nsw_io_config_cmd = 1; 			/* NSW initialisiern, damit Druckerausgang und Walzenverriegelung geforct werden k�nnen. */
		ein_Etikett_man_gespendet = 0;
		ref_im_handstart_ausgefuehrt = 1; /* f�r erneuten Start Automatik merken */
		man_Eti_spenden_laeuft = 1;

		Spenden_Manuell_old_lokal = 1;
	}	
	
	if ( (Spenden_Manuell == 0) && (Spenden_Manuell_old_lokal == 1) && (ein_Etikett_man_gespendet == 1) )
	{
		spenden_manuell_cmd = 0;
		stop_bewegung = 1;
		datierung_aus = 1;
		dig_e_init = 0;
		regler_ein = 0;
		achse_ref = 0;
		enc1_init = 0; 
		enc2_init = 0; 
		Spenden_Manuell_old_lokal = 0;
		ein_Etikett_man_gespendet = 0;
		man_Eti_spenden_laeuft = 0;
	}
	/*******************************************/
	/*	Antriebswalzenverriegelung Einschalten */
	/*******************************************/
	if ((HM_Walzen_verriegeln == 1) && (HM_Walzen_verriegeln_old_lokal == 0) && (APS_2 == 1))
	{
		HM_Walzen_verriegeln_old_lokal = 1;
		Walzen_verriegelt = 1;
		nsw_io_config_cmd = 1; 			/* NSW initialisiern, damit Druckerausgang und Walzenverriegelung geforct werden k�nnen. */
		walzen_zu_cmd = 1;
	}
		
	/*******************************************/
	/*	Antriebswalzenverriegelung Ausschalten */
	/*******************************************/
	if ((HM_Walzen_verriegeln == 0) && (HM_Walzen_verriegeln_old_lokal == 1) && (APS_2 == 1))
	{
		HM_Walzen_verriegeln_old_lokal = 0;
		Walzen_verriegelt = 0;
		walzen_auf_cmd = 1;
	}

	/*****************************************************************************************/
	/*	Wenn Walzen offen sind muss beim Starten von Autobetrieb zwingend referenziert werden*/
	/*****************************************************************************************/
	if ((HM_Walzen_verriegeln == 0) || (Walzen_verriegelt == 0))
	{
		etikett_ref_ok = 0;	
	}

	/**************************************/
	/*	Diagnosedaten vom Umrichter lesen */
	/**************************************/

	if ( ((read_diag == 1) || (bus_read_diag == 1))&& (read_diag_init == 0) )
	{
		enc1_init = !enc1_init_ok; 
		enc2_init = !enc2_init_ok; 
		read_diag_init = 1;
	}	
	
	if ( (read_diag == 0) && (bus_read_diag == 0) )
	{
		read_diag_init = 0;
	}
	
	if (read_diag_init == 1)
	{
		zeit_read_diag = zeit_read_diag + 1;
		
		if (zeit_read_diag > 1)  /* versuchsweise Zeit �berbr�ckt */
		{
			read_diag_cmd = 1;
			zeit_read_diag = 1;
		}	
	}
	
	/********************************************************************************/
	/*	additives Element f�r ma_s_start w�hrend Automatikbetrieb zum ACOPOS senden */
	/********************************************************************************/

	if (Startpos_geaendert == 1)
	{
		cam_ma_add_el = 1;
		Startpos_geaendert = 0;
	}	
	
	/***********************************************************************************************/
	/* Parameter wurden vom Bediener ge�ndert oder Fehlerfall --->>>> Variablen neu initialisieren */
	/***********************************************************************************************/

	if ( (Parameter_init == 1) || (ACOPOS_Fehler == 1) || (Zuviele_Warnungen_ACOPOS == 1) || (Eti_Trigger_fehlt == 1) || (Fehler_Etikettenbandriss == 1) || (SU_Geoeffnet == 1)  || (Reset_comp_gear_run == 1))
	{
		/* bei aktiver Bewegung oder eingeschalteten Lageregler stoppen */
		if ( ((p_ax_dat->bewegung.modus != ncAUS) && (p_ax_dat->bewegung.modus != ncABBRUCH)) || (p_ax_dat->regler.status == ncEIN) )
		{
			stop_bewegung = 1; 
		} 
		else
		{
			ACOPOS_Aus_laeuft = 0;
		}
		
		/* Bei neuen Parametern oder ACOPOS - Fehler Kurvenscheibenautomat das n�chste Mal neu initialisieren. */
		if ( ((Parameter_init == 1) || (ACOPOS_Fehler == 1) || (Reset_comp_gear_run == 1) || (Zuviele_Warnungen_ACOPOS == 1) ) && (comp_gear_run == 1) )
		{
			cmd_cam_aut_abbruch = 1;  
		}		
		Parameter_init = 0;
		dig_e_init_ok = 0;
		achse_ref_ok = 0;
		etikett_ref_ok = 0;
		enc1_init_ok = 0; 
		enc2_init_ok = 0; 
		comp_gear_init_ok = 0;
		nsw_init_ok = 0;

		dig_e_init = 0;				/* noch anstehende Kommandos zur�cknehmen */
		regler_ein = 0;
 		etikett_ref = 0;
		enc1_init = 0; 
		enc2_init = 0; 
		nsw_init_cmd = 0;
		nsw_start_cmd = 0;
		comp_gear_init = 0;
		comp_gear_start = 0;
		Enc2_Ref_Fahrt = 0;
		spenden_manuell_cmd = 0;
		Spenden_Manuell_old_lokal = 0;
		ein_Etikett_man_gespendet = 0;
		man_Eti_spenden_laeuft = 0;
		ACOPOS_Ein_laeuft = 0;
		Reset_comp_gear_run = 0;
	}
	
	/*******************************************/
	/*	Inhibit ein	-> nicht mehr etikettieren */
	/*******************************************/

	if ((I_Inhibit == 1) && (inhibit_ist_ein == 0))
	{
		cmd_inhibit_ein = 1;
		inhibit_ist_ein = 1;
		cmd_inhibit_aus = 0;	/*V2.55*/
	}	

	/***************************************/
	/*	Inhibit aus	-> wieder etikettieren */
	/***************************************/

	if ( (I_Inhibit == 0) && (inhibit_ist_ein == 1) )
	{
		cmd_inhibit_aus = 1;
		inhibit_ist_ein = 0;
		cmd_inhibit_ein = 0;	/*V2.55*/
	}

	/********************************************************************************/
	/* Auf Achsfehler pr�fen - Schrittzaehler manipulieren 							*/
	/* Warnungen nur lesen wenn nichts anderes aktiv - werden sp�ter ausgelesen 	*/
	/********************************************************************************/
	if ( (( p_ax_dat->monitor.status.fehler == ncWAHR ) && ( errorstep == 0 ) )
		|| (( p_ax_dat->monitor.status.warnung == ncWAHR ) && (step_slave_ax == COMMAND) && ( errorstep == 0 ) ) )
	{
		errorstep = step_slave_ax;
		step_slave_ax = FEHLER_TEXT;
	}

	/************************************************************************************/
	/* Sequenz zur �bergabe von Befehlen an das NC-Objekt 								*/
	/************************************************************************************/

	switch ( step_slave_ax )

	{

	/* Wenn Speicher nicht allokiert werden konnte, geht nur Neustart */
	case ALLOC_STATUS_FEHLER:
		if ( ACOPOS_Neustart_noetig == 1 )
		{
			return;
		}
		else
		{
			global_init = 1;
			step_slave_ax = COMMAND;
		}
		break;
		
	/****************************************************************************/
	/*		Auswahl des Kommandos 												*/
	/****************************************************************************/
	case COMMAND:
		if ( global_init == 1 )
			step_slave_ax = REGLER_AUSSCH;
		else if ( check_parameter == 1 )
			step_slave_ax = CHECK_PARAMETER;
		else if ( stop_bewegung == 1 )
			step_slave_ax = STOP_BEWEGUNG;
		else if ( comp_gear_stop == 1 ) 
			step_slave_ax = CAM_CTRL_AUS;
		else if ( cmd_cam_aut_abbruch == 1 ) 
			step_slave_ax = STOP_CAM_AUT_ABBRUCH;			
		else if ( cmd_check_restart == 1 ) 
			step_slave_ax = CHECK_MASTER_POS;
		else if ( datierung_aus == 1 )
			step_slave_ax = MS_DATIERUNG_AUS;
		else if ( dig_e_init == 1)
			step_slave_ax = DIG_E_INIT;
		else if ( enc1_init == 1 )
			step_slave_ax = ENCODER1_INIT;
		else if ( enc2_init == 1 )
			step_slave_ax = ENCODER2_INIT;
		else if ( regler_ein == 1)
			step_slave_ax = REGLER_EINSCH;
		else if ( achse_ref == 1 )
			step_slave_ax = REF_START;

		else if ( nsw_io_config_cmd == 1)			
			step_slave_ax = NSW_IO_CONFIG;
		else if ( nsw_init_cmd == 1)			
			step_slave_ax = NSW_INIT;

		else if ( walzen_zu_cmd == 1)			
			step_slave_ax = WALZENVERRIEGELUNG_ZU;
		else if ( walzen_auf_cmd == 1)			
			step_slave_ax = WALZENVERRIEGELUNG_AUF;

		else if ( etikett_ref == 1 )
			step_slave_ax = READ_EDGE_1;
		else if ( chk_enc2_home == 1)
			step_slave_ax = MASTER_REF_LESE_POS;	/*READ_ENC2_HOME_OK;*/		/*V02.78*/
		else if ( comp_gear_init == 1 )
			step_slave_ax = COMP_GEAR_INIT;

		else if ( nsw_start_cmd == 1)			
			step_slave_ax = NSW_START;

		else if ( cmd_inhibit_ein == 1 ) 
			step_slave_ax = CAM_INHIBIT_EIN;
		else if ( cmd_inhibit_aus == 1 ) 
			step_slave_ax = CAM_INHIBIT_AUS;

		else if ( comp_gear_start == 1 )
			step_slave_ax = C_GEAR_START;
		else if ( comp_gear_restart == 1 )
			step_slave_ax = C_GEAR_RESTART;		

		else if ( cam_ma_add_el == 1 )
			step_slave_ax = SEND_CAM_SL_ADD_EL;						
		else if ( spenden_manuell_cmd == 1 )
			step_slave_ax = MANUELL_SPENDEN;
		else if	( read_diag_cmd == 1 )
			step_slave_ax = READ_DIAG_INIT;
		/*else if (servo_stop == 1)
			step_slave_ax = VISION_OUT_ON;*/
		/*else step_slave_ax = CHECK_VISION_OUT;*/
	
	break;	
	
	case CHECK_VISION_OUT:
			p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_DIO_STATE;
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&dio_state_data;
		    
		    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
		    {
		    	if ((dio_state_data & 0x00000040) == 0x00000040)
		    	{
		    		step_slave_ax = VISION_OUT_OFF;
		    	}
		    	else step_slave_ax = COMMAND;
		    }
	break; 
	
	/****************************************************************************/
	/*		Initialisierung der Achse bei Spannungseinschalten oder Fehler		*/
	/****************************************************************************/

	/* Regler ausschalten */
	case REGLER_AUSSCH:
	
		if ( ncaction(ax_obj,ncREGLER,ncAUSSCHALTEN) == ncOK )
		{
			step_slave_ax = W_REGLER_AUS;
		}
		break;
		    		    
	/* Warten bis Regler AUS */
	case W_REGLER_AUS:
		if ( p_ax_dat->regler.status == ncAUS )
		{
			step_slave_ax = GLOBAL_INIT;		
		}
		break;

	/* Globale Initialisierung durchf�hren */
	case GLOBAL_INIT:
		if ( ncaction(ax_obj,ncGLOBAL,ncINIT) == ncOK )
		{
			step_slave_ax = W_GLOBAL_INIT;
		}
		break;

	/* Warten bis Globale Initialisierung beendet ist */
	case W_GLOBAL_INIT:
		if ( p_ax_dat->global.init == ncWAHR )
		{
			step_slave_ax = SIM_MODUS;
		}
		break;

	/* Simulationsmodus ein- oder ausschalten */
	case SIM_MODUS:
		if ( simulation == ncEIN )
		{
			action_status = ncaction(ax_obj,ncSIMULATION,ncEINSCHALTEN);
		}
		else
		{
			action_status = ncaction(ax_obj,ncSIMULATION,ncAUSSCHALTEN);
		}

		if ( action_status == ncOK )
		{
			step_slave_ax = W_SIM_STATUS;
		}
		break;
			
	/* Warten bis Simulations-Status richtig ist */
	case W_SIM_STATUS:
		if ( p_ax_dat->simulation.status == simulation )
		{
			step_slave_ax = HW_LIM_IGNORE;
		}
		break;
	
	/* Hardwareendschalter ignorieren, damit sie als Eing�nge verwendet werden k�nnen, */	
	case HW_LIM_IGNORE:
	    p_ax_dat->netzwerk.service.request.par_id = hw_lim_ignore.id;
	    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&hw_lim_ignore.data;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
	    	step_slave_ax = W_HW_LIM_IGNORE;
	    }
	    break;
	
	  case W_HW_LIM_IGNORE:
	  	if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
	    /* Operation erfolgreich abgeschlossen */
	    {
			step_slave_ax = REGL_INIT;
		}	
		break;

	/*	Regler initialisieren */
	case REGL_INIT:
		if (APS_1 == 1)
		{
	    	p_ax_dat->regler.lage.kv = 500;
		    p_ax_dat->regler.lage.tn = 0;
		    p_ax_dat->regler.lage.t_voraus = 1.61e-003;
		    p_ax_dat->regler.lage.t_gesamt = 1.61e-003;
	  	    p_ax_dat->regler.lage.p_max = 100000;
		    p_ax_dat->regler.lage.i_max = 0;
		    p_ax_dat->regler.drehzahl.kv = 2;
		    p_ax_dat->regler.drehzahl.tn = 5.1e-003;
		}
		else if (APS_2 == 1)
		{
			if (Schleifenauslauf == 0)
			{
				/* normaler APS 2  */
		    	p_ax_dat->regler.lage.kv = 200; /*300;*/
			    p_ax_dat->regler.lage.tn = 0;
			    p_ax_dat->regler.lage.t_voraus = 0.41e-003;
			    p_ax_dat->regler.lage.t_gesamt = 0.41e-003;
		  	    p_ax_dat->regler.lage.p_max = 100000;
			    p_ax_dat->regler.lage.i_max = 0;
			    p_ax_dat->regler.drehzahl.kv = 1.5;			/*2; V2.58*/	
			    p_ax_dat->regler.drehzahl.tn = 5.1e-003;
			}
			else
			{
				/* APS 2 mit Schleifenauslauf  - zus�tzliche F�rdermechanik am Kopf */
		    	p_ax_dat->regler.lage.kv = 200; /*300;*/
			    p_ax_dat->regler.lage.tn = 0;
			    p_ax_dat->regler.lage.t_voraus = 0.41e-003;
			    p_ax_dat->regler.lage.t_gesamt = 0.41e-003;
		  	    p_ax_dat->regler.lage.p_max = 100000;
			    p_ax_dat->regler.lage.i_max = 0;
			    p_ax_dat->regler.drehzahl.kv = 1.5;
			    p_ax_dat->regler.drehzahl.tn = 5.1e-003;
			}
		}
			
	    if ( ncaction(ax_obj,ncREGLER,ncINIT) == ncOK )
	    {
	    	step_slave_ax = W_REGL_INIT;
	    }
	    break;
	
	case W_REGL_INIT:
		if (p_ax_dat->regler.init == ncWAHR)
		{
			step_slave_ax = REGL_MODE;
		}
	    break;

	/*	Regler Modus einstellen	*/
	case REGL_MODE:
	    p_ax_dat->netzwerk.service.request.par_id = pctrl_mode_switch.id;
	    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&pctrl_mode_switch.data;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
	    	step_slave_ax = W_REGL_MODE;
	    }
	    break;
	
	case W_REGL_MODE:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		    /* Operation erfolgreich abgeschlossen */
		{
			global_init = 0;
			step_slave_ax = COMMAND;
		}
	    break;

	/* Parameter f�r die eingestellte Sorte �berpr�fen */		
	case CHECK_PARAMETER:
		
		check_parameter = 0;
		
		/* L�nge einer Maschinenteilung an der Flaschenaussenkante aus den Parametern berechnen */
		if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen > 1) /* Rundl�ufer */
		{
			Laenge_Maschinenteilung_ber = ( (  Akt_Produkt_Param.SK_Tellermitte + ( Akt_Maschinen_Param.Laenge_Maschinenteilung_allg *  Akt_Maschinen_Param.Anzahl_Maschinenteilungen / 6.2831853) ) * 6.2831853) / Akt_Maschinen_Param.Anzahl_Maschinenteilungen;
		}
		if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen == 1) /* Geradl�ufer */
		{
			Laenge_Maschinenteilung_ber = Akt_Maschinen_Param.Laenge_Maschinenteilung_allg;
		}
		
		if ( (Laenge_Maschinenteilung_ber < 1000) || (Laenge_Maschinenteilung_ber > 50000) || (Akt_Produkt_Param.Laenge_Slave_Intervall < 10) || (Akt_Produkt_Param.Laenge_Slave_Intervall > 50000) || (Akt_Produkt_Param.Laenge_Maschinen_SR < Laenge_Maschinenteilung_ber) || (Akt_Produkt_Param.Start_Pos_Synchron < 0) || (Akt_Produkt_Param.Start_Pos_Synchron > Laenge_Maschinenteilung_ber) || (Akt_Produkt_Param.Laenge_Comp_weg_Slave < 10) || (Akt_Produkt_Param.Laenge_Comp_weg_Slave > 50000) || (Akt_Produkt_Param.Faktor_Synchronweg_Master < 10) || (Akt_Produkt_Param.Faktor_Synchronweg_Master > 500) || (Akt_Produkt_Param.Weg_LS_Spendekante < 500) || (Akt_Produkt_Param.Weg_LS_Spendekante > 300000) )
		{
			Parameterwerte_falsch = 1;
		}
		
    	step_slave_ax = COMMAND;
	    break;
		
	/* Dig. Eing�nge initialisieren. */		
	case DIG_E_INIT:
		
		dig_e_init = 0;
		
		p_ax_dat->dig_e.pegel.trigger2 = ncAKTIV_HI;

	    if ( ncaction(ax_obj,ncDIG_E,ncINIT) == ncOK )
	    {
	    	step_slave_ax = W_DIG_E_INIT;
	  	}
	    break;
	    
	case W_DIG_E_INIT:
	    if (p_ax_dat->dig_e.init == ncWAHR)
	    {
	    	dig_e_init_ok = 1;
	    	step_slave_ax = COMMAND;
		}
		break;

	/* Regler einschalten */
	case REGLER_EINSCH:
		regler_ein = 0;
		if ( p_ax_dat->regler.status == ncEIN )
		{
			step_slave_ax = COMMAND;
		}
		else if ( ncaction(ax_obj,ncREGLER,ncEINSCHALTEN) == ncOK )
		{
			step_slave_ax = W_REGLER_EIN;
		}
		break;
			
    /* Warten bis Regler EIN */
	case W_REGLER_EIN:
		if ( p_ax_dat->regler.status == ncEIN )
		{
			step_slave_ax = COMMAND;
		}
		break;
			
	/* Referenzieren starten */
	case REF_START:
		achse_ref = 0;
		if ( ncaction(ax_obj,ncREFERENZ,ncSTART) == ncOK )
		{
			step_slave_ax = W_REF_OK;
			servo_stop_pos = 0;
		}
		break;

	/* Warten bis Referenzieren abgeschlossen ist */
	case W_REF_OK:
		if ( p_ax_dat->bewegung.referenz.status.ok == ncWAHR )
		{
			achse_ref_ok = 1;
			step_slave_ax = COMMAND;
		}
		break;

	/****************************************************************************/
	/*	Tr�gerband f�r Bewegungstart richtig positionieren 						*/
	/****************************************************************************/
	/* Anzahl der bisherigen Etitriggerflanken auslesen */
	case READ_EDGE_1:
	
		if( p_ax_dat->regler.status == ncEIN )
		{
			etikett_ref = 0;
			if (vorwaerts_referenzieren == 0) /* beim r�ckwaerts referenzieren mu� die steigende Flanke abgefragt werden */
			{ 
		    	p_ax_dat->netzwerk.service.request.par_id = trigger2.rise_edge_cnt.id;
				p_ax_dat->netzwerk.service.daten_adr = (UDINT)&trigger2.rise_edge_cnt.data;
		    }
			else if (vorwaerts_referenzieren == 1) /* beim vorwaerts referenzieren mu� die fallende Flanke abgefragt werden */
			{ 
		    	p_ax_dat->netzwerk.service.request.par_id = trigger2.fall_edge_cnt.id;
				p_ax_dat->netzwerk.service.daten_adr = (UDINT)&trigger2.fall_edge_cnt.data;
		    }
		    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
		    {
		    	step_slave_ax = W_READ_EDGE_1;
		    }
		}
		else
		{
			/* Regler einschalten */
			regler_ein = 1;
			step_slave_ax = COMMAND;
		}
	    break;
	    
	case W_READ_EDGE_1:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{
			step_slave_ax = BASIS_INIT;
		}
		break;
		
	/* Bewegung initialisieren */
	case BASIS_INIT:
		if (vorwaerts_referenzieren == 0) /*Rueckwaerts Referenzieren - Geschwindigkeit kann schneller sein*/ 
		{
			p_ax_dat->bewegung.basis.parameter.v_pos = 7800; /* p_ax_dat->grenzwert.parameter.v_pos/20; */
			p_ax_dat->bewegung.basis.parameter.v_neg = 7800; /*p_ax_dat->grenzwert.parameter.v_neg/20; */
			p_ax_dat->bewegung.basis.parameter.a1_pos = 100000; /* p_ax_dat->grenzwert.parameter.a1_pos/20; */
			p_ax_dat->bewegung.basis.parameter.a2_pos = 100000; /* p_ax_dat->grenzwert.parameter.a2_pos/20; */
			p_ax_dat->bewegung.basis.parameter.a1_neg = 100000; /* p_ax_dat->grenzwert.parameter.a1_neg/20; */
			p_ax_dat->bewegung.basis.parameter.a2_neg = 100000; /*p_ax_dat->grenzwert.parameter.a2_neg/20; */
		}
		else if (vorwaerts_referenzieren == 1) /*Vorwaerts Referenzieren - Geschwindigkeit mu� reduziert werden*/
		{
			p_ax_dat->bewegung.basis.parameter.v_pos = 5000; /* p_ax_dat->grenzwert.parameter.v_pos/20; */
			p_ax_dat->bewegung.basis.parameter.v_neg = 7800; /*p_ax_dat->grenzwert.parameter.v_neg/20; */
			p_ax_dat->bewegung.basis.parameter.a1_pos = 100000; /* p_ax_dat->grenzwert.parameter.a1_pos/20; */
			p_ax_dat->bewegung.basis.parameter.a2_pos = 100000; /* p_ax_dat->grenzwert.parameter.a2_pos/20; */
			p_ax_dat->bewegung.basis.parameter.a1_neg = 100000; /* p_ax_dat->grenzwert.parameter.a1_neg/20; */
			p_ax_dat->bewegung.basis.parameter.a2_neg = 100000; /*p_ax_dat->grenzwert.parameter.a2_neg/20; */
		}
		if ( ncaction(ax_obj,ncBASIS_BEW,ncINIT) == ncOK )
		{
			if ( (vorwaerts_referenzieren == 1) && ( (Akt_Produkt_Param.Typ_Datierung == 1) || (Akt_Produkt_Param.Typ_Datierung == 2) || (Akt_Produkt_Param.Typ_Datierung == 4) || (Akt_Produkt_Param.Typ_Datierung == 5) ) )
			{
				etikett_ref_aktiv = 1;		/* Damit wird der R�cksprung, nach der Datierung, zu ABS_BEW1 festgelegt! */
				step_slave_ax = MS_DATIERUNG_EIN;	/* vor dem Start der Bewegung (vorw�rts ref.) das Etikett datieren*/
				/*step_slave_ax = VISION_OUT_ON;*/
			}
			else
			{
				step_slave_ax = ABS_BEW1;
			}
		}
		break;
		
	/* Etikettenband vorwaerts oder r�ckwaerts fahren bis Flanke des Etitriggers kommt (oder max. Weg zur�ckgelegt, 2 Etikettenteilungen).  */	
	case ABS_BEW1:
		etikett_ref_aktiv = 0;	/* Damit wird der R�cksprung, nach der Datierung, zu ABS_BEW1 festgelegt! */

		/* Etikettenband r�ckw�rtsfahren bis Flanke des Etitriggers kommt.  */	
		if (vorwaerts_referenzieren == 0) 
		{
			p_ax_dat->bewegung.basis.parameter.s = p_ax_dat->monitor.s - 2*(Akt_Produkt_Param.Laenge_Slave_Intervall);	
		}
		/* Etikettenband vorw�rtsfahren bis Flanke des Etitriggers kommt.  */	
		else if (vorwaerts_referenzieren == 1)
		{
			p_ax_dat->bewegung.basis.parameter.s = p_ax_dat->monitor.s + 2*(Akt_Produkt_Param.Laenge_Slave_Intervall);	
		}

		if ( ncaction(ax_obj,ncABS_BEW,ncSTART) == ncOK )
		{
			step_slave_ax = READ_EDGE_2;
			Zeit_Eti_Trigger = 0;
		}
		break;		
	
	/* Auf Flanke des Etikettentriggers warten. */	
	case READ_EDGE_2:
		if (vorwaerts_referenzieren == 0) /* Beim r�ckwaerts referenzieren mu� die steigende Flanke abgefragt werden*/
		{ 
	    	p_ax_dat->netzwerk.service.request.par_id = trigger2.rise_edge_cnt.id;
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&trigger2_r_e_c_n;
	    }
		else if (vorwaerts_referenzieren == 1) /* Beim vorwaerts referenzieren mu� die fallende Flanke abgefragt werden*/
		{ 
	    	p_ax_dat->netzwerk.service.request.par_id = trigger2.fall_edge_cnt.id;
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&trigger2_f_e_c_n;
	    }
	    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
	    {
	    	step_slave_ax = W_READ_EDGE_2;
	    	Zeit_Eti_Trigger = Zeit_Eti_Trigger + 1;
	    }
	    break;
	    
	case W_READ_EDGE_2:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{
		
			if ( (vorwaerts_referenzieren == 0) && ( (trigger2_r_e_c_n > trigger2.rise_edge_cnt.data) || (En_Testbetrieb == 1) )  )
			{
				step_slave_ax = READ_EDGE_POS;
			}
			else if ( (vorwaerts_referenzieren == 1) && ( (trigger2_f_e_c_n > trigger2.fall_edge_cnt.data) || (En_Testbetrieb == 1) ) )
			{
				step_slave_ax = READ_EDGE_POS;
			}
			else
			{
				step_slave_ax = READ_EDGE_2;
				
				if (Zeit_Eti_Trigger > 400)		/* Es kann kein Etikettentrigger gelesen werden. */
				{
					Eti_Trigger_fehlt = 1;
					step_slave_ax = COMMAND;				/* Bewegungsabbruch durch BA_AUTO r�cksetzten in "aggregat" */
				}
			}
		}
		break;

	/* Position der Flanke des Etikettentriggers auslesen */
	case READ_EDGE_POS:
		if (vorwaerts_referenzieren == 0)
		{ 	
	    	p_ax_dat->netzwerk.service.request.par_id = trigger2.rise_edge_s_act.id;
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&trigger2.rise_edge_s_act.data;
	    }
		else if (vorwaerts_referenzieren == 1)
		{ 	
	    	p_ax_dat->netzwerk.service.request.par_id = trigger2.fall_edge_s_act.id;
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&trigger2.fall_edge_s_act.data;
	    }
	    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
	    {
	    	step_slave_ax = W_READ_EDGE_POS;
	    }
	    break;

	/* Etikettenband vorw�rts fahren bis das 1. Etikett in Startposition steht. */	    
	case W_READ_EDGE_POS:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{			
			if (vorwaerts_referenzieren == 0)
			{ 	
				trigger_pos = trigger2.rise_edge_s_act.data;
		    }
			else if (vorwaerts_referenzieren == 1)
			{ 	
				trigger_pos = trigger2.fall_edge_s_act.data;
		    }
			/* Wenn der Eti-Sensor vor dem Referenzieren im letzten Bereich des Etikettes steht,
			   muss zusaetzlich ein Etikettenintervall addiert werden */
			if (( Akt_Produkt_Param.Weg_LS_Spendekante % Akt_Produkt_Param.Laenge_Slave_Intervall) 
				> (Akt_Produkt_Param.Laenge_Comp_weg_Slave / 2))
			{
				p_ax_dat->bewegung.basis.parameter.s = trigger_pos 
					+ ( Akt_Produkt_Param.Weg_LS_Spendekante % Akt_Produkt_Param.Laenge_Slave_Intervall) 
					- (Akt_Produkt_Param.Laenge_Comp_weg_Slave / 2);
			}
			else 
			{
				p_ax_dat->bewegung.basis.parameter.s = trigger_pos 
					+ ( Akt_Produkt_Param.Weg_LS_Spendekante % Akt_Produkt_Param.Laenge_Slave_Intervall) 
					- (Akt_Produkt_Param.Laenge_Comp_weg_Slave / 2) + Akt_Produkt_Param.Laenge_Slave_Intervall;
			}
			
			/* Im Testbetrieb ist kein Sensor angeschlossen -> gelatchte Positionen sind ung�ltig. */
			if (En_Testbetrieb == 1)
			{
				p_ax_dat->bewegung.basis.parameter.s = p_ax_dat->monitor.s + Akt_Produkt_Param.Laenge_Slave_Intervall;
			}
			
			if ( ncaction(ax_obj,ncABS_BEW,ncSTART) == ncOK )
			{
				step_slave_ax = W_POS_END;
			}
		}
		break;
		
	/* Auf Bewegungsende warten */
	case W_POS_END:
		if (p_ax_dat->bewegung.basis.status.in_pos == ncWAHR)
		{
			etikett_ref_ok = 1;
			eti_ref_ausgefuehrt = 1; /* f�r erneuten Start Automatik merken */
			geberueberw_gebrueckt_ax_pos = p_ax_dat->monitor.s;	/* Zwischenspeicherung der Axposition nach der Referenzierung zur Deaktivierung Geber�berwachung */
			step_slave_ax = COMMAND;
			/*step_slave_ax = VISION_OUT_ON;*/
		}
		break;
	case VISION_OUT_ON:
		
		daten_ausgaenge = visionioadr;	/* 1. Ausgang (nach 4 Eing�ngen) auf Karte in Steckplatz 3 */
		
	    p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CMD_DO_SET;
	    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&daten_ausgaenge;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
			step_slave_ax = VISION_OUT_OFF;
		}
		break;
	case VISION_OUT_OFF:
		vision_out_time = vision_out_time + 1;
		if (vision_out_time > 4) {
			vision_out_time = 0;
			daten_ausgaenge = visionioadr;	/* 1. Ausgang (nach 4 Eing�ngen) auf Karte in Steckplatz 3 */

	    	p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CMD_DO_CLR;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&daten_ausgaenge;
			
		    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
		    {
		    	vision_off_counts = vision_off_counts + 1;
		    	if ( etikett_ref_aktiv ==1)
      			{
					step_slave_ax = MS_DATIERUNG_EIN;			/* nur Datierung aktiv gewesen - f�r vorw�rts referenzieren. */
      			}
				step_slave_ax = COMMAND;
			}
		}
		break;

	/****************************************************************************/
	/*	Bewegungsabbruch aus dem Automatikbetrieb vorbereiten					*/
	/****************************************************************************/
	/* Die aktuelle Maschinenposition auslesen, da erst bei Stillstand des Masters die 
	   Kurvenscheibe ausgeschaltet werden soll. */
	case CAM_CTRL_AUS:
		comp_gear_stop = 0;

	    p_ax_dat->netzwerk.service.request.par_id = PARID_ENCOD2_S_ACT;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&ma_s_act;
	    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
	    {
			Zeit_lese_ma_s_act = 0;
	    	step_slave_ax = W_CAM_CTRL_AUS;
	    }
	    break;
	    
	case W_CAM_CTRL_AUS:
	
		if ( ba_auto_aus_laeuft == 0)		/* Ausschalten wurde abgebrochen */
		{
			step_slave_ax = COMMAND;
		}
		else
		{
			Zeit_lese_ma_s_act = Zeit_lese_ma_s_act + 1;		/* Wartezeit, um Positions�nderung des Masters zu erfassen */
	
			if (Zeit_lese_ma_s_act > 5)
			{
				if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
				{
					if ( ( abs(ma_s_act_letzter_stop - ma_s_act) < 2) || (anz_versuche_cam_ctrl_aus > 100) )
					{
						/* Ausschalten, wenn Achse nach Timeout  nicht zum Stillstand kommt 
						   oder keine Positions�nderung mehr feststellbar ist. */
						anz_versuche_cam_ctrl_aus = 0; 
						ba_auto_aus_laeuft = 0;
						ACOPOS_Aus_laeuft = 0;
						step_slave_ax = STOP_BEWEGUNG;			
						Geberueberw_Gebrueckt 	= 1;						/* Geberfehler wird w�hrend der ersten 4 Teilungen �berbr�ckt, da St�rungen auf der Geberleitung diese beeinflussen k�nnen */
						geberueberw_gebrueckt_ax_pos = p_ax_dat->monitor.s;	/* Zwischenspeicherung der Axposition beim Einschalten zur Deaktivierung Geber�berwachung
																			 Wird hier nochmal zwischengespeichert, da beim n�chsten Einschalten unter Umst�nden keine positive Flanke von "Geberueberw_Gebrueckt"
																			 kommt (steht noch an vom Ausschalten), der zuletzt zwischengespeicherte Wert jedoch zum �berbr�cken der Startphase nicht mehr ausreicht (Wegen Abbremsphase) */

					}
					else
					{
						anz_versuche_cam_ctrl_aus = anz_versuche_cam_ctrl_aus + 1;
						step_slave_ax = CAM_CTRL_AUS; 			/* Position nochmal lesen */
					}
					/* Position speichern, um beim n�chsten Start Masterbewegungen zu erkennen. */
					ma_s_act_letzter_stop = ma_s_act;
				}
			}
		}
		break;
		
	/****************************************************************************/
	/*	 	Bewegung abbrechen													*/
	/****************************************************************************/
	case STOP_BEWEGUNG:
		stop_bewegung = 0;
	    p_ax_dat->bewegung.abbruch.index.befehl = 0;
    	if ( ncaction(ax_obj,ncBEWEGUNG,ncABBRUCH) == ncOK )
   		{
			step_slave_ax = W_STOP_BEWEGUNG;
    		Zeit_Stop_Bew_fertig = 0;
    	}
	    break;

	/* Warten bis Bewegung abgebrochen, mit zus�tzlicher Timeout-Funktion */
 	case W_STOP_BEWEGUNG:
 	
 		Zeit_Stop_Bew_fertig = Zeit_Stop_Bew_fertig + 1;
 		
	    if ( (p_ax_dat->bewegung.modus == ncAUS) && (p_ax_dat->monitor.v < 10.0) && (Zeit_Stop_Bew_fertig > 10) )
	    {
			step_slave_ax = STOP_BEW_LESE_CAM_STATE;
		}

	    break;
	    
	/* Zustand des Kurvenscheibenautomaten speichern - zur Auswertung beim Wiedereinschalten. */    
	case STOP_BEW_LESE_CAM_STATE:
		
		p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CAM_ACT_ST_INDEX;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam_state_letzter_stop;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
	    {
	    	step_slave_ax = W_STOP_BEW_LESE_CAM_STATE;
	    }
	    break;
	    
	case W_STOP_BEW_LESE_CAM_STATE:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{	
			step_slave_ax = STOP_BEW_NSW_AUS;	
		}
		break;

	/* NSW deaktivieren */
	case STOP_BEW_NSW_AUS:
		
		cmd_NSW_data = ncAUS;
		
	    p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CAM_DRUMSEQ_ENABLE;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cmd_NSW_data;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
	    	step_slave_ax = W_STOP_BEW_NSW_AUS;
	    }
	    break;
	    
	case W_STOP_BEW_NSW_AUS :
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{	
			step_slave_ax = STOP_BEW_REGLER_AUS;
		}
		break;
		
	/* Regler ausschalten */
	case STOP_BEW_REGLER_AUS:

		if ( p_ax_dat->regler.status == ncAUS )		/* Regler nur ausschalten, wenn aktiv */
		{
			step_slave_ax = COMMAND;		
			ACOPOS_Aus_laeuft = 0;
		}
		else
		{
			if ( ncaction(ax_obj,ncREGLER,ncAUSSCHALTEN) == ncOK )
			{
				step_slave_ax = W_STOP_BEW_REGLER_AUS;
			}
		}
		break;
		    		    
	/* Warten bis Regler AUS */
	case W_STOP_BEW_REGLER_AUS:
		if ( p_ax_dat->regler.status == ncAUS )
		{
			step_slave_ax = COMMAND;		
			ACOPOS_Aus_laeuft = 0;
		}
		break;
		
	/* Kurvenscheibenautomat abbrechen */
	case STOP_CAM_AUT_ABBRUCH:
		cmd_cam_aut_abbruch = 0;
		cam.cmd_start.data = ncABBRUCH;
    	p_ax_dat->netzwerk.service.request.par_id = cam.cmd_start.id;
    	p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.cmd_start.data;
    	if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
			step_slave_ax = W_STOP_CAM_AUT_ABBRUCH;		
		}
	    break;
	    
	case W_STOP_CAM_AUT_ABBRUCH:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{	
			comp_gear_run = 0;
			step_slave_ax = COMMAND;		
		}
		break;


	/****************************************************************************/
	/*	 	Drehrichtung und Getriebe�bersetzung f�r Servogeber festlegen 	*/
	/****************************************************************************/
	case ENCODER1_INIT:
		enc1_init=0;

		if (Akt_Maschinen_Param.Servo_Drehrichtung == 0)
		{
			p_ax_dat->geber_if.parameter.zaehlrchtg = ncSTANDARD;
		}
		else 
		{
			p_ax_dat->geber_if.parameter.zaehlrchtg = ncINVERS;
		}

		if ( (APS_1 == 1) && (APS_1_iGetriebe2 == 0) )
		{
			p_ax_dat->geber_if.parameter.maszstab.last.einheiten = 12566;
			p_ax_dat->geber_if.parameter.maszstab.last.umdr_motor = 4;
		}
		if ( (APS_1 == 1) && (APS_1_iGetriebe2 == 1) )
		{
			p_ax_dat->geber_if.parameter.maszstab.last.einheiten = 12566;
			p_ax_dat->geber_if.parameter.maszstab.last.umdr_motor = 2;
		}
		if (APS_2 == 1)
		{
			p_ax_dat->geber_if.parameter.maszstab.last.einheiten = 12566;
			p_ax_dat->geber_if.parameter.maszstab.last.umdr_motor = 2;
		}

	    if ( ncaction(ax_obj,ncGEBER_IF,ncINIT) == ncOK )
	    {
	    	step_slave_ax = W_ENCODER1_INIT;
	    }
	    break;
	    
	case W_ENCODER1_INIT:
	    if (p_ax_dat->geber_if.init == ncWAHR)
	    {
	    	step_slave_ax = COMMAND;
	    	enc1_init_ok = 1;
		}
		break;
		
	/****************************************************************************/
	/*	 	Drehrichtung und Getriebe�bersetzung f�r Maschinengeber festlegen 	*/
	/****************************************************************************/
	case ENCODER2_INIT:
		enc2_init = 0;
		parameter = 0;

		/* L�nge einer Maschinenteilung an der Flaschenauskante aus den Parametern berechnen
						                                2 * Pi * ( Entfernung_SK_Tellermitte + Maschinenradius )
		  Laenge_einer_Teilung_an_der_Aussenkante   =  ----------------------------------------------------------
		 														        Anzahl_Teilung 
		*/
		/* L�nge einer Maschinenteilung an der Flaschenaussenkante aus den Parametern berechnen */
		if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen > 1) /* Rundl�ufer */
		{
			Laenge_Maschinenteilung_ber = ( (  Akt_Produkt_Param.SK_Tellermitte + ( Akt_Maschinen_Param.Laenge_Maschinenteilung_allg *  Akt_Maschinen_Param.Anzahl_Maschinenteilungen / 6.2831853) ) * 6.2831853) / Akt_Maschinen_Param.Anzahl_Maschinenteilungen;
		}
		if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen == 1) /* Geradl�ufer */
		{
			Laenge_Maschinenteilung_ber = Akt_Maschinen_Param.Laenge_Maschinenteilung_allg;
		}

		encoder2.encoder_type.data = 1;	/* Ink.-Geber mit A-B-Signal */
		encoder2.scale_inc.data = 20000; /* Anzahl Impulse * 4 pro Umdrehung */
		encoder2.scale_units.data = Laenge_Maschinenteilung_ber;  /* Einheiten / Umdrehung Maschinengeber = Teilungsl�nge */
		encoder2.scale_rev.data = 1; 				/* Eine Geberumdrehung */
		
		if ((Akt_Maschinen_Param.Maschine_Drehrichtung == 2) || (Akt_Maschinen_Param.Maschine_Drehrichtung == 3))
		{
			/* Geber - aktiv */
			encoder2.ref_chk_window.data = 100;	 						/*2; Referenzimpuls - �berwachungsfenster */
			encoder2.ref_width.data	= encoder2.scale_inc.data / 2; 	/* Referenzimpuls - Breite */
			encoder2.ref_interval.data = encoder2.scale_inc.data; 	/* Referenzimpuls - Intervall */
			encoder2.ref_check_mode.data = 2; 						/* �berwachung und Geberkorrektur eingeschaltet */
		}
		else
		{
			/* Geber - �berwachung inaktiv */
			encoder2.ref_chk_window.data = 0; 	/* Referenzimpuls - �berwachungsfenster */
			encoder2.ref_width.data	= 0; 		/* Referenzimpuls - Breite */
			encoder2.ref_interval.data = 0; 	/* Referenzimpuls - Intervall */
			encoder2.ref_check_mode.data = 0; 	/* �berwachung ausgeschaltet */
			Fehler_Geberueberwachung = 0;
		}	
		
		if ((Akt_Maschinen_Param.Maschine_Drehrichtung == 0) || (Akt_Maschinen_Param.Maschine_Drehrichtung == 2))
		{
			encoder2.count_dir.data = ncSTANDARD;
		}
		else
		{
			encoder2.count_dir.data = ncINVERS;
		}
		encoder2_homing_mode = 0x08;
		encoder2_homing_s = 0;
		memcpy(&encoder2.homing.data[0], &encoder2_homing_s, sizeof (encoder2_homing_s));
		memcpy(&encoder2.homing.data[sizeof (encoder2_homing_s)], &encoder2_homing_mode, sizeof (encoder2_homing_mode));
		
		step_slave_ax = ENCODER2_PAR;
		break;
		
	case ENCODER2_PAR:
		parameter = parameter + 1;

		/* Parametrierung mit ENDAT - Geber beendet? */
		if ( (parameter > 3) && (Akt_Maschinen_Param.Maschine_Gebertyp == 0) ) 
		{	
			enc2_init_ok = 1;
			step_slave_ax = COMMAND;
			break;
		}

		/* Parametrierung mit Inkremental - Geber beendet? */
		if ( (parameter > 9) && (Akt_Maschinen_Param.Maschine_Gebertyp == 1) ) 
		{	
			enc2_init_ok = 1;
			chk_enc2_home = 1;	/* sp�ter warten bis Leitgeber referenziert ist */
			step_slave_ax = COMMAND;
			break;
		}

		switch (parameter)
		{
		case 1:
		    p_ax_dat->netzwerk.service.request.par_id = encoder2.scale_units.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&encoder2.scale_units.data;
			break;
		case 2:
		    p_ax_dat->netzwerk.service.request.par_id = encoder2.scale_rev.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&encoder2.scale_rev.data;
			break;
		case 3:
		    p_ax_dat->netzwerk.service.request.par_id = encoder2.count_dir.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&encoder2.count_dir.data;
			break;
		case 4:
		    p_ax_dat->netzwerk.service.request.par_id = encoder2.scale_inc.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&encoder2.scale_inc.data;
			break;
		case 5:
		    p_ax_dat->netzwerk.service.request.par_id = encoder2.homing.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&encoder2.homing.data[0];
			break;
		case 6:
		    p_ax_dat->netzwerk.service.request.par_id = encoder2.ref_interval.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&encoder2.ref_interval.data;
			break;
		case 7:
		    p_ax_dat->netzwerk.service.request.par_id = encoder2.ref_width.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&encoder2.ref_width.data;
			break;
		case 8:
		    p_ax_dat->netzwerk.service.request.par_id = encoder2.ref_chk_window.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&encoder2.ref_chk_window.data;
			break;
		case 9:
		    p_ax_dat->netzwerk.service.request.par_id = encoder2.ref_check_mode.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&encoder2.ref_check_mode.data;
			break;
		}
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
	    	step_slave_ax = W_ENCODER2_PAR;
	    }
	    break;
	
	  case W_ENCODER2_PAR:
    	if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
   		{
      		/* Operation erfolgreich abgeschlossen */
        	step_slave_ax = ENCODER2_PAR;
		}
	    break;

	
	/****************************************************************************/
	/* Master referenzieren - warten bis Maschine eine Teilung gefahren ist		*/		/*V02.78*/
	/****************************************************************************/
	case MASTER_REF_LESE_POS:		/*Master referenzieren - warten bis Maschine eine Teilung gefahren ist - Startwert auslesen*/
			Enc2_Ref_Fahrt = 1;
		    p_ax_dat->netzwerk.service.request.par_id = encoder2.s_act.id;
		    p_ax_dat->netzwerk.service.daten_adr = (DINT)&master_pos_ref_start;
	    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
	    {
	    	step_slave_ax = W_MASTER_REF_LESE_POS;
	    }
	    break;

	case W_MASTER_REF_LESE_POS:		/*Master referenzieren - warten bis Maschine eine Teilung gefahren ist - Startwert auslesen*/
    	if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
   		{
      		/* Operation erfolgreich abgeschlossen */
        	step_slave_ax = MASTER_REF_LESE_POS2;
		}
	    break;
	
	case MASTER_REF_LESE_POS2: 		/*Master referenzieren - warten bis Maschine eine Teilung gefahren ist - Aktuellen Wert auslesen*/
	    p_ax_dat->netzwerk.service.request.par_id = encoder2.s_act.id;
	    p_ax_dat->netzwerk.service.daten_adr = (DINT)&master_pos_ref_akt;
	    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
	    {
	    	step_slave_ax = W_MASTER_REF_LESE_POS2;
	    }
	    break;

	case W_MASTER_REF_LESE_POS2:		/*Master referenzieren - warten bis Maschine eine Teilung gefahren ist - Aktuellen Wert auslesen und mit Startwert vergleichen*/
		if (BA_Auto == 0) /*Im Handmodus Homing �berspringen*/
		{
			step_slave_ax = MASTER_REF_INIT;
			Enc2_Ref_Fahrt = 0;
		}
	    if ((p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id) && ( ( (master_pos_ref_akt - master_pos_ref_start) >= 1000 ) || ( (master_pos_ref_start - master_pos_ref_akt) >= 1000 ) ) )
   		{
      		/* Operation erfolgreich abgeschlossen */
        	step_slave_ax = MASTER_REF_INIT;	/*COMMAND;	/*ENCODER2_PAR;*/
        	Enc2_Ref_Fahrt = 0;
		}
		else if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{
	    	step_slave_ax = MASTER_REF_LESE_POS2;
		}
	    break;
		
		
	case MASTER_REF_INIT:
		encoder2_homing_mode = 0x08;	/*V02.78*/
		encoder2_homing_s = 0;
		memcpy(&encoder2.homing.data[0], &encoder2_homing_s, sizeof (encoder2_homing_s));
		memcpy(&encoder2.homing.data[sizeof (encoder2_homing_s)], &encoder2_homing_mode, sizeof (encoder2_homing_mode));
		
		p_ax_dat->netzwerk.service.request.par_id = encoder2.homing.id;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&encoder2.homing.data[0];
		
		if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
	    	step_slave_ax = W_MASTER_REF_INIT;
	    }
	    break;
	
	case W_MASTER_REF_INIT:
	    if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
   		{
      		/* Operation erfolgreich abgeschlossen */
        	step_slave_ax = READ_ENC2_HOME_OK;
		}
	    break;

		    
	/****************************************************************************/
	/*	 	Referenzfahrt beendet ? (Wurde beim Laden der Parameter gestartet) 	*/
	/****************************************************************************/
	case READ_ENC2_HOME_OK:
		chk_enc2_home = 0;
	    p_ax_dat->netzwerk.service.request.par_id = encoder2.homing_ok.id;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&encoder2.homing_ok.data;
	    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
	    {
	    	step_slave_ax = W_READ_ENC2_HOME_OK;
			Enc2_Ref_Fahrt = 1;
	    }
	    break;
	    
	case W_READ_ENC2_HOME_OK:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{
			if (encoder2.homing_ok.data == ncWAHR)
			{
				enc2_home_ok = 1;
				Enc2_Ref_Fahrt = 0;
				step_slave_ax = COMMAND;
			}
			else if (BA_Auto == 0)
			{
				enc2_home_ok = 0;
				Enc2_Ref_Fahrt = 0;
				step_slave_ax = COMMAND;
			}
			else
			{
				step_slave_ax = READ_ENC2_HOME_OK;
			}
		}
		break;

	/****************************************************************************************/
	/*	Zustand des Kurvenscheibenautomaten vor dem Wieder-Einschalten lesen				*/
	/****************************************************************************************/
	case CHECK_MASTER_POS:
		cmd_check_restart = 0;

		/* Pr�fen des Kurvenscheibenzustandes beim letzten Ausschalten */
		if ((cam_state_letzter_stop == 0) || (cam_state_letzter_stop == 1) || (cam_state_letzter_stop == 3))
		{
			/* Wenn beim Ausschalten der Automat im Ruhezustand war (kein Produkt - Sperre zu), 
			   kann sofort wieder eingeschaltet werden - ohne Pr�fung ob Master verfahren wurde.
			   Etikett steht richtig an der SK - da zuende gespendet wurde */
			step_slave_ax = COMMAND;
		}
		else
		{
			/* Wenn beim Ausschalten der Automat beim Etikettieren war (Etikett halb auf der Flasche), 
			   muss gepr�ft werden ob Master verfahren wurde w�hrend das Tr�gerband stand. */
		    p_ax_dat->netzwerk.service.request.par_id = PARID_ENCOD2_S_ACT;
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&ma_s_act_check;
		    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
		    {
		    	step_slave_ax = W_CHECK_MASTER_POS;
		    }
		}
	    break;
	    
	/* Masterposition mit der des letzten Ausschalten vergleichen */
	case W_CHECK_MASTER_POS:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{
			if (abs(ma_s_act_check - ma_s_act_letzter_stop) > 25)
			{
				etikett_ref_ok = 0;
				etikett_ref = 1;		/* Etikett muss referenziert werden, da Master bewegt wurde */
			}
			step_slave_ax = COMMAND;
		}
		break;
	
	
	/****************************************************************************************/
	/*	Erneutes Einschalten der Kurvenscheibenkopplung, ohne Initialisierung des Automaten */
	/****************************************************************************************/
	case C_GEAR_RESTART:

		comp_gear_restart = 0;

		/* Wurde Etiband referenziert oder verfahren? */
		if (eti_ref_ausgefuehrt == 0)
		{
	 		/* Etiband nicht verfahren. */
			step_slave_ax = RESTART_LESE_MASTER_POS;
		}
		else
		{
	 		/* Etiband verfahren und referenziert -> vor dem Einschalten: Slave - FIFO l�schen. */
			cmd_clear_trig_fifo_data = 0x04;
			
		    p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CAM_CLEAR_TRIG_FIFO;
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cmd_clear_trig_fifo_data;
		    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
		    {
		    	step_slave_ax = W_CLEAR_SL_TRIG_FIFO;
		    }
		}
	    break;
	    
	case W_CLEAR_SL_TRIG_FIFO:
			
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{
			step_slave_ax = RESTART_LESE_MASTER_POS;
		}
		break;

	/* Pr�fen des Masterposition und des Kurvenscheibenzustandes beim letzten Ausschalten */
	case RESTART_LESE_MASTER_POS:

		/* Wenn beim Ausschalten der Automat im Ruhezustand war (kein Produkt - Sperre zu) 
		   kann sofort wieder eingeschaltet werden - ohne Pr�fung ob Master verfahren wurde. */
		if ((cam_state_letzter_stop == 0) || (cam_state_letzter_stop == 1) || (cam_state_letzter_stop == 3))
		{
			step_slave_ax = CAM_CTRL_EIN;
		}
		else
		{
			/* Wenn beim Ausschalten der Automat beim Etikettieren war (Etikett halb auf der Flasche), 
			   muss gepr�ft werden ob Master verfahren wurde w�hrend das Tr�gerband stand. */
		    p_ax_dat->netzwerk.service.request.par_id = PARID_ENCOD2_S_ACT;
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&ma_s_act_restart;
		    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
		    {
		    	step_slave_ax = W_RESTART_LESE_MASTER_POS;
		    }
		}
	    break;
	    
	case W_RESTART_LESE_MASTER_POS:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{
			if ( (abs(ma_s_act_restart - ma_s_act_letzter_stop) > 25) || (eti_ref_ausgefuehrt == 1) )
			{
				/* Master um 0.25mm bewegt oder Etiketten manuell gespendet - 
				   zum Einschalten warten bis Kurvenscheibenautomat im Ruhezustand ist. */
				step_slave_ax = RESTART_LESE_CAM_STATE;
			}
			else
			{
				step_slave_ax = CAM_CTRL_EIN;
			}
		}
		break;
	    
	case RESTART_LESE_CAM_STATE:
		
		p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CAM_ACT_ST_INDEX;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam_state_data;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
	    {
	    	step_slave_ax = W_RESTART_LESE_CAM_STATE;
	    }
	    break;
	    
	case W_RESTART_LESE_CAM_STATE:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{	
			if ( (cam_state_data != 1) && (cam_state_data != 3) )
			{
		 		/* Auf richtigen Kurvenscheibenzustand warten */
		 		/* Maschine mu� sich bewegen, damit Kurvenscheibenautomat den Zustand 1 oder 3 erreicht. */
		 		if (BA_Auto == 1)
				{
					step_slave_ax = RESTART_LESE_CAM_STATE;
				}
				else
				{
					step_slave_ax = COMMAND;  /* Automatik wieder ausgeschaltet */
				}
			}
			else
			{
				step_slave_ax = CAM_CTRL_EIN;
			}
		}
		break;
		
	case CAM_CTRL_EIN:
		cmd_cam_ctrl_data = ncRESTART;
		
	    p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CMD_CAM_START;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cmd_cam_ctrl_data;
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
	    	step_slave_ax = W_CAM_CTRL_EIN;
	    }
	    break;
	    
	case W_CAM_CTRL_EIN:

		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{
			ACOPOS_Ein_laeuft = 0;
			step_slave_ax = COMMAND;
		}
		break;
		
	/********************************************************************************/
	/*		Kurvenscheibenbewegung parametrieren und initialisieren					*/
	/********************************************************************************/
	case COMP_GEAR_INIT:
		comp_gear_init=0;
		parameter = 0;
		
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{
			/* L�nge einer Maschinenteilung an der Flaschenauskante aus den Parametern berechnen
								                                2 * Pi * ( Entfernung_SK_Tellermitte + Maschinenradius )
				  Laenge_einer_Teilung_an_der_Aussenkante   =  ----------------------------------------------------------
				 														        Anzahl_Teilung 
			*/
			
			/* L�nge einer Maschinenteilung an der Flaschenaussenkante aus den Parametern berechnen */
			if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen > 1) /* Rundl�ufer */
			{
				Laenge_Maschinenteilung_ber = ( (  Akt_Produkt_Param.SK_Tellermitte + ( Akt_Maschinen_Param.Laenge_Maschinenteilung_allg *  Akt_Maschinen_Param.Anzahl_Maschinenteilungen / 6.2831853) ) * 6.2831853) / Akt_Maschinen_Param.Anzahl_Maschinenteilungen;
			}
			if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen == 1) /* Geradl�ufer */
			{
				Laenge_Maschinenteilung_ber = Akt_Maschinen_Param.Laenge_Maschinenteilung_allg;
			}

			/*	Parameter�bergaben Kurvenscheibe  */
			cam.ma_axis.data = 	ncEXTGEBER;				/* Maschinengeber */
			cam.ma_v_max.data = 10000; 					/* max. Geschwindigkeit des Maschinengebers - bei zu kleinem Wert erfolgt keine Fehlerausgabe */
			cam.ma_s_sync.data 		= (Akt_Produkt_Param.Laenge_Slave_Intervall - Akt_Produkt_Param.Laenge_Comp_weg_Slave) * Akt_Produkt_Param.Faktor_Synchronweg_Master / 100; 	/* Mastersynchronweg bezogen auf den Maschinengeber (Master) */
			cam.ma_s_comp.data 		= Laenge_Maschinenteilung_ber - cam.ma_s_sync.data; /* Ausgleichsweg Master = Maschinenteilung - Mastersynchronweg */
			cam.ma_s_trig.data 		= Akt_Produkt_Param.Laenge_Maschinen_SR; /* L�nge des Beh�lterschieberegisters = Anzahl der Positionen * Maschinenteilung */
			cam.ma_s_start.data 	= Akt_Produkt_Param.Start_Pos_Synchron; 		/* Offset Start Mastersynchronweg */
			cam.ma_s_comp_trig.data	= Akt_Produkt_Param.Weg_Ma_Trig_Spendekante;  /* Distanz Ausgleichstrigger Masterachse zum Beginn Synchronbereich */
			cam.ma_comp_trig_window.data = Laenge_Maschinenteilung_ber/10;	/* �berwachungsfenster f�r Nachtriggersignal */
			cam.ma_trig_window.data	= Laenge_Maschinenteilung_ber/2;	/* �berwachungsfenster f�r FlaDa-Signal*/
			cam.sl_s_sync.data 		= Akt_Produkt_Param.Laenge_Slave_Intervall - Akt_Produkt_Param.Laenge_Comp_weg_Slave; 	/* Etikettensynchronweg des Spenders */
			cam.sl_s_comp.data 		= Akt_Produkt_Param.Laenge_Comp_weg_Slave; 	/* Ausgleichsweg nach Etikettensynchronweg bis zum n�chsten Etikett */
			cam.sl_s_trig.data 		= Akt_Produkt_Param.Weg_LS_Spendekante; 	/* Abstand zwischen Eti-LS und Aufbringpunkt (Start der Synchronphase) */
			cam.sl_s_comp_min.data 	= Akt_Produkt_Param.Laenge_Comp_weg_Slave / 5; /* Minimal zul�ssiger Restweg */
			cam.sl_s_comp_max.data 	= Akt_Produkt_Param.Laenge_Comp_weg_Slave * 20;  	/* 3000; Maximal zul�ssiger Restweg */
			cam.sl_trig_window.data = Akt_Produkt_Param.Laenge_Slave_Intervall * 0.5;	/* Fenster um die Etikettentriggerposition */
			cam.ma_add_el.data 		= 0;	/* R�cksetzten - kann w�hrend der aktiven Kopplung ge�ndert werden. */
			
			if ((Akt_Produkt_Param.Typ_Datierung == 4) || (Akt_Produkt_Param.Typ_Datierung == 5))
			{
				cam.comp_gear_type.data = 2; /* 0 = 5 Polynom (ohne R�ckzug); 1 = 1 Polynom (mit R�ckzug); 2 = 5 Polynom (ohne R�ckzug, mit Stillstand) */	
			}
			else
			{
				cam.comp_gear_type.data = 0; /* 0 = 5 Polynom (ohne R�ckzug); 1 = 1 Polynom (mit R�ckzug); 2 = 5 Polynom (ohne R�ckzug, mit Stillstand) */	
			}



			cam.sl_trig_iv_min.data	= Akt_Produkt_Param.Laenge_Slave_Intervall * 10;   /* min. Slaveintervall - Diagnosewerte beim Einschalten r�cksetzten */
			cam.sl_trig_iv_max.data	= 0;		/* max. Slaveintervall - Diagnosewerte beim Einschalten r�cksetzten */
			
			ma_s_start_alt = cam.ma_s_start.data; /* Startpos zur Bearbeitung im "menue"-task */
			
			/* Trigger bei Betrieb ohne Etikettenband "Testbetrieb" ausschalten! */
			if (En_Testbetrieb == 1)
			{
				cam.ma_trig_mode.data 		= ncAUS;
				cam.sl_trig_mode.data 		= ncAUS;
				cam.ma_comp_trig_mode.data	= ncAUS;
			}
			else
			{
				if ( kein_Flada_Signal == 1 )
				{
					cam.ma_trig_mode.data 		= ncAUS;	/* Vorfuehrbetrieb fuer Messeaggregat */
				}	
				else
				{
					cam.ma_trig_mode.data 		= ncREFERENZ+ncP_FLANKE;
				}
				
				cam.sl_trig_mode.data 		= ncTRIGGER2+ncN_FLANKE;
		
				if (cam.ma_s_comp_trig.data == 0)
				{
					cam.ma_comp_trig_mode.data	= ncAUS;
				}
				else
				{
					cam.ma_comp_trig_mode.data	= ncTRIGGER1 + ncP_FLANKE;
 				}
			}

			step_slave_ax = COMP_GEAR_PAR;
		}
		break;
	
	/* Parameter�bergabe an ACOPOS starten. */		
	case COMP_GEAR_PAR:
		parameter = parameter+1;
		if (parameter > 21)
		{	
			step_slave_ax = COMMAND;
			comp_gear_init_ok = 1;
			break;
		}
		switch (parameter)
		{
		case 1:
		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_axis.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_axis.data;
			break;
		case 2:
		    p_ax_dat->netzwerk.service.request.par_id = cam.sl_trig_window.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.sl_trig_window.data;
			break;
		case 3:
		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_v_max.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_v_max.data;
			break;
		case 4:
		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_s_sync.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_s_sync.data;
			break;
		case 5:
		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_s_comp.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_s_comp.data;
			break;
		case 6:
		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_trig_mode.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_trig_mode.data;
			break;
		case 7:
		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_s_trig.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_s_trig.data;
			break;
		case 8:
		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_trig_window.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_trig_window.data;
			break;
		case 9:
		    p_ax_dat->netzwerk.service.request.par_id = cam.sl_s_sync.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.sl_s_sync.data;
			break;
		case 10:
		    p_ax_dat->netzwerk.service.request.par_id = cam.sl_s_comp.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.sl_s_comp.data;
			break;
		case 11:
		    p_ax_dat->netzwerk.service.request.par_id = cam.sl_trig_mode.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.sl_trig_mode.data;
			break;
		case 12:
		    p_ax_dat->netzwerk.service.request.par_id = cam.sl_s_trig.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.sl_s_trig.data;
			break;
		case 13:
		    p_ax_dat->netzwerk.service.request.par_id = cam.sl_s_comp_min.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.sl_s_comp_min.data;
			break;
		case 14:
		    p_ax_dat->netzwerk.service.request.par_id = cam.sl_s_comp_max.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.sl_s_comp_max.data;
			break;
		case 15:
		    p_ax_dat->netzwerk.service.request.par_id = cam.comp_gear_type.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.comp_gear_type.data;
		    break;
		case 16:
		    p_ax_dat->netzwerk.service.request.par_id = cam.sl_trig_iv_min.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.sl_trig_iv_min.data;
		    break;
		case 17:
		    p_ax_dat->netzwerk.service.request.par_id = cam.sl_trig_iv_max.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.sl_trig_iv_max.data;
			break;
		case 18:
		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_comp_trig_mode.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_comp_trig_mode.data;
			if (cam.ma_comp_trig_mode.data == ncAUS) /* kein Master-Ausgleichs-Trigger aktivieren */
				parameter = parameter + 2;
			break;
		case 19:
		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_s_comp_trig.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_s_comp_trig.data;
			break;
		case 20:
		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_comp_trig_window.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_comp_trig_window.data;
			break;
		case 21:
		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_add_el.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_add_el.data;
			break;
		}
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
	    	step_slave_ax = W_COMP_GEAR_PAR;
	    }
	    break;
	
	case W_COMP_GEAR_PAR:
     	if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
        {
      		/* Operation erfolgreich abgeschlossen */
        	step_slave_ax = COMP_GEAR_PAR;
        }
	    break;

	/* Nockenschaltwerk IO-Karte initialisieren */	
	case NSW_IO_CONFIG:
		nsw_io_config_cmd = 0;

		nsw_io_config = 1; 				/* Startsignal f�r NSW - Task */				
		step_slave_ax = W_NSW_START;
		break;

	case W_NSW_IO_CONFIG:
		if (nsw_io_config == 0)			/* Starten abgeschlossen */
		{
			nsw_io_config_ok = 1;
			step_slave_ax = COMMAND;
		}
		break;

	/* Nockenschaltwerk initialisieren */	
	case NSW_INIT:
		nsw_init_cmd = 0;

		/* Masterposition erneut auslesen --> Berechung Startpos fuer NSW */
	    p_ax_dat->netzwerk.service.request.par_id = PARID_ENCOD2_S_ACT;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&ma_s_act_nsw;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
	    { 
	     	step_slave_ax = NSW_READ_MA_POS;
	    }

	case NSW_READ_MA_POS:
		
		if ( (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id) 
			|| (p_ax_dat->netzwerk.service.request.par_id == cam.ma_s_start.id) )
		{
			/* L�nge einer Maschinenteilung an der Flaschenaussenkante aus den Parametern berechnen */
			if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen > 1) /* Rundl�ufer */
			{
				Laenge_Maschinenteilung_ber = ( (  Akt_Produkt_Param.SK_Tellermitte + ( Akt_Maschinen_Param.Laenge_Maschinenteilung_allg *  Akt_Maschinen_Param.Anzahl_Maschinenteilungen / 6.2831853) ) * 6.2831853) / Akt_Maschinen_Param.Anzahl_Maschinenteilungen;
			}
			if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen == 1) /* Geradl�ufer */
			{
				Laenge_Maschinenteilung_ber = Akt_Maschinen_Param.Laenge_Maschinenteilung_allg;
			}
	
			/* Berechnung der Startpos wie zum Einschalten der Kurvenscheibe weiter unten */
			ma_s_start_NSW = Akt_Produkt_Param.Start_Pos_Synchron
								+ (ma_s_act_nsw - Akt_Produkt_Param.Start_Pos_Synchron + Akt_Produkt_Param.Laenge_Maschinen_SR) 
								/ Laenge_Maschinenteilung_ber * Laenge_Maschinenteilung_ber;
	
			nsw_init = 1; 				/* Startsignal f�r Initialisierung im anderen Task */
			step_slave_ax = W_NSW_INIT;
		}
		break;
		
	case W_NSW_INIT:
		if (nsw_init == 0)			/* Initialisierung abgeschlossen */
		{
			step_slave_ax = COMMAND;
			nsw_init_ok = 1;
		}
		break;
		
	/* Nockenschaltwerk starten */	
	case NSW_START:
		nsw_start_cmd = 0;

		if (Akt_Produkt_Param.Typ_Datierung == 0) /* nur Einschalten, wenn angew�hlt! */
		{
			step_slave_ax = COMMAND;
		}
		else
		{
			nsw_start = 1; 				/* Startsignal f�r NSW - Task */				
			step_slave_ax = W_NSW_START;
		}
		break;

	case W_NSW_START:
		if (nsw_start == 0)			/* Starten abgeschlossen */
		{
			nsw_start_ok = 1;
			step_slave_ax = COMMAND;
		}
		break;

	case C_GEAR_START:
	
		if( p_ax_dat->regler.status == ncEIN )
		{
			/* Masterposition zum 1. Mal auslesen --> Sch�tzung des zur�ckgelegten Weges bis zum Einkuppeln */
		    p_ax_dat->netzwerk.service.request.par_id = PARID_ENCOD2_S_ACT;
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&ma_s_act_start_1;
			
		    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
		    { 
		    	zeit_c_gear_start = 0;
		     	step_slave_ax = C_GEAR_START_W;
		    }
		}
		else
		{
			/* Regler einschalten */
			regler_ein = 1;
			step_slave_ax = COMMAND;
		}
	    break;
	    
	case C_GEAR_START_W:
	
		zeit_c_gear_start = zeit_c_gear_start + 1;
		if ( (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id) && (zeit_c_gear_start >= par_zeit_c_gear_start) )
		{
		   	step_slave_ax = C_GEAR_START_1;
		}
	    break;
	    
	    
	case C_GEAR_START_1:
			
		/* Masterposition erneut auslesen --> Berechung Einkuppelposition */
	    p_ax_dat->netzwerk.service.request.par_id = PARID_ENCOD2_S_ACT;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&ma_s_act_start_2;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
	    { 
			zeit_c_gear_start_diag = 0;
     		step_slave_ax = C_GEAR_START_SND_MA_POS;
	    }
	    break;
	    
	case C_GEAR_START_SND_MA_POS:
	
		zeit_c_gear_start_diag =  zeit_c_gear_start_diag + 1;

		if ( (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id) 
			|| (p_ax_dat->netzwerk.service.request.par_id == cam.ma_s_start.id) )
		{
			/* voraussichtliche Startposition bei �bergabe des Einschaltkommandos */	
			ma_s_act_start = ma_s_act_start_2 + (ma_s_act_start_2 - ma_s_act_start_1);
						
			cam.ma_s_start.data = Akt_Produkt_Param.Start_Pos_Synchron
				+ (( ma_s_act_start - Akt_Produkt_Param.Start_Pos_Synchron + cam.ma_s_trig.data ) 
				/ (cam.ma_s_comp.data + cam.ma_s_sync.data))
				* (cam.ma_s_comp.data + cam.ma_s_sync.data);

		    p_ax_dat->netzwerk.service.request.par_id = cam.ma_s_start.id;
		    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_s_start.data;
		    
		    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
		    {
	    		step_slave_ax = C_GEAR_START_SIGNAL;
	    	}
		}
		break;

	case C_GEAR_START_SIGNAL:

		zeit_c_gear_start_diag =  zeit_c_gear_start_diag + 1;

		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{
			/* Masterstartposition gesetzt -> einschalten*/
			cam.cmd_start.data = ncEINSCH;
	    	p_ax_dat->netzwerk.service.request.par_id = cam.cmd_start.id;
	    	p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.cmd_start.data;
	    	
	    	if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
		    {
		    	step_slave_ax = W_C_GEAR_START;
	    	}
		}
		break;
	
	case W_C_GEAR_START:

		zeit_c_gear_start_diag =  zeit_c_gear_start_diag + 1;

    	/* Operation beendet */
      	if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
        {
      		/* Operation erfolgreich abgeschlossen */
			ACOPOS_Ein_laeuft = 0;
			comp_gear_start = 0 ;
			comp_gear_run = 1;

			step_slave_ax = COMMAND;
		}
	    break;
	    
	/********************************************************************************/
	/*		additves Element w�hrend aktiver Kurvenscheibenkopplung �bertragen 		*/
	/********************************************************************************/
	case SEND_CAM_SL_ADD_EL:
		cam_ma_add_el = 0;
		
		cam.ma_add_el.data = ma_s_start_add_el;
	    p_ax_dat->netzwerk.service.request.par_id = cam.ma_add_el.id;
	    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cam.ma_add_el.data;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
	    	step_slave_ax = W_SEND_CAM_SL_ADD_EL;
	    }
	    break;
	
	case W_SEND_CAM_SL_ADD_EL:
    	/* Operation beendet */
      	if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
        {
			step_slave_ax = COMMAND;
		}
	    break;
	    
	/********************************************************************************/
	/*		Bei eingeschalteter Kurvenscheibenkopplung nicht mehr etikettieren		*/
	/********************************************************************************/
	case CAM_INHIBIT_EIN:
		cmd_inhibit_ein = 0;
		Aggregat_standby = 1;
		
		cmd_inhibit_data = Akt_Maschinen_Param.Abstand_Aggregat;
		
	    p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CAM_INHIBIT_ON;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cmd_inhibit_data;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
	    	step_slave_ax = W_CAM_INHIBIT_EIN;
	    }
	    break;
	    
	case W_CAM_INHIBIT_EIN :
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{	
			step_slave_ax = COMMAND;
			inhibit_ist_ein = 1;
		}
		break;

	/********************************************************************************/
	/*		Bei eingeschalteter Kurvenscheibenkopplung wieder etikettieren			*/
	/********************************************************************************/
	case CAM_INHIBIT_AUS:
		cmd_inhibit_aus = 0;
		Aggregat_standby = 0;
   		
		cmd_inhibit_data = Akt_Maschinen_Param.Abstand_Aggregat;
		
	    p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CAM_INHIBIT_OFF;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cmd_inhibit_data;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
	    	step_slave_ax = W_CAM_INHIBIT_AUS;
	    }
	    break;
	    
	case W_CAM_INHIBIT_AUS:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{	
			step_slave_ax = COMMAND;
			inhibit_ist_ein = 0;
		}
		break;

	/********************************************************************************/
	/*		Etiketten manuell spenden												*/
	/********************************************************************************/
	case MANUELL_SPENDEN:
	
		ein_Etikett_man_gespendet = 0;	/* muss bei jedem spendevorgang einmal zurueckgesetzt werden */ 
		
		if( p_ax_dat->regler.status == ncEIN )
		{
 			if ( (Akt_Produkt_Param.Typ_Datierung == 1) || (Akt_Produkt_Param.Typ_Datierung == 2) || (Akt_Produkt_Param.Typ_Datierung == 4) || (Akt_Produkt_Param.Typ_Datierung == 5) )
			{
				step_slave_ax = MS_DATIERUNG_ZEIT_EIN;	/* Datiersignal f�r Pr�gedatierung vor Bewegung man. Spenden ausgeben */
				Zeit_Datierung_Ein = 0;
	 		}
			else
			{
				step_slave_ax = MS_INIT_BEWEG;  		/* ohne Pr�gedatierung Bewegung gleich ausf�hren */
			}
		}
		else
		{
			/* Regler einschalten */
			regler_ein = 1;
			step_slave_ax = COMMAND;
		}
			
		break;
		
	case MS_DATIERUNG_ZEIT_EIN:

		Zeit_Datierung_Ein = Zeit_Datierung_Ein + 1;
     	if (Zeit_Datierung_Ein > 10)
        {
      		/* Zeit abgelaufen */
			step_slave_ax = MS_DATIERUNG_EIN;
		}
		break;

	/* Ausgang f�r Pr�ge-Datierung einschalten */
	case MS_DATIERUNG_EIN:

		if (Akt_Maschinen_Param.Maschine_Gebertyp == 1)	/* Inkrementalgeber */
		{
			daten_ausgaenge = 0x00000010;	/* 1. Ausgang (nach 4 Eing�ngen) auf Karte in Steckplatz 3 */
		}
		else if (Akt_Maschinen_Param.Maschine_Gebertyp == 0)	/* ENDAT-Geber */
		{
			daten_ausgaenge = 0x00100000;	 /* 1. Ausgang (nach 4 Eing�ngen) auf Karte in Steckplatz 4 */
		}

	    p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CMD_DO_SET;
	    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&daten_ausgaenge;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
			step_slave_ax = MS_W_DATIERUNG_EIN;
		}
		break;

	case MS_W_DATIERUNG_EIN:

     	if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
        {
      		/* Operation erfolgreich abgeschlossen */
			step_slave_ax = MS_W_ZEIT;
			Zeit_Datierung_Aus = 0;
		}
		break;
		
	case MS_W_ZEIT:

		Zeit_Datierung_Aus = Zeit_Datierung_Aus + 1;
     	if (Zeit_Datierung_Aus > 15)
        {
      		/* Zeit abgelaufen */
			step_slave_ax = MS_DATIERUNG_AUS;
		}

		break;
		
	/* Datierung aus wird auch vom Schritt COMMAND angesprungen */
	case MS_DATIERUNG_AUS:

		if ( (Akt_Produkt_Param.Typ_Datierung == 0) && (datierung_aus == 1) )
		{
			/* Datierung nur ausschalten, wenn vorhanden */
			datierung_aus = 0;
			step_slave_ax = COMMAND;
			break;
		}

		datierung_aus = 0;

		if (Akt_Maschinen_Param.Maschine_Gebertyp == 1)	/* Inkrementalgeber */
		{
			daten_ausgaenge = 0x00000010;	/* 1. Ausgang (nach 4 Eing�ngen) auf Karte in Steckplatz 3 */
		}
		else if (Akt_Maschinen_Param.Maschine_Gebertyp == 0)	/* ENDAT-Geber */
		{
			daten_ausgaenge = 0x00100000;	 /* 1. Ausgang (nach 4 Eing�ngen) auf Karte in Steckplatz 4 */
		}

	    p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CMD_DO_CLR;
	    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&daten_ausgaenge;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
			step_slave_ax = MS_W_DATIERUNG_AUS;
		}
		break;

	case MS_W_DATIERUNG_AUS:

     	if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
        {
      		/* Operation erfolgreich abgeschlossen */
      		if ( etikett_ref_aktiv ==1)
      		{
				step_slave_ax = ABS_BEW1;			/* nur Datierung aktiv gewesen - f�r vorw�rts referenzieren. */
      		}
      		else if ( spenden_manuell_cmd ==1)
      		{
				step_slave_ax = MS_INIT_BEWEG;  	/* man. Spenden aktiv */
      		}
      		else
      		{
				step_slave_ax = COMMAND;			/* nur Datierung aussch. aktiv gewesen */
			}
		}
		break;	
	

	case MS_INIT_BEWEG:

		/* Ab 5 gespendeten Etiketten schneller werden */
		anz_man_gespendet = anz_man_gespendet + 1;
		if (anz_man_gespendet < 5)
		{
			p_ax_dat->bewegung.basis.parameter.v_pos = p_ax_dat->grenzwert.parameter.v_pos/20; 
			p_ax_dat->bewegung.basis.parameter.v_neg = p_ax_dat->grenzwert.parameter.v_neg/20; 
			p_ax_dat->bewegung.basis.parameter.a1_pos = p_ax_dat->grenzwert.parameter.a1_pos/20; 
			p_ax_dat->bewegung.basis.parameter.a2_pos = p_ax_dat->grenzwert.parameter.a2_pos/20; 
			p_ax_dat->bewegung.basis.parameter.a1_neg = p_ax_dat->grenzwert.parameter.a1_neg/20; 
			p_ax_dat->bewegung.basis.parameter.a2_neg = p_ax_dat->grenzwert.parameter.a2_neg/20; 
		}
		else if (En_Testbetrieb == 0) /*V2.56*/
		{
			if (Akt_Maschinen_Param.Geschwindigkeit_Handspenden == 0)	/*V5.06*/
			{
				Akt_Maschinen_Param.Geschwindigkeit_Handspenden = 40;
			}
			handspenden_Teiler = 200 / Akt_Maschinen_Param.Geschwindigkeit_Handspenden;		/*V5.06*/		/*Eingabe Geschwindigkeit_Handspenden in %. Damit der Wert nicht zu hoch wird, steht im Z�hler 200 statt den rechnerisch richtigen 100*/
			
			p_ax_dat->bewegung.basis.parameter.v_pos = p_ax_dat->grenzwert.parameter.v_pos/handspenden_Teiler;						/* /5; */ 
			p_ax_dat->bewegung.basis.parameter.v_neg = p_ax_dat->grenzwert.parameter.v_neg/handspenden_Teiler;						/* /5; */  
			p_ax_dat->bewegung.basis.parameter.a1_pos = p_ax_dat->grenzwert.parameter.a1_pos/handspenden_Teiler;						/* /5; */  
			p_ax_dat->bewegung.basis.parameter.a2_pos = p_ax_dat->grenzwert.parameter.a2_pos/handspenden_Teiler;						/* /5; */  
			p_ax_dat->bewegung.basis.parameter.a1_neg = p_ax_dat->grenzwert.parameter.a1_neg/handspenden_Teiler;						/* /5; */  
			p_ax_dat->bewegung.basis.parameter.a2_neg = p_ax_dat->grenzwert.parameter.a2_neg/handspenden_Teiler;						/* /5; */  
		}
		else 
		{
			v_max_etikettenband = ( Laenge_Maschinenteilung_ber * Akt_Produkt_Param.Leistung_Fl_h / 3600.0 ) / (Akt_Produkt_Param.Faktor_Synchronweg_Master / 100.0);	
			
			p_ax_dat->bewegung.basis.parameter.v_pos = v_max_etikettenband; /*p_ax_dat->grenzwert.parameter.v_pos/2;*/ 
			p_ax_dat->bewegung.basis.parameter.v_neg = v_max_etikettenband; /*p_ax_dat->grenzwert.parameter.v_neg/2;*/ 
			p_ax_dat->bewegung.basis.parameter.a1_pos = p_ax_dat->grenzwert.parameter.a1_pos/5; 
			p_ax_dat->bewegung.basis.parameter.a2_pos = p_ax_dat->grenzwert.parameter.a2_pos/5; 
			p_ax_dat->bewegung.basis.parameter.a1_neg = p_ax_dat->grenzwert.parameter.a1_neg/5; 
			p_ax_dat->bewegung.basis.parameter.a2_neg = p_ax_dat->grenzwert.parameter.a2_neg/5;
		}
				
		if ( ncaction(ax_obj,ncBASIS_BEW,ncINIT) == ncOK )
		{
			step_slave_ax = MS_READ_TRIGGER_POS;
		}
		break;

	case MS_READ_TRIGGER_POS:
	    p_ax_dat->netzwerk.service.request.par_id = trigger2.fall_edge_s_act.id;
		p_ax_dat->netzwerk.service.daten_adr = (UDINT)&trigger2.fall_edge_s_act.data;
	    if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
	    {
	    	step_slave_ax = MS_W_READ_EDGE_POS;
	    }
	    break;
	    
	case MS_W_READ_EDGE_POS:
		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{			
			/* Wenn der Eti-Sensor vor dem Referenzieren im letzten Bereich des Etikettes steht,
			   muss zusaetzlich ein Etikettenintervall addiert werden */
			if (( Akt_Produkt_Param.Weg_LS_Spendekante % Akt_Produkt_Param.Laenge_Slave_Intervall) 
				> (Akt_Produkt_Param.Laenge_Comp_weg_Slave / 2))
			{
				p_ax_dat->bewegung.basis.parameter.s = trigger2.fall_edge_s_act.data
					+ ( Akt_Produkt_Param.Weg_LS_Spendekante % Akt_Produkt_Param.Laenge_Slave_Intervall) 
					- (Akt_Produkt_Param.Laenge_Comp_weg_Slave / 2)
					+ Akt_Produkt_Param.Laenge_Slave_Intervall;
			}
			else 
			{
				p_ax_dat->bewegung.basis.parameter.s = trigger2.fall_edge_s_act.data
					+ ( Akt_Produkt_Param.Weg_LS_Spendekante % Akt_Produkt_Param.Laenge_Slave_Intervall) 
					- (Akt_Produkt_Param.Laenge_Comp_weg_Slave / 2) 
					+ 2 * Akt_Produkt_Param.Laenge_Slave_Intervall;
			}
			
			/* Wenn kein Trigger gekommen ist oder Testbetrieb aktiv ist trotzdem eine Etikettel�nge weiterfahren */
			if ( (p_ax_dat->bewegung.basis.parameter.s < (p_ax_dat->monitor.s + Akt_Produkt_Param.Laenge_Slave_Intervall / 2))
			  || (p_ax_dat->bewegung.basis.parameter.s > (p_ax_dat->monitor.s + 2 * Akt_Produkt_Param.Laenge_Slave_Intervall))
			  || (En_Testbetrieb == 1) )
 			{
				p_ax_dat->bewegung.basis.parameter.s = p_ax_dat->monitor.s + Akt_Produkt_Param.Laenge_Slave_Intervall;
			}

			if ( ncaction(ax_obj,ncABS_BEW, ncSTART) == ncOK)
			{
				step_slave_ax = MS_W_REL_POS;
			}
		}
		break;
		
	case MS_W_REL_POS:

		eti_ref_ausgefuehrt = 1; /* f�r erneuten Start Automatik merken */

		ein_Etikett_man_gespendet = 1;

		if (p_ax_dat->bewegung.basis.status.in_pos == ncWAHR)
		{
			step_slave_ax = COMMAND;
			/*step_slave_ax = VISION_OUT_ON;*/
		}
		break;	

	/********************************************************************************/
	/*	Antriebswalzen ver- und entriegeln - Ausgang auf ACOPOS ansteuern			*/
	/********************************************************************************/
	/* Antriebswalzen verriegeln */
	case WALZENVERRIEGELUNG_ZU:

		walzen_zu_cmd = 0;

		if (Akt_Maschinen_Param.Maschine_Gebertyp == 1)	/* Inkrementalgeber */
		{
			daten_ausgaenge = 0x00000020;	/* 2.Ausgang (nach 4 Eing. f. Geber + 1 Ausg. f. Dat.) auf AC130 in Steckplatz 3 */
		}
		else if (Akt_Maschinen_Param.Maschine_Gebertyp == 0)	/* ENDAT-Geber */
		{
			daten_ausgaenge = 0x00200000;	 /* 2.Ausgang (nach 4 Eing. f. Geber + 1 Ausg. f. Dat.) auf AC130 in Steckplatz 4 */
		}

	    p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CMD_DO_SET;
	    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&daten_ausgaenge;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
			step_slave_ax = W_WALZENVERRIEGELUNG_ZU;
		}
		break;

	case W_WALZENVERRIEGELUNG_ZU:

     	if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
        {
      		/* Operation erfolgreich abgeschlossen */
			step_slave_ax = COMMAND;
		}
		break;
		
	/* Antriebswalzen entriegeln */
	case WALZENVERRIEGELUNG_AUF:

		walzen_auf_cmd = 0;

		if (Akt_Maschinen_Param.Maschine_Gebertyp == 1)	/* Inkrementalgeber */
		{
			daten_ausgaenge = 0x00000020;	/* 2.Ausgang (nach 4 Eing. f. Geber + 1 Ausg. f. Dat.) auf AC130 in Steckplatz 3 */
		}
		else if (Akt_Maschinen_Param.Maschine_Gebertyp == 0)	/* ENDAT-Geber */
		{
			daten_ausgaenge = 0x00200000;	 /* 2.Ausgang (nach 4 Eing. f. Geber + 1 Ausg. f. Dat.) auf AC130 in Steckplatz 4 */
		}

	    p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CMD_DO_CLR;
	    p_ax_dat->netzwerk.service.daten_adr = (UDINT)&daten_ausgaenge;
		
	    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
	    {
			step_slave_ax = W_WALZENVERRIEGELUNG_AUF;
		}
		break;

	case W_WALZENVERRIEGELUNG_AUF:

     	if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
        {
			step_slave_ax = COMMAND;
		}
		break;	

	/********************************************************************************/
	/*	Auslesen einer anstehenden Fehlermeldung 						 			*/
	/********************************************************************************/
	/* Im Fehlerfall muss ein aktiver Automat abgebrochen werden, damit dieser keine weiteren Fehler produziert */
	case FEHLER:
	
		if (p_ax_dat->monitor.status.fehler == ncWAHR) 
		{
			cmd_cam_ctrl_data = ncABBRUCH;
			
		    p_ax_dat->netzwerk.service.request.par_id = ACP10PAR_CMD_CAM_START;
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&cmd_cam_ctrl_data;
		    if ( ncaction(ax_obj,ncSERVICE,ncSETZEN) == ncOK )
		    {
		    	step_slave_ax = FEHLER_W_ABBRUCH;
		    }
	    }
	    else
	   	{
	   		step_slave_ax = FEHLER_TEXT;
	   	}
	    
	    break;
	    
	case FEHLER_W_ABBRUCH:

		if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{
			step_slave_ax = FEHLER_TEXT;
		}
		break;
	
	/* Warten bis Fehlernummer in die Achs - Struktur eingetragen ist  */
	case FEHLER_TEXT:
		 
		if ( p_ax_dat->meldung.satz.nummer != 0 )
		{
			step_slave_ax = FEHLER_INIT;
		}
		break;

	case FEHLER_INIT:

		/* Geberfehler wird w�hrend der ersten 4 Teilungen �berbr�ckt, da St�rungen auf der Geberleitung diese beeinflussen k�nnen*/
		if ( (p_ax_dat->meldung.satz.nummer == 39018) && (Geberueberw_Gebrueckt == 1) )
		{
			step_slave_ax = FEHLER_QUIT;
			ACOPOS_Warnung = 0;
			break;
		}
		
		memset(&meldung_text, 0 , sizeof(meldung_text) ); 	/* Fehlertextfeld initialisiern, damit keine alte Reste stehen bleiben */

		p_ax_dat->meldung.text.parameter.format = ncUMBRUCH;
		p_ax_dat->meldung.text.parameter.spalten = 20;
		p_ax_dat->meldung.text.parameter.daten_len = 320;
		p_ax_dat->meldung.text.parameter.daten_adr = ((UDINT)&(meldung_text)) + 20;
		
		if (Akt_Maschinen_Param.Sprache <= 1 )
		{
			strcpy(&p_ax_dat->meldung.text.parameter.datenmodul[0], "deutsch");
		}
		else
		{
			strcpy(&p_ax_dat->meldung.text.parameter.datenmodul[0], "english");
		}
		
		if ( ncaction(ax_obj,ncMELDUNG,ncTEXT) == ncOK )
		{
			step_slave_ax = W_FEHLER_TEXT;
		}
		break;

	/* Warten bis die Fehlertext eingetragen ist */
  	case W_FEHLER_TEXT:

    	if ( (p_ax_dat->meldung.text.status.fehler != 0) || 
    		 (p_ax_dat->meldung.text.status.zeilen != 0) )
    	{
    	
	  		/* vor dem Quittieren Fehlertext noch wegkopieren in Array f�r Anzeige */
			if (Fehler_ACOPOS_Anzahl < 10)	/* Ringpuffer max. 10 Eintr�ge sind m�glich */
			{
				Fehler_ACOPOS_Anzahl = Fehler_ACOPOS_Anzahl + 1;
			}
			else
			{
				Fehler_ACOPOS_Anzahl = 1;
			}
	
			memcpy(&Fehler_ACOPOS[Fehler_ACOPOS_Anzahl] , &meldung_text, sizeof(meldung_text));	
		
			Fehler_Info_ACOPOS[Fehler_ACOPOS_Anzahl].Nummer = p_ax_dat->meldung.satz.nummer;
			Fehler_Info_ACOPOS[Fehler_ACOPOS_Anzahl].Anzahl_Zeilen = p_ax_dat->meldung.text.status.zeilen + 1;
	
			if (p_ax_dat->monitor.status.warnung == ncWAHR )
			{
				Fehler_Info_ACOPOS[Fehler_ACOPOS_Anzahl].Fehler_Warnung = 2;
			}
			else 
			{
				Fehler_Info_ACOPOS[Fehler_ACOPOS_Anzahl].Fehler_Warnung = 1;
			}

			/* W�hrend des Einschalten Mastertrigger-warnungen unterdr�cken */
/*			if ( ((p_ax_dat->meldung.satz.nummer == 37109) || (p_ax_dat->meldung.satz.nummer == 37110)) && (ACOPOS_Ein_laeuft == 1) )
			{
				step_slave_ax = FEHLER_QUIT;
				break;
			}
*/	
			/* Fehler ausgeben, Quittieren im Task "Zustand" */
			if ( p_ax_dat->monitor.status.fehler == ncWAHR ) 
			{
				ACOPOS_Fehler = 1;
			}
			
			/* Warnung ausgeben, wenn nicht "slavetrigger missing" oder "out of window" Quittieren im Task "Zustand" */
			if ( ( p_ax_dat->monitor.status.warnung == ncWAHR ) && (p_ax_dat->meldung.satz.nummer != 37103) && (p_ax_dat->meldung.satz.nummer != 37104) )
			{
				ACOPOS_Warnung = 1;
			}
	
			if ( p_ax_dat->meldung.satz.nummer == 37104 )
			{
				Anz_ACOPOS_Warnung_trig_missing = Anz_ACOPOS_Warnung_trig_missing + 1;
			}
	
			if (p_ax_dat->meldung.satz.nummer == 37103)
			{
				Anz_ACOPOS_Warnung_trig_out_of_w = Anz_ACOPOS_Warnung_trig_out_of_w + 1;
			}
	
	  		/* Antrieb antwortet nicht auf Softwarereset - Antrieb nicht im Netzwerk? */
	  		if (p_ax_dat->meldung.satz.nummer == 32011)
	  		{
	  			ACOPOS_Neustart_noetig = 1;
	  		}
	  		/* Fehlermeldung(en) wegen FIFO-�berlauf verloren (Fehler quittieren) */
/*	  		if (p_ax_dat->meldung.satz.nummer == 32037)
	  		{
	  			ACOPOS_Neustart_noetig = 1;
	  		}
*/
	  		/* Referenzimpuls-�berwachung: Position, Aufl�sung od. Referenzimpuls fehlerhaft V2.57*/
	  		if (p_ax_dat->meldung.satz.nummer == 39018)
	  		{
	  			Fehler_Geberueberwachung = 1;
	  			ACOPOS_Fehler = 1;		/* Fehler setzen obwohl nur eine Warnung vom ACOPOS ansteht, bewirkt Maschinenstillstand und Geber Interface Neuinitialisierung */
				ACOPOS_Warnung = 0;
	  		}

	  		
			/* Spannungsversorgung an der Geberkarte AC130 fehlerhaft */
			/* - dadurch koennen Gebertakte verloren gehen --> Unsynchronitaet */
			if (p_ax_dat->meldung.satz.nummer == 39303)
			{
				Fehler_Info_ACOPOS[Fehler_ACOPOS_Anzahl].Fehler_Warnung = 1; /* Fehler setzen */	
				ACOPOS_Fehler = 1;		/* Fehler setzen obwohl nur eine Warnung vom ACOPOS ansteht */
				ACOPOS_Warnung = 0;
			}
	
	  		/* Anzahl der Warnungen vom ACOPOS z�hlen. */
			if ( p_ax_dat->monitor.status.warnung == ncWAHR ) 
			{
				Anz_ACOPOS_Warnung = Anz_ACOPOS_Warnung + 1;
	
				/* Warnungen werden gez�hlt, wenn innerhalb von 3 Etiketten keine Warnung aufgetritt, Z�hler r�cksetzen. */
				if ( abs(Pos_ACOPOS_Warnung - p_ax_dat->monitor.s) > (3 * Akt_Produkt_Param.Laenge_Slave_Intervall) )
				{
					Anz_ACOPOS_Warnung = 1; 	/* Mu� auf 1 gesetzt werden, da dies ja die erste Warnung ist! */
				}

				/* Wenn die parametrierte Anzahl der Folgefehler 0 oder 1 ist, die Folgefehleranzahl auf den alten Default Wert von 8 setzen */
				/* dadurch wird sichergestellt, dass alle Versionen vor V2.55 gleich arbeiten */
				if (Akt_Maschinen_Param.Folgefehler <= 1)
				{
					Akt_Maschinen_Param.Folgefehler = 8;
				}
				
				
				/* Wenn mehr als die parametrierte Anzahl der Folgefehler innerhalb kurzer Zeit aufgetreten sind, wird das Aggregat angehalten. */
				if (Anz_ACOPOS_Warnung >= Akt_Maschinen_Param.Folgefehler)
				{
					Zuviele_Warnungen_ACOPOS = 1; /* Anzeige im Display */
				}
				
				Pos_ACOPOS_Warnung = p_ax_dat->monitor.s; /* Position der Warnung speichern */
			}
	  		if (Akt_Maschinen_Param.Sprache <= 1)
	  		{
	  			strcpy(info_string,"Fehlernummer   ");
	  			itoa(p_ax_dat->meldung.satz.nummer, (UDINT)&nummer_string);
	  			strcat(info_string,nummer_string);
	  			memcpy(((UDINT)&(Fehler_ACOPOS[Fehler_ACOPOS_Anzahl])),(UDINT)&info_string,20);
	  		}
	  		else
	  		{
	  			strcpy(info_string,"Error-Number   ");
	  			itoa(p_ax_dat->meldung.satz.nummer, (UDINT)&nummer_string);
	  			strcat(info_string,nummer_string);
	  			memcpy(((UDINT)&(Fehler_ACOPOS[Fehler_ACOPOS_Anzahl])),(UDINT)&info_string,20);
	  		}

        	step_slave_ax = FEHLER_QUIT;
        }
	    break;

	/* Fehler quittieren */
  	case FEHLER_QUIT:

   		if ( ncaction(ax_obj,ncMELDUNG,ncQUITTIEREN) == ncOK )
	  	{	
   	 		if ( ((p_ax_dat->meldung.anzahl.fehler == 0) && (p_ax_dat->meldung.anzahl.warnung == 0)) || (ACOPOS_Neustart_noetig == 1) )
			{
   				/* Alle Fehler quittiert*/
   				step_slave_ax = NEUSTART;
				errorstep = 0;
			}
			else
			{
				step_slave_ax = FEHLER_TEXT;
			}
    	}
    	break;
		
	/* Neustart nach Fehler */
	case NEUSTART:
		if ( p_ax_dat->global.init == ncFALSCH )
		{
			step_slave_ax = REGLER_AUSSCH;
		}
		else
		{
			step_slave_ax = COMMAND;
		}
		break;
		
	/********************************************************************************/
	/*	Diagnosedaten entsprechend der Anforderung  aus dem Umrichter lesen 		*/
	/********************************************************************************/
	case READ_DIAG_INIT:
		read_diag_cmd = 0;
		step_slave_ax = READ_DIAG_DATA;
		
		switch (read_diag_Parameter)
		{
		
		case 0:				/* maximale Bandgeschwindigkeit ermittlen */
			if (lfd_Nr_Bandgeschw > 20)
			{
				read_diag_Datenwert = (DINT)(Max_Bandgeschw);
				Max_Bandgeschw = 0;
				lfd_Nr_Bandgeschw = 0;
			}	
			else
			{	
				lfd_Nr_Bandgeschw = lfd_Nr_Bandgeschw + 1;
				if (Max_Bandgeschw < p_ax_dat->monitor.v)
				{
					Max_Bandgeschw = p_ax_dat->monitor.v;
				}
			}
			step_slave_ax = COMMAND;	
			break;

		case 1:
			read_diag_id = PARID_ENCOD2_S_ACT; 		/* Encoder 2 Istwert */
			read_diag_datentyp = 1;
			read_diag_Datenwert = read_diag_gelesen; /* zuletzt gelesenen Wert an Display �bergeben */
			step_slave_ax = READ_DIAG_DATA;
			break;
			
		case 2:
			/* L�nge einer Maschinenteilung an der Flaschenaussenkante aus den Parametern berechnen */
			if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen > 1) /* Rundl�ufer */
			{
				Laenge_Maschinenteilung_ber = ( (  Akt_Produkt_Param.SK_Tellermitte + ( Akt_Maschinen_Param.Laenge_Maschinenteilung_allg *  Akt_Maschinen_Param.Anzahl_Maschinenteilungen / 6.2831853) ) * 6.2831853) / Akt_Maschinen_Param.Anzahl_Maschinenteilungen;
			}
			if (Akt_Maschinen_Param.Anzahl_Maschinenteilungen == 1) /* Geradl�ufer */
			{
				Laenge_Maschinenteilung_ber = Akt_Maschinen_Param.Laenge_Maschinenteilung_allg;
			}
				
			read_diag_id = PARID_ENCOD2_S_ACT; 		/* Encoder 2 Istwert bezogen auf Teilung */
			read_diag_datentyp = 1;
			/* zuletzt gelesenen Wert umrechnen und an Display �bergeben */
			read_diag_Datenwert = read_diag_gelesen % Laenge_Maschinenteilung_ber; 
			step_slave_ax = READ_DIAG_DATA;
			break;
			
		case 3:				/* Motorposition auslesen */
			read_diag_Datenwert = p_ax_dat->monitor.s;
			step_slave_ax = COMMAND;
			break;
			
		case 4:
			read_diag_id = PARID_UDC_ACT;			/* Zwischenkreisspannung */
			read_diag_datentyp = 0;
			read_diag_Datenwert = read_diag_gelesen * 100;
			step_slave_ax = READ_DIAG_DATA;
			break;
			
		case 5:
			read_diag_id = PARID_TEMP_HEATSINK;		/* K�hlk�rpertemperatur */
			read_diag_datentyp = 0;
			read_diag_Datenwert = read_diag_gelesen * 100;
			step_slave_ax = READ_DIAG_DATA;
			break;

		case 6:
			read_diag_id = PARID_TEMP_JUNCTION;		/* Sperrschichttemperatur */
			read_diag_datentyp = 0;
			read_diag_Datenwert = read_diag_gelesen * 100;
			step_slave_ax = READ_DIAG_DATA;
			break;
			
		case 7:
			read_diag_id = PARID_TEMP_MOTOR;		/* Motortemperatur */
			read_diag_datentyp = 0;
			read_diag_Datenwert = read_diag_gelesen * 100;
			step_slave_ax = READ_DIAG_DATA;
			break;
			
		case 8:
			if (lfd_Nr_Drehzahl > 10)
			{
				read_diag_Datenwert = Max_Drehzahl;
				Max_Drehzahl = 0;
				lfd_Nr_Drehzahl = 0;
			}	
			else
			{	
				read_diag_id = PARID_SCTRL_SPEED_ACT;		/* Motor Istdrehzahl */
				read_diag_datentyp = 0;

				lfd_Nr_Drehzahl = lfd_Nr_Drehzahl + 1;
				if (Max_Drehzahl < abs(read_diag_gelesen * 6000))	/* V2.62 */ 
				{
					Max_Drehzahl = abs(read_diag_gelesen * 6000);
				}
			}
			step_slave_ax = READ_DIAG_DATA;
			break;
			
		case 9:
			read_diag_id = PARID_TRIG2_FALL_EDGE_S_ACT;	/* Istposition fallende Flanke Trigger2 (Etikettentrigger) */
			read_diag_datentyp = 1;
			read_diag_Datenwert = read_diag_gelesen;
			step_slave_ax = READ_DIAG_DATA;
			break;
			
		case 10:
			read_diag_id = PARID_TRIG2_FALL_EDGE_COUNT;	/* Z�hler fallende Flanken Trigger2 (Etikettentrigger) */
			read_diag_datentyp = 1;
			read_diag_Datenwert = read_diag_gelesen * 100;
			step_slave_ax = READ_DIAG_DATA;
			break;

		case 11:
			if (read_diag_id != PARID_TRIG2_FALL_EDGE_S_ACT) /*Wenn zuletzt was anderes gelesen wurde initialisieren */
			{
				read_diag_Datenwert = 0;
				read_diag_gelesen = 0;
				Pos_letztes_Etikett = 0;
			}
			read_diag_id = PARID_TRIG2_FALL_EDGE_S_ACT;	/* L�nge des letzten Etikettes anzeigen */
			read_diag_datentyp = 1;
			
			if ( (Pos_letztes_Etikett != read_diag_gelesen) && (Pos_letztes_Etikett != 0) ) 
			{
				read_diag_Datenwert = read_diag_gelesen - Pos_letztes_Etikett;
				
				anz_eti = (UINT)((1.0 * read_diag_Datenwert / Akt_Produkt_Param.Laenge_Slave_Intervall) + 0.5);
				if (anz_eti != 0)
				{
					read_diag_Datenwert = read_diag_Datenwert / anz_eti; 
				}
				else
				{
					read_diag_Datenwert = read_diag_Datenwert; 
				}				
			}
	
			Pos_letztes_Etikett = read_diag_gelesen;
			step_slave_ax = READ_DIAG_DATA;
			break;
			
		case 12:
			read_diag_id = ACP10PAR_CAM_SL_TRIG_IV_MIN;	/* min. L�nge Etikett */
			read_diag_datentyp = 1;
			read_diag_Datenwert = read_diag_gelesen;
			step_slave_ax = READ_DIAG_DATA;
			break;
			
		case 13:
			read_diag_id = ACP10PAR_CAM_SL_TRIG_IV_MAX;	/* max. L�nge Etikett */
			read_diag_datentyp = 1;
			read_diag_Datenwert = read_diag_gelesen;
			step_slave_ax = READ_DIAG_DATA;
			break;
			
		case 14:
			/* Position letzter Nachtrigger ermitteln */
			read_diag_id = PARID_ENCOD2_S_ACT; 
			read_diag_datentyp = 1;
			
			if (letzte_read_diag_id != PARID_ENCOD2_S_ACT) /*Wenn zuletzt was anderes gelesen wurde initialisieren */
			{
				read_diag_gelesen = 0;
				read_diag_Datenwert = 0;
				HM_ZW_Nachtrigger_war_da = 1;
			}
			
			if ( (p_ax_dat->dig_e.status.trigger1 == 1) && (HM_ZW_Nachtrigger_war_da == 0) )
			{
				read_diag_Datenwert = read_diag_gelesen;
			}
			
			HM_ZW_Nachtrigger_war_da = p_ax_dat->dig_e.status.trigger1;

			step_slave_ax = READ_DIAG_DATA;
			break;
			
		case 15:
			read_diag_id = PARID_TRIG1_RISE_EDGE_COUNT;	/* Z�hler steigende Flanken Trigger1 (Nachtrigger) */
			read_diag_datentyp = 1;
			read_diag_Datenwert = read_diag_gelesen * 100;
			step_slave_ax = READ_DIAG_DATA;
		break;
		
		case 16:
			/* Position letzter Flada ermitteln */
			read_diag_id = PARID_ENCOD2_S_ACT; 
			read_diag_datentyp = 1;
			
			if (letzte_read_diag_id != PARID_ENCOD2_S_ACT) /* Wenn zuletzt was anderes gelesen wurde initialisieren */
			{
				read_diag_gelesen = 0;
				read_diag_Datenwert = 0;
				HM_ZW_Flada_war_da = 1;
			}
			
			if ( (p_ax_dat->dig_e.status.referenz == 1) && (HM_ZW_Flada_war_da == 0) ) 
			{
				read_diag_Datenwert = read_diag_gelesen;
			}
			HM_ZW_Flada_war_da = p_ax_dat->dig_e.status.referenz ;
			
			step_slave_ax = READ_DIAG_DATA;
			break;

		case 17:
			/* Differenz zum letzten Flada ermitteln */
			read_diag_id = PARID_ENCOD2_S_ACT; 
			read_diag_datentyp = 1;
			
			if (letzte_read_diag_id != PARID_ENCOD2_S_ACT) /* Wenn zuletzt was anderes gelesen wurde initialisieren */
			{
				read_diag_gelesen = 0;
				read_diag_Datenwert = 0;
				HM_ZW_Flada_war_da = 1;
				Pos_letzter_Flada = 0;
			}
			
			if ( (p_ax_dat->dig_e.status.referenz == 1) && (HM_ZW_Flada_war_da == 0) ) 
			{
				Pos_letzter_Flada = read_diag_gelesen;
			}
			HM_ZW_Flada_war_da = p_ax_dat->dig_e.status.referenz ;

			if (Pos_letzter_Flada != 0)
			{
				read_diag_Datenwert = read_diag_gelesen - Pos_letzter_Flada;
			}
			
			step_slave_ax = READ_DIAG_DATA;
			break;

		case 18:
			read_diag_Datenwert = Anz_ACOPOS_Warnung_trig_missing * 100;
			step_slave_ax = COMMAND; /* keine Parid auslesen */
		break;
		
		case 19:
			read_diag_Datenwert = Anz_ACOPOS_Warnung_trig_out_of_w * 100;
			step_slave_ax = COMMAND; /* keine Parid auslesen */
		break;
		
		case 20:
			read_diag_Datenwert = Akt_Anz_Eti_Rolle_Vk2 * 100;
			step_slave_ax = COMMAND; /* keine Parid auslesen */
		break;
		
		case 21:
			read_diag_Datenwert = Akt_Anz_Eti_Vmin_Vk2 * 100;
			step_slave_ax = COMMAND; /* keine Parid auslesen */
		break;
			
			/* ung�ltiger Wert angefordert */
			step_slave_ax = COMMAND;

		}
		letzte_read_diag_id = read_diag_id;
		break;
		
	case READ_DIAG_DATA:
		
		p_ax_dat->netzwerk.service.request.par_id = read_diag_id;
		if (read_diag_datentyp == 0)
		{
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&float_diag_daten;
		}
		else
		{
			p_ax_dat->netzwerk.service.daten_adr = (UDINT)&long_diag_daten;
		}
		
		if ( ncaction(ax_obj,ncSERVICE,ncLESEN) == ncOK )
		{
			step_slave_ax = W_READ_DIAG_DATA;
	    }
	    break;
	
	case W_READ_DIAG_DATA:
      	if (p_ax_dat->netzwerk.service.response.par_id == p_ax_dat->netzwerk.service.request.par_id)
		{  
			if (read_diag_datentyp == 0)
			{
				read_diag_gelesen = (DINT)float_diag_daten;
			}
			else
			{
				read_diag_gelesen = long_diag_daten;
			}
			
			step_slave_ax = COMMAND;
		}
	    break;
	
		
	}  /* Ende Schrittkette */


	/***************************************************************/
	/* Einzelne Aktionen unabh�ngig von der Schrittkette ausf�hren */
	/***************************************************************/
	if (service_setzen == 1)
	{
		service_setzen = 0;
		ncaction(ax_obj,ncSERVICE,ncSETZEN+ncDATEN_TEXT);
	}

	if (service_lesen == 1)
	{
		service_lesen = 0;
		ncaction(ax_obj,ncSERVICE,ncLESEN+ncDATEN_TEXT);
	}
	
	/* Etikettenband referenziert und 50mm gefahren - Bandrissauswertung kann aktiviert werden. */
	if (etikett_ref_ok == 1)
	{
		if (p_ax_dat->monitor.s > (pos_ref_servo + (5 * Akt_Produkt_Param.Laenge_Slave_Intervall) ) ) 
		{
			Enc1_Ref_ok = 1;
		}
	}
	else
	{
		Enc1_Ref_ok = 0;
		pos_ref_servo = p_ax_dat->monitor.s;
	}

	/* Anzahl Eti bis Rollenende Signal weitergegeben wird. */
	/* Ausgewertet, wenn keine Verklebung angew�hlt, also Verklebungstyp 0 oder 7. 7 ist eine Sondervariante, es ist eine APS2 Verklebung */
	/* angebaut, diese wird aber nicht benutzt V2.63*/
	if (Rollenende == 1)
	{
		if (((I_Rollenanwahl_Rolle_2 == 0) && (Akt_Produkt_Param.Typ_Verklebung == 0)) || ((Rollenanwahl_Rolle_2 == 0) && (Akt_Produkt_Param.Typ_Verklebung == 7)))
		{
			pos_rollenwechsel = pos_servo_rollenende + (Akt_Produkt_Param.Anz_Eti_Schnitt_1 * Akt_Produkt_Param.Laenge_Slave_Intervall);
		}
		else if (((I_Rollenanwahl_Rolle_2 == 1) && (Akt_Produkt_Param.Typ_Verklebung == 0)) || ((Rollenanwahl_Rolle_2 == 1) && (Akt_Produkt_Param.Typ_Verklebung == 7)))
		{
			pos_rollenwechsel = pos_servo_rollenende + (Akt_Produkt_Param.Anz_Eti_Schnitt_2 * Akt_Produkt_Param.Laenge_Slave_Intervall);
		}

		if (p_ax_dat->monitor.s >= pos_rollenwechsel)
		{
			Rollenende_Sperre = 1;
		}
	}
	else
	{
		Rollenende_Sperre = 0;
		pos_servo_rollenende = p_ax_dat->monitor.s;
	}

	/* Auswertung, ob ein Etikett auf dem Tr�gerband fehlt */
	if (check_Etikett == 1)
	{
		if ((I_Eti_Trigger == 1) && (BA_Auto == 1))
		{
			if (p_ax_dat->monitor.s > (pos_servo_ende_etikett + (0.5 * Akt_Produkt_Param.Laenge_Slave_Intervall) ) ) 
			{
				Etikettenluecke = 1; 			/* Wird im "aggregat"-Task quittiert. */
			}
		}
		else
		{
			pos_servo_ende_etikett = p_ax_dat->monitor.s;
		}
	}

	
	/* �berpr�fung ob Aufroller l�uft w�hrend Tr�gerband gef�rdert wird, wenn Aufroller nicht l�uft -> Web Jam - nur ausgewertet, wenn kein Testbetrieb */
	if ( ( Ana_O_Stellw_Aufroller == 0 ) && (Ana_O_Stellw_Aufroller_2 == 0) && (Haecksler_Aufroller == 0) && ( (BA_Auto == 1) || (Spenden_Manuell == 1) ) && (En_Testbetrieb == 0) )
	{
		if ( (APS_1 == 1) && ( (p_ax_dat->monitor.s - pos_stop_aufroller) > 100000 ) )
		{
			Stoerung_Bandauslauf = 1;
		}
		else if ( (APS_2 == 1) && ( (p_ax_dat->monitor.s - pos_stop_aufroller) > 100000 ) )
		{
			Stoerung_Bandauslauf = 1;
		}
	}
	else
	{
		pos_stop_aufroller = p_ax_dat->monitor.s;  
	}

	/* Geberwert des Motorgebers */
	Pos_Servo = p_ax_dat->monitor.s;

	/* Geschwindigkeit des Servos */
	/*Code added to support an accurate vision system trigger for quality assurance*/
	/*Store previous cycle speed*/
	V_Servo_old = V_Servo;
		
	V_Servo = p_ax_dat->monitor.v;
	
	
	/*Check for stop condition. 0 not used due to scan time limitations. 9/14/17 JLS*/
	if (V_Servo_old > 200 && V_Servo < 200)
		{
			servo_stop = servo_stop + 1; /*count stops for debugging purposes*/
			/*Check servo movement since last vision trigger to ensure full label speed*/
			if (Pos_Servo > servo_stop_pos)
				servo_travel = Pos_Servo - servo_stop_pos;
			else /*Rollover condition*/
				servo_travel = (Pos_Servo - 2147483647) + (217483647 - servo_stop_pos);
			
			/*Check servo movement at least 1 label length based in input parameters*/	
			if ((servo_travel) > ( Akt_Produkt_Param.Laenge_Slave_Intervall - 100) ) 
			{
				DO_PLC_VISION = 1; /*Set output to vision PLC*/
				servo_stop_pos = Pos_Servo; /*Store stop position*/
				stop_pos_record[record_index] = servo_stop_pos;/*Store stopped position for debugging purposes*/
				record_index = record_index + 1;
				if (record_index > 9)/*Reset record index at end of buffer*/
					record_index = 0;
			}
			
		}
	
	/*Turn off output after 50ms*/	
	if (DO_PLC_VISION == 1)
	{
		vision_out_time = vision_out_time + 1;
		if (vision_out_time > 5)
		{
			DO_PLC_VISION = 0;
			vision_out_time = 0;
		}
	}
	
	/* Zustand der Referenzfahrt des Etikettenbandes �bergeben */
	Etiband_ref_ok = etikett_ref_ok;

	/* HW-Endschalter als normale Eing�nge verwenden */
	I_LS_Durchhang_unten = p_ax_dat->dig_e.status.neg_hw_end;
	I_LS_Durchhang_oben  = p_ax_dat->dig_e.status.pos_hw_end;

	/* Trigger-Signale zur Anzeige im Display */
	I_Trigger_1 = p_ax_dat->dig_e.status.trigger1;
	I_Trigger_2 = p_ax_dat->dig_e.status.trigger2;
	I_Referenz  = p_ax_dat->dig_e.status.referenz;

	/* Schrittnummernaufzeichnung */
	if (step_slave_ax != step_alt)
	{		
    	step_debug[step_debug_lfd_nr] = step_slave_ax;
    	step_debug_lfd_nr = step_debug_lfd_nr + 1;
    }
    if (step_debug_lfd_nr >=100)
    {
        step_debug_lfd_nr = 0;
    }
    
    step_alt = step_slave_ax;
	
}

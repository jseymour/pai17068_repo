/*  Parameter f�r Lageregler, Sollwertgenerator, Trace und Progmod */
#define PARID_PCTRL_V_ACT                  92 /* Lageregler: Istgeschwindigkeit */
#define PARID_CMD_CONTROLLER               93 /* Kommando: Regler ein-/ausschalten */
#define PARID_STATUS_CYCLIC_S              94 /* Position f�r zyklischen Status */
                                              /* Sollposition vom letzten Aktualisierungs-Zeitpunkt (durch Broadcast-Kommando) */
#define PARID_STATUS_CYCLIC_V              95 /* Geschwindigkeit f�r zyklischen Status */
                                              /* Sollgeschwindigkeit vom letzten Aktualisierungs-Zeitpunkt (durch Broadcast-Kommando) */
#define PARID_ENCOD_COUNT_DIR              96 /* Geber-Interface: Z�hlrichtung */
                                              /* ncSTANDARD/ncINVERS */
#define PARID_ENCOD_TYPE                   97 /* Geber-Interface: Typ */
                                              /* ncENDAT */
#define PARID_AXLIM_DS_STOP                98 /* Schleppfehlergrenzwert f�r Abbruch einer Bewegung */
                                              /* Bewegungsabbruch, wenn der Schleppfehler AXLIM_DS_STOP �berschreitet */
#define PARID_AXLIM_DS_WARNING             99 /* Schleppfehlergrenzwert f�r Anzeige einer Warnung */
                                              /* Warnungs-Bit wird gesetzt, wenn der Schleppfehler AXLIM_DS_WARNING �berschreitet */
#define PARID_POS_CTRL_KV                 100 /* Lageregler: Proportional-Verst�rkung */
#define PARID_POS_CTRL_TN                 101 /* Lageregler: Integrator-Nachstellzeit */
                                              /* Mit 0.0 wird der Integrator deaktiviert */
#define PARID_POS_CTRL_T_PREDICT          102 /* Lageregler: Vorausschauzeit */
                                              /* Mit 0.0 wird die Sollwertaufschaltung deaktiviert */
#define PARID_POS_CTRL_T_TOTAL            103 /* Lageregler: Sollwert-Gesamtverz�gerungszeit */
                                              /* Kann nicht kleiner als POS_CTRL_T_PREDICT sein. */
#define PARID_POS_CTRL_P_MAX              104 /* Lageregler: Maximaler Proportionaleingriff */
#define PARID_POS_CTRL_I_MAX              105 /* Lageregler: Maximaler Integraleingriff */
#define PARID_SCALE_LOAD_UNITS            106 /* Last-Ma�stab: Einheiten pro SCALE_LOAD_MOTOR_REV Motorumdrehungen */
#define PARID_SCALE_LOAD_MOTOR_REV        107 /* Last-Ma�stab: Motorumdrehungen */
                                              /* Getriebeanpassung �ber SCALE_LOAD_UNITS und SCALE_LOAD_MOTOR_REV */
#define PARID_SCALE_ENCOD_MOTOR_REV       108 /* Geber-Ma�stab: Motorumdrehungen */
#define PARID_SCALE_ENCOD_INCR            109 /* Geber-Ma�stab: Inkremente pro SCALE_ENCOD_MOTOR_REV Motorumdrehungen */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_CMD_SIMULATION              110 /* Kommando: Simulationsmodus ein-/ausschalten */
#define PARID_PCTRL_S_ACT                 111 /* Lageregler: Istposition */
#define PARID_PCTRL_LAG_ERROR             112 /* Lageregler: Schleppfehler */
                                              /* Regelabweichung Lageregler */
#define PARID_PCTRL_S_SET                 113 /* Lageregler: Sollposition */
#define PARID_PCTRL_V_SET                 114 /* Lageregler: Sollgeschwindigkeit */
#define PARID_CMD_ABS_MOVE                115 /* Kommando: Bewegung mit absoluter Zielposition starten */
#define PARID_OVERRIDE                    116 /* Basis-Bewegungen: Override */
#define PARID_V_OVERRIDE                  117 /* Basis-Bewegungen: Geschwindigkeits-Override */
#define PARID_A_OVERRIDE                  118 /* Basis-Bewegungen: Beschleunigungs-Override */
#define PARID_AXLIM_V_POS                 119 /* Maximale Geschwindigkeit in positive Richtung */
#define PARID_AXLIM_V_NEG                 120 /* Maximale Geschwindigkeit in negative Richtung */
#define PARID_AXLIM_A1_POS                121 /* Maximale Beschleunigung in positive Richtung */
#define PARID_AXLIM_A2_POS                122 /* Maximale Verz�gerung in positive Richtung */
#define PARID_AXLIM_A1_NEG                123 /* Maximale Beschleunigung in negative Richtung */
#define PARID_AXLIM_A2_NEG                124 /* Maximale Verz�gerung in negative Richtung */
#define PARID_AXLIM_T_JOLT                125 /* Ruckzeit */
#define PARID_AXLIM_POS_SW_END            126 /* Positive SW-Endlage */
#define PARID_AXLIM_NEG_SW_END            127 /* Negative SW-Endlage */
#define PARID_SGEN_SW_END_IGNORE          128 /* SW-Endlagen ignorieren */
                                              /* 0: Vor Bewegungsstart ist Referenzieren erforderlich und SW-Endlagen */
                                              /* werden �berpr�ft */
                                              /* 1: Referenzieren nicht erforderlich, SW-Endlagen werden ignoriert */
#define PARID_STATUS_TRACE                129 /* Trace-Status  */
                                              /* ncAUS,ncTR_END,ncTR_TRACE,ncTR_TRIGG,ncTR_RING,ncTR_REST,ncTR_VERZ */
#define PARID_PCTRL_S_ACT_FRAC            130 /* Lageregler: Istposition Nachkommateil */
#define PARID_TRACE_TRIGGER_PARID         131 /* Trace: Paramter-ID f�r Trigger-Ereignis  */
#define PARID_TRACE_TRIGGER_EVENT         132 /* Trace: Trigger-Ereignis  */
                                              /* ncAUS,ncIN_FENSTER,ncAUS_FENSTER,ncUEBER_FENSTER,ncUNTER_FENSTER */
#define PARID_TRACE_TRIGGER_THRESHOLD     133 /* Trace: Trigger-Schwelle */
#define PARID_TRACE_TRIGGER_WINDOW        134 /* Trace: Trigger-Fenster */
#define PARID_TRACE_TEST_PARID            135 /* Trace: Parameter-ID f�r Test-Datum */
                                              /* wird bei TRACE_TEST_INDEX gelesen bzw. geschrieben */
#define PARID_TRACE_TEST_TYPE             136 /* Trace: Datentyp f�r Test-Datum */
                                              /* wird bei TRACE_TEST_INDEX gelesen */
#define PARID_CMD_TRACE                   137 /* Kommando: Trace starten/abbrechen */
                                              /* ncSTART: Trace starten,  ncABBRUCH: Trace abbrechen */
#define PARID_LOW_PRIOR_SW_CYCLE          138 /* Interrupt-Zyklen niederpriore SW  */
                                              /* zur Zeit maximal 20 Zyklen (50us pro Zyklus)  */
#define PARID_LOW_PRIOR_SW_MAX_CYCLE      139 /* Spitzenwert Interrupt-Zyklen niederpriore SW  */
#define PARID_TRACE_TEST_INDEX            140 /* Trace: Index f�r Test-Datum */
                                              /* f�r TRACE_TEST_PARID */
#define PARID_TRACE_T_TRACE               141 /* Trace: Aufzeichnungsdauer */
#define PARID_TRACE_T_SAMPLING            142 /* Trace: Abtastzeit */
#define PARID_TRACE_T_DELAY               143 /* Trace: Verz�gerungszeit (relativ zum Trigger-Ereignis) */
#define PARID_TRACE_MAX_DATLEN            144 /* Maximall�nge der Trace-Daten */
#define PARID_TRACE_CNT_DAT_REC           145 /* Trace-Daten: Anzahl der aufgezeichneten Datens�tze */
#define PARID_TRACE_DRV_T_SAMPLING        146 /* Trace-Daten: Korrigierte Abtastzeit */
#define PARID_TRACE_DRV_T_DELAY           147 /* Trace-Daten: Korrigierte Verz�gerungszeit */
#define PARID_TRACE_OFFS_TRIGG_REC        148 /* Trace-Daten: Offset des Trigger-Ereignisses */
                                              /* Offset des Datensatzes (ohne Header) bei dem der Trigger aufgetreten ist   */
#define PARID_TRACE_T_FIRST_REC           149 /* Trace-Daten: Zeitpunkt f�r ersten Datensatz */
#define PARID_AXLIM_T_INPOS               150 /* Wartezeit vor Meldung "In Position" */
#define PARID_HOMING_TR_S_REL             151 /* Referenzimpuls-Distanz */
#define PARID_HOMING_S                    152 /* Referenzposition */
#define PARID_HOMING_V_SWITCH             153 /* Geschwindigkeit f�r Referenzschaltersuche */
#define PARID_HOMING_V_TRIGGER            154 /* Triggergeschwindigkeit (nach Erreichen des Referenzschalters) */
#define PARID_HOMING_A                    155 /* Beschleunigung f�r Referenzieren */
#define PARID_HOMING_MODE                 156 /* Referenzier-Modus */
                                              /* ncDIREKT, ncSCHALTER_TOR, ncABS_SCHALTER, ncEND_SCHALTER */
#define PARID_HOMING_MODE_BITS            157 /* Referenzier-Modus-Steuerbits */
                                              /* Referenzschalterflanke, Startrichtung, Triggerrichtung, Ref-Impuls */
#define PARID_HOMING_TR_S_BLOCK           158 /* Distanz f�r Referenzimpuls-Blockade */
                                              /* Motorumdrehungen */
#define PARID_BASIS_MOVE_S_ABS            159 /* Basis-Bewegungen: Zielposition */
#define PARID_BASIS_MOVE_S_REL            160 /* Basis-Bewegungen: Relative Verfahrdistanz */
#define PARID_BASIS_MOVE_V_POS            161 /* Basis-Bewegungen: Geschwindigkeit in positive Richtung */
#define PARID_BASIS_MOVE_V_NEG            162 /* Basis-Bewegungen: Geschwindigkeit in negative Richtung */
#define PARID_BASIS_MOVE_A1_POS           163 /* Basis-Bewegungen: Beschleunigung in positive Richtung */
#define PARID_BASIS_MOVE_A2_POS           164 /* Basis-Bewegungen: Verz�gerung in positive Richtung */
#define PARID_BASIS_MOVE_A1_NEG           165 /* Basis-Bewegungen: Beschleunigung in negative Richtung */
#define PARID_BASIS_MOVE_A2_NEG           166 /* Basis-Bewegungen: Verz�gerung in negative Richtung */
#define PARID_CMD_HOMING                  167 /* Kommando: Referenzieren starten */
#define PARID_CMD_REL_MOVE                168 /* Kommando: Bewegung mit relativer Verfahrdistanz starten */
#define PARID_CMD_POS_MOVE                169 /* Kommando: Bewegung in positive Richtung starten */
                                              /* Geschwindigkeits-Steuerung in positive Richtung */
#define PARID_CMD_NEG_MOVE                170 /* Kommando: Bewegung in negative Richtung starten */
                                              /* Geschwindigkeits-Steuerung in negative Richtung */
#define PARID_STOP_CMD_CONF_INDEX         171 /* Index des Parametersatzes f�r den n�chsten Abbruchbefehl */
#define PARID_HOMING_OFFSET               172 /* Referenzier-Offset */
#define PARID_STOP_CONF_INDEX             173 /* Index eines Parametersatzes f�r die Abbruchkonfiguration */
#define PARID_STOP_CONF_DECEL_RAMP        174 /* Bremsrampe f�r Bewegungsabbruch */
#define PARID_STOP_CONF_CTRL_STATE        175 /* Reglerzustand nach Bewegungsabbruch */
#define PARID_CMD_MOVEMENT_STOP           176 /* Kommando: Bewegung abbrechen */
#define PARID_SGEN_TEST_INTERNAL          177 /* Interner Test-Paramter f�r Entwicklung */
#define PARID_STATUS_BITS                 178 /* Allgemeine Status-Bits  */
                                              /* Antrieb synchron, Simulationsmodus, ... */
#define PARID_STATUS_CYCLIC_BITS          179 /* Zyklische Status-Bits  */
                                              /* Digitale Eing�nge, Regler aus/ein, Referenzpunkt ok, Fehler-Bit, ... */
#define PARID_ERROR_NUMBER                180 /* Fehlernummer */
#define PARID_ERROR_INFO                  181 /* Fehler-Zusatzinfo */
                                              /* Typ abh�ngig von Fehlernummer (I4, R4, ..) */
#define PARID_ERROR_TIME                  182 /* Fehlerzeit */
                                              /* Antriebs-Systemzeit */
#define PARID_ERROR_REC                   183 /* Fehlersatz vom Antrieb */
                                              /* Der Lesevorgang entfernt (quittiert) den Fehlersatz in der FIFO */
#define PARID_DIG_IN_FORCE_ENABLE         184 /* Force-Freigabe-Bits f�r digitale Eing�nge */
                                              /* Bit 0: Force f�r Eingang nicht freigegeben, Bit 1: Force f�r Eingang freigegeben */
#define PARID_CMD_DIG_IN_FORCE            185 /* Kommando: Force-Funktion f�r digitale Eing�nge */
#define PARID_DIG_IN_ACTIVE_LEVEL         186 /* Aktiv-Pegel-Bits digitale Eing�nge */
                                              /* Bit 0: Eingang aktiv bei Pegel "low", Bit 1: Eingang aktiv bei Pegel "high" */
#define PARID_INTERRUPT_CPUTICKS          187 /* Prozessor-Zyklen Interrupt-SW */
                                              /* zur Zeit maximal 2000 Ticks (25ns pro Tick)  */
#define PARID_INTERRUPT_MAX_CPUTICKS      188 /* Spitzenwert Prozessor-Zyklen Interrupt-SW  */
#define PARID_NETWORK_LIVE_CTRL           189 /* Zeit f�r Netzwerk-Lebens�berwachung    */
                                              /* Die �berwachung wird durch einen Wert > 0 aktiviert und  */
                                              /* mit 0 deaktiviert. Der �berwachungszyklus betr�gt 4ms. */
#define PARID_CMD_CYC_SET_VALUE_MODE      190 /* Modus f�r zyklische, externe Sollwertvorgabe einschalten/ausschalten */
                                              /* Externe, zyklische Sollwertvorgabe jeden  */
                                              /* Masterzyklus zum Antrieb. Istposition und Status zum Master. */
#define PARID_CMD_BASIS_MOVE_HALT         191 /* Kommando: Basis-Bewegung anhalten (mit akt. Parametern abbrechen) */
#define PARID_CYC_MASTER_SET_POS          192 /* Zyklische Sollposition */
                                              /* Externe Sollwerte von der NC157 jeden Masterzyklus */
#define PARID_CYC_MASTER_SET_POS_FRAC     193 /* Zyklische Sollposition Nachkommateil */
                                              /* Externe Sollwerte von der NC157 jeden Masterzyklus Einheiten-Rest  */
#define PARID_TIME_CYCLE_MAX_CPUTICKS     194 /* Zyklus-Nr mit Spitzenwert Prozessor-Zyklen Interrupt-SW  */
#define PARID_TRIG1_RISE_EDGE_COUNT       195 /* Z�hler steigende Flanken Trigger1 */
#define PARID_TRIG1_FALL_EDGE_COUNT       196 /* Z�hler fallende Flanken Trigger1 */
#define PARID_TRIG2_RISE_EDGE_COUNT       197 /* Z�hler steigende Flanken Trigger2 */
#define PARID_TRIG2_FALL_EDGE_COUNT       198 /* Z�hler fallende Flanken Trigger2 */
#define PARID_TRIGGER_STOP_MODE           199 /* Modus f�r Trigger-Halt   */
                                              /* Bewegungs-Halt bei Trigger-Flanke parametrieren u. aktivieren */
                                              /* ncAUS,ncTRIGGER1+ncP_FLANKE/ncN_FLANKE,ncTRIGGER2+ncP_FLANKE/ncN_FLANKE */
#define PARID_CMD_DELETE_SYSMOD           200 /* Kommando: System-Modul l�schen */
#define PARID_SYSMOD_PARID                201 /* Parameter-ID des Systemmodules */
#define PARID_CMD_SAFETY_CODE             202 /* Befehls-Sicherungs-Code */
/* Motorparameter */
#define PARID_DATABASE_TYPE                30 /* Motor: Typ */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_SW_COMPATIBILITY_MOTOR       31 /* Motor: Softwarekompatibilit�t */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_CALIBRATION_DATE_MOTOR       32 /* Motor: Kalibrierungs-Datum */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_U_OFF_ENC_A                  33 /* Geber: Offsetspannung Kanal A */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_U_MAX_ENC_A                  34 /* Geber: Spitzenspannung Kanal A */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_RI_ENC_A                     35 /* Geber: Innenwiderstand Kanal A */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_U_OFF_ENC_B                  36 /* Geber: Offsetspannung Kanal B */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_U_MAX_ENC_B                  37 /* Geber: Spitzenspannung Kanal B */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_RI_ENC_B                     38 /* Geber: Innenwiderstand Kanal B */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_PHI_OFF_ENC                  39 /* Geber: Phasenfehler */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_MODEL_ID_MOTOR               40 /* Motor: Modell-ID */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_SERIAL_NUMBER_MOTOR          41 /* Motor: Seriennummer */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_I_RATED_BRAKE                42 /* Haltebremse: Nennstrom */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TORQUE_RATED_BRAKE           43 /* Haltebremse: Nennmoment */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_T_ON_BRAKE                   44 /* Haltebremse: Verz�gerungszeit f�r Einschalten (Blockieren) */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_T_OFF_BRAKE                  45 /* Haltebremse: Verz�gerungszeit f�r Ausschalten (L�sen) */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_WINDING_CON_MOTOR            46 /* Motor: Wicklungsverschaltung */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_POLEPAIRS_MOTOR              47 /* Motor: Polpaarzahl */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_U_RATED_MOTOR                48 /* Motor: Nennspannung */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_KE_MOTOR                     49 /* Motor: EMK-Konstante */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_N_RATED_MOTOR                50 /* Motor: Nenndrehzahl */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_N_MAX_MOTOR                  51 /* Motor: Maximaldrehzahl */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TORQUE_STALLED_MOTOR         52 /* Motor: Stillstandsdauerdrehmoment */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TORQUE_RATED_MOTOR           53 /* Motor: Nenndauerdrehmoment */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TORQUE_MAX_MOTOR             54 /* Motor: Maximal zul�ssiges Drehmoment */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_KT_MOTOR                     55 /* Motor: Drehmoment/Kraft-Konstante */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_I_STALLED_MOTOR              56 /* Motor: Stillstandsstrom */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_I_RATED_MOTOR                57 /* Motor: Nenndauerstrom */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_I_MAX_MOTOR                  58 /* Motor: Maximal zul�ssiger Strom */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_CROSS_SECTION_CU_MOTOR       59 /* Motor: Drahtquerschnitt der Statorwicklung */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_RS_MOTOR                     60 /* Motor: Stator Wicklungswiderstand */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_LS_MOTOR                     61 /* Motor: Stator Induktivit�t */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_JR_MOTOR                     62 /* Motor: L�ufertr�gheitsmoment */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_RHO_OFF_MOTOR                63 /* Geber: Nullpunktverschiebung */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_R0_MOTOR                     64 /* Motor: St�tzpunktwiderstand R0 f�r Temperaturkennlinie */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_R7_MOTOR                     65 /* Motor: St�tzpunktwiderstand R7 f�r Temperaturkennlinie */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TEMP0_MOTOR                  66 /* Motor: St�tzpunkt T0 f�r Temperaturkennlinie */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TEMP1_MOTOR                  67 /* Motor: St�tzpunkt T1 f�r Temperaturkennlinie */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TEMP2_MOTOR                  68 /* Motor: St�tzpunkt T2 f�r Temperaturkennlinie */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TEMP3_MOTOR                  69 /* Motor: St�tzpunkt T3 f�r Temperaturkennlinie */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TEMP4_MOTOR                  70 /* Motor: St�tzpunkt T4 f�r Temperaturkennlinie */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TEMP5_MOTOR                  71 /* Motor: St�tzpunkt T5 f�r Temperaturkennlinie */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TEMP6_MOTOR                  72 /* Motor: St�tzpunkt T6 f�r Temperaturkennlinie */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TEMP7_MOTOR                  73 /* Motor: St�tzpunkt T7 f�r Temperaturkennlinie */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TEMP_MAX_CU_MOTOR            74 /* Motor: Maximal zul�ssige Statorwicklungstemperatur */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_TAU_CU_MOTOR                 75 /* Motor: Ansprechzeitkonstante der Statorwicklung */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_RR_MOTOR                     76 /* Motor: Rotor Wicklungswiderstand */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_LR_MOTOR                     77 /* Motor: Rotor Induktivit�t */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_LH_MOTOR                     78 /* Motor: Hauptinduktivit�t */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_I0_MOTOR                     79 /* Motor: Magnetisierungsstrom */
                                              /* Wird �ber Geber-EnDat gelesen */
#define PARID_CMD_ENDAT_PARA               81 /* Kommando: Endat Parameter brennen/lesen */
#define PARID_ENC1_STATE                   82 /* Status: Geber f�r Transvektor-Regelung */
#define PARID_ENDAT_MANU_DATA              83 /* Endat Motorgeberdaten */
#define PARID_ENDAT_OEM_DATA               84 /* Endat OEM Datenblock */
#define PARID_ENDAT_CORR_NUM               85 /* Anzahl der Endat Korrekturen */
#define PARID_CMD_BRAKE                    86 /* Kommando: Bremse l�sen/blockieren */
#define PARID_OEM_DATA                     87 /* OEM-Datenblock */
#define PARID_ENC2_STATE                   88 /* Test Variable */
#define PARID_ENDAT_ENC2_DATA              89 /* Daten des externe Endat-Geber */
/* Statorstromauswertung */
#define PARID_I1S_ANA                     205 /* Statorstrangstrom Phase 1 */
#define PARID_I2S_ANA                     206 /* Statorstrangstrom Phase 2 */
#define PARID_I1S_OFFSET                  207 /* Statorstromoffset Phase 1 */
#define PARID_I2S_OFFSET                  208 /* Statorstromoffset Phase 2 */
#define PARID_I1S                         209 /* Statorstrangstrom Phase 1 */
#define PARID_I2S                         210 /* Statorstrangstrom Phase 2 */
/* Eingangskoordinatentransformation (Pack'sche Transformation) */
#define PARID_ISA                         211 /* Statorstrom in kartesischen Statorkoordinaten Richtung A */
#define PARID_ISB                         212 /* Statorstrom in kartesischen Statorkoordinaten Richtung B */
/* ISQ-Regler */
#define PARID_ICTRL_ISQ_REF               213 /* Stromregler: Sollstatorstrom Querkomponente */
                                              /* Raumzeiger im feldorientierten Koordinatensystem */
#define PARID_ICTRL_ISQ_ACT               214 /* Stromregler: Iststatorstrom Querkomponente */
                                              /* Raumzeiger im feldorientierten Koordinatensystem */
#define PARID_INT_ISQ                     215 /* Stromregler: ISQ Integratorwert */
                                              /* Raumzeiger im feldorientierten Koordinatensystem */
#define PARID_ICTRL_USQ_REF               216 /* Stromregler: Statorspannung Querkomponente */
                                              /* Spannungvorgabe in Raumzeigerdarstellung */
                                              /* Raumzeiger im feldorientierten Koordinatensystem */
#define PARID_ESQ_REF                     217 /* Stromregler: EMK_Vorsteuerung */
/* ISD-Regler */
#define PARID_ICTRL_ISD_REF               218 /* Stromregler: Sollstatorstrom Hauptkomponente */
                                              /* Raumzeiger im feldorientierten Koordinatensystem */
#define PARID_ICTRL_ISD_ACT               219 /* Stromregler: Iststatorstrom Hauptkomponente */
                                              /* Raumzeiger im feldorientierten Koordinatensystem */
#define PARID_INT_ISD                     220 /* Stromregler: ISD Integratorwert */
                                              /* Raumzeiger im feldorientierten Koordinatensystem */
#define PARID_ICTRL_USD_REF               221 /* Stromregler: Stellgr��e der Statorspannung (Hauptkomponente) */
                                              /* Raumzeiger im feldorientierten Koordinatensystem */
#define PARID_ISD_REF_FLUX                222 /* Flu�regler: Stellgr��e der Statorstromhauptkomponente */
                                              /* Sollwert der Statorstrom-Hauptkomponent bei IM */
                                              /* Im feldorientierten Koordinatensystem */
#define PARID_ICTRL_KV                    223 /* Stromregler: Proportional-Verst�rkung */
#define PARID_I_IS                        224 /* Stromregler: Integral Faktor */
#define PARID_ICTRL_TI                    225 /* Stromregler: Integrierzeitkonstante */
/* Ausgangskoordinatentransformation (Pack` sche R�cktransformation) */
#define PARID_USA_REF                     226 /* Statorspannung in kartesischen Koordinaten Richtung A */
#define PARID_USB_REF                     227 /* Statorspannung in kartesischen Koordinaten Richtung B */
#define PARID_US1_REF                     228 /* Sollwert der Ausgangspannung des Frequenzumrichters in Phase U */
                                              /* Mit Spannung der 3. Harmonischen (amplitutenmoduliert) */
                                              /* Bezugspotential ist -Udc */
#define PARID_US2_REF                     229 /* Sollwert der Ausgangspannung des Frequenzumrichters in Phase V */
                                              /* Mit Spannung der 3. Harmonischen (amplitutenmoduliert) */
                                              /* Bezugspotential ist -Udc */
#define PARID_US3_REF                     230 /* Sollwert der Ausgangspannung des Frequenzumrichters in Phase W */
                                              /* Mit Spannung der 3. Harmonischen (amplitutenmoduliert) */
                                              /* Bezugspotential ist -Udc */
#define PARID_UHAR3                       231 /* Spannung der 3. Harmonischen */
/* Schaltzeitenberechnung */
#define PARID_TU1                         232 /* Einschaltzeit der Phase U */
#define PARID_TU2                         233 /* Ausschaltzeit der Phase U */
#define PARID_TV1                         234 /* Einschaltzeit der Phase V */
#define PARID_TV2                         235 /* AUSschaltzeit der Phase V */
#define PARID_TW1                         236 /* Einschaltzeit der Phase W */
#define PARID_TW2                         237 /* Ausschaltzeit der Phase W */
/* Hochaufgel�ste Positionsauswertung */
#define PARID_ENC1_POS_DIG                238 /* Digitale Position f�r Motor-Geber */
#define PARID_POS_ENC_ANA                 239 /* Analoge Encoder-Position */
#define PARID_POS_ENC_HRES                240 /* hochaufgel�ste Position f�r Motor-Geber */
#define PARID_POS_ENC_OLD                 241 /* letzte hochaufgel�ste Encoder-Position */
#define PARID_D_POS_ENC                   242 /* Positionsdifferenz pro Abtastzeit */
                                              /* hochaufgel�st */
#define PARID_POS_ENC2_HRES               243 /* hochaufgel�ste Position f�r externer Geber */
#define PARID_POS_ENDAT                   244 /* �bermittelte ENDAT-Position */
                                              /* digitale Encoder-Position */
#define PARID_ENC_COS_ANA                 245 /* Analogwert von Geber Spur 1 */
#define PARID_ENC_SIN_ANA                 246 /* Analogwert von Geber Spur 2 */
#define PARID_ENC2_POS_DIG                247 /* Digitale Position f�r externen Geber */
#define PARID_LIM_TORQUE_POS              248 /* Momentenbegrenzung in positive Richtung */
#define PARID_LIM_TORQUE_NEG              249 /* Momentenbegrenzung in negative Richtung */
/* Drehzahlregler */
#define PARID_SCTRL_SPEED_REF             250 /* Drehzahlregler: Solldrehzahl */
#define PARID_SCTRL_SPEED_ACT             251 /* Drehzahlregler: Istdrehzahl */
#define PARID_INT_SPEED                   252 /* Drehzahlregler: Integratorwert */
#define PARID_SCTRL_KV                    253 /* Drehzahlregler: Proportional-Verst�rkung */
#define PARID_I_SPEED                     254 /* Integral Faktor des Integrators des Drehzahlreglers */
#define PARID_SCTRL_TN                    255 /* Drehzahlregler: Integrator-Nachstellzeit */
#define PARID_ISQ_REF_SPEED               256 /* Stellgr��e des Drehzahlreglers */
                                              /* Sollwert f�r ISQ-Regler (Statorquerstromkomponente) */
/* Flu�modell */
#define PARID_UHD                         257 /* D-Komponente der Hauptfeldspannung */
                                              /* nur Asynchronantriebe */
#define PARID_UHQ                         258 /* Q-Komponente der Hauptfeldspannung */
                                              /* nur Asynchronantriebe */
#define PARID_IMR                         259 /* Magnetisierungsstrom */
                                              /* nur Asynchronantriebe */
#define PARID_LRREC_MOD                   260 /* Reziproker Wert der Rotorinduktivit�t */
                                              /* nur Asynchronantriebe */
#define PARID_LRREC_MOD_NOM               261 /* Nomineller Wert der invertierten Rotorinduktivit�t */
                                              /* nur Asynchronantriebe */
#define PARID_FLUX_NOM                    262 /* Nomineller Flu� */
                                              /* nur Asynchronantriebe */
#define PARID_FLUX_MAX                    263 /* Maximaler Flu� */
                                              /* nur Asynchronantriebe */
#define PARID_FLUX_MIN                    264 /* Maximaler Flu� */
                                              /* nur Asynchronantriebe */
/* Regelung f�r Feldschw�chung (UEMF-Regler) */
#define PARID_UEMF_MAX                    265 /* Maximale EMK-Spannung */
                                              /* nur Asynchronantriebe */
#define PARID_FCTRL_UEMF_ACT              266 /* Feldschw�cheregler: EMK */
                                              /* nur Asynchronantriebe */
#define PARID_INT_UEMF                    267 /* Feldschw�cheregler: Integratorwert */
                                              /* nur Asynchronantriebe */
#define PARID_FCTRL_UEMF_KV               268 /* Feldschw�cheregler: Proportional-Verst�rkungsfaktor */
                                              /* nur Asynchronantriebe */
#define PARID_I_UEMF                      269 /* Feldschw�cheregler: I-Faktor des Integrator */
                                              /* nur Asynchronantriebe */
#define PARID_FCTRL_UEMF_TI               270 /* Feldschw�cheregler: Integrierzeitkonstante */
                                              /* nur Asynchronantriebe */
/* Flu�regler */
#define PARID_FCTRL_FLUX_REF              271 /* Flu�regler: Sollflu� */
                                              /* nur Asynchronantriebe */
#define PARID_FCTRL_FLUX_ACT              272 /* Flu�regler: Istflu� */
                                              /* nur Asynchronantriebe */
#define PARID_INT_FLUX                    273 /* Flu�regler: Integratorwert */
                                              /* nur Asynchronantriebe */
#define PARID_FCTRL_FLUX_KV               274 /* Flu�regler: Proportional-Verst�rkungsfaktor */
                                              /* nur Asynchronantriebe */
#define PARID_I_FLUX                      275 /* Flu�regler: Integral Faktor */
                                              /* nur Asynchronantriebe */
#define PARID_FCTRL_FLUX_TI               276 /* Flu�regler: Integrierzeitkonstante */
                                              /* nur Asynchronantriebe */
/* Drehmoment- und Leistungsberechnung und Begrenzung */
#define PARID_TORQUE_ACT                  277 /* Luftspaltdrehmoment */
#define PARID_POWER_ACT                   278 /* Luftspaltleistung */
/* Drehzahlfilter */
#define PARID_SPEED_FILTER                280 /* Gefilterter Drehzahlwert */
#define PARID_SPEED_F1                    281 /* Drehzahlfilterfaktor F1 */
#define PARID_SPEED_F2                    282 /* Drehzahlfilterfaktor F2 */
#define PARID_SCTRL_TI_FIL                283 /* Drehzahlregler: Filterzeitkonstante */
/* Strombeobachtung (I2T-Beobachtung) */
#define PARID_ABS_IS                      284 /* Statorstromamplitude */
#define PARID_IS_MAX                      285 /* Maximale Statorstromamplitude */
#define PARID_IS_FILTER                   286 /* Gefiltete Statorstromamplitude */
#define PARID_I_I2T                       290 /* Integral-Faktor f�r I2T_�berwachung */
#define PARID_TI_I2T                      291 /* Integrierzeitkonstante f�r I2T_�berwachung */
/* Rotorflu�winkel */
#define PARID_RHO                         292 /* Rotorflu�winkel */
#define PARID_SINRHO                      293 /* Sinuswert des Rotorflu�winkels */
#define PARID_COSRHO                      294 /* Cosinuswert des Rotorflu�winkels */
/* Rotor- und Schlupfwinkelgeschwindigkeit */
#define PARID_OMEGAM                      295 /* Mechanische Winkelgeschwindigkeit des Rotors */
#define PARID_OMEGAS                      296 /* Schlupfwinkelgeschwindigkeit */
#define PARID_OMEGAMR                     297 /* Winkelgeschindigkeit des Rotorsflu� */
/* Zwischenkreisspannungsmessung */
#define PARID_UDC_ACT                     298 /* Zwischenkreisspannungsregler: Istzwischenkreisspannung */
#define PARID_UDC_CTRL_TI_FIL             299 /* Zwischenkreisspannungsregler: Filterzeitkonstante */
#define PARID_UDC_F1                      300 /* Zwischenkreisspannungsregler: Filterfaktor F1 */
#define PARID_UDC_F2                      301 /* Zwischenkreisspannungsregler: Filterfaktor F2 */
#define PARID_UDC_FILTER                  302 /* Zwischenkreisspannungsregler: Gefilterte Zwischenkreisspannung */
#define PARID_UDC_CTRL_KV                 303 /* Zwischenkreisspannungsregler: Proportional-Verst�rkungsfaktor */
#define PARID_CTRL_ERROR_STATE            304 /* Fehlerstatus des Vektorreglers */
#define PARID_CHP_REF                     305 /* Zwischenkreisspannungsregler: Chopper Stellgr��e */
#define PARID_VCO                         306 /* Registerinhalt von VCO-Messung */
/* Begrenzungen */
#define PARID_LIM_SPEED                   307 /* Maximal erlaubte Drehzahl des Motors */
#define PARID_LIM_ISQ_MAX                 308 /* Maximale Statorquerstromkomponente (Begrenzung: IS_MAX) */
                                              /* Begrenzung auf Statorstromamplitute IS_MAX */
#define PARID_LIM_ISQ_POW                 309 /* Maximale Statorquerstromkomponente (Begrenzung: IS_MAX,TORQUE_MAX) */
                                              /* Begrenzung auf Statorstromamplitute IS_MAX */
                                              /* zus�tzliche Begrenzung auf maximales Drehmoment */
#define PARID_LIM_ISD                     311 /* Maximale Statorhauptstromkomponente (Begenzung: I0) */
                                              /* Begenzung aufgrund des Magnetisierungsstrom */
                                              /* Bei Synchronmotor wird LIM_ISD auf 0 begenzt */
#define PARID_LIM_FLUX_MAX                312 /* Nomineller Flu� */
#define PARID_LIM_FLUX_MIN                313 /* Untere Flu�begenzung */
#define PARID_LIM_CHP_MAX                 314 /* Maximale Choppereinschaltzeit */
#define PARID_LIM_CHP_MIN                 315 /* Minimale Choppereinschaltzeit */
#define PARID_CMD_SPEED_CONTROLLER        316 /* Geschwindigkeitsregelung einschalten/ausschalten */
#define PARID_CMD_VOLT_CONTROLLER         317 /* Drehfeldregelung mit Statorspannungsvorgabe einschalten/ausschalten */
                                              /* Mit eingepr�gter Statorspannung */
#define PARID_CMD_CURR_CONTROLLER         318 /* Drehfehlregelung mit Statorstromvorgabe einschalten/ausschalten */
                                              /* Mit eingepr�gten Statorstrom */
#define PARID_CMD_SET_VALUE_GEN           319 /* Sollwertgenerator f�r unterlagerte Regler einschalten/ausschalten */
#define PARID_CMD_TORQUE_CONTROLLER       320 /* Momentenregelung einschalten/ausschalten */
                                              /* Mit eingepr�gter Statorspannung */
/*  Inbetriebnahme */
#define PARID_USQ_IBN                     321 /* Sollwert der Statorquerspannungkomponente f�r Inbetriebnahme */
#define PARID_RHO_IBN                     322 /* Sollwert der Statorflu�winkel�nderung pro Abtastung f�r Inbetriebnahme */
#define PARID_CHP_IBN                     323 /* Chopperstellgr��e f�r Inbetriebnahme */
/*  Funktionsgenerierte und manuelle Sollwertvorgaben */
#define PARID_ISQ_REF_MAN                 324 /* Manueller Sollwert des Statorquerstrom */
#define PARID_ISQ_REF_GEN                 325 /* Funktionsgenerierter Sollwert des Statorquerstroms */
#define PARID_ISQ_REF_AMP                 326 /* Amplitude des Statorquerstroms f�r Rechteckgenerator */
#define PARID_ISQ_REF_OFF                 327 /* Offset des Statorquerstroms f�r Rechteckgenerator */
#define PARID_SPEED_REF_MAN               328 /* Manueller Sollwert der Drehzahl */
#define PARID_SPEED_REF_GEN               329 /* Funktionsgenerierter Sollwert der Drehzahl */
#define PARID_SPEED_REF_AMP               330 /* Amplitude der Drehzahl f�r Rechteckgenerator */
#define PARID_SPEED_REF_OFF               331 /* Offset der Drehzahl f�r Rechteckgenerator */
#define PARID_T_ON                        332 /* Einschaltzeit (maximale Amplitude des Rechteckimpuls) */
#define PARID_T_OFF                       333 /* Ausschaltzeit (minimale Amplitude des Rechteckimpuls) */
#define PARID_CMD_CALC_RHO_OFFSET         334 /* Drehfeldregelung einschalten/ausschalten */
                                              /* Mit eingepr�gter Statorspannung */
#define PARID_RELAY_STATE                 335 /* Relais Zustand */
                                              /* 0...ausgeschaltet, 1...eingeschaltet */
#define PARID_RHO_OB                      336 /* Beobachteter L�uferwinkel */
#define PARID_SPEED_OB                    337 /* Beobachteter L�ufergeschwindigkeit */
#define PARID_ISA_SIM                     338 /* Beobachteter L�ufergeschwindigkeit */
#define PARID_POS_OB                      339 /* Beobachteter L�ufergeschwindigkeit */
#define PARID_RHO_ENC                     340 /* Beobachteter L�ufergeschwindigkeit */
#define PARID_EMF_ASUM                    341 /* Beobachteter L�ufergeschwindigkeit */
#define PARID_EMF_BSUM                    342 /* Beobachteter L�ufergeschwindigkeit */
#define PARID_ISB_SIM                     343 /* Beobachteter L�ufergeschwindigkeit */
/* Zustandsmaschine */
#define PARID_ISA_FILTER                  350 /* gefilterter Statorquerstrom */
#define PARID_ISB_FILTER                  351 /* gefilterter Statorhauptstrom */
#define PARID_TI_IS_FIL                   352 /* Statorstromfilterzeitkonstante */
#define PARID_IS_FILTER_STATE             353 /* Stromregler: Filter Status (1..ein/0..aus) */
/* Antriebs-Synchronisierungs-Parameter */
#define PARID_SYS_TIME                    355 /* Gesamtzeit des Antriebs */
#define PARID_SYNC_MASTERDRIVE            356 /* Master f�r Antriebssynchronisation */
                                              /* (0...Slave, 1...Master) */
#define PARID_SYNC_MASTERPERIOD           357 /* Masterzykluszeit */
                                              /* Mu� ein Vielfaches von der PWM-Periode (50,100 bzw. 200 us) sein */
#define PARID_DIFF_SYS_TIME               358 /* Zeitabweichung von Mastersignal ohne Einrechnung des CAN-Timer */
#define PARID_SYNC_SYS_TIME_DIFF          359 /* Abweichung von Masterzeit */
#define PARID_SYNC_T                      360 /* Periodendauer des Rechteckimpuls f�r Systemzeit�nderung des Master */
                                              /* Nur zum Testen der Synchronisation */
#define PARID_SYNC_OFF                    361 /* Offset des Rechteckimpuls f�r Systemzeit�nderung des Master */
                                              /* Nur zum Testen der Synchronisation */
#define PARID_SYNC_AMP                    362 /* Amplitude des Rechteckimpuls f�r Systemzeit�nderung des Master */
                                              /* Nur zum Testen der Synchronisation */
#define PARID_SYNC_MSG_PERIOD             363 /* Periodendauer des Sync-Telegramms */
#define PARID_SYNC_BALANCED               364 /* Motor synchron ohne Filter */
#define PARID_VAR_I4                       24 /* Test Variable */
#define PARID_VAR_R4                       25 /* Test Variable */
#define PARID_SPEED_FILTER_TYPE            26 /* Test Variable */
#define PARID_BRMOD_BSL                  1001 /* B&R-Modul: BS-Loader */
#define PARID_BRMOD_NCSYS                1002 /* B&R-Modul: NC-Betriebssystem */
#define PARID_BRMOD_VERSION              1010 /* Version eines B&R-Modules */
#define PARID_BRMOD_DATE_TIME            1011 /* Datum und Zeit eines B&R-Modules */
#define PARID_BRC_CANID_DRV_SYNC         1020 /* CAN-ID f�r Antriebs-Synchronisierung */
#define PARID_BASIS_CANID_WR_REQU        1030 /* Basis-CAN-ID f�r Write-Request */
#define PARID_DRV_CANID_WR_REQU          1031 /* Antriebs-CAN-ID f�r Write-Request */
#define PARID_BASIS_CANID_WR_RESP        1032 /* Basis-CAN-ID f�r Write-Response */
#define PARID_DRV_CANID_WR_RESP          1033 /* Antriebs-CAN-ID f�r Write-Request */
#define PARID_BASIS_CANID_RD_REQU        1034 /* Basis-CAN-ID f�r Read-Request */
#define PARID_DRV_CANID_RD_REQU          1035 /* Antriebs-CAN-ID f�r Read-Request */
#define PARID_BASIS_CANID_RD_RESP        1036 /* Basis-CAN-ID f�r Read-Response */
#define PARID_DRV_CANID_RD_RESP          1037 /* Antriebs-CAN-ID f�r Read-Response */
#define PARID_CMD_SW_RESET               1050 /* Kommando: SW-Reset */
#define PARID_CMD_BOOT_STATE             1051 /* Kommando: Boot-Zustand wechseln */
#define PARID_BOOT_STATE                 1052 /* Boot-Zustand des Antriebes */
#define PARID_CMD_BURN_SYSMOD            1053 /* Kommando: System-Modul auf FPROM brennen */
#define PARID_STAT_BURN_SYSMOD           1054 /* Status f�r System-Modul auf FPROM brennen */
#define PARID_RD_BLOCK_SEGM              1060 /* Datenblocksegment lesen */
#define PARID_RD_BLOCK_LAST_SEGM         1061 /* Letztes Datenblocksegment lesen */
#define PARID_CMD_RD_BLOCK_ABORT         1062 /* Kommando: Datenblock-Lesezugriff abbrechen */
#define PARID_WR_BLOCK_SEGM              1070 /* Datenblocksegment schreiben */
#define PARID_WR_BLOCK_LAST_SEGM         1071 /* Letztes Datenblocksegment schreiben */
#define PARID_CMD_WR_BLOCK_ABORT         1072 /* Kommando: Datenblock-Schreibzugriff abbrechen */
#define PARID_TRACE_DATA                 1100 /* Trace-Daten */
#define PARID_BRC_REQU_CYCLIC_STATUS     1200 /* Broadcast-Anforderung des zyklischen Status */
#define PARID_DRIVE_SYNC_CMD_LATCH       1201 /* Latch-Kommando f�r Antriebssynchronisierung */
#define PARID_DRIVE_SYNC_TIMESTAMP       1202 /* Zeitstempel f�r Antriebssysnchronisierung */
#define PARID_ERROR_RESPONSE            65535 /* Fehler-Response */
/*  BLEEDER-CHOPPER-STEUERUNG */
#define PARID_UDC_CHP_START0              345 /* Zwischenkreisspannung bei der Brems-Chopper startet */
#define PARID_UDC_CHP_MAX                 346 /* Zwischenkreisspannung bei der Brems-Chopper voll durchsteuert */
#define PARID_DELTA_UDC_400C              376 /* Testpar */
/*  PWM-Schaltfrequenz */
#define PARID_F_SWITCH                    347 /* PWM-Schaltfrequenz */
/*  TEMPERATURUBERWACHUNG */
#define PARID_TEMP_HEATSINK_ANA           365 /* AD-Wandlerwert K�hlk�rpertemperatur */
#define PARID_TEMP_MOTOR_ANA              366 /* AD-Wandlerwert Motortemperatur */
#define PARID_TEMP_HEATSINK               380 /* K�hlk�rpertemperatur */
#define PARID_TEMP_MOTOR                  381 /* Motortemperatur */
#define PARID_TEMP_JUNCTION               382 /* Sperrschichttemperatur */
#define PARID_TEMP_BLEEDER                383 /* Bremswiderstandstemperatur */
#define PARID_TEMP_HEATSINK_MAX           384 /* Schleppzeiger K�hlertemperatur */
#define PARID_TEMP_MOTOR_MAX              385 /* Schleppzeiger Motortemperatur */
#define PARID_TEMP_JUNCTION_MAX           386 /* Schleppzeiger Sperrschichttemperatur */
#define PARID_TEMP_BLEEDER_MAX            387 /* Schleppzeiger Bremswiderstandstemperatur */
#define PARID_CMD_CL_TEMP_MAX             388 /* Kommando: Schleppzeiger l�schen */
                                              /* 0x0001 K�hlk�rper, 0x0002 Motor, 0x0004 Junction, 0x0008 Bleeder */
#define PARID_TEMP_WARNINGS_TO_SEND       389 /* Kommando: Temperaturwarnungen ein/ausschalten */
#define PARID_RHO_REF_GEN                 370 /* simulierte Rotorwinkelposition */
/*  KALMANFILTER FUER STROMMESSUNG */
#define PARID_IS_KAL_A                    371 /* Testparameter */
#define PARID_IS_KAL_B                    372 /* Testparameter */
#define PARID_IS_KAL_L                    373 /* Testparameter */
#define PARID_IS_KAL_C                    374 /* Testparameter */
#define PARID_IS_KAL_D                    375 /* Testparameter */
/*  EEPROM ZUGANG */
#define PARID_EEPROM_SELECT                 1 /* Nummer f�r EEPROM-Auswahl */
#define PARID_EEPROM_IDX                    2 /* Index f�r EEPROM-Daten */
#define PARID_EEPROM_DAT_R4                 3 /* EEPROM-Datum (R4-Format) */
#define PARID_EEPROM_DAT_I4                 4 /* EEPROM-Datum (I4-Format) */
#define PARID_CMD_DAT_WR                    5 /* Kommando: EEPROM-Datum ins RAM schreiben */
#define PARID_CMD_DAT_RD                    6 /* Kommando: EEPROM-Datum lesen */
#define PARID_CMD_EEPROM_WR                 7 /* Kommando: Alle EEPROM-Daten von RAM auf EEPROM schreiben */
#define PARID_TEST_R4                       8 /* Testparameter */
/*  Parameter f�r Virtuelle Achse1, 2. Geber */
#define PARID_CMD_ABS_MOVE_VAX1           400 /* Kommando: Bewegung mit absoluter Zielposition starten */
#define PARID_CMD_REL_MOVE_VAX1           401 /* Kommando: Bewegung mit relativer Verfahrdistanz starten */
#define PARID_CMD_POS_MOVE_VAX1           402 /* Kommando: Bewegung in positive Richtung starten */
                                              /* Geschwindigkeits-Steuerung in positive Richtung */
#define PARID_CMD_NEG_MOVE_VAX1           403 /* Kommando: Bewegung in negative Richtung starten */
                                              /* Geschwindigkeits-Steuerung in negative Richtung */
#define PARID_BASIS_MOVE_V_POS_VAX1       404 /* Basis-Bewegungen: Geschwindigkeit in positive Richtung */
#define PARID_BASIS_MOVE_V_NEG_VAX1       405 /* Basis-Bewegungen: Geschwindigkeit in negative Richtung */
#define PARID_BASIS_MOVE_A1_POS_VAX1      406 /* Basis-Bewegungen: Beschleunigung in positive Richtung */
#define PARID_BASIS_MOVE_A2_POS_VAX1      407 /* Basis-Bewegungen: Verz�gerung in positive Richtung */
#define PARID_BASIS_MOVE_A1_NEG_VAX1      408 /* Basis-Bewegungen: Beschleunigung in negative Richtung */
#define PARID_BASIS_MOVE_A2_NEG_VAX1      409 /* Basis-Bewegungen: Verz�gerung in negative Richtung */
#define PARID_BASIS_MOVE_S_ABS_VAX1       410 /* Basis-Bewegungen: Zielposition */
#define PARID_BASIS_MOVE_S_REL_VAX1       411 /* Basis-Bewegungen: Relative Verfahrdistanz */
#define PARID_S_SET_VAX1                  412 /* Virtuelle Achse1: Position */
#define PARID_V_SET_VAX1                  413 /* Virtuelle Achse1: Geschwindigkeit */
#define PARID_VAX1_TEST_INTERNAL          414 /* Interner Test-Paramter f�r Entwicklung */
#define PARID_SCALE_ENCOD2_UNITS          420 /* Geber2-Ma�stab: Einheiten pro SCALE_ENCOD2_REV Geberumdrehungen */
#define PARID_SCALE_ENCOD2_REV            421 /* Geber2-Ma�stab: Geberumdrehungen */
#define PARID_ENCOD2_COUNT_DIR            422 /* Geber2-Interface: Z�hlrichtung */
                                              /* ncSTANDARD/ncINVERS */
#define PARID_ENCOD2_S_ACT                423 /* Geber2: Istposition */
#define PARID_CMD_CAM_START               430 /* Kommando: Kurvenscheibenkopplung starten */
#define PARID_CAM_MA_AXIS                 431 /* Elektronische Kurvenscheibe: Master-Achse */
                                              /* ncEXTGEBER, ncV_ACHSE */
#define PARID_CAM_MA_V_MAX                433 /* Elektronische Kurvenscheibe: Maximale Geschwindigkeit der Master-Achse */
                                              /* Zur Berechnung der Ausgleichs-Polynome (Einhaltung der Achsgrenzen) */
#define PARID_CAM_MA_S_START              434 /* Elektronische Kurvenscheibe: Startposition der Master-Achse */
                                              /* 1. Ausgleich beginnt bei Masterposition = CAM_MA_S_START oder */
                                              /* falls Masterposition bereits > CAM_MA_S_START nach dem n�chsten */
                                              /* Vielfachen des Master-Intervalles (CAM_MA_S_SYNC + CAM_MA_S_COMP) */
#define PARID_CAM_MA_S_SYNC               435 /* Elektronische Kurvenscheibe: Synchronweg der Master-Achse */
                                              /* L�nge der linearen Kurve auf der Master-Seite */
                                              /* Master-Intervall = CAM_MA_S_SYNC + CAM_MA_S_COMP */
#define PARID_CAM_MA_S_COMP               436 /* Elektronische Kurvenscheibe: Ausgleichsweg der Master-Achse */
                                              /* L�nge der Ausgleichs-Kurve auf der Master-Seite */
                                              /* Master-Intervall = CAM_MA_S_SYNC + CAM_MA_S_COMP */
#define PARID_CAM_MA_TRIG_MODE            437 /* Elektronische Kurvenscheibe: Trigger-Modus Master-Achse */
                                              /* Nr. und Flanke Produkt-Trigger der Master-Achse */
                                              /* ncAUS,ncTRIGGER1+ncP_FLANKE/ncN_FLANKE,ncTRIGGER2+ncP_FLANKE/ncN_FLANKE */
                                              /* bei ncAUS wird jeder Ausgleich gefahren (keine "Produkt-Ausfall"-Behandlung)  */
#define PARID_CAM_MA_S_TRIG               438 /* Elektronische Kurvenscheibe: Relative Trigger-Position Master-Achse */
                                              /* Distanz Master-Trigger ("Produkt vorhanden") zum Beginn des Synchron- */
                                              /* Bereiches */
#define PARID_CAM_SL_S_SYNC               439 /* Elektronische Kurvenscheibe: Synchronweg der Slave-Achse */
                                              /* L�nge der linearen Kurve auf der Slave-Achse */
                                              /* Slave-Intervall = CAM_SL_S_SYNC + CAM_SL_S_COMP */
#define PARID_CAM_SL_S_COMP               440 /* Elektronische Kurvenscheibe: Ausgleichsweg der Slave-Achse */
                                              /* L�nge der Ausgleichs-Kurve auf der Slave-Achse */
                                              /* Slave-Intervall = CAM_SL_S_SYNC + CAM_SL_S_COMP */
#define PARID_CAM_SL_S_TRIG               441 /* Elektronische Kurvenscheibe: Relative Trigger-Position Slave-Achse */
                                              /* Distanz Slave-Trigger (Produkt-Vorderkante) zum Beginn des Synchron- */
                                              /* Bereiches (Spender-Kante) */
#define PARID_CAM_SL_TRIG_MODE            442 /* Elektronische Kurvenscheibe: Trigger-Modus Slave-Achse */
                                              /* Nr. und Flanke Produkt-Trigger der Slave-Achse */
                                              /* ncAUS,ncTRIGGER1+ncP_FLANKE/ncN_FLANKE,ncTRIGGER2+ncP_FLANKE/ncN_FLANKE */
                                              /* bei ncAUS wird zyklisch der parametrierte Ausgleich gefahren  */
#define PARID_CAM_SL_S_COMP_MIN           443 /* Elektronische Kurvenscheibe: Minimaler Ausgleichsweg der Slave-Achse */
                                              /* Der berechnete Ausgleichsweg (mittels Trigger-Position) wird */
                                              /* auf CAM_SL_S_COMP_MIN und CAM_SL_S_COMP_MAX begrenzt */
#define PARID_CAM_SL_S_COMP_MAX           444 /* Elektronische Kurvenscheibe: Maximaler Ausgleichsweg der Slave-Achse */
                                              /* Der berechnete Ausgleichsweg (mittels Trigger-Position) wird */
                                              /* auf CAM_SL_S_COMP_MIN und CAM_SL_S_COMP_MAX begrenzt */
#define PARID_CAM_COMP_GEAR_TYPE          445 /* Elektronische Kurvenscheibe: Ausgleichsgetriebe-Typ */
                                              /* 0 = 7Polynome (Standard), 1 = 1Polynom (Alternativ) */
#define PARID_CAM_SL_TRIG_WINDOW          446 /* Elektronische Kurvenscheibe: Trigger-Fenster Slave-Achse */
                                              /* G�ltigkeits-Fenster f�r Trigger der Slave-Achse */
                                              /* Trigger vor (Pos - Fenster) werden ignoriert */
                                              /* Trigger nach (Pos + Fenster) werden mit Pos eingetragen */
                                              /* Die n�chste "Erwartungs-Position" wird mit Pos += Sl_IV berechnet */
#define PARID_PCTRL_MODE_SWITCH           450 /* Lageregler: Schalter f�r Regler-Modus */
                                              /* 0 Default-Einstellung "normaler" Pr�diktiver Regler, */
                                              /* 1 Sollwert ohne Gesamtverz�gerungszeit (bei "Fremdachs-Kopplung") */
#define PARID_PCTRL_TEST_INTERNAL         451 /* Lageregler: Interner Test-Parameter f�r Entwicklung */
#define PARID_TRIG1_RISE_EDGE_S_ACT       452 /* Istposition steigende Flanke Trigger1 */
                                              /* Istposition bei letzter Trigger-Flankte (Latch-Pos) */
#define PARID_TRIG1_FALL_EDGE_S_ACT       453 /* Istposition fallende Flanke Trigger1 */
                                              /* Istposition bei letzter Trigger-Flankte (Latch-Pos) */
#define PARID_TRIG2_RISE_EDGE_S_ACT       454 /* Istposition steigende Flanke Trigger2 */
                                              /* Istposition bei letzter Trigger-Flankte (Latch-Pos) */
#define PARID_TRIG2_FALL_EDGE_S_ACT       455 /* Istposition fallende Flanke Trigger2 */
                                              /* Istposition bei letzter Trigger-Flankte (Latch-Pos) */

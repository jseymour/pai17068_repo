PROGRAM aggregat 5 (* Ladder Diagram *)
	CODE_INIT
		<?xml version="1.0"?>
		<?ladder Version = 1.0?>
		<!-- Automation Studio Generated XML Section -->
		<networks xmlns:dt="urn:schemas-microsoft-com:datatypes" maxcolumn="2">
			<network label="" comment="Variablen mit '0' initialisieren" row="13" column="2">
				<row><contact type="open" name="True"/><coil type="reset" name="power_on_CAN"/></row>
				<row><empty or="true"/><coil type="reset" name="Freig_BA_Auto"/></row>
				<row><empty or="true"/><coil type="reset" name="HM_BA_Auto"/></row>
				<row><empty or="true"/><coil type="reset" name="BA_Auto"/></row>
				<row><empty or="true"/><coil type="reset" name="HM_Spenden_Manuell"/></row>
				<row><empty or="true"/><coil type="reset" name="MMI_LED_BA_Auto"/></row>
				<row><empty or="true"/><coil type="reset" name="MMI_LED_Spenden_Manuell"/></row>
				<row><empty or="true"/><coil type="reset" name="Spenden_Manuell"/></row>
				<row><empty or="true"/><coil type="reset" name="Rollenende"/></row>
				<row><empty or="true"/><coil type="reset" name="Fehler_Etikettenbandriss"/></row>
				<row><empty or="true"/><coil type="reset" name="MMI_LED_Quit_Allgemein"/></row>
				<row><empty or="true"/><coil type="reset" name="MMI_LED_Walzen_Verriegelung"/></row>
				<row><empty or="true"/><coil type="reset" name="HM_Walzen_verriegeln"/></row>
			</network>
		</networks>
	END_CODE_INIT
	CODE_CYCLIC
		<?xml version="1.0"?>
		<?ladder Version = 1.0?>
		<!-- Automation Studio Generated XML Section -->
		<networks xmlns:dt="urn:schemas-microsoft-com:datatypes" maxcolumn="12">
			<network label="" comment="�nderungen: 
		****************	
		Datum    	Vers.  	Bemerkung                               											Name
		
		24.09.02 		01.94	O_Verklebung_aktiv wird jetzt im aggregat-task angesteuert.							SNH/PET
		01.10.02 		01.97	Bandriss-Initiator 2 Sekunden verz�gert										PET
		24.10.02 		01.98	Sicherungsfall an APS2 Verklebestation korrigiert, bei Verklebung_nicht_moeglich voll nur noch Sperre zu machen		PET
		24.10.02 		01.98	Ein- und Ausschalten der Automatik f�r Modulmaschine �berarbeitet,Speziell Tippbetrieb mit offnem Schutz.			PET/SIS
		06.03.03		02.54	Bei &quot;Ansteuerung_Netzschuetz&quot; die &quot;Freig_BA_Auto&quot; wieder eingef�gt, dadurch funktioniert die Netzsch�tzzeitrelais-
					�berwachung bei Nichtmodulaggregaten wieder einwandfrei.							SNH
		28.05.03		02.57	 &quot;Fehler_Geberueberwachung&quot; eingef�gt, ETIMA soll sofort stehenbleiben.						SNH
		06.06.03		02.59	 Damit bei der Modul ETIMA die Autochange Fkt. ausgef�hrt werden kann, kommt nun  gleichzeitig mit dem &quot;Sperre zu&quot;		SNH
					 Signal auch das &quot;Blitzlampen&quot; Signal. Sperre zu wenn Aufroller ganz voll (nur bei APS2 Verklebung)									
		02.07.03		02.61	Statusbits f�r Ein-/Ausschalten ge�ndert									PUA/PET
		31.07.03		02.62	Wenn die Verklebung nich m�glich ist, soll ebenfalls Rollenende ausgegeben werden, damit bei Maschinen mit			PET/SNH
					Verklebung + Autochange, der Autochange ausgef�hrt werden kann.
		04.08.03		02.63	F�r Verklebungstyp 7 - neue Rollenendeauswertung. Wenn Verklebungstyp 7 selektiert ist. (Sondervariante: Aggrega		SNH
					 mit APS2 Verklebung, die Verklebung wird aber nicht betrieben). Der gelbe Wahlschalter eines normalen Aggregates ohne
					 Verklebung fehlt, deshalb muss mit den Verklebungstastern die aktive Rolle selektiert werden. 
					Neue Auswertung aps2_verklebung_vorhanden - Aufroller voll und Sicherungsfall Verklebung ansteuern, wenn APS2-
					Verklebungs Hardware vorhanden ist.
					Signale f�r neuen Verklebungstyp 5 und 6: Aggregat mit Verklebung und Autochange eingefuegt. Bei Rollenende wird zuerst
					der Autochange auf das zweite Aggregat ausgef�hrt, am ersten Aggregat wird dann w�hrend Standby verklebt.
					Bei Aggregattyp 3 oder 4 (Verklebung und auch Autochange), hier w�rde bei einem Rollenende zuerst verklebt werden und
					erst wenn die zweite Rolle leer ist mit der Autochange Funktion auf das zweite Aggregat gewechselt werden. 
		05.08.03		02.65	Verarbeitung der optionalen Wartungsfreigabe f�r ACOPOS &quot;I_Wartungsfreigabe&quot;					PET
		14.08.03		02.66	Rollenende_Sperre wird nicht mehr w�hrend des Schnitt-Verklebeprozesses (Etima_Stop_Verklebung_2)			SNH
					der APS2 Verklebung angesteuert.
					Bei Vk7 wird das Rollenende automatisch, nach dem Auflegen einer neuen Rolle und dem Umschalten auf die
					andere Rolle, quittiert. Quittierlampenansteuerung ebenfalls geaendert.
		24.09.03		02.71	�berbr�ckung Geberfehler w�hrend erster Teilung eingef�gt								HOG
		30.01.04		02.76	Reset f�r BA_Auto eingef�gt, bei Produktwechsel w�hrend Automatik-Betrieb						PUA
		18.03.04		02.77	Bitverkn�pfungen f�r Kreuzschlitten eingef�gt. Wenn Aggregat hinten ist wird eine St�rung generiert. 		HOG
						Auch ist dann kein Auto m�glich (Einschalten wird verhindert bzw. es wird ausgeschalten)
		05.06.04		02.79	Beim bet�tigen der Taste F11 wird ein Wechsel in den Autobetrieb verhindert					HOG
						Meldung &quot;In Automatik nicht m�glich&quot;, wenn Aggregat deaktiviert werden soll, aber eingeschaltet ist 
						(incl. Handspenden)
		05.06.04		02.79	Wird die Taste &quot;Aggregat deaktivieren&quot; gedr�ckt und ist der Spender ein (etikettiert), wird kurzzeitig 		HOG
						Rollenende generiert, damit die Grundmaschine das zweite Aggregat, welches im Standby sein muss, einschaltet.
		01.07.04		02.79	Fehler CAN kann nur quittiert werden, wenn CAN_Ok auf 1 steht (siehe Task can_io)					HOG
		01.07.04		02.79	Zum richtigen Ansteuern der Blitzlampe wird bei erkanntem Rollenende sofort O_Anforderung_Rollenwechsel 		HOG
						ausgegeben (Bit: Rollenende)" row="1" column="2">
				<row><contact type="open" name="True"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Versionsgeschichte"/></row>
			</network>
			<network label="" comment="Stoerung / Anzeige Motorschutz Aggregat ausgeloest" row="1" column="2">
				<row><contact type="closed" name="I_Moschu_Aggr"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[2]"/></row>
			</network>
			<network label="" comment="Stoerung / Anzeige Sicherungsfall Aufroller- oder Abrollerpotential" row="1" column="2">
				<row><contact type="closed" name="I_Sich_Aufr_Abr"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[8]"/></row>
			</network>
			<network label="" comment="HM_Sammelstoerung1 (1 = keine St�rung)
		(NA_SU_Freigabe  beim MODUL APS Spender zeitverz�gert)" row="1" column="9">
				<row><contact type="closed" name="Fehler_Netzschuetz"/><contact type="closed" name="Fehler_Zeitrelais"/><contact type="open" name="I_Moschu_Aggr"/><contact type="open" name="I_Sich_Aufr_Abr"/><contact type="closed" name="Sich_Verkl_2"/><contact type="closed" name="Fehler_CAN"/><contact type="open" name="NA_SU_Freigabe"/><contact type="closed" name="ACOPOS_Fehler"/><line/><line/><line/><coil type="open" name="HM_Sammelstoerung1"/></row>
			</network>
			<network label="" comment="HM_Sammelstoerung2 (1 = keine St�rung)
		(NA_SU_Freigabe  beim MODUL APS Spender zeitverz�gert)" row="1" column="9">
				<row><contact type="open" name="HM_Sammelstoerung1"/><contact type="closed" name="Stoerung_Datierung"/><contact type="closed" name="SU_Geoeffnet"/><contact type="closed" name="SU_Geoeffn_Vk2"/><contact type="closed" name="Fehler_Etikettenbandriss"/><contact type="closed" name="Fehler_Etikettenbandriss_VK2"/><contact type="closed" name="Eti_Trigger_fehlt"/><contact type="closed" name="Parameterwerte_falsch"/><line/><line/><line/><coil type="open" name="HM_Sammelstoerung2"/></row>
			</network>
			<network label="" comment="Freigabe f�r Betriebsart Automatik" row="1" column="7">
				<row><contact type="open" name="HM_Sammelstoerung2"/><contact type="closed" name="Zuviele_Warnungen_ACOPOS"/><contact type="closed" name="Bandriss_Aggr_Kopf"/><contact type="closed" name="Stoerung_Bandauslauf"/><contact type="closed" name="Stoerung_Bandauslauf_2"/><contact type="closed" name="I_Wartungsfreigabe"/><line/><line/><line/><line/><line/><coil type="open" name="Freig_BA_Auto"/></row>
			</network>
			<network label="" comment="HM_Stoermeldung1 - ohne NotAus Fremd - dadurch wird an die ETIMA nicht ihr eigenes NA als St�rung gemeldet (1 = keine Stoerung)" row="1" column="9">
				<row><contact type="closed" name="Fehler_Netzschuetz"/><contact type="closed" name="Fehler_Zeitrelais"/><contact type="open" name="I_Moschu_Aggr"/><contact type="open" name="I_Sich_Aufr_Abr"/><contact type="closed" name="Sich_Verkl_2"/><contact type="closed" name="Fehler_CAN"/><contact type="open" name="NA_SU_Aggr"/><contact type="closed" name="ACOPOS_Fehler"/><line/><line/><line/><coil type="open" name="HM_Stoermeldung1"/></row>
			</network>
			<network label="" comment="HM_Stoermeldung2 (1 = keine Stoerung)" row="1" column="9">
				<row><contact type="open" name="HM_Stoermeldung1"/><contact type="closed" name="Stoerung_Datierung"/><contact type="closed" name="SU_Geoeffnet"/><contact type="closed" name="SU_Geoeffn_Vk2"/><contact type="closed" name="Fehler_Etikettenbandriss"/><contact type="closed" name="Fehler_Etikettenbandriss_VK2"/><contact type="closed" name="Eti_Trigger_fehlt"/><contact type="closed" name="Parameterwerte_falsch"/><line/><line/><line/><coil type="open" name="HM_Stoermeldung2"/></row>
			</network>
			<network label="" comment="Signalaustausch Stoerung am Spender - bei Fehler Etikettenluecke muss das Aggr. in Auto bleiben -&gt; Etima Stoerung.   Bei Verkl. nicht moeglich braucht nur die ETIMA auf Stoerung zu gehen.
		ETIMA stoppt sofort" row="2" column="9">
				<row><contact type="open" name="HM_Stoermeldung2"/><contact type="closed" name="Zuviele_Warnungen_ACOPOS"/><contact type="closed" name="Bandriss_Aggr_Kopf"/><contact type="closed" name="Etikettenluecke"/><contact type="closed" name="Stoerung_Bandauslauf"/><contact type="closed" name="Stoerung_Bandauslauf_2"/><contact type="closed" name="Aggregat_nicht_vorne"/><contact type="closed" name="auto_deaktiviert"/><line/><line/><line/><coil type="open" name="O_Stoerung_Spender"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="open" name="O_Bus_Stoerung_Spender"/></row>
			</network>
			<network label="" comment="R�cksetzen - Diagnose Ausschaltgrund - Anzeige im Display" row="5" column="2">
				<row><contact type="positive transition" name="HM_BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Auschaltgrund_1"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Auschaltgrund_2"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Auschaltgrund_3"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Auschaltgrund_4"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="reset" name="Auschaltgrund_5"/></row>
			</network>
			<network label="" comment="Diagnose Ausschaltgrund - Anzeige im Display" row="1" column="3">
				<row><contact type="open" name="HM_BA_Auto"/><contact type="closed" name="HM_Sammelstoerung1"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="set" name="Auschaltgrund_1"/></row>
			</network>
			<network label="" comment="Diagnose Ausschaltgrund - Anzeige im Display" row="1" column="3">
				<row><contact type="open" name="HM_BA_Auto"/><contact type="closed" name="Freig_BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="set" name="Auschaltgrund_2"/></row>
			</network>
			<network label="" comment="Diagnose Ausschaltgrund - Anzeige im Display" row="1" column="3">
				<row><contact type="open" name="HM_BA_Auto"/><contact type="negative transition" name="I_Ein_von_ETIMA"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="set" name="Auschaltgrund_3"/></row>
			</network>
			<network label="" comment="Diagnose Ausschaltgrund - Anzeige im Display" row="1" column="3">
				<row><contact type="open" name="HM_BA_Auto"/><contact type="negative transition" name="I_Bus_Ein_von_ETIMA"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="set" name="Auschaltgrund_4"/></row>
			</network>
			<network label="" comment="Diagnose Ausschaltgrund - Anzeige im Display" row="1" column="3">
				<row><contact type="open" name="HM_BA_Auto"/><contact type="positive transition" name="I_MMI_Auto_Taste"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="set" name="Auschaltgrund_5"/></row>
			</network>
			<network label="" comment="Ein von Etima mu� etwas verz�gert werden, da es passieren kann das die Hardware-NA-Kette etwas sp�ter bereit wird.
		In diesem Fall w�rde das SR-&quot;Aggregat Ein&quot; nicht schalten." row="4" column="6">
				<row><empty/><empty/><functionblock position="header" name="ton_aggr_ein"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="I_Ein_von_ETIMA"/><line/><functionblock position="top" type="TON"><input name="IN"/><output name="Q"/></functionblock><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="hm_i_aggregat_ein"/></row>
				<row><empty or="true"/><analog type="input" name="t#1s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><empty/></row>
				<row><empty or="true"/><contact type="open" name="Freig_BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Aggregat &quot;Ein- und Ausschalten&quot;
		Freig_BA_Auto muss wegen dem Tippen bei geoeffnetem SU (Modul-ETIMA) als erstes in der Reihe programmiert sein." row="10" column="9">
				<row><empty/><empty/><empty/><empty/><empty/><empty/><functionblock position="header" name="BA_Auto_SR"/><empty/><empty/><empty/><empty/></row>
				<row><contact type="positive transition" name="I_MMI_Auto_Taste"/><contact type="open" name="Freig_BA_Auto"/><contact type="closed" name="HM_BA_Auto"/><contact type="closed" name="auto_deaktiviert"/><contact type="closed" name="Aggregat_nicht_vorne"/><contact type="closed" name="Spenden_Manuell"/><functionblock position="top" type="SR"><input name="SET1"/><output name="Q1"/></functionblock><line/><line/><line/><coil type="open" name="HM_BA_Auto"/></row>
				<row><contact type="positive transition" name="hm_i_aggregat_ein" or="true"/><empty/><empty/><empty/><empty/><empty/><functionblock position="middle"/><empty/><empty/><empty/><empty/></row>
				<row><contact type="positive transition" name="I_Bus_Ein_von_ETIMA" or="true"/><empty/><empty/><empty/><empty/><empty/><functionblock position="middle"/><empty/><empty/><empty/><empty/></row>
				<row><contact type="positive transition" name="I_MMI_Auto_Taste"/><contact type="open" name="HM_BA_Auto"/><line/><line/><line/><line/><functionblock position="bottom"><input name="RESET"/></functionblock><empty/><empty/><empty/><empty/></row>
				<row><contact type="closed" name="Freig_BA_Auto"/><line or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="negative transition" name="I_Bus_Ein_von_ETIMA"/><line or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="negative transition" name="I_Ein_von_ETIMA"/><line or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="positive transition" name="Cmd_Reset_BA_Auto"/><line or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="Aggregat_nicht_vorne"/><line or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Kommado f�r BA_Auto l�schen zur�cksetzen" row="1" column="2">
				<row><contact type="open" name="Cmd_Reset_BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Cmd_Reset_BA_Auto"/></row>
			</network>
			<network label="" comment="Beim Zur�ckziehen des Aggregats wird eine St�rung generiert. Meldung Aggregat ist hinten&quot;." row="2" column="2">
				<row><contact type="open" name="I_Aggregat_zurueckgezogen"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="set" name="Aggregat_nicht_vorne"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="open" name="Meldung[65]"/></row>
			</network>
			<network label="" comment="Beim Quittieren die St�rung Aggregat nicht vorne zur�cksetzen, wenn Aggregat jetzt vorne ist." row="2" column="3">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><contact type="closed" name="I_Aggregat_zurueckgezogen"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Aggregat_nicht_vorne"/></row>
				<row><contact name="I_Bus_Quit_Allg" or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Meldung Aggregat war hinten - Quittieren&quot;" row="2" column="3">
				<row><contact type="open" name="Aggregat_nicht_vorne"/><contact type="closed" name="I_Aggregat_zurueckgezogen"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[66]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Beim bet�tigen der Taste F11 wird ein Wechsel in den Autobetrieb verhindert" row="5" column="10">
				<row><empty/><empty/><empty/><empty/><functionblock position="header" name="Auto_deaktiv_SR"/><empty/><functionblock position="header" name="Zeit_Auto_deaktiv"/><empty/><empty/><empty/></row>
				<row><contact type="positive transition" name="I_MMI_Auto_deaktivieren"/><contact type="closed" name="HM_BA_Auto"/><contact type="closed" name="Spenden_Manuell"/><contact type="closed" name="auto_deaktiviert"/><functionblock position="top" type="SR"><input name="SET1"/><output name="Q1"/></functionblock><line/><functionblock position="top" type="TOF"><input name="IN"/><output name="Q"/></functionblock><line/><line/><coil type="open" name="auto_deaktiviert"/></row>
				<row><empty or="true"/><contact type="open" name="auto_deaktiviert"/><line/><line/><functionblock position="bottom" or="true"><input name="RESET"/></functionblock><analog type="input" name="t#1s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[67]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty or="true"/><line/><line/><line/><line/><line/><coil type="open" name="MMI_LED_Auto_deaktivieren"/></row>
			</network>
			<network label="" comment="Meldung &quot;In Automatik nicht m�glich&quot;, wenn Aggregat deaktiviert werden soll, aber eingeschaltet ist (incl. Handspenden)" row="2" column="3">
				<row><contact type="open" name="I_MMI_Auto_deaktivieren"/><contact type="open" name="HM_BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[68]"/></row>
				<row><empty or="true"/><contact type="open" name="Spenden_Manuell"/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Wird die Taste &quot;Aggregat deaktivieren&quot; gedr�ckt und ist der Spender ein (etikettiert), wird kurzzeitig Rollenende generiert, damit die Grundmaschine das zweite Aggregat, welches im Standby sein muss, einschaltet." row="3" column="6">
				<row><empty/><empty/><empty/><functionblock position="header" name="Zeit_aggregat_stoerung_manuell"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="positive transition" name="I_MMI_Auto_deaktivieren"/><contact type="open" name="HM_BA_Auto"/><contact type="closed" name="Aggregat_standby"/><functionblock position="top" type="TOF"><input name="IN"/><output name="Q"/></functionblock><line/><line/><line/><line/><line/><line/><coil type="open" name="aggregat_rollenende_manuell"/></row>
				<row><empty/><empty/><analog type="input" name="t#1s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Geberfehler wird w�hrend der ersten 4 Teilungen �berbr�ckt, da St�rungen auf der Geberleitung diese beeinflussen k�nnen" row="1" column="2">
				<row><contact type="transition" name="HM_BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="set" name="Geberueberw_Gebrueckt"/></row>
			</network>
			<network label="" comment="Verzoegerungszeit f�r Spender &quot;BA-Auto&quot; Ein (um Netzsch�tz vorher anzusteueren) --&gt; Etikettenspender-, Abroller-, Aufroller Ein " row="3" column="5">
				<row><empty/><functionblock position="header" name="Zeit_Spender_Ein"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="closed" name="I_Rueck_Netz_Aggr"/><functionblock position="top" type="TON"><input name="IN"/><output name="Q"/></functionblock><contact type="open" name="HM_BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="BA_Auto"/></row>
				<row><analog type="input" name="t#1s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="ACOPOS_Aus_laeuft hier setzen wegen sonst auftretenden Timing-Problemen " row="1" column="2">
				<row><contact type="negative transition" name="BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="set" name="ACOPOS_Aus_laeuft"/></row>
			</network>
			<network label="" comment="Kontaktaustausch Spender ist ein (auch w�hrend Ausschalten l�uft) - Signalaustausch, LED-Anzeige, Betriebsartmeldung Auto Ein/Aus" row="4" column="7">
				<row><empty/><empty/><functionblock position="header" name="Zeit_Spender_Aus"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="BA_Auto"/><line/><functionblock position="top" type="TOF"><input name="IN"/><output name="Q"/></functionblock><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="MMI_LED_BA_Auto"/></row>
				<row><contact name="ACOPOS_Aus_laeuft" or="true"/><analog type="input" name="t#0.5s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty or="true"/><contact type="open" name="Etiband_ref_ok"/><line/><line/><line/><line/><line/><coil type="open" name="O_Spender_Ein"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="open" name="O_Bus_Spender_Ein"/></row>
			</network>
			<network label="" comment="Anzeige Spender ist Ein" row="2" column="8">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="NE"><input name="IN1"/><output name=""/></function><contact type="open" name="BA_Auto"/><contact type="closed" name="ACOPOS_Aus_laeuft"/><contact type="closed" name="ACOPOS_Ein_laeuft"/><contact type="closed" name="Aggregat_standby"/><line/><line/><line/><line/><coil type="open" name="Meldung[1]"/></row>
				<row><analog type="input" name="7"/><function position="bottom"><input name="IN2"/></function><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Anzeige Spender wird ausgeschaltet" row="1" column="2">
				<row><contact type="open" name="ACOPOS_Aus_laeuft"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[23]"/></row>
			</network>
			<network label="" comment="Anzeige Spender wird eingeschaltet" row="1" column="2">
				<row><contact type="open" name="ACOPOS_Ein_laeuft"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[47]"/></row>
			</network>
			<network label="" comment="Anzeige Spender ist ausgeschaltet" row="2" column="7">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="NE"><input name="IN1"/><output name=""/></function><contact type="closed" name="BA_Auto"/><contact type="closed" name="ACOPOS_Aus_laeuft"/><contact type="closed" name="ACOPOS_Ein_laeuft"/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[0]"/></row>
				<row><analog type="input" name="7"/><function position="bottom"><input name="IN2"/></function><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Manuelles Spenden (verl�ngert bis Netzsch�tz angesteuert hat, damit mind. 1 Etikett gespendet wird)" row="3" column="5">
				<row><contact type="open" name="I_MMI_Spenden_Man_Taste"/><line/><contact type="closed" name="BA_Auto"/><contact type="open" name="Freig_BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="HM_Spenden_Manuell"/></row>
				<row><contact type="open" name="I_Bus_Spenden_Man"/><line or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="HM_Spenden_Manuell"/><contact type="closed" name="Spenden_Manuell" or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Verzoegerungszeit f�r Manuelles Spenden Ein - Weitergabe &quot;slave_ax&quot;  (um Netzsch�tz vorher anzusteueren)" row="3" column="4">
				<row><empty/><functionblock position="header" name="Zeit_Manuell_Spenden_Ein"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="HM_Spenden_Manuell"/><functionblock position="top" type="TON"><input name="IN"/><output name="Q"/></functionblock><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Spenden_Manuell"/></row>
				<row><analog type="input" name="t#1s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Ausgabe LED - Spenden manuell" row="2" column="2">
				<row><contact type="open" name="HM_Spenden_Manuell"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="MMI_LED_Spenden_Manuell"/></row>
				<row><contact type="open" name="man_Eti_spenden_laeuft"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Freigabe Abroller / Aufroller" row="5" column="6">
				<row><empty/><functionblock position="header" name="Zeit_Man_Spenden"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="HM_Spenden_Manuell"/><functionblock position="top" type="TOF"><input name="IN"/><output name="Q"/></functionblock><contact type="closed" name="BA_Auto"/><contact type="open" name="Freig_BA_Auto"/><line/><line/><line/><line/><line/><line/><coil type="open" name="O_Freig_Aufr_Abr"/></row>
				<row><analog type="input" name="T#1s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><empty/></row>
				<row><contact type="open" name="BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="ACOPOS_Aus_laeuft"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Abfallzeit Netzschuetz" row="3" column="6">
				<row><empty/><empty/><functionblock position="header" name="Zeit_Abfall_Netzschuetz"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="HM_BA_Auto"/><line/><functionblock position="top" type="TOF"><input name="IN"/><output name="Q"/></functionblock><contact type="open" name="Freig_BA_Auto"/><line/><line/><line/><line/><line/><line/><coil type="open" name="Ansteuerung_Netzschuetz"/></row>
				<row><contact name="HM_Spenden_Manuell" or="true"/><analog type="input" name="T#300s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Netzsch�tz Aggregat / Abroller / Aufroller --- Zeitabfallansteuerung, wie mit HW-Zeitrelais
		(NA_SU_Freigabe  beim MODUL APS Spender zeitverz�gert, deshalb �berbr�cken der Zeit.)" row="4" column="6">
				<row><empty/><empty/><functionblock position="header" name="Zeit_Abfall_Zeitrelais"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="Ansteuerung_Netzschuetz"/><contact type="closed" name="Si_Kreis_Modul_Etima"/><functionblock position="top" type="TOF"><input name="IN"/><output name="Q"/></functionblock><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="O_Netz_Aggr"/></row>
				<row><empty or="true"/><analog type="input" name="T#2s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><empty/></row>
				<row><empty or="true"/><contact type="open" name="Si_Kreis_Modul_Etima"/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Automatische Verklebung aktiv (extern oder intern)
		Signal  wird auch benutzt um die ETIMA Geschwindigkeit zu reduzieren!
		APS2_Verklebung_laeuft darf bei der Autochange_Verklebung die Maschine nicht verzoegern, da hier ja w�hrend des Standby Betriebes des Aggregates verklebt wird. V2.63
		Wird bei der Autochange Verklebung die Verklebung von Hand ausgeloest muss die ETIMA verzoegern. V2.63 " row="4" column="3">
				<row><contact type="open" name="APS1_Verklebung_laeuft"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="O_Verklebung_aktiv"/></row>
				<row><contact type="open" name="APS2_Verklebung_laeuft"/><contact type="closed" name="Autochange_Verklebung"/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="Aufroller_2_laeuft"/><contact type="closed" name="APS2_Verklebung_laeuft"/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="APS2_Verklebung_manuell"/><contact type="open" name="Autochange_Verklebung"/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Automatische Verklebung aktiv (extern oder intern), soll aber nur bei aktiver interner Verklebung angezeigt werden," row="3" column="2">
				<row><contact type="open" name="I_Verklebung_aktiv"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[20]"/></row>
				<row><contact type="open" name="APS2_Verklebung_laeuft"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="APS1_Verklebung_laeuft"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Aufroller 2 l�uft muss angezeigt werden falls NJ - dauerhaft belegt ist" row="2" column="2">
				<row><contact type="open" name="Aufroller_2_laeuft"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[55]"/></row>
				<row><contact type="open" name="I_NJ_Pos_Aufroller_2"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Rollenendeauswertung f�r Rolle 1 bei &quot;1&quot; selektiert. Nur aktiv wenn Verklebungstyp 7 selektiert ist. (Sondervariante: Aggregat mit APS2 Verklebung, die Verklebung wird aber nicht betrieben. Der gelbe Wahlschalter eines normalen Aggregates ohne Verklebung fehlt, deshalb muss mit den Verklebungstastern die aktive Rolle selektiert werden. " row="5" column="6">
				<row><empty/><empty/><empty/><functionblock position="header" name="Rollenanwahl_Rolle_2_SR"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="positive transition" name="I_Taster_Walze_2"/><line/><line/><functionblock position="top" type="SR"><input name="SET1"/><output name="Q1"/></functionblock><line/><line/><line/><line/><line/><line/><coil type="open" name="Rollenanwahl_Rolle_2"/></row>
				<row><contact type="positive transition" name="I_Taster_Walze_1"/><line/><line/><functionblock position="bottom"><input name="RESET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="NE" or="true"><input name="IN1"/><output name=""/></function><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><analog type="input" name="7"/><function position="bottom"><input name="IN2"/></function><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Anzeige Spender ist Aus und welche Rolle ist selektiert" row="2" column="8">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="EQ"><input name="IN1"/><output name=""/></function><contact type="closed" name="BA_Auto"/><contact type="closed" name="ACOPOS_Aus_laeuft"/><contact type="closed" name="ACOPOS_Ein_laeuft"/><contact type="open" name="Rollenanwahl_Rolle_2"/><line/><line/><line/><line/><coil type="open" name="Meldung[58]"/></row>
				<row><analog type="input" name="7"/><function position="bottom"><input name="IN2"/></function><empty/><empty/><empty or="true"/><contact type="closed" name="Rollenanwahl_Rolle_2"/><line/><line/><line/><line/><coil type="open" name="Meldung[57]"/></row>
			</network>
			<network label="" comment="Anzeige Spender ist Ein und welche Rolle ist selektiert" row="2" column="9">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="EQ"><input name="IN1"/><output name=""/></function><contact type="open" name="BA_Auto"/><contact type="closed" name="ACOPOS_Aus_laeuft"/><contact type="closed" name="ACOPOS_Ein_laeuft"/><contact type="closed" name="Aggregat_standby"/><contact type="open" name="Rollenanwahl_Rolle_2"/><line/><line/><line/><coil type="open" name="Meldung[60]"/></row>
				<row><analog type="input" name="7"/><function position="bottom"><input name="IN2"/></function><empty/><empty/><empty/><empty or="true"/><contact type="closed" name="Rollenanwahl_Rolle_2"/><line/><line/><line/><coil type="open" name="Meldung[59]"/></row>
			</network>
			<network label="" comment="Eingang f�r Rollenende verz�gern, da bei Umschaltung von Rolle 1 auf 2 der Eingang kurz &quot;LOW&quot; wird (Schalterzwischenzustand)" row="3" column="4">
				<row><empty/><functionblock position="header" name="Zeit_Rollenende"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="closed" name="I_LS_Rollenende_1"/><functionblock position="top" type="TON"><input name="IN"/><output name="Q"/></functionblock><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="hm_I_LS_Rollenende_1"/></row>
				<row><analog type="input" name="t#1s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Eingang f�r Rollenende verz�gern, da bei Umschaltung von Rolle 1 auf 2 der Eingang kurz &quot;LOW&quot; wird (Schalterzwischenzustand)" row="3" column="4">
				<row><empty/><functionblock position="header" name="ton_Rollenende_2"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="closed" name="I_LS_Rollenende_2"/><functionblock position="top" type="TON"><input name="IN"/><output name="Q"/></functionblock><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="hm_I_LS_Rollenende_2"/></row>
				<row><analog type="input" name="t#1s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Abroller Rollenende erreicht
		Wenn die Verklebung nich m�glich ist, soll ebenfalls Rollenende ausgegeben werden, damit bei Maschinen mit Verklebung + Autochange, der Autochange ausgef�hrt werden kann.
		Wenn Verklebungstyp 7 selektiert ist, soll ebenfalls das Rollenende der Rolle 1 ausgewertet werden.
		Bei Vk7 wird das Rollenende automatisch, nach dem Auflegen einer neuen Rolle und dem Umschalten auf die andere Rolle, quittiert. V2.66 " row="7" column="12">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="EQ"><input name="IN1"/><output name=""/></function><contact type="open" name="hm_I_LS_Rollenende_1"/><contact type="open" name="BA_Auto"/><contact type="closed" name="O_Verklebung_aktiv"/><line/><line/><line/><line/><line/><coil type="open" name="hm_rollenende_1"/></row>
				<row><analog type="input" name="0"/><function position="bottom" or="true"><input name="IN2"/></function><empty or="true"/><contact name="ACOPOS_Aus_laeuft" or="true"/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><empty/></row>
				<row><empty/><empty/><empty or="true"/><contact type="closed" name="I_MMI_Quit_Allg_Taste"/><contact type="closed" name="I_Bus_Quit_Allg"/><contact type="open" name="hm_rollenende_1"/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><empty/><empty/><empty or="true"/><contact type="open" name="hm_I_LS_Rollenende_1"/><line or="true"/><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="EQ"><input name="IN1"/><output name=""/></function><contact type="open" name="hm_I_LS_Rollenende_1"/><contact type="open" name="BA_Auto"/><contact type="closed" name="O_Verklebung_aktiv" or="true"/><empty/></row>
				<row><empty/><empty/><empty/><empty/><empty/><analog type="input" name="7"/><function position="bottom" or="true"><input name="IN2"/></function><empty or="true"/><contact name="ACOPOS_Aus_laeuft" or="true"/><empty or="true"/><empty/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><contact type="open" name="hm_I_LS_Rollenende_1"/><contact type="open" name="hm_rollenende_1"/><line or="true"/><empty/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><contact type="closed" name="Rollenanwahl_Rolle_2" or="true"/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Abroller Rollenende2 erreicht  - nur ausgewertet f�r Verklebungstyp 7
		Wenn die Verklebung nich m�glich ist, soll ebenfalls Rollenende ausgegeben werden, damit bei Maschinen mit Verklebung + Autochange, der Autochange ausgef�hrt werden kann.
		Bei Vk7 wird das Rollenende automatisch, nach dem Auflegen einer neuen Rolle und dem Umschalten auf die andere Rolle, quittiert. V2.66" row="4" column="7">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="EQ"><input name="IN1"/><output name=""/></function><contact type="open" name="hm_I_LS_Rollenende_2"/><contact type="open" name="BA_Auto"/><contact type="closed" name="O_Verklebung_aktiv"/><line/><line/><line/><line/><line/><coil type="open" name="hm_rollenende_2"/></row>
				<row><analog type="input" name="7"/><function position="bottom" or="true"><input name="IN2"/></function><empty or="true"/><contact name="ACOPOS_Aus_laeuft" or="true"/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><empty/></row>
				<row><empty/><empty/><empty or="true"/><contact type="open" name="hm_I_LS_Rollenende_2"/><contact type="open" name="hm_rollenende_2"/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><empty/><empty/><empty or="true"/><contact name="Rollenanwahl_Rolle_2" or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Abroller Rollenende 1 oder Rollenende 2  erreicht - Anzeige Rollenende" row="2" column="3">
				<row><contact type="open" name="hm_rollenende_1"/><contact type="closed" name="Rollenanwahl_Rolle_2"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Rollenende"/></row>
				<row><contact type="open" name="hm_rollenende_2"/><contact type="open" name="Rollenanwahl_Rolle_2"/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><coil type="open" name="Meldung[5]"/></row>
			</network>
			<network label="" comment="Abroller Rollenende erreicht - Anzahl Etiketten gez�hlt(slave_ax) / Weitergabe an Maschine
		Aufroller voll 
		...
		Beh�ltersperre an der ETIMA wird geschlossen (Sperre zu durch Aggregat)
		Autochange_Uebernahme veranlasst den Autochange Wechsel an der ETIMA. Wird erzeugt bei Verklebungstyp 5 und 6, wo bei Rollenende zuerst ein Autochange ausgefuehrt wird und dann erst verklebt das Standby-Aggregat. V2.63
		Etima_Stop_Verklebung_2 w�rde die ETIMA verlangsamen, muss wenn Verklebung 5 oder 6 deshalb mit der Autochange_Verklebung gesperrt werden. V2.63" row="5" column="2">
				<row><contact type="open" name="Rollenende_Sperre"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="negated" name="O_Rollenende_Sperre"/></row>
				<row><contact type="open" name="Verklebung_nicht_moeglich"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="Aufroller_ganz_voll_APS2"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="Autochange_Uebernahme"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="aggregat_rollenende_manuell"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="HM KL Quittieren Allgemein
		Nur bei Verklebungstyp 0 muss die Quittierleuchte angesteuert werden, bei Verklebungstyp 7 ist das Rollenende selbsquittierend mit dem Auflegen einer neuen Rolle und dem Umschalten der aktiven Rolle. V2.66" row="2" column="6">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="EQ"><input name="IN1"/><output name=""/></function><contact type="open" name="hm_rollenende_1"/><contact type="open" name="I_LS_Rollenende_1"/><line/><line/><line/><line/><line/><line/><coil type="set" name="HM_KL_Quit_Allg"/></row>
				<row><analog type="input" name="0"/><function position="bottom"><input name="IN2"/></function><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Stoerung Etikettenbandriss vor Aggregatkopf - Option bei automatischer Verklebung" row="4" column="8">
				<row><empty/><functionblock position="header" name="zeit_bandriss_vk2"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="I_LS_Bandriss_Vk2"/><functionblock position="top" type="TON"><input name="IN"/><output name="Q"/></functionblock><contact type="open" name="Enc1_Ref_ok"/><contact type="open" name="BA_Auto"/><contact type="closed" name="En_Testbetrieb"/><line/><line/><line/><line/><line/><coil type="open" name="Fehler_Etikettenbandriss_VK2"/></row>
				<row><analog type="input" name="t#0.1s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty or="true"/><contact name="ACOPOS_Aus_laeuft" or="true"/><empty or="true"/><contact type="closed" name="I_LS_Bandriss_Vk2"/><line/><line/><line/><line/><coil type="set" name="HM_KL_Quit_Allg"/></row>
				<row><contact type="open" name="Fehler_Etikettenbandriss_VK2"/><contact type="closed" name="I_Bus_Quit_Allg"/><contact type="closed" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line or="true"/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[64]"/></row>
			</network>
			<network label="" comment="Stoerung Etikettenbandriss" row="4" column="8">
				<row><empty/><functionblock position="header" name="zeit_bandriss"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="closed" name="I_NJ_Etikettenbandriss"/><functionblock position="top" type="TON"><input name="IN"/><output name="Q"/></functionblock><contact type="open" name="Enc1_Ref_ok"/><contact type="open" name="BA_Auto"/><contact type="closed" name="En_Testbetrieb"/><line/><line/><line/><line/><line/><coil type="open" name="Fehler_Etikettenbandriss"/></row>
				<row><analog type="input" name="t#2s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty or="true"/><contact name="ACOPOS_Aus_laeuft" or="true"/><empty or="true"/><contact type="open" name="I_NJ_Etikettenbandriss"/><line/><line/><line/><line/><coil type="set" name="HM_KL_Quit_Allg"/></row>
				<row><contact type="open" name="Fehler_Etikettenbandriss"/><contact type="closed" name="I_Bus_Quit_Allg"/><contact type="closed" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line or="true"/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[13]"/></row>
			</network>
			<network label="" comment="Etikettenbandriss NACH Aggregatkopf" row="2" column="2">
				<row><contact type="open" name="Bandriss_Aggr_Kopf"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[53]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Etikettenbandriss NACH Aggregatkopf quittieren" row="2" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Bandriss_Aggr_Kopf"/></row>
				<row><contact type="open" name="I_Bus_Quit_Allg"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Stau Tr�gerband im Aggregatkopf - Erfassung in &quot;slave_ax&quot;" row="2" column="2">
				<row><contact type="open" name="Stoerung_Bandauslauf"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[45]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Stau Tr�gerband im Aggregatkopf quittieren - Erfassung in &quot;slave_ax&quot;" row="2" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Stoerung_Bandauslauf"/></row>
				<row><contact type="open" name="I_Bus_Quit_Allg"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Stau Tr�gerband im Aggregatkopf - Erfassung in &quot;aufroll&quot;" row="2" column="2">
				<row><contact type="open" name="Stoerung_Bandauslauf_2"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[54]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Stau Tr�gerband im Aggregatkopf quittieren - Erfassung in &quot;aufroll&quot;" row="2" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Stoerung_Bandauslauf_2"/></row>
				<row><contact type="open" name="I_Bus_Quit_Allg"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Fehler Geber�berwachung - Erfassung in &quot;slave_ax&quot;
		Im slave_ax wird ein ACOPOS_Fehler generiert, der die Maschine anh�lt. Steht die Maschine still, kann quittiert werden. Der ACOPOS_Fehler stoesst auch eine Neuinitialisierung der Geber Interfaces an.
		MMI_LED_BA_AUTO beinhaltet BA_Auto und ACOPOS_Aus_laeuft" row="1" column="3">
				<row><contact type="open" name="Fehler_Geberueberwachung"/><contact type="closed" name="MMI_LED_BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Fehler Geber�berwachung quittieren - - Erfassung in &quot;slave_ax&quot;
		MMI_LED_BA_AUTO beinhaltet BA_Auto und ACOPOS_Aus_laeuft" row="2" column="4">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><contact type="open" name="Fehler_Geberueberwachung"/><contact type="closed" name="MMI_LED_BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Fehler_Geberueberwachung"/></row>
				<row><contact name="I_Bus_Quit_Allg" or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="APS2 Verklebung ist vorhanden, genauer gesagt die Hardware. - Verklebungstypen: 3, 4, 5, 6, 7, 8 und 9. Bei 7 wird nicht verklebt, aber die Verklebungshardware wird genutzt." row="2" column="4">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="GE"><input name="IN1"/><output name=""/></function><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="APS2_Verklebung_vorhanden"/></row>
				<row><analog type="input" name="3"/><function position="bottom"><input name="IN2"/></function><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="APS 2 Aufroller voll -  Blitzlampe ansteuern (Auswertung auch bei Verklebungstyp 7)" row="2" column="3">
				<row><contact type="open" name="APS2_Verklebung_vorhanden"/><contact type="open" name="I_LS_Aufroller_voll"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[41]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="open" name="Aufroller_voll_APS2"/></row>
			</network>
			<network label="" comment="APS 2 Sicherungsfall an Verklebung (Auswertung auch bei Verklebungstyp 7)" row="2" column="3">
				<row><contact type="open" name="APS2_Verklebung_vorhanden"/><contact type="closed" name="I_Sich_Verkl_2"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[44]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="open" name="Sich_Verkl_2"/></row>
			</network>
			<network label="" comment="APS 2 Meldung: Neue Rolle vorbereiten - bei geoeffnetem SU und bei NA unterdruecken
		(NA_SU_Freigabe  beim MODUL APS Spender zeitverz�gert)" row="1" column="4">
				<row><contact type="open" name="Rollenwechsel_Abroller_Vk2"/><contact type="closed" name="SU_Geoeffn_Vk2"/><contact type="open" name="NA_SU_Freigabe"/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[43]"/></row>
			</network>
			<network label="" comment="Blitzlampe Rollenwechsel ansteuern
		(NA_SU_Freigabe  beim MODUL APS Spender zeitverz�gert)
		Warnung Etikettenmangel - Blitzlampe an der ETIMA wird angesteuert
		Damit bei der Modul ETIMA die Autochange Fkt. ausgef�hrt werden kann, mu� gleichzeitig mit dem &quot;Sperre zu&quot; Signal auch das &quot;Blitzlampen&quot; Signal kommen.
		&quot;Sperre zu&quot; wird erzeugt durch Rollenende_Sperre, Verklebung_nicht_moeglich, Aufroller_ganz_voll_APS2 -&gt; ACHTUNG: Etima_Stop_Verklebung_2 darf den Rollenwechsel nicht mit ansteuern. V2.62
		Autochange_Uebernahme veranlasst den Autochange Wechsel an der ETIMA. Wird erzeugt bei Verklebungstyp 5 und 6, wo bei Rollenende zuerst ein Autochange ausgefuehrt wird und dann erst verklebt das Standby-Aggregat. V2.63
		Zum richtigen Ansteuern der Blitzlampe wird bei erkanntem Rollenende sofort O_Anforderung_Rollenwechsel ausgegeben (Bit: Rollenende). V02.79" row="9" column="4">
				<row><contact type="open" name="Aufroller_voll_APS2"/><line/><contact type="open" name="NA_SU_Freigabe"/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="O_Anforderung_Rollenwechsel"/></row>
				<row><contact type="open" name="Rollenwechsel_Abroller_Vk1"/><line or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><empty/></row>
				<row><contact type="open" name="Rollenwechsel_Abroller_Vk2"/><contact type="closed" name="SU_Geoeffn_Vk2" or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><empty/></row>
				<row><contact type="open" name="Rollenende_Sperre"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="Verklebung_nicht_moeglich"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="Aufroller_ganz_voll_APS2"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="Autochange_Uebernahme"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="aggregat_rollenende_manuell"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="open" name="Rollenende"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Stoerung Datierung" row="3" column="5">
				<row><contact type="open" name="I_Dat_Stoerung"/><contact type="open" name="BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Stoerung_Datierung"/></row>
				<row><contact name="Stoerung_Datierung" or="true"/><contact type="closed" name="I_Bus_Quit_Allg"/><contact type="closed" name="I_MMI_Quit_Allg_Taste" or="true"/><contact type="closed" name="I_Dat_Stoerung"/><line/><line/><line/><line/><line/><line/><line/><coil type="set" name="HM_KL_Quit_Allg"/></row>
				<row><empty or="true"/><contact type="open" name="ACOPOS_Aus_laeuft"/><line or="true"/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[52]"/></row>
			</network>
			<network label="" comment="ACOPOS - Fehleanzeige mit Quittierlampe" row="2" column="2">
				<row><contact type="open" name="ACOPOS_Fehler"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[11]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="ACOPOS - Fehler quittieren" row="2" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="ACOPOS_Fehler"/></row>
				<row><contact type="open" name="I_Bus_Quit_Allg"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="ACOPOS - Warnung" row="1" column="2">
				<row><contact type="open" name="ACOPOS_Warnung"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[14]"/></row>
			</network>
			<network label="" comment="ACOPOS - Warnung quittieren auch beim Einschalten von Automatik" row="3" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="ACOPOS_Warnung"/></row>
				<row><contact type="open" name="I_Bus_Quit_Allg"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><contact type="positive transition" name="BA_Auto"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="zuviele ACOPOS - Warnungen" row="2" column="2">
				<row><contact type="open" name="Zuviele_Warnungen_ACOPOS"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[21]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="St�rung zuviele Warnungen ACOPOS - quittieren" row="2" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Zuviele_Warnungen_ACOPOS"/></row>
				<row><contact type="open" name="I_Bus_Quit_Allg"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Meldung: Power On - Reset n�tig" row="1" column="2">
				<row><contact type="open" name="ACOPOS_Neustart_noetig"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[42]"/></row>
			</network>
			<network label="" comment="Parameter falsch oder nicht eingestellt" row="2" column="2">
				<row><contact type="open" name="Parameterwerte_falsch"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[22]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Parameter falsch oder nicht eingestellt -  quittieren" row="2" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Parameterwerte_falsch"/></row>
				<row><contact type="open" name="I_Bus_Quit_Allg"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Kein Triggereingang w�hrend der Referenzfahrt" row="2" column="2">
				<row><contact type="open" name="Eti_Trigger_fehlt"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[15]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Kein Triggereingang w�hrend der Referenzfahrt - quittieren" row="2" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Eti_Trigger_fehlt"/></row>
				<row><contact type="open" name="I_Bus_Quit_Allg"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Pufferbatterie leer" row="1" column="2">
				<row><contact type="open" name="Batterie_Fehler"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[19]"/></row>
			</network>
			<network label="" comment="St�rung Klimager�t" row="1" column="2">
				<row><contact type="open" name="I_Stoerung_Klima"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[56]"/></row>
			</network>
			<network label="" comment="Aggregat im Bereitschaftsbetrieb - anderes Aggr. etikettiert" row="2" column="7">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="NE"><input name="IN1"/><output name=""/></function><contact type="open" name="Aggregat_standby"/><contact type="open" name="BA_Auto"/><contact type="closed" name="ACOPOS_Aus_laeuft"/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[36]"/></row>
				<row><analog type="input" name="7"/><function position="bottom"><input name="IN2"/></function><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Aggregat im Bereitschaftsbetrieb - anderes Aggr. etikettiert und welche Rolle ist selektiert" row="2" column="8">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="EQ"><input name="IN1"/><output name=""/></function><contact type="open" name="Aggregat_standby"/><contact type="open" name="BA_Auto"/><contact type="closed" name="ACOPOS_Aus_laeuft"/><contact type="open" name="Rollenanwahl_Rolle_2"/><line/><line/><line/><line/><coil type="open" name="Meldung[62]"/></row>
				<row><analog type="input" name="7"/><function position="bottom"><input name="IN2"/></function><empty/><empty/><empty or="true"/><contact type="closed" name="Rollenanwahl_Rolle_2"/><line/><line/><line/><line/><coil type="open" name="Meldung[61]"/></row>
			</network>
			<network label="" comment="Referenzfahrt f�r Inkrementalgeber (als Maschinengeber) ist aktiv" row="1" column="2">
				<row><contact type="open" name="Enc2_Ref_Fahrt"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[18]"/></row>
			</network>
			<network label="" comment="Etikettenluecke festgestelt" row="2" column="2">
				<row><contact type="open" name="Etikettenluecke"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[37]"/></row>
				<row><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Etikettenlueckenueerwachung - quittieren" row="2" column="2">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Etikettenluecke"/></row>
				<row><contact type="open" name="I_Bus_Quit_Allg"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
			</network>
			<network label="" comment="Anzeige der optionalen Wartungsfreigabe f�r ACOPOS" row="1" column="2">
				<row><contact type="open" name="I_Wartungsfreigabe"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[63]"/></row>
			</network>
			<network label="" comment="Walzenverriegelung nach laengerer Stillstandszeit ausschalten" row="3" column="4">
				<row><empty/><functionblock position="header" name="Zeit_Abfall_Walzenverr"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="O_Netz_Aggr"/><functionblock position="top" type="TOF"><input name="IN"/><output name="Q"/></functionblock><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Abfall_Walzenverr"/></row>
				<row><analog type="input" name="T#3h"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Walzenverriegelungt &quot;Ein- und Ausschalten&quot; - wird beim Spenden automatisch eingeschalten, kann bei geschlossenem SU von Hand geschalten werden
		Walzenverriegelung darf bei NA nicht �ffnen, da dadurch der Folgefehler &quot;Bandriss&quot; generiert wird." row="9" column="6">
				<row><empty/><empty/><empty/><functionblock position="header" name="Walzen_verriegeln_SR"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="HM_Spenden_Manuell"/><line/><contact type="closed" name="SU_Geoeffnet"/><functionblock position="top" type="SR"><input name="SET1"/><output name="Q1"/></functionblock><line/><line/><line/><line/><line/><line/><coil type="open" name="HM_Walzen_verriegeln"/></row>
				<row><contact type="open" name="HM_BA_Auto"/><line or="true"/><empty/><functionblock position="middle"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="positive transition" name="I_MMI_Walzen_Verriegelung"/><contact type="closed" name="HM_Walzen_verriegeln" or="true"/><empty/><functionblock position="middle"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="positive transition" name="I_BUS_Walzen_Verriegelung" or="true"/><empty/><empty/><functionblock position="middle"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="positive transition" name="I_MMI_Walzen_Verriegelung"/><contact type="open" name="HM_Walzen_verriegeln"/><contact type="closed" name="BA_Auto"/><functionblock position="bottom"><input name="RESET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="positive transition" name="I_BUS_Walzen_Verriegelung" or="true"/><empty/><empty or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="negative transition" name="Abfall_Walzenverr"/><line/><line or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
				<row><contact type="open" name="SU_Geoeffnet"/><line/><line or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="Anzeige Walzen sind verriegelt" row="1" column="2">
				<row><contact type="open" name="Walzen_verriegelt"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="MMI_LED_Walzen_Verriegelung"/></row>
			</network>
			<network label="" comment="Fehler dezentrales CAN - Modul der Verklebung APS 1 (Typ 2) und APS 2 (Typ 3, 4, 5, 6, 7, 8 und 9)" row="2" column="5">
				<row><analog type="input" name="Akt_Produkt_Param.Typ_Verklebung"/><function position="top" type="GE"><input name="IN1"/><output name=""/></function><contact type="open" name="Fehler_CAN"/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="Meldung[10]"/></row>
				<row><analog type="input" name="2"/><function position="bottom"><input name="IN2"/></function><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="HM_KL_Quit_Allg"/></row>
			</network>
			<network label="" comment="Fehler dezentrales CAN - Modul der Verklebung - quittieren" row="5" column="4">
				<row><contact type="open" name="I_MMI_Quit_Allg_Taste"/><contact type="open" name="CAN_Ok"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="Fehler_CAN"/></row>
				<row><contact name="I_Bus_Quit_Allg" or="true"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><coil type="set" name="power_on_CAN"/></row>
				<row><empty/><functionblock position="header" name="Zeit_power_on_CAN"/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty or="true"/><empty/></row>
				<row><contact type="closed" name="power_on_CAN"/><functionblock position="top" type="TOF"><input name="IN"/><output name="Q"/></functionblock><line/><line/><line/><line/><line/><line/><line/><line or="true"/><empty/></row>
				<row><analog type="input" name="T#5s"/><functionblock position="bottom"><input name="PT"/><output name="ET"/></functionblock><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/><empty/></row>
			</network>
			<network label="" comment="KL Quittieren Allgemein" row="2" column="4">
				<row><contact type="open" name="HM_KL_Quit_Allg"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="MMI_LED_Quit_Allgemein"/></row>
				<row><contact type="open" name="HM_LED_Quit_Si_Kreis"/><contact name="Takt_1Hz" or="true"/><contact type="closed" name="HM_LED_Quit_Si_Kreis"/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="open" name="O_Bus_KL_Quit"/></row>
			</network>
			<network label="" comment="KL Quittieren Allgemein Ruecksetzten" row="1" column="2">
				<row><contact type="open" name="HM_KL_Quit_Allg"/><line/><line/><line/><line/><line/><line/><line/><line/><line/><line/><coil type="reset" name="HM_KL_Quit_Allg"/></row>
			</network>
		</networks>
	END_CODE_CYCLIC
END_PROGRAM

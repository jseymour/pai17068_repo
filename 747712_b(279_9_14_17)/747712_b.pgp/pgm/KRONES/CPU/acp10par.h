/**************************************************************************** 
*                    B & R   P O S I T I O N I N G                          * 
***************************************************************************** 
*                                                                           * 
*            Header File for Library ACP10PAR (Version 0544)                * 
*                                                                           * 
**************************** COPYRIGHT (C) ********************************** 
*     THIS SOFTWARE IS THE PROPERTY OF B&R AUSTRIA: ALL RIGHTS RESERVED.    * 
*     NO PART OF THIS SOFTWARE MAY BE USED OR COPIED IN ANY WAY WITHOUT     * 
*              THE PRIOR WRITTEN PERMISSION OF B&R AUSTRIA.                 * 
****************************************************************************/ 

#ifndef ACP10PAR_H_ 
#define ACP10PAR_H_ 

#define ACP10PAR_R_BLEEDER_EXT               10 /* (REAL) Widerstandswert des externen Bremswiderstands [Ohm] */
#define ACP10PAR_TEMP_MAX_BLEEDER_EXT        11 /* (REAL) Maximal zul�ssige Temperatur des externen Bremswiderstands [Grad] */
#define ACP10PAR_RTH_BLEEDER_AMB_EXT         12 /* (REAL) Thermischer Widerstandswert des externen Bremswiderstands [Grad] */
#define ACP10PAR_CTH_BLEEDER_EXT             13 /* (REAL) Thermische Kapazit�t des externen Bremswiderstands [Ws] */
#define ACP10PAR_CMD_EN_OC_STOP              28 /* (UINT) Kommando: Freigabe f�r Notstop bei �berstrom */
#define ACP10PAR_MOTOR_TYPE                  30 /* (UINT) Motor: Typ */
#define ACP10PAR_MOTOR_COMPATIBILITY         31 /* (UINT) Motor: Softwarekompatibilit�t */
#define ACP10PAR_MOTOR_BRAKE_CURR_RATED      42 /* (REAL) Motor-Haltebremse: Nennstrom [A] */
#define ACP10PAR_MOTOR_BRAKE_TORQ_RATED      43 /* (REAL) Motor-Haltebremse: Nennmoment [Nm] */
#define ACP10PAR_MOTOR_BRAKE_ON_TIME         44 /* (REAL) Motor-Haltebremse: Verz�gerungszeit Blockieren [s] */
#define ACP10PAR_MOTOR_BRAKE_OFF_TIME        45 /* (REAL) Motor-Haltebremse: Verz�gerungszeit L�ften [s] */
#define ACP10PAR_MOTOR_WIND_CONNECT          46 /* (USINT) Motor: Wicklungsverschaltung */
#define ACP10PAR_MOTOR_POLEPAIRS             47 /* (USINT) Motor: Polpaarzahl */
#define ACP10PAR_MOTOR_VOLTAGE_RATED         48 /* (REAL) Motor: Nennspannung [V] */
#define ACP10PAR_MOTOR_VOLTAGE_CONST         49 /* (REAL) Motor: Spannungskonstante [mVmin] */
#define ACP10PAR_MOTOR_SPEED_RATED           50 /* (REAL) Motor: Nenndrehzahl [1/min] */
#define ACP10PAR_MOTOR_SPEED_MAX             51 /* (REAL) Motor: Maximaldrehzahl [1/min] */
#define ACP10PAR_MOTOR_TORQ_STALL            52 /* (REAL) Motor: Stillstandsmoment [Nm] */
#define ACP10PAR_MOTOR_TORQ_RATED            53 /* (REAL) Motor: Nennmoment [Nm] */
#define ACP10PAR_MOTOR_TORQ_MAX              54 /* (REAL) Motor: Spitzenmoment [Nm] */
#define ACP10PAR_MOTOR_TORQ_CONST            55 /* (REAL) Motor: Momentkonstante [Nm/A] */
#define ACP10PAR_MOTOR_CURR_STALL            56 /* (REAL) Motor: Stillstandsstrom [A] */
#define ACP10PAR_MOTOR_CURR_RATED            57 /* (REAL) Motor: Nennstrom [A] */
#define ACP10PAR_MOTOR_CURR_MAX              58 /* (REAL) Motor: Spitzenstrom [A] */
#define ACP10PAR_MOTOR_WIND_CROSS_SECT       59 /* (REAL) Motor: Strangquerschnitt [mm^2] */
#define ACP10PAR_MOTOR_STATOR_RESISTANCE     60 /* (REAL) Motor: Statorwiderstand [Ohm] */
#define ACP10PAR_MOTOR_STATOR_INDUCTANCE     61 /* (REAL) Motor: Statorinduktivit�t [Henry] */
#define ACP10PAR_MOTOR_INERTIA               62 /* (REAL) Motor: Tr�gheitsmoment [kgm^2] */
#define ACP10PAR_MOTOR_COMMUT_OFFSET         63 /* (REAL) Motor: Kommutierungsoffset [rad] */
#define ACP10PAR_MOTOR_TEMPSENS_PAR1         64 /* (REAL) Motor-Temperatursensor: Parameter1 */
#define ACP10PAR_MOTOR_TEMPSENS_PAR2         65 /* (REAL) Motor-Temperatursensor: Parameter2 */
#define ACP10PAR_MOTOR_TEMPSENS_PAR3         66 /* (REAL) Motor-Temperatursensor: Parameter3 */
#define ACP10PAR_MOTOR_TEMPSENS_PAR4         67 /* (REAL) Motor-Temperatursensor: Parameter4 */
#define ACP10PAR_MOTOR_TEMPSENS_PAR5         68 /* (REAL) Motor-Temperatursensor: Parameter5 */
#define ACP10PAR_MOTOR_TEMPSENS_PAR6         69 /* (REAL) Motor-Temperatursensor: Parameter6 */
#define ACP10PAR_MOTOR_TEMPSENS_PAR7         70 /* (REAL) Motor-Temperatursensor: Parameter7 */
#define ACP10PAR_MOTOR_TEMPSENS_PAR8         71 /* (REAL) Motor-Temperatursensor: Parameter8 */
#define ACP10PAR_MOTOR_TEMPSENS_PAR9         72 /* (REAL) Motor-Temperatursensor: Parameter9 */
#define ACP10PAR_MOTOR_TEMPSENS_PAR10        73 /* (REAL) Motor-Temperatursensor: Parameter10 */
#define ACP10PAR_MOTOR_WIND_TEMP_MAX         74 /* (REAL) Motor: Maximal zul�ssige Wicklungstemperatur [Grad] */
#define ACP10PAR_MOTOR_THERMAL_CONST         75 /* (REAL) Motor: Thermische Zeitkonstante [s] */
#define ACP10PAR_MOTOR_ROTOR_RESISTANCE      76 /* (REAL) Motor: Rotorwiderstand [Ohm] */
#define ACP10PAR_MOTOR_ROTOR_INDUCTANCE      77 /* (REAL) Motor: Rotorinduktivit�t [Henry] */
#define ACP10PAR_MOTOR_MUTUAL_INDUCTANCE     78 /* (REAL) Motor: Hauptinduktivit�t [Henry] */
#define ACP10PAR_MOTOR_MAGNETIZING_CURR      79 /* (REAL) Motor: Magnetisierungsstrom [A] */
#define ACP10PAR_CMD_MOTOR_DATA              81 /* (UINT) Kommando: Motor Parameter */
#define ACP10PAR_ENCOD2_TYPE                 85 /* (USINT) Geber2-Interface: Typ */
#define ACP10PAR_CMD_BRAKE                   86 /* (UINT) Kommando: Haltebremse l�sen/blockieren */
#define ACP10PAR_BRAKE_MODE                  90 /* (UINT) Haltebremse: Modus */
#define ACP10PAR_ENCOD1_S_ACT                91 /* (DINT) Geber1: Istposition [Einh.] */
#define ACP10PAR_PCTRL_V_ACT                 92 /* (REAL) Lageregler: Istgeschwindigkeit [Einh./s] */
#define ACP10PAR_CMD_CONTROLLER              93 /* (UINT) Kommando: Regler ein-/ausschalten */
#define ACP10PAR_STATUS_CYCLIC_S             94 /* (DINT) Position f�r zyklischen Status [Einh.] */
#define ACP10PAR_STATUS_CYCLIC_V             95 /* (REAL) Geschwindigkeit f�r zyklischen Status [Einh./s] */
#define ACP10PAR_ENCOD_COUNT_DIR             96 /* (USINT) Geber-Interface: Z�hlrichtung */
#define ACP10PAR_ENCOD_TYPE                  97 /* (USINT) Geber-Interface: Typ */
#define ACP10PAR_AXLIM_DS_STOP               98 /* (REAL) Schleppfehlergrenzwert f�r Abbruch einer Bewegung [Einh.] */
#define ACP10PAR_AXLIM_DS_WARNING            99 /* (REAL) Schleppfehlergrenzwert f�r Anzeige einer Warnung [Einh.] */
#define ACP10PAR_POS_CTRL_KV                100 /* (REAL) Lageregler: Proportional-Verst�rkung [1/s] */
#define ACP10PAR_POS_CTRL_TN                101 /* (REAL) Lageregler: Integrator-Nachstellzeit [s] */
#define ACP10PAR_POS_CTRL_T_PREDICT         102 /* (REAL) Lageregler: Vorausschauzeit [s] */
#define ACP10PAR_POS_CTRL_T_TOTAL           103 /* (REAL) Lageregler: Gesamtverz�gerungszeit [s] */
#define ACP10PAR_POS_CTRL_P_MAX             104 /* (REAL) Lageregler: Maximaler Proportionaleingriff [Einh./s] */
#define ACP10PAR_POS_CTRL_I_MAX             105 /* (REAL) Lageregler: Maximaler Integraleingriff [Einh./s] */
#define ACP10PAR_SCALE_LOAD_UNITS           106 /* (UDINT) Last-Ma�stab: Einheiten pro SCALE_LOAD_MOTOR_REV Motorumdrehungen [Einh.] */
#define ACP10PAR_SCALE_LOAD_MOTOR_REV       107 /* (UDINT) Last-Ma�stab: Motorumdrehungen */
#define ACP10PAR_SCALE_ENCOD_MOTOR_REV      108 /* (UDINT) Geber-Ma�stab: Motorumdrehungen */
#define ACP10PAR_SCALE_ENCOD_INCR           109 /* (UDINT) Geber-Ma�stab: Inkremente pro SCALE_ENCOD_MOTOR_REV Motorumdrehungen */
#define ACP10PAR_CMD_SIMULATION             110 /* (UINT) Kommando: Simulationsmodus ein-/ausschalten */
#define ACP10PAR_PCTRL_S_ACT                111 /* (DINT) Lageregler: Istposition [Einh.] */
#define ACP10PAR_PCTRL_LAG_ERROR            112 /* (REAL) Lageregler: Schleppfehler [Einh.] */
#define ACP10PAR_PCTRL_S_SET                113 /* (DINT) Lageregler: Sollposition [Einh.] */
#define ACP10PAR_PCTRL_V_SET                114 /* (REAL) Lageregler: Sollgeschwindigkeit [Einh./s] */
#define ACP10PAR_CMD_ABS_MOVE               115 /* (BASIS_MOVE_S_ABS, BASIS_MOVE_MODE) Kommando: Bewegung mit absoluter Zielposition starten */
#define ACP10PAR_OVERRIDE                   116 /* (V_OVERRIDE, A_OVERRIDE) Basis-Bewegungen: Override */
#define ACP10PAR_V_OVERRIDE                 117 /* (INT) Basis-Bewegungen: Geschwindigkeits-Override */
#define ACP10PAR_A_OVERRIDE                 118 /* (INT) Basis-Bewegungen: Beschleunigungs-Override */
#define ACP10PAR_AXLIM_V_POS                119 /* (REAL) Maximale Geschwindigkeit in positive Richtung [Einh./s] */
#define ACP10PAR_AXLIM_V_NEG                120 /* (REAL) Maximale Geschwindigkeit in negative Richtung [Einh./s] */
#define ACP10PAR_AXLIM_A1_POS               121 /* (REAL) Maximale Beschleunigung in positive Richtung [Einh./s^2] */
#define ACP10PAR_AXLIM_A2_POS               122 /* (REAL) Maximale Verz�gerung in positive Richtung [Einh./s^2] */
#define ACP10PAR_AXLIM_A1_NEG               123 /* (REAL) Maximale Beschleunigung in negative Richtung [Einh./s^2] */
#define ACP10PAR_AXLIM_A2_NEG               124 /* (REAL) Maximale Verz�gerung in negative Richtung [Einh./s^2] */
#define ACP10PAR_AXLIM_T_JOLT               125 /* (REAL) Ruckzeit [s] */
#define ACP10PAR_AXLIM_POS_SW_END           126 /* (DINT) Positive SW-Endlage [Einh.] */
#define ACP10PAR_AXLIM_NEG_SW_END           127 /* (DINT) Negative SW-Endlage [Einh.] */
#define ACP10PAR_SGEN_SW_END_IGNORE         128 /* (USINT) SW-Endlagen ignorieren */
#define ACP10PAR_STATUS_TRACE               129 /* (USINT) Trace-Status */
#define ACP10PAR_TRACE_TRIGGER_PARID        131 /* (UINT) Trace: Paramter-ID f�r Trigger-Ereignis */
#define ACP10PAR_TRACE_TRIGGER_EVENT        132 /* (USINT) Trace: Trigger-Ereignis */
#define ACP10PAR_TRACE_TRIGGER_THRESHOLD    133 /* (REAL) Trace: Trigger-Schwelle */
#define ACP10PAR_TRACE_TRIGGER_WINDOW       134 /* (REAL) Trace: Trigger-Fenster */
#define ACP10PAR_TRACE_TEST_PARID           135 /* (UINT) Trace: Parameter-ID f�r Test-Datum */
#define ACP10PAR_CMD_TRACE                  137 /* (UINT) Kommando: Trace starten/abbrechen */
#define ACP10PAR_TRACE_TEST_INDEX           140 /* (UINT) Trace: Index f�r Test-Datum */
#define ACP10PAR_TRACE_T_TRACE              141 /* (REAL) Trace: Aufzeichnungsdauer [s] */
#define ACP10PAR_TRACE_T_SAMPLING           142 /* (REAL) Trace: Abtastzeit [s] */
#define ACP10PAR_TRACE_T_DELAY              143 /* (REAL) Trace: Verz�gerungszeit (relativ zum Trigger-Ereignis) [s] */
#define ACP10PAR_TRACE_MAX_DATLEN           144 /* (UDINT) Maximall�nge der Trace-Daten [Byte] */
#define ACP10PAR_AXLIM_T_INPOS              150 /* (REAL) Wartezeit vor Meldung "In Position" [s] */
#define ACP10PAR_HOMING_TR_S_REL            151 /* (REAL) Referenzimpuls-Distanz [Umdr.] */
#define ACP10PAR_HOMING_S                   152 /* (DINT) Referenzposition [Einh.] */
#define ACP10PAR_HOMING_V_SWITCH            153 /* (REAL) Geschwindigkeit f�r Referenzschaltersuche [Einh./s] */
#define ACP10PAR_HOMING_V_TRIGGER           154 /* (REAL) Triggergeschwindigkeit (nach Erreichen des Referenzschalters) [Einh./s] */
#define ACP10PAR_HOMING_A                   155 /* (REAL) Beschleunigung f�r Referenzieren [Einh./s^2] */
#define ACP10PAR_HOMING_MODE                156 /* (USINT) Referenzier-Modus */
#define ACP10PAR_HOMING_MODE_BITS           157 /* (USINT) Referenzier-Modus-Steuerbits */
#define ACP10PAR_HOMING_TR_S_BLOCK          158 /* (REAL) Distanz f�r Referenzimpuls-Blockade [Umdr.] */
#define ACP10PAR_BASIS_MOVE_S_ABS           159 /* (DINT) Basis-Bewegungen: Zielposition [Einh.] */
#define ACP10PAR_BASIS_MOVE_S_REL           160 /* (DINT) Basis-Bewegungen: Relative Verfahrdistanz [Einh.] */
#define ACP10PAR_BASIS_MOVE_V_POS           161 /* (REAL) Basis-Bewegungen: Geschwindigkeit in positive Richtung [Einh./s] */
#define ACP10PAR_BASIS_MOVE_V_NEG           162 /* (REAL) Basis-Bewegungen: Geschwindigkeit in negative Richtung [Einh./s] */
#define ACP10PAR_BASIS_MOVE_A1_POS          163 /* (REAL) Basis-Bewegungen: Beschleunigung in positive Richtung [Einh./s^2] */
#define ACP10PAR_BASIS_MOVE_A2_POS          164 /* (REAL) Basis-Bewegungen: Verz�gerung in positive Richtung [Einh./s^2] */
#define ACP10PAR_BASIS_MOVE_A1_NEG          165 /* (REAL) Basis-Bewegungen: Beschleunigung in negative Richtung [Einh./s^2] */
#define ACP10PAR_BASIS_MOVE_A2_NEG          166 /* (REAL) Basis-Bewegungen: Verz�gerung in negative Richtung [Einh./s^2] */
#define ACP10PAR_CMD_HOMING                 167 /* (HOMING_S, HOMING_MODE, HOMING_MODE_BITS) Kommando: Referenzieren starten */
#define ACP10PAR_CMD_REL_MOVE               168 /* (BASIS_MOVE_S_REL, BASIS_MOVE_MODE) Kommando: Bewegung mit relativer Verfahrdistanz starten */
#define ACP10PAR_CMD_POS_MOVE               169 /* (BASIS_MOVE_V_POS, BASIS_MOVE_MODE) Kommando: Bewegung in positive Richtung starten */
#define ACP10PAR_CMD_NEG_MOVE               170 /* (BASIS_MOVE_V_NEG, BASIS_MOVE_MODE) Kommando: Bewegung in negative Richtung starten */
#define ACP10PAR_STOP_CMD_CONF_INDEX        171 /* (USINT) Index des Parametersatzes f�r den n�chsten Abbruchbefehl */
#define ACP10PAR_HOMING_OFFSET              172 /* (DINT) Referenzier-Offset [Einh.] */
#define ACP10PAR_STOP_CONF_INDEX            173 /* (USINT) Index eines Parametersatzes f�r die Abbruchkonfiguration */
#define ACP10PAR_STOP_CONF_DECEL_RAMP       174 /* (USINT) Bremsrampe f�r Bewegungsabbruch */
#define ACP10PAR_STOP_CONF_CTRL_STATE       175 /* (USINT) Reglerzustand nach Bewegungsabbruch */
#define ACP10PAR_CMD_MOVEMENT_STOP          176 /* (STOP_CMD_CONF_INDEX) Kommando: Bewegung abbrechen */
#define ACP10PAR_STATUS_BITS                178 /* (UDINT) Allgemeine Status-Bits */
#define ACP10PAR_STATUS_CYCLIC_BITS         179 /* (UDINT) Zyklische Status-Bits */
#define ACP10PAR_ERROR_NUMBER               180 /* (UINT) Fehlernummer */
#define ACP10PAR_ERROR_INFO                 181 /* (DINT) Fehler-Zusatzinfo */
#define ACP10PAR_ERROR_TIME                 182 /* (UDINT) Fehlerzeit [us] */
#define ACP10PAR_ERROR_REC                  183 /* (ERROR_NUMBER, ERROR_INFO) Fehlersatz vom Antrieb */
#define ACP10PAR_DIG_IN_FORCE_ENABLE        184 /* (USINT) Force-Freigabe-Bits f�r digitale Eing�nge */
#define ACP10PAR_CMD_DIG_IN_FORCE           185 /* (USINT) Kommando: Force-Funktion f�r digitale Eing�nge */
#define ACP10PAR_DIG_IN_ACTIVE_LEVEL        186 /* (USINT) Aktiv-Pegel-Bits digitale Eing�nge */
#define ACP10PAR_NETWORK_LIVE_CTRL          189 /* (UDINT) Zeit f�r Netzwerk-Lebens�berwachung [us] */
#define ACP10PAR_CMD_BASIS_MOVE_HALT        191 /* (NIL) Kommando: Basis-Bewegung anhalten (mit akt. Parametern abbrechen) */
#define ACP10PAR_TRIGGER_STOP_MODE          199 /* (USINT) Modus f�r Trigger-Halt */
#define ACP10PAR_CYCLIC_TODRV_PARID         207 /* (UINT) Zyklische Daten zum Antrieb: Parameter-ID */
#define ACP10PAR_CYCLIC_TODRV_OFFSET        208 /* (UINT) Zyklische Daten zum Antrieb: Daten-Offset */
#define ACP10PAR_ICTRL_ISQ_REF              213 /* (REAL) Stromregler: Sollstatorstrom Querkomponente [A] */
#define ACP10PAR_ICTRL_ISQ_ACT              214 /* (REAL) Stromregler: Iststatorstrom Querkomponente [A] */
#define ACP10PAR_ICTRL_USQ_REF              216 /* (REAL) Stromregler: Statorspannung Querkomponente [V] */
#define ACP10PAR_ICTRL_ISD_REF              218 /* (REAL) Stromregler: Sollstatorstrom Hauptkomponente [A] */
#define ACP10PAR_ICTRL_ISD_ACT              219 /* (REAL) Stromregler: Iststatorstrom Hauptkomponente [A] */
#define ACP10PAR_ICTRL_USD_REF              221 /* (REAL) Stromregler: Statorspannung Hauptkomponente [V] */
#define ACP10PAR_ICTRL_KV                   223 /* (REAL) Stromregler: Proportional-Verst�rkung [V/A] */
#define ACP10PAR_ICTRL_TI                   225 /* (REAL) Stromregler: Integrierzeitkonstante [s] */
#define ACP10PAR_SCTRL_FILTER_F0            226 /* (REAL) Drehzahlregler-Sperrfilter: Frequenz [1/s] */
#define ACP10PAR_SCTRL_FILTER_B             227 /* (REAL) Drehzahlregler-Sperrfilter: Bandbreite [1/s] */
#define ACP10PAR_PCTRL_LAG2_ERROR           228 /* (REAL) Zwei-Geberregelung: Positionsdifferenz [Einh.] */
#define ACP10PAR_AXLIM_DS_STOP2             229 /* (REAL) Zwei-Geberregelung: Positionsdifferenzgrenze f�r Abbruch einer Bewegung [Einh.] */
#define ACP10PAR_PCTRL_S_ACT_PARID          230 /* (UINT) Lageregler: Parameter-ID der Geber-Istposition */
#define ACP10PAR_CYCLIC_TODRV_PAR_INDEX     232 /* (USINT) Zyklische Daten zum Antrieb: Parameter-Index */
#define ACP10PAR_CYCLIC_TODRV_TEL_INDEX     233 /* (USINT) Zyklische Daten zum Antrieb: Telegramm-Index */
#define ACP10PAR_CYCLIC_FRDRV_REC_INDEX     234 /* (USINT) Zyklische Daten vom Antrieb: Index des Monitorsatzes */
#define ACP10PAR_CYCLIC_FRDRV_PAR_INDEX     235 /* (USINT) Zyklische Daten vom Antrieb: Parameter-Index */
#define ACP10PAR_CYCLIC_FRDRV_PARID         236 /* (UINT) Zyklische Daten vom Antrieb: Parameter-ID */
#define ACP10PAR_ENCOD_SSI_ZERO_BITS        237 /* (USINT) SSI-Motor-Geber: Anzahl der vorlaufenden Nullen */
#define ACP10PAR_ENCOD_SSI_BITS             238 /* (USINT) SSI-Motor-Geber: Anzahl der Datenbits */
#define ACP10PAR_ENCOD_SSI_CODE             239 /* (USINT) SSI-Motor-Geber: Datencodierung [ncGRAY/ncBINAER] */
#define ACP10PAR_ENCOD_SSI_PARITY_CHK       240 /* (USINT) SSI-Motor-Geber: Parity-Pr�fung [ncAUS/ncODD/ncEVEN] */
#define ACP10PAR_ENCOD2_SSI_ZERO_BITS       241 /* (USINT) SSI-Geber2: Anzahl der vorlaufenden Nullen */
#define ACP10PAR_ENCOD2_SSI_BITS            242 /* (USINT) SSI-Geber2: Anzahl der Datenbits */
#define ACP10PAR_ENCOD2_SSI_CODE            243 /* (USINT) SSI-Geber2: Datencodierung [ncGRAY/ncBINAER] */
#define ACP10PAR_ENCOD2_SSI_PARITY_CHK      244 /* (USINT) SSI-Geber2: Parity-Pr�fung [ncAUS/ncODD/ncEVEN] */
#define ACP10PAR_ICTRL_ADD_TRQ              247 /* (REAL) Stromregler: Additives Moment [Nm] */
#define ACP10PAR_LIM_T1_POS                 248 /* (REAL) Maximales Beschleunigungsmoment in positive Richtung [Nm] */
#define ACP10PAR_LIM_T1_NEG                 249 /* (REAL) Maximales Beschleunigungsmoment in negative Richtung [Nm] */
#define ACP10PAR_SCTRL_SPEED_REF            250 /* (REAL) Drehzahlregler: Solldrehzahl [1/s] */
#define ACP10PAR_SCTRL_SPEED_ACT            251 /* (REAL) Drehzahlregler: Istdrehzahl [1/s] */
#define ACP10PAR_SCTRL_KV                   253 /* (REAL) Drehzahlregler: Proportional-Verst�rkung [As/U] */
#define ACP10PAR_SCTRL_TN                   255 /* (REAL) Drehzahlregler: Integrator-Nachstellzeit [s] */
#define ACP10PAR_TORQUE_ACT                 277 /* (REAL) Luftspaltdrehmoment [Nm] */
#define ACP10PAR_POWER_ACT                  278 /* (REAL) Luftspaltleistung [W] */
#define ACP10PAR_SCTRL_TI_FIL               283 /* (REAL) Drehzahlregler: Filterzeitkonstante [s] */
#define ACP10PAR_SCALE_ENCOD2_INCR          289 /* (UDINT) Geber2-Ma�stab: Inkremente pro Geberumdrehung */
#define ACP10PAR_BRC_CANID_DRV_SYNC         290 /* (UINT) Broadcast-CAN-ID f�r SYNC-Telegramm */
#define ACP10PAR_BRC_CANID_BRC_CMD          291 /* (UINT) Broadcast-CAN-ID f�r Broadcast-Kommando */
#define ACP10PAR_BASIS_CANID_USCYC_TODRV    293 /* (UINT) Basis-CAN-ID f�r zyklisches Anwender-Telegramm zum Antrieb */
#define ACP10PAR_BASIS_CANID_USCYC_FRDRV    294 /* (UINT) Basis-CAN-ID f�r zyklisches Anwender-Telegramm vom Antrieb */
#define ACP10PAR_BASIS_CANID_WR_RD_2        295 /* (UINT) Basis-CAN-ID f�r Write- und Read-Kanal2 */
#define ACP10PAR_BASIS_CANID_WR_RD_3        296 /* (UINT) Basis-CAN-ID f�r Write- und Read-Kanal3 */
#define ACP10PAR_CMD_CANID_ACOPOS_STD       297 /* (USINT) Kommando: CAN-ID-Standardeinstellung */
#define ACP10PAR_UDC_ACT                    298 /* (REAL) Zwischenkreisspannungsregler: Istzwischenkreisspannung [V] */
#define ACP10PAR_UDC_CTRL_TI_FIL            299 /* (REAL) Zwischenkreisspannungsregler: Filterzeitkonstante [s] */
#define ACP10PAR_UDC_CTRL_KV                303 /* (REAL) Zwischenkreisspannungsregler: Proportional-Verst�rkungsfaktor [us/V] */
#define ACP10PAR_CONTROLLER_MODE            328 /* (USINT) Vektorregler: Modus */
#define ACP10PAR_SCTRL_V_SET_UNITS          329 /* (REAL) Drehzahlregler: Solldrehzahl [Einh./s] */
#define ACP10PAR_SCTRL_V_SET_SCALE          339 /* (INT) Drehzahlregler: Normierte Solldrehzahl [Einh.(normiert)/s] */
#define ACP10PAR_CMD_ERR_STATE_INTO_FIFO    340 /* (NIL) Kommando: Meldungen f�r Fehlerzustand in die Fehler-FIFO schreiben */
#define ACP10PAR_SCTRL_K_V_SET_SCALE        341 /* (REAL) Drehzahlregler: Faktor f�r normierte Solldrehzahl [Einh./Einh.(normiert)] */
#define ACP10PAR_LIM_T_OVR_GRP              343 /* (LIM_T1_POS_OVR, LIM_T1_NEG_OVR, LIM_T2_POS_OVR, LIM_T2_NEG_OVR,) Momentenbegrenzer: Override [%] */
#define ACP10PAR_LIM_T1_POS_OVR             344 /* (USINT) Momentenbegrenzer: LIM_T1_POS-Override [%] */
#define ACP10PAR_LIM_T1_NEG_OVR             346 /* (USINT) Momentenbegrenzer: LIM_T1_NEG-Override [%] */
#define ACP10PAR_F_SWITCH                   347 /* (REAL) PWM-Schaltfrequenz [Hz] */
#define ACP10PAR_LIM_T2_POS                 348 /* (REAL) Maximales Verz�gerungsmoment in positive Richtung [Nm] */
#define ACP10PAR_LIM_T2_NEG                 349 /* (REAL) Maximales Verz�gerungsmoment in negative Richtung [Nm] */
#define ACP10PAR_SYS_TIME                   355 /* (UDINT) Gesamtzeit des Antriebs [us] */
#define ACP10PAR_SYNC_MASTERDRIVE           356 /* (BOOL) Master f�r Antriebssynchronisation */
#define ACP10PAR_SYNC_MASTERPERIOD          357 /* (UDINT) Masterzykluszeit [us] */
#define ACP10PAR_DIO_STATE                  358 /* (UDINT) Digital IO: Status */
#define ACP10PAR_SYNC_SYS_TIME_DIFF         359 /* (DINT) Abweichung von Masterzeit [us] */
#define ACP10PAR_DIO_CONFIG                 360 /* (UDINT) Digital IO: Konfigurierung */
#define ACP10PAR_CMD_DO_SET                 361 /* (UDINT) Kommando: Digitale Ausg�nge setzen */
#define ACP10PAR_CMD_DO_CLR                 362 /* (UDINT) Kommando: Digitale Ausg�nge l�schen */
#define ACP10PAR_SYNC_MSG_PERIOD            363 /* (UDINT) Periodendauer des Sync-Telegramms [us] */
#define ACP10PAR_TEMP_HEATSINK_ANA          365 /* (INT) AD-Wandlerwert der K�hlk�rpertemperatur */
#define ACP10PAR_TEMP_MOTOR_ANA             366 /* (INT) AD-Wandlerwert der Motortemperatur */
#define ACP10PAR_LIM_T2_POS_OVR             374 /* (USINT) Momentenbegrenzer: LIM_T2_POS-Override [%] */
#define ACP10PAR_LIM_T2_NEG_OVR             375 /* (USINT) Momentenbegrenzer: LIM_T2_NEG-Override [%] */
#define ACP10PAR_TEMP_HEATSINK              380 /* (REAL) K�hlk�rpertemperatur [Grad] */
#define ACP10PAR_TEMP_MOTOR                 381 /* (REAL) gemessene Motortemperatur [Grad] */
#define ACP10PAR_TEMP_JUNCTION              382 /* (REAL) Sperrschichttemperatur [Grad] */
#define ACP10PAR_TEMP_BLEEDER               383 /* (REAL) Bremswiderstandstemperatur [Grad] */
#define ACP10PAR_TEMP_HEATSINK_MAX          384 /* (REAL) Maximale K�hlk�rpertemperatur [Grad] */
#define ACP10PAR_TEMP_MOTOR_MAX             385 /* (REAL) Maximale gemessene Motortemperatur [Grad] */
#define ACP10PAR_TEMP_JUNCTION_MAX          386 /* (REAL) Maximale Sperrschichttemperatur [Grad] */
#define ACP10PAR_TEMP_BLEEDER_MAX           387 /* (REAL) Maximale Bremswiderstandstemperatur [Grad] */
#define ACP10PAR_BLEEDER_SELECTOR_EXT       398 /* (USINT) Kommando: Umschalten zwischen internen und externen Bremswiderstand */
#define ACP10PAR_CMD_ABS_MOVE_VAX1          400 /* (BASIS_MOVE_S_ABS_VAX1) Kommando VAX1: Bewegung mit absoluter Zielposition starten */
#define ACP10PAR_CMD_REL_MOVE_VAX1          401 /* (BASIS_MOVE_S_REL_VAX1) Kommando VAX1: Bewegung mit relativer Verfahrdistanz starten */
#define ACP10PAR_CMD_POS_MOVE_VAX1          402 /* (BASIS_MOVE_V_POS_VAX1) Kommando VAX1: Bewegung in positive Richtung starten */
#define ACP10PAR_CMD_NEG_MOVE_VAX1          403 /* (BASIS_MOVE_V_NEG_VAX1) Kommando VAX1: Bewegung in negative Richtung starten */
#define ACP10PAR_BASIS_MOVE_V_POS_VAX1      404 /* (REAL) Basis-Bewegungen VAX1: Geschwindigkeit in positive Richtung [Einh./s] */
#define ACP10PAR_BASIS_MOVE_V_NEG_VAX1      405 /* (REAL) Basis-Bewegungen VAX1: Geschwindigkeit in negative Richtung [Einh./s] */
#define ACP10PAR_BASIS_MOVE_A1_POS_VAX1     406 /* (REAL) Basis-Bewegungen VAX1: Beschleunigung in positive Richtung [Einh./s^2] */
#define ACP10PAR_BASIS_MOVE_A2_POS_VAX1     407 /* (REAL) Basis-Bewegungen VAX1: Verz�gerung in positive Richtung [Einh./s^2] */
#define ACP10PAR_BASIS_MOVE_A1_NEG_VAX1     408 /* (REAL) Basis-Bewegungen VAX1: Beschleunigung in negative Richtung [Einh./s^2] */
#define ACP10PAR_BASIS_MOVE_A2_NEG_VAX1     409 /* (REAL) Basis-Bewegungen VAX1: Verz�gerung in negative Richtung [Einh./s^2] */
#define ACP10PAR_BASIS_MOVE_S_ABS_VAX1      410 /* (DINT) Basis-Bewegungen VAX1: Zielposition [Einh.] */
#define ACP10PAR_BASIS_MOVE_S_REL_VAX1      411 /* (DINT) Basis-Bewegungen VAX1: Relative Verfahrdistanz [Einh.] */
#define ACP10PAR_S_SET_VAX1                 412 /* (DINT) VAX1: Position [Einh.] */
#define ACP10PAR_V_SET_VAX1                 413 /* (REAL) VAX1: Geschwindigkeit [Einh./s] */
#define ACP10PAR_AXLIM_T_JOLT_VAX1          415 /* (REAL) VAX1 Ruckzeit [s] */
#define ACP10PAR_BASIS_TRG_STOP             416 /* (BASIS_TRG_STOP_S_REST, BASIS_TRG_STOP_EVENT) Basis-Bewegungen: Modus "Stop nach Trigger" */
#define ACP10PAR_BASIS_TRG_STOP_S_REST      417 /* (DINT) Basis-Bewegungen: Restweg f�r Modus "Stop nach Trigger" [Einh.] */
#define ACP10PAR_BASIS_TRG_STOP_EVENT       418 /* (USINT) Basis-Bewegungen: Trigger-Ereignis f�r Modus "Stop nach Trigger" */
#define ACP10PAR_BASIS_MOVE_MODE            419 /* (USINT) Basis-Bewegungen: Modus */
#define ACP10PAR_SCALE_ENCOD2_UNITS         420 /* (UDINT) Geber2-Ma�stab: Einheiten pro SCALE_ENCOD2_REV Geberumdrehungen [Einh.] */
#define ACP10PAR_SCALE_ENCOD2_REV           421 /* (UDINT) Geber2-Ma�stab: Geberumdrehungen */
#define ACP10PAR_ENCOD2_COUNT_DIR           422 /* (USINT) Geber2-Interface: Z�hlrichtung */
#define ACP10PAR_ENCOD2_S_ACT               423 /* (DINT) Geber2: Istposition [Einh.] */
#define ACP10PAR_ENCOD2_HOMING_S            424 /* (DINT) Geber2: Referenzposition [Einh.] */
#define ACP10PAR_ENCOD2_HOMING_MODE_BITS    425 /* (USINT) Geber2: Referenzier-Modus-Steuerbits */
#define ACP10PAR_CMD_ENCOD2_HOMING          426 /* (ENCOD2_HOMING_S, ENCOD2_HOMING_MODE_BITS) Kommando Geber2: Referenzieren starten */
#define ACP10PAR_STAT_ENC2_HOMING_OK        427 /* (UDINT) Geber2: Status Referenzposition g�ltig */
#define ACP10PAR_SGEN_S_SET                 428 /* (DINT) Sollposition [Einh.] */
#define ACP10PAR_ENCOD2_S_FILTER_T10        429 /* (REAL) Geber2: Zeitkonstante f�r Istpositions-Filter [s] */
#define ACP10PAR_CMD_CAM_START              430 /* (UINT) Kommando: Kurvenscheibenkopplung starten */
#define ACP10PAR_CAM_MA_AXIS                431 /* (UINT) Kurvenscheibe: Master-Achse */
#define ACP10PAR_CAM_MA_V_MAX               433 /* (REAL) Kurvenscheibe: Maximale Geschwindigkeit der Master-Achse [Einh./s] */
#define ACP10PAR_CAM_MA_S_START             434 /* (DINT) Kurvenscheibe: Startposition der Master-Achse [Einh.] */
#define ACP10PAR_CAM_MA_S_SYNC              435 /* (DINT) Kurvenscheibe: Synchronweg der Master-Achse [Einh.] */
#define ACP10PAR_CAM_MA_S_COMP              436 /* (DINT) Kurvenscheibe: Ausgleichsweg der Master-Achse [Einh.] */
#define ACP10PAR_CAM_MA_TRIG_MODE           437 /* (USINT) Kurvenscheibe: Trigger-Modus Master-Achse */
#define ACP10PAR_CAM_MA_S_TRIG              438 /* (DINT) Kurvenscheibe: Relative Trigger-Position Master-Achse [Einh.] */
#define ACP10PAR_CAM_SL_S_SYNC              439 /* (DINT) Kurvenscheibe: Synchronweg der Slave-Achse [Einh.] */
#define ACP10PAR_CAM_SL_S_COMP              440 /* (DINT) Kurvenscheibe: Ausgleichsweg der Slave-Achse [Einh.] */
#define ACP10PAR_CAM_SL_S_TRIG              441 /* (DINT) Kurvenscheibe: Relative Trigger-Position Slave-Achse [Einh.] */
#define ACP10PAR_CAM_SL_TRIG_MODE           442 /* (USINT) Kurvenscheibe: Trigger-Modus Slave-Achse */
#define ACP10PAR_CAM_SL_S_COMP_MIN          443 /* (DINT) Kurvenscheibe: Minimaler Ausgleichsweg der Slave-Achse [Einh.] */
#define ACP10PAR_CAM_SL_S_COMP_MAX          444 /* (DINT) Kurvenscheibe: Maximaler Ausgleichsweg der Slave-Achse [Einh.] */
#define ACP10PAR_CAM_COMP_GEAR_TYPE         445 /* (USINT) Kurvenscheibe: Ausgleichsgetriebe-Typ */
#define ACP10PAR_CAM_SL_TRIG_WINDOW         446 /* (DINT) Kurvenscheibe: Trigger-Fenster Slave-Achse [Einh.] */
#define ACP10PAR_CAM_DRUMSEQ_ENABLE         447 /* (BOOL) Kurvenscheibe: Status f�r Nockenschaltwerk Aktivierung */
#define ACP10PAR_CAM_MA_COMP_TRIG_MODE      448 /* (USINT) Kurvenscheibe: Ausgleichs-Trigger-Modus Master-Achse */
#define ACP10PAR_CAM_MA_S_COMP_TRIG         449 /* (DINT) Kurvenscheibe: Relative Position Ausgleichs-Trigger Master-Achse [Einh.] */
#define ACP10PAR_PCTRL_MODE_SWITCH          450 /* (DINT) Lageregler: Schalter f�r Regler-Modus */
#define ACP10PAR_TRIG1_RISE_EDGE_S_ACT      452 /* (DINT) Istposition steigende Flanke Trigger1 [Einh.] */
#define ACP10PAR_TRIG1_FALL_EDGE_S_ACT      453 /* (DINT) Istposition fallende Flanke Trigger1 [Einh.] */
#define ACP10PAR_TRIG2_RISE_EDGE_S_ACT      454 /* (DINT) Istposition steigende Flanke Trigger2 [Einh.] */
#define ACP10PAR_TRIG2_FALL_EDGE_S_ACT      455 /* (DINT) Istposition fallende Flanke Trigger2 [Einh.] */
#define ACP10PAR_CAM_MA_COMP_TRIG_WINDOW    456 /* (DINT) Kurvenscheibe: Ausgleichs-Trigger-Fenster Master-Achse [Einh.] */
#define ACP10PAR_LIMIT_SWITCH_IGNORE        459 /* (BOOL) Endschalter ignorieren */
#define ACP10PAR_CMD_DRUMSEQ                472 /* (UINT) Kommando: Nockenschaltwerk starten/abbrechen */
#define ACP10PAR_DRUMSEQ_MA_AXIS            473 /* (UINT) Nockenschaltwerk: Master-Achse */
#define ACP10PAR_DRUMSEQ_S_START_ID         474 /* (UINT) Nockenschaltwerk: ID f�r Online-�nderung der Startposition [Einh.] */
#define ACP10PAR_DRUMSEQ_S_START            475 /* (DINT) Nockenschaltwerk: Startposition [Einh.] */
#define ACP10PAR_DRUMSEQ_S_IV               476 /* (DINT) Nockenschaltwerk: Intervall [Einh.] */
#define ACP10PAR_DRUMSEQ_TRACK_INDEX        477 /* (UINT) Nockenschaltwerk: Index eines Parametersatzes f�r eine Spur */
#define ACP10PAR_DRUMSEQ_DO_CHAN            478 /* (UINT) Nockenschaltwerk: Kanalnummer Digital-Ausgang */
#define ACP10PAR_DRUMSEQ_TRACK_ENABLE_ID    479 /* (UINT) Nockenschaltwerk: Aktivierungs ID */
#define ACP10PAR_DRUMSEQ_DO_DELAY           480 /* (REAL) Nockenschaltwerk: Schaltverz�gerung [s] */
#define ACP10PAR_DRUMSEQ_CAM_INDEX          481 /* (UINT) Nockenschaltwerk: Index eines Parametersatzes f�r eine Nocke */
#define ACP10PAR_DRUMSEQ_S_DO_ON            482 /* (DINT) Nockenschaltwerk: Position innerhalb intervall f�r Ausgang EIN [Einh.] */
#define ACP10PAR_DRUMSEQ_S_DO_OFF           483 /* (DINT) Nockenschaltwerk: Position innerhalb intervall f�r Ausgang AUS [Einh.] */
#define ACP10PAR_MA1_CYCLIC_SEND            484 /* (UINT) Master1 f�r Netzwerk-Kopplung */
#define ACP10PAR_MA2_CYCLIC_SEND            485 /* (UINT) Master2 f�r Netzwerk-Kopplung  */
#define ACP10PAR_CAM_MA_COMP_TRIG_IV_MIN    486 /* (DINT) Kurvenscheibe: Min. Ausgleichs-Trigger Intervall Master-Achse [Einh.] */
#define ACP10PAR_CAM_MA_COMP_TRIG_IV_MAX    487 /* (DINT) Kurvenscheibe: Max. Ausgleichs-Trigger Intervall Master-Achse [Einh.] */
#define ACP10PAR_CAM_SL_TRIG_IV_MIN         488 /* (DINT) Kurvenscheibe: Min. Trigger Intervall Slave-Achse [Einh.] */
#define ACP10PAR_CAM_SL_TRIG_IV_MAX         489 /* (DINT) Kurvenscheibe: Max. Trigger Intervall Slave-Achse [Einh.] */
#define ACP10PAR_CMD_CAM_CONTROLLER         491 /* (UINT) Kommando: Regler ein-/ausschalten bei aktiver Kurvenscheibe */
#define ACP10PAR_CAM_MA_ADD_EL              492 /* (DINT) Kurvenscheibe: Additives Element der Master-Achse [Einh.] */
#define ACP10PAR_CAM_MA_TRIG_WINDOW         493 /* (DINT) Kurvenscheibe: Trigger-Fenster Master-Achse [Einh.] */
#define ACP10PAR_MA3_CYCLIC_SEND            494 /* (UINT) Master3 f�r Netzwerk-Kopplung */
#define ACP10PAR_AUT_START_ST_INDEX         495 /* (USINT) Kurvenautomat: Index f�r den Start-Zustand */
#define ACP10PAR_AUT_START_ST_INDEX_VAX1    496 /* (USINT) Kurvenautomat VAX1: Index f�r den Start-Zustand */
#define ACP10PAR_CYCL_MON_REQU1             497 /* (UINT) Parameter1 f�r zyklischen Status */
#define ACP10PAR_CYCL_MON_REQU2             498 /* (UINT) Parameter2 f�r zyklischen Status */
#define ACP10PAR_AUT_POLY_DATA              500 /* (DATA) Kurvenautomat: Kurvenscheiben Polynom-Daten  */
#define ACP10PAR_AUT_DATA_INDEX             501 /* (UINT) Kurvenautomat: Index der Kurvenscheibendaten f�r �bertragung */
#define ACP10PAR_CMD_AUT_START              502 /* (UINT) Kommando: Kurvenscheibenautomat starten */
#define ACP10PAR_AUT_MA_AXIS                503 /* (UINT) Kurvenautomat: Master-Achse */
#define ACP10PAR_AUT_MA_S_START             504 /* (DINT) Kurvenautomat: Startposition der Master-Achse [Einh.] */
#define ACP10PAR_AUT_MA_IVSTART             505 /* (UDINT) Kurvenautomat: Startintervall der Master-Achse [Einh.] */
#define ACP10PAR_AUT_MA_V_MAX               506 /* (REAL) Kurvenautomat: Maximale Geschwindigkeit der Master-Achse [Einh./s] */
#define ACP10PAR_AUT_ST_INDEX               507 /* (USINT) Kurvenautomat: Index eines Parametersatzes f�r einen Zustand */
#define ACP10PAR_AUT_EV_INDEX               508 /* (USINT) Kurvenautomat: Index eines Parametersatzes f�r ein Ereignis  */
#define ACP10PAR_AUT_ST_DATA_INDEX          509 /* (UINT) Kurvenautomat: Index der Kurvenscheibendaten f�r einen Zustand */
#define ACP10PAR_AUT_COMP_MODE              510 /* (USINT) Kurvenautomat: Ausgleichsgetriebe-Modus */
#define ACP10PAR_AUT_COMP_MA_S              511 /* (DINT) Kurvenautomat: Ausgleichsweg der Master-Achse [Einh.] */
#define ACP10PAR_AUT_COMP_SL_S              512 /* (DINT) Kurvenautomat: Ausgleichsweg der Slave-Achse [Einh.] */
#define ACP10PAR_AUT_EVENT_TYPE             513 /* (USINT) Kurvenautomat: Ereignis Typ */
#define ACP10PAR_AUT_EVENT_ATTR             514 /* (USINT) Kurvenautomat: Ereignis Attribut */
#define ACP10PAR_AUT_EVENT_ST_INDEX         515 /* (USINT) Kurvenautomat: Index n�chster Zustand */
#define ACP10PAR_AUT_SIGNAL_SET             516 /* (USINT) Kurvenautomat: Signal setzen */
#define ACP10PAR_AUT_ACT_ST_INDEX           517 /* (USINT) Kurvenautomat: Index des aktuellen Zustands */
#define ACP10PAR_AUT_SIGNAL_RESET           518 /* (USINT) Kurvenautomat: Signal r�cksetzen */
#define ACP10PAR_AUT_MA_FACTOR              519 /* (DINT) Kurvenautomat: Multiplikationsfaktor der Master-Achse */
#define ACP10PAR_AUT_SL_FACTOR              520 /* (DINT) Kurvenautomat: Multiplikationsfaktor der Slave-Achse */
#define ACP10PAR_AUT_ST_COUNT_INIT          521 /* (UINT) Kurvenautomat: Anfangswert Zustandswiederholungen f�r Ereignis ncANZAHL */
#define ACP10PAR_AUT_ST_COUNT_SET           522 /* (UINT) Kurvenautomat: Z�hler Zustandswiederholungen f�r das Ereignis ncANZAHL */
#define ACP10PAR_AUT_COMP_MA_S_MIN          523 /* (DINT) Kurvenautomat: Minimaler Ausgleichsweg der Master-Achse [Einh.] */
#define ACP10PAR_CMD_QUICKSTOP              524 /* (NIL) Kommando: Quickstop */
#define ACP10PAR_OFFSET_ACT_POS             526 /* (DINT) Offset f�r Istposition (Referenzier-Offset) [Einh.] */
#define ACP10PAR_AUT_ONL_PAR_LOCK           527 /* (USINT) Kurvenautomat: Sperre f�r konsistente Online-Parameter-�nderung */
#define ACP10PAR_AUT_EVENT_ACTION           528 /* (UDINT) Kurvenautomat: Aktion bei Zustands�bergang */
#define ACP10PAR_AUT_ACT_CAM_TYPE           529 /* (USINT) Kurvenautomat: Kurventyp des aktuellen Zustands */
#define ACP10PAR_LATCH1_EVENT               530 /* (USINT) Latch1: Trigger-Ereignis */
#define ACP10PAR_LATCH1_VALUE_PARID         531 /* (UINT) Latch1: Parameter-ID des Latch-Wertes */
#define ACP10PAR_LATCH1_WINDOW              532 /* (DINT) Latch1: Fenster */
#define ACP10PAR_LATCH1_WINDOW_POS          533 /* (DINT) Latch1: Position des Fensters und Aktivierung */
#define ACP10PAR_LATCH1_VALUE               534 /* (DINT) Latch1: Ergebnis-Wert */
#define ACP10PAR_LATCH1_STATUS              535 /* (USINT) Latch1: Status */
#define ACP10PAR_LATCH2_EVENT               536 /* (USINT) Latch2: Trigger-Ereignis */
#define ACP10PAR_LATCH2_VALUE_PARID         537 /* (UINT) Latch2: Parameter-ID des Latch-Wertes */
#define ACP10PAR_LATCH2_WINDOW              538 /* (DINT) Latch2: Fenster */
#define ACP10PAR_LATCH2_WINDOW_POS          539 /* (DINT) Latch2: Position des Fensters und Aktivierung */
#define ACP10PAR_LATCH2_VALUE               540 /* (DINT) Latch2: Ergebnis-Wert */
#define ACP10PAR_LATCH2_STATUS              541 /* (USINT) Latch2: Status */
#define ACP10PAR_MA1_CYCLIC_POS             542 /* (DINT) Zyklische Position Netzwerk-Kopplung Master1 */
#define ACP10PAR_MA2_CYCLIC_POS             543 /* (DINT) Zyklische Position Netzwerk-Kopplung Master2 */
#define ACP10PAR_CAM_INHIBIT_OFF            545 /* (DINT) Kurvenscheibe: Bewegung frei geben [Einh.] */
#define ACP10PAR_CAM_INHIBIT_ON             546 /* (DINT) Kurvenscheibe: �bergang in den Stillstand und Bewegung sperren [Einh.] */
#define ACP10PAR_CAM_ACT_ST_INDEX           547 /* (USINT) Kurvenscheibe: Index des aktuellen Zustands */
#define ACP10PAR_MA3_CYCLIC_POS             548 /* (DINT) Zyklische Position Netzwerk-Kopplung Master3 */
#define ACP10PAR_AUT_CAM_MA_S_REL           549 /* (DINT) Kurvenautomat: Relative Position der Master-Achse in der Kurvenscheibe [Einh.] */
#define ACP10PAR_AUT_CAM_MA_S_REL_VAX1      550 /* (DINT) Kurvenautomat VAX1: Relative Position der Master-Achse in der Kurvenscheibe [Einh.] */
#define ACP10PAR_CMD_AUT_START_VAX1         551 /* (UINT) Kommando VAX1: Kurvenscheibenautomat starten */
#define ACP10PAR_AUT_MA_AXIS_VAX1           552 /* (UINT) Kurvenautomat VAX1: Master-Achse */
#define ACP10PAR_AUT_MA_S_START_VAX1        553 /* (DINT) Kurvenautomat VAX1: Startposition der Master-Achse [Einh.] */
#define ACP10PAR_AUT_MA_IVSTART_VAX1        554 /* (UDINT) Kurvenautomat VAX1: Startintervall der Master-Achse [Einh.] */
#define ACP10PAR_AUT_MA_V_MAX_VAX1          555 /* (REAL) Kurvenautomat VAX1: Maximale Geschwindigkeit der Master-Achse [Einh./s] */
#define ACP10PAR_AUT_ST_INDEX_VAX1          556 /* (USINT) Kurvenautomat VAX1: Index eines Parametersatzes f�r einen Zustand */
#define ACP10PAR_AUT_EV_INDEX_VAX1          557 /* (USINT) Kurvenautomat VAX1: Index eines Parametersatzes f�r ein Ereignis  */
#define ACP10PAR_AUT_ST_DATA_INDEX_VAX1     558 /* (UINT) Kurvenautomat VAX1: Index der Kurvenscheibendaten f�r einen Zustand */
#define ACP10PAR_AUT_COMP_MODE_VAX1         559 /* (USINT) Kurvenautomat VAX1: Ausgleichsgetriebe-Modus */
#define ACP10PAR_AUT_COMP_MA_S_VAX1         560 /* (DINT) Kurvenautomat VAX1: Ausgleichsweg der Master-Achse [Einh.] */
#define ACP10PAR_AUT_COMP_SL_S_VAX1         561 /* (DINT) Kurvenautomat VAX1: Ausgleichsweg der Slave-Achse [Einh.] */
#define ACP10PAR_AUT_EVENT_TYPE_VAX1        562 /* (USINT) Kurvenautomat VAX1: Ereignis Typ */
#define ACP10PAR_AUT_EVENT_ATTR_VAX1        563 /* (USINT) Kurvenautomat VAX1: Ereignis Attribut */
#define ACP10PAR_AUT_EVENT_ST_INDEX_VAX1    564 /* (USINT) Kurvenautomat VAX1: Index n�chster Zustand */
#define ACP10PAR_AUT_SIGNAL_SET_VAX1        565 /* (USINT) Kurvenautomat VAX1: Signal setzen */
#define ACP10PAR_AUT_ACT_ST_INDEX_VAX1      566 /* (USINT) Kurvenautomat VAX1: Index des aktuellen Zustands */
#define ACP10PAR_AUT_SIGNAL_RESET_VAX1      567 /* (USINT) Kurvenautomat VAX1: Signal r�cksetzen */
#define ACP10PAR_AUT_MA_FACTOR_VAX1         568 /* (DINT) Kurvenautomat VAX1: Multiplikationsfaktor der Master-Achse */
#define ACP10PAR_AUT_SL_FACTOR_VAX1         569 /* (DINT) Kurvenautomat VAX1: Multiplikationsfaktor der Slave-Achse */
#define ACP10PAR_AUT_ST_COUNT_INIT_VAX1     570 /* (UINT) Kurvenautomat VAX1: Anfangswert Zustandswiederholungen f�r Ereignis ncANZAHL */
#define ACP10PAR_AUT_ST_COUNT_SET_VAX1      571 /* (UINT) Kurvenautomat VAX1: Z�hler Zustandswiederholungen f�r das Ereignis ncANZAHL */
#define ACP10PAR_AUT_COMP_MA_S_MIN_VAX1     572 /* (DINT) Kurvenautomat VAX1: Minimaler Ausgleichsweg der Master-Achse [Einh.] */
#define ACP10PAR_AUT_ONL_PAR_LOCK_VAX1      573 /* (USINT) Kurvenautomat VAX1: Sperre f�r konsistente Online-Parameter-�nderung */
#define ACP10PAR_AUT_EVENT_ACTION_VAX1      574 /* (UDINT) Kurvenautomat VAX1: Aktion bei Zustands�bergang */
#define ACP10PAR_AUT_ACT_CAM_TYPE_VAX1      575 /* (USINT) Kurvenautomat VAX1: Kurventyp des aktuellen Zustands */
#define ACP10PAR_CMD_HOMING_VAX1            577 /* (DINT) Kommando VAX1: Referenzieren starten */
#define ACP10PAR_ENCOD2_S_ACT_FILTER        578 /* (DINT) Geber2: Gefilterte Istposition [Einh.] */
#define ACP10PAR_AUT_MA_ADD_AXIS            579 /* (UINT) Kurvenautomat: Additive Master-Achse */
#define ACP10PAR_AUT_MA_ADD_AXIS_VAX1       580 /* (UINT) Kurvenautomat VAX1: Additive Master-Achse */
#define ACP10PAR_AUT_SL_ADD_AXIS            581 /* (UINT) Kurvenautomat: Additive Slave-Achse */
#define ACP10PAR_AUT_SL_ADD_AXIS_VAX1       582 /* (UINT) Kurvenautomat VAX1: Additive Slave-Achse */
#define ACP10PAR_DIG_IN_QUICKSTOP_ENABLE    583 /* (USINT) Quickstop-Freigabe-Bits f�r digitale Eing�nge */
#define ACP10PAR_USER_I4_VAR1               584 /* (DINT) Anwender I4 Variable1 */
#define ACP10PAR_USER_I4_VAR2               585 /* (DINT) Anwender I4 Variable2 */
#define ACP10PAR_USER_R4_VAR1               586 /* (REAL) Anwender R4 Variable1 */
#define ACP10PAR_USER_R4_VAR2               587 /* (REAL) Anwender R4 Variable2 */
#define ACP10PAR_CMD_AUT_CONTROLLER         590 /* (UINT) Kommando: Regler ein-/ausschalten bei aktivem Kurvenautomaten */
#define ACP10PAR_STATION_NUMBER_CYCLIC      591 /* (UINT) Stationsnummer des Senders f�r Netzwerk-Kopplung */
#define ACP10PAR_MA_PARID_CYCLIC_POS        592 /* (UINT) Master Paramter-ID der Sende-Station f�r Netzwerk-Kopplung */
#define ACP10PAR_CONFIG_MA1_CYCLIC_POS      593 /* (STATION_NUMBER_CYCLIC, MA_PARID_CYCLIC_POS) Netzwerk-Station zum Empfang auf MA1_CYCLIC_POS konfigurieren */
#define ACP10PAR_CONFIG_MA2_CYCLIC_POS      594 /* (STATION_NUMBER_CYCLIC, MA_PARID_CYCLIC_POS) Netzwerk-Station zum Empfang auf MA2_CYCLIC_POS konfigurieren */
#define ACP10PAR_CONFIG_MA3_CYCLIC_POS      595 /* (STATION_NUMBER_CYCLIC, MA_PARID_CYCLIC_POS) Netzwerk-Station zum Empfang auf MA3_CYCLIC_POS konfigurieren */
#define ACP10PAR_SCALE_R4_PARID             596 /* (UINT) Parameter-Normierung: Parameter-ID des Eingabewertes */
#define ACP10PAR_SCALE_R4_OFFSET            597 /* (REAL) Parameter-Normierung: Offset */
#define ACP10PAR_SCALE_R4_FACTOR            598 /* (REAL) Parameter-Normierung: Faktor */
#define ACP10PAR_SCALE_R4_TO_I2             599 /* (INT) Parameter-Normierung: Ausgabewert (I2-Format) */
#define ACP10PAR_VCTRL_ENCOD_COUNT_DIR      672 /* (USINT) Motorgeber: Z�hlrichtung */
#define ACP10PAR_VCTRL_SCALE_LOAD_UNITS     673 /* (UDINT) Last-Ma�stab des Motorgebers: Einheiten pro VCTRL_SCALE_MOTOR_REV Motorumdrehungen [Einh.] */
#define ACP10PAR_VCTRL_SCALE_LOAD_MOTREV    674 /* (UDINT) Last-Ma�stab des Motorgebers: Motorumdrehungen */
#define ACP10PAR_ENCOD_REF_CHK_MODE         675 /* (UINT) Geber 1: Referenzimpuls-�berwachungsmodus */
#define ACP10PAR_ENCOD2_REF_CHK_MODE        676 /* (UINT) Geber 2: Referenzimpuls-�berwachungsmodus */
#define ACP10PAR_ENCOD3_REF_CHK_MODE        677 /* (UINT) Geber 3: Referenzimpuls-�berwachungsmodus */
#define ACP10PAR_ENCOD_REF_CHK_WINDOW       678 /* (UDINT) Geber 1: Referenzimpuls-�berwachungsfenster [Inkr] */
#define ACP10PAR_ENCOD2_REF_CHK_WINDOW      679 /* (UDINT) Geber 2: Referenzimpuls-�berwachungsfenster [Inkr] */
#define ACP10PAR_ENCOD3_REF_CHK_WINDOW      680 /* (UDINT) Geber 3: Referenzimpuls-�berwachungsfenster [Inkr] */
#define ACP10PAR_ENCOD_REF_WIDTH            681 /* (UDINT) Geber 1: Referenzimpuls-Breite [Inkr] */
#define ACP10PAR_ENCOD2_REF_WIDTH           682 /* (UDINT) Geber 2: Referenzimpuls-Breite [Inkr] */
#define ACP10PAR_ENCOD3_REF_WIDTH           683 /* (UDINT) Geber 3: Referenzimpuls-Breite [Inkr] */
#define ACP10PAR_ENCOD_REF_INTERVAL         684 /* (UDINT) Geber 1: Referenzimpuls-Intervall [Inkr] */
#define ACP10PAR_ENCOD2_REF_INTERVAL        685 /* (UDINT) Geber 2: Referenzimpuls-Intervall [Inkr] */
#define ACP10PAR_ENCOD3_REF_INTERVAL        686 /* (UDINT) Geber 3: Referenzimpuls-Intervall [Inkr] */
#define ACP10PAR_SCTRL_LIM_V_POS            687 /* (REAL) Drehzahlregler: Geschwindigkeits-Grenzwert in positive Richtung [1/s] */
#define ACP10PAR_SCTRL_LIM_V_NEG            688 /* (REAL) Drehzahlregler: Geschwindigkeits-Grenzwert in negative Richtung [1/s] */
#define ACP10PAR_SCALE_ENCOD3_UNITS         694 /* (UDINT) Geber3-Ma�stab: Einheiten pro SCALE_ENCOD3_REV Geberumdrehungen [Einh.] */
#define ACP10PAR_SCALE_ENCOD3_REV           695 /* (UDINT) Geber3-Ma�stab: Geberumdrehungen */
#define ACP10PAR_ENCOD3_COUNT_DIR           696 /* (USINT) Geber3-Interface: Z�hlrichtung */
#define ACP10PAR_ENCOD3_S_ACT               697 /* (DINT) Geber3: Istposition [Einh.] */
#define ACP10PAR_ENCOD3_SSI_ZERO_BITS       699 /* (USINT) SSI-Geber3: Anzahl der vorlaufenden Nullen */
#define ACP10PAR_ENCOD3_SSI_BITS            700 /* (USINT) SSI-Geber3: Anzahl der Datenbits */
#define ACP10PAR_ENCOD3_SSI_CODE            701 /* (USINT) SSI-Geber3: Datencodierung [ncGRAY/ncBINAER] */
#define ACP10PAR_ENCOD3_SSI_PARITY_CHK      702 /* (USINT) SSI-Geber3: Parity-Pr�fung [ncAUS/ncODD/ncEVEN] */
#define ACP10PAR_SCALE_ENCOD3_INCR          703 /* (UDINT) Geber3-Ma�stab: Inkremente pro Geberumdrehung */
#define ACP10PAR_ENCOD3_TYPE                704 /* (USINT) Geber3-Interface: Typ */
#define ACP10PAR_ENCOD3_HOMING_S            705 /* (DINT) Geber3: Referenzposition [Einh.] */
#define ACP10PAR_ENCOD3_HOMING_MODE_BITS    706 /* (USINT) Geber3: Referenzier-Modus-Steuerbits */
#define ACP10PAR_CMD_ENCOD3_HOMING          707 /* (ENCOD3_HOMING_S, ENCOD3_HOMING_MODE_BITS) Kommando Geber3: Referenzieren starten */
#define ACP10PAR_ENCOD3_S_FILTER_T10        708 /* (REAL) Geber3: Zeitkonstante f�r Istpositions-Filter [s] */
#define ACP10PAR_STAT_ENC3_HOMING_OK        709 /* (UDINT) Geber3: Status Referenzposition g�ltig */
#define ACP10PAR_ENCOD3_S_ACT_FILTER        710 /* (DINT) Geber3: Gefilterte Istposition [Einh.] */
#define ACP10PAR_ENCOD3_OUT_PARID           711 /* (UINT) Geber3: Parameter-ID des Ausgabewerts */
#define ACP10PAR_ENCOD2_OUT_PARID           712 /* (UINT) Geber2: Parameter-ID des Ausgabewerts */
#define ACP10PAR_ICTRL_SET_PARID            713 /* (UINT) Stromregler: Parameter-ID des Sollwerts */
#define ACP10PAR_VCTRL_S_ACT_PARID          722 /* (UINT) Vektoregler: Parameter-ID der Geber-Istposition */
#define ACP10PAR_ENCOD_LINE_CHK_IGNORE      727 /* (UINT) Geber: Signalleitungs-�berwachung ignorieren */
#define ACP10PAR_ENCOD2_LINE_CHK_IGNORE     728 /* (UINT) Geber2: Signalleitungs-�berwachung ignorieren */
#define ACP10PAR_ENCOD3_LINE_CHK_IGNORE     729 /* (UINT) Geber3: Signalleitungs-�berwachung ignorieren */
#define ACP10PAR_ISQ_MAX_UDC                748 /* (REAL) Zwischenkreisspannungsregler: Obere Stromgrenze */
#define ACP10PAR_ISQ_MIN_UDC                749 /* (REAL) Zwischenkreisspannungsregler: Untere Stromgrenze */
#define ACP10PAR_PAR_SEQU                   750 /* (DATA) Parameter-Sequenz */
#define ACP10PAR_PAR_SEQU_INDEX             751 /* (UINT) Index der Parameter-Sequenz f�r �bertragung */
#define ACP10PAR_CMD_PAR_SEQU_INIT          752 /* (UINT) Kommando: Parameter-Sequenz initialisieren */
#define ACP10PAR_CAM_CLEAR_TRIG_FIFO        753 /* (USINT) Kurvenscheibe: Werte in Trigger-FIFO beim ReStart l�schen */
#define ACP10PAR_CAM_S_SET                  754 /* (DINT) Kurvenscheibe: Sollposition [Einh.] */
#define ACP10PAR_ENCOD1_S_FILTER_T10        755 /* (REAL) Geber1: Zeitkonstante f�r Istpositions-Filter [s] */
#define ACP10PAR_ENCOD1_S_ACT_FILTER        756 /* (DINT) Geber1: Gefilterte Istposition [Einh.] */
#define ACP10PAR_PCTRL_SYS_TIME             757 /* (UDINT) Gesamtzeit des Antriebs im Lageregler-Zyklus [us] */
#define ACP10PAR_DRUMSEQ_ONL_PAR_LOCK       758 /* (USINT) Nockenschaltwerk: Sperre f�r konsistente Online-Parameter-�nderung */
#define ACP10PAR_ONL_PAR_LOCK               759 /* (DRUMSEQ_ONL_PAR_LOCK, AUT_ONL_PAR_LOCK, AUT_ONL_PAR_LOCK_VAX1) Sperre f�r konsistente Online-Parameter-�nderung */
#define ACP10PAR_MSG_CONF_ERROR_NUMBER      762 /* (UINT) Fehlernummer f�r die Meldungskonfiguration */
#define ACP10PAR_MSG_CONF_ACTIVATE_ERROR    763 /* (USINT) Meldungskonfiguration: Fehlernummer aktivieren */
#define ACP10PAR_AUT_COMP_SL_V_MAX          764 /* (REAL) Kurvenautomat: Maximale Geschwindigkeit der Slave-Achse im Ausgleich [Einh./s] */
#define ACP10PAR_AUT_COMP_SL_V_MAX_VAX1     765 /* (REAL) Kurvenautomat VAX1: Maximale Geschwindigkeit der Slave-Achse im Ausgleich [Einh./s] */
#define ACP10PAR_AUT_COMP_SL_A1_MAX         766 /* (REAL) Kurvenautomat: Maximale Beschleunigung der Slave-Achse in Ausgleichs-Phase1 [Einh./s^2] */
#define ACP10PAR_AUT_COMP_SL_A1_MAX_VAX1    767 /* (REAL) Kurvenautomat VAX1: Maximale Beschleunigung der Slave-Achse in Ausgleichs-Phase1 [Einh./s^2] */
#define ACP10PAR_AUT_COMP_SL_A2_MAX         768 /* (REAL) Kurvenautomat: Maximale Beschleunigung der Slave-Achse in Ausgleichs-Phase2 [Einh./s^2] */
#define ACP10PAR_AUT_COMP_SL_A2_MAX_VAX1    769 /* (REAL) Kurvenautomat VAX1: Maximale Beschleunigung der Slave-Achse in Ausgleichs-Phase2 [Einh./s^2] */
#define ACP10PAR_AUT_POLY_CHECK             770 /* (UINT) Kurvenautomat: Kurvenscheiben Polynom-Daten �berpr�fen */
#define ACP10PAR_AUT_S_SET                  771 /* (DINT) Kurvenautomat: Sollposition [Einh.] */
#define ACP10PAR_CMD_AUT_ST_CHECK           772 /* (USINT) Kommando: Parametersatzes f�r einen Kurvenautomaten-Zustand �berpr�fen */
#define ACP10PAR_CMD_AUT_ST_CHECK_VAX1      773 /* (USINT) Kommando VAX1: Parametersatzes f�r einen Kurvenautomaten-Zustand �berpr�fen */
#define ACP10PAR_ENCOD1_S_FILTER_TE         774 /* (REAL) Geber1: Extrapolations-Zeit f�r Istpositions-Filter [s] */
#define ACP10PAR_ENCOD2_S_FILTER_TE         775 /* (REAL) Geber2: Extrapolations-Zeit f�r Istpositions-Filter [s] */
#define ACP10PAR_ENCOD3_S_FILTER_TE         776 /* (REAL) Geber3: Extrapolations-Zeit f�r Istpositions-Filter [s] */
#define ACP10PAR_FUNCTION_BLOCK_CREATE      777 /* (UINT) Funktions-Block erzeugen */
#define ACP10PAR_CMD_CYC_ABS_MOVE           778 /* (UINT) Kommando: Bewegung mit zyklischer Positionsvorgabe starten */
#define ACP10PAR_CMD_CYC_ABS_MOVE_VAX1      779 /* (UINT) Kommando VAX1: Bewegung mit zyklischer Positionsvorgabe starten */
#define ACP10PAR_CYC_ABS_MOVE_PARID         780 /* (UINT) Paramter-ID f�r zyklische Position */
#define ACP10PAR_CYC_ABS_MOVE_PARID_VAX1    781 /* (UINT) VAX1: Paramter-ID f�r zyklische Position */
#define ACP10PAR_OVERRIDE_VAX1              782 /* (V_OVERRIDE_VAX1, A_OVERRIDE_VAX1) Basis-Bewegungen VAX1: Override */
#define ACP10PAR_V_OVERRIDE_VAX1            783 /* (INT) Basis-Bewegungen VAX1: Geschwindigkeits-Override */
#define ACP10PAR_A_OVERRIDE_VAX1            784 /* (INT) Basis-Bewegungen VAX1: Beschleunigungs-Override */
#define ACP10PAR_DRUMSEQ_DO_DELAY_ON        785 /* (REAL) Nockenschaltwerk: Schaltverz�gerung f�r Ausgang EIN [s] */
#define ACP10PAR_DRUMSEQ_DO_DELAY_OFF       786 /* (REAL) Nockenschaltwerk: Schaltverz�gerung f�r Ausgang AUS [s] */
#define ACP10PAR_AUT_MSG_MODE_BITS          798 /* (UDINT) Kurvenautomat: Meldungs-Modus-Steuerbits */
#define ACP10PAR_BRMOD_BSL                 1001 /* (BRMOD) B&R-Modul: BS-Loader */
#define ACP10PAR_BRMOD_NCSYS               1002 /* (BRMOD) B&R-Modul: NC-Betriebssystem */
#define ACP10PAR_BRMOD_VERSION             1010 /* (UINT) Version eines B&R-Modules */
#define ACP10PAR_BRMOD_DATE_TIME           1011 /* (T5) Datum und Zeit eines B&R-Modules */
#define ACP10PAR_BRMOD_NC_MOD_TYP          1012 /* (USINT) NC-Modul-Typ eines B&R-Modules */
#define ACP10PAR_BRMOD_RD_SECTION          1013 /* (DINT) B&R-Modul: Section f�r Lese-Zugriff */
#define ACP10PAR_BRMOD_WR_SECTION          1014 /* (DINT) B&R-Modul: Section f�r Schreib-Zugriff */
#define ACP10PAR_CMD_SW_RESET              1050 /* (NIL) Kommando: SW-Reset */
#define ACP10PAR_CMD_BOOT_STATE            1051 /* (UINT) Kommando: Boot-Zustand wechseln */
#define ACP10PAR_BOOT_STATE                1052 /* (UINT) Boot-Zustand des Antriebes */
#define ACP10PAR_CMD_BURN_SYSMOD           1053 /* (NIL) Kommando: System-Modul auf FPROM brennen */
#define ACP10PAR_STAT_BURN_SYSMOD          1054 /* (USINT) Status f�r System-Modul auf FPROM brennen */
#define ACP10PAR_RD_BLOCK_SEGM             1060 /* (BYTES6) Datenblocksegment lesen */
#define ACP10PAR_RD_BLOCK_LAST_SEGM        1061 /* (BYTES6) Letztes Datenblocksegment lesen */
#define ACP10PAR_CMD_RD_BLOCK_ABORT        1062 /* (NIL) Kommando: Datenblock-Lesezugriff abbrechen */
#define ACP10PAR_RD_BLOCK_OFFSET           1063 /* (UDINT) Offset f�r Datenblock-Lesezugriff */
#define ACP10PAR_RD_BLOCK_BYTES            1064 /* (UDINT) Restliche Bytes f�r Datenblock-Lesezugriff */
#define ACP10PAR_WR_BLOCK_SEGM             1070 /* (BYTES6) Datenblocksegment schreiben */
#define ACP10PAR_WR_BLOCK_LAST_SEGM        1071 /* (BYTES6) Letztes Datenblocksegment schreiben */
#define ACP10PAR_CMD_WR_BLOCK_ABORT        1072 /* (NIL) Kommando: Datenblock-Schreibzugriff abbrechen */
#define ACP10PAR_WR_BLOCK_OFFSET           1073 /* (UDINT) Offset f�r Datenblock-Schreibzugriff */
#define ACP10PAR_WR_BLOCK_BYTES            1074 /* (UDINT) Restliche Bytes f�r Datenblock-Schreibzugriff */
#define ACP10PAR_TRACE_DATA                1100 /* (DATA) Trace-Daten */
#define ACP10PAR_BRC_REQU_CYCLIC_STATUS    1200 /* (UINT) Broadcast-Anforderung des zyklischen Status */
#define ACP10PAR_DRIVE_SYNC_CMD_LATCH      1201 /* (NIL) Latch-Kommando f�r Antriebssynchronisierung */
#define ACP10PAR_DRIVE_SYNC_TIMESTAMP      1202 /* (BYTES6) Zeitstempel f�r Antriebssysnchronisierung */
#define ACP10PAR_ERROR_RESPONSE           65535 /* (ERROR_NUMBER, ERROR_INFO) Fehler-Response */

#endif /* ACP10PAR_H_ */ 

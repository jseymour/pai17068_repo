[hardwarekonfig]
wurzel: 1
[1.1]
spi: OBJEKTID=2
{1.3}
klasse: "HC_Moduluebersicht.Class"
name: _PRJ-Moduluebersicht
[10.1]
[10.100]
moduladr: 0
steckplatzgrp: "sysbus"
steckplatzinfo: NUMMER=1, MODULID=0, FLAGS=0
steckplatzgrp: "iobus"
steckplatzinfo: NUMMER=1, MODULID=11, FLAGS=1
steckplatzinfo: NUMMER=2, MODULID=13, FLAGS=3
steckplatzinfo: NUMMER=3, MODULID=13, FLAGS=2
steckplatzinfo: NUMMER=4, MODULID=0, FLAGS=0
{10.101}
klasse: "2003"
[11.1]
[11.100]
moduladr: 0
modulname: CPU
steckplatzgrp: "subbus"
steckplatzinfo: NUMMER=1, MODULID=19, FLAGS=1
steckplatzinfo: NUMMER=2, MODULID=7, FLAGS=1
steckplatzinfo: NUMMER=3, MODULID=17, FLAGS=1
steckplatzinfo: NUMMER=4, MODULID=5, FLAGS=1
anschlussgrp: "canio2"
anschlussinfo: NUMMER=1, MODULID=12, FLAGS=1
anschlussinfo: NUMMER=5, MODULID=18, FLAGS=1
anschlussinfo: NUMMER=6, MODULID=3, FLAGS=1
[11.101]
[11.106]
comparam_canio2: HANDLING=11, BAUD=500000, IOTIMEOUT=1000, SLAVETIMEOUT=8000, REPCOUNT=3, ANPCOUNT=80, PRI=1, EXCEPTION
{11.108}
klasse: "7CP476.60-1"
[2.1]
spi: OBJEKTID=10
spi: OBJEKTID=20
spi: OBJEKTID=12
spi: OBJEKTID=4
{2.2}
klasse: "HC_SPS.Class"
familie: 2003
[7.1]
[7.100]
moduladr: 0
slotnr: 2
{7.101}
klasse: "7DI135.7"
[17.1]
[17.100]
moduladr: 0
slotnr: 3
{17.101}
klasse: "7DI135.7"
[5.1]
[5.100]
moduladr: 0
slotnr: 4
{5.101}
klasse: "7DI135.7"
[13.1]
[13.100]
moduladr: 1
{13.101}
klasse: "7CM211.7"
[18.1]
[18.100]
moduladr: 0
verbunden: 11
busnr: 1
verwaltungsobj: MODULID=11
knotennr: 5
[18.101]
{18.107}
klasse: "7EX270.50-1"
[21.1]
[21.100]
moduladr: 1
busnr: 1
verwaltungsobj: MODULID=11
knotennr: 5
{21.101}
klasse: "7DM435.7"
[20.1]
[20.100]
moduladr: 0
busnr: 1
verwaltungsobj: MODULID=11
knotennr: 5
modulname: APS1 Verklebung 2003 CAN I/O
steckplatzgrp: "iobus"
steckplatzinfo: NUMMER=1, MODULID=18, FLAGS=1
steckplatzinfo: NUMMER=2, MODULID=21, FLAGS=1
steckplatzinfo: NUMMER=3, MODULID=0, FLAGS=0
steckplatzinfo: NUMMER=4, MODULID=0, FLAGS=0
{20.101}
klasse: "2003 CAN I/O"
[14.1]
[14.100]
busnr: 1
verwaltungsobj: MODULID=11
knotennr: 1
slotnr: 1
{14.101}
klasse: "8AC110.00"
[15.1]
[15.100]
busnr: 1
verwaltungsobj: MODULID=11
knotennr: 1
slotnr: 2
{15.101}
klasse: "8AC120.00"
[3.1]
[3.100]
moduladr: 0
verbunden: 11
busnr: 1
verwaltungsobj: MODULID=11
knotennr: 6
[3.101]
{3.107}
klasse: "7EX270.50-1"
[6.1]
[6.100]
moduladr: 1
busnr: 1
verwaltungsobj: MODULID=11
knotennr: 6
{6.101}
klasse: "7CM211.7"
[8.1]
[8.100]
moduladr: 3
busnr: 1
verwaltungsobj: MODULID=11
knotennr: 6
{8.101}
klasse: "7CM211.7"
[4.1]
[4.100]
moduladr: 0
busnr: 1
verwaltungsobj: MODULID=11
knotennr: 6
modulname: APS2 Verklebung 2003 CAN I/O 
steckplatzgrp: "iobus"
steckplatzinfo: NUMMER=1, MODULID=3, FLAGS=1
steckplatzinfo: NUMMER=2, MODULID=6, FLAGS=3
steckplatzinfo: NUMMER=3, MODULID=6, FLAGS=2
steckplatzinfo: NUMMER=4, MODULID=8, FLAGS=3
steckplatzinfo: NUMMER=5, MODULID=8, FLAGS=2
steckplatzinfo: NUMMER=6, MODULID=0, FLAGS=0
{4.101}
klasse: "2003 CAN I/O"
[19.1]
[19.100]
moduladr: 0
slotnr: 1
{19.101}
klasse: "7IF361.70-1"
[12.1]
[12.100]
moduladr: 0
verbunden: 11
busnr: 1
verwaltungsobj: MODULID=11
knotennr: 1
station: 1
steckplatzgrp: "acobus"
steckplatzinfo: NUMMER=1, MODULID=14, FLAGS=1
steckplatzinfo: NUMMER=2, MODULID=15, FLAGS=1
steckplatzinfo: NUMMER=3, MODULID=0, FLAGS=0
steckplatzinfo: NUMMER=4, MODULID=0, FLAGS=0
steckplatzgrp: "motorbus"
steckplatzinfo: NUMMER=1, MODULID=0, FLAGS=0
[12.101]
[12.107]
{12.1456}
klasse: "8V1022.00"
ncsoftware: NAME="acp10", ID=128, NETWORK=0x8000

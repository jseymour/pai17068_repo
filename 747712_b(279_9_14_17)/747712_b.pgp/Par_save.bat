@echo off
rem 
rem  �nderungen:
rem  ***********
rem  Datum     Vers.  	Bemerkung                                              	Name
rem 
rem  01.01.00  00.00  	neu erstellt                                            PET
rem  --------------------------------------------------------------------------------
rem  09.10.00  00.01  	Parameter von verschiedenen Aggregaten in verschiedene 
rem 			Verzeichnisse sichern	                                PET
rem  --------------------------------------------------------------------------------
rem  13.02.01  00.02  	Kommandozeilenparameter Auswertung eingebaut		PET
rem  --------------------------------------------------------------------------------
rem  12.03.01  00.03  	Datenmodul mit Sortennamen laden			PET
rem  --------------------------------------------------------------------------------
rem  02.04.01  00.04  	Parameter f�r 6 Aggregate verwalten			PET
rem  --------------------------------------------------------------------------------
rem  06.04.01  00.05    Fehler beim Kopieren der Parameter f�r Aggregat 6	PET
rem  --------------------------------------------------------------------------------
rem  13.06.02  01.06    Lange Dateinamen umgehen beim Kopieren unter DOS        PET
rem
rem 

cls
echo ********************************************
echo * Laden der Maschinen- und Sortenparameter *
echo *  aus dem Autocolaggregat in den Laptop   *
echo *                                          *
echo * machine- and product-parameter upload    *
echo *    from the aggregate to the laptop      *
echo ********************************************
echo 

set COM=com2
set PORT=2

rem m�gliche Kommandozeilenparameter:
rem  load [COM] [AGGREGAT_NR]
rem        COM         = COM1 / COM2
rem	   AGGREGAT_NR = 1 / 2 / 3 / 4 / 5 / 6

REM testen des ersten Parameters - serielle Schnittstelle
if "%1" == "COM1" GOTO COM1
if "%1" == "COM2" GOTO COM2

rem 1 . Kommandozeilenparameter ung�ltig
GOTO FRAGE_COM

:COM1
set COM=com1
GOTO UPLOAD

:COM2
set COM=com2
GOTO UPLOAD

:FRAGE_COM

echo 
echo Choose the transmission port COM1=[1] or COM2=[2] ?
CHOICE /C:12 Soll das Laden ueber COM1=[1] oder COM2=[2] laufen?
IF ERRORLEVEL 2 GOTO UPLOAD

set COM=com1
set PORT=1

:UPLOAD

cls

rem ----------Parameter Datenobjekte hochladen-------------------------------------------

set ZEIT=15
set ACKN=300

tools\ul ma_par.br /C%COM% /R%ZEIT% /A%ACKN%
if errorlevel 1 goto ERROR

tools\ul sort_par.br /C%COM% /R%ZEIT% /A%ACKN%
if errorlevel 1 goto ERROR

tools\ul sor_text.br /C%COM% /R%ZEIT% /A%ACKN%
if errorlevel 1 goto ERROR

cls


REM testen des zweiten Parameters - Nummer des Aggregates?

if "%2" == "1" GOTO AGGR_1
if "%2" == "2" GOTO AGGR_2
if "%2" == "3" GOTO AGGR_3
if "%2" == "4" GOTO AGGR_4
if "%2" == "5" GOTO AGGR_5
if "%2" == "6" GOTO AGGR_6

rem 2. Kommandozeilenparameter ung�ltig


echo From wich aggregate did you upload the parameter?
CHOICE /C:123456 Von welchem Aggregat wurden die Parameter geladen?
if errorlevel 6 goto AGGR_6
if errorlevel 5 goto AGGR_5
if errorlevel 4 goto AGGR_4
if errorlevel 3 goto AGGR_3
if errorlevel 2 goto AGGR_2
if errorlevel 1 goto AGGR_1

goto FERTIG

:AGGR_1
echo 
echo Kopieren der Parameter in das Verzeichniss fuer Aggregat 1
if not exist Aggregat_1\ma_par.br goto AGGR_1_K
move ma_par.br .\Aggregat_1\ma_par.br
move sort_par.br .\Aggregat_1\sort_par.br
move sor_text.br .\Aggregat_1\sor_text.br
goto FERTIG
:AGGR_1_K
move ma_par.br .\Aggreg~1\ma_par.br
move sort_par.br .\Aggreg~1\sort_par.br
move sor_text.br .\Aggreg~1\sor_text.br
goto FERTIG

:AGGR_2
echo 
echo Kopieren der Parameter in das Verzeichniss fuer Aggregat 2
if not exist Aggregat_2\ma_par.br goto AGGR_2_K
move ma_par.br .\Aggregat_2\ma_par.br
move sort_par.br .\Aggregat_2\sort_par.br
move sor_text.br .\Aggregat_2\sor_text.br
goto FERTIG
:AGGR_2_K
move ma_par.br .\Aggreg~2\ma_par.br
move sort_par.br .\Aggreg~2\sort_par.br
move sor_text.br .\Aggreg~2\sor_text.br
goto FERTIG

:AGGR_3
echo 
echo Kopieren der Parameter in das Verzeichniss fuer Aggregat 3
if not exist Aggregat_3\ma_par.br goto AGGR_3_K
move ma_par.br .\Aggregat_3\ma_par.br
move sort_par.br .\Aggregat_3\sort_par.br
move sor_text.br .\Aggregat_3\sor_text.br
goto FERTIG
:AGGR_3_K
move ma_par.br .\Aggreg~3\ma_par.br
move sort_par.br .\Aggreg~3\sort_par.br
move sor_text.br .\Aggreg~3\sor_text.br
goto FERTIG

:AGGR_4
echo 
echo Kopieren der Parameter in das Verzeichniss fuer Aggregat 4
if not exist Aggregat_4\ma_par.br goto AGGR_4_K
move ma_par.br .\Aggregat_4\ma_par.br
move sort_par.br .\Aggregat_4\sort_par.br
move sor_text.br .\Aggregat_4\sor_text.br
goto FERTIG
:AGGR_4_K
move ma_par.br .\Aggreg~4\ma_par.br
move sort_par.br .\Aggreg~4\sort_par.br
move sor_text.br .\Aggreg~4\sor_text.br
goto FERTIG

:AGGR_5
echo 
echo Kopieren der Parameter in das Verzeichniss fuer Aggregat 5
if not exist Aggregat_5\ma_par.br goto AGGR_5_K
move ma_par.br .\Aggregat_5\ma_par.br
move sort_par.br .\Aggregat_5\sort_par.br
move sor_text.br .\Aggregat_5\sor_text.br
goto FERTIG
:AGGR_5_K
move ma_par.br .\Aggreg~5\ma_par.br
move sort_par.br .\Aggreg~5\sort_par.br
move sor_text.br .\Aggreg~5\sor_text.br
goto FERTIG

:AGGR_6
echo 
echo Kopieren der Parameter in das Verzeichniss fuer Aggregat 6
if not exist Aggregat_6\ma_par.br goto AGGR_6_K
move ma_par.br .\Aggregat_6\ma_par.br
move sort_par.br .\Aggregat_6\sort_par.br
move sor_text.br .\Aggregat_6\sor_text.br
goto FERTIG
:AGGR_6_K
move ma_par.br .\Aggreg~6\ma_par.br
move sort_par.br .\Aggreg~6\sort_par.br
move sor_text.br .\Aggreg~6\sor_text.br
goto FERTIG

:FERTIG
echo 
echo 
echo -----------------------------------------
echo Sicherung erfolgreich abgeschlossen!
echo -----------------------------------------
echo   backup successfully completed!
echo -----------------------------------------
echo --- KRONES - Perfection in Automation ---

goto ENDE
:ERROR
echo 
echo 
echo !!! Sicherungung fehlerhaft abgebrochen !!!
echo 
echo !!! FAULT - backup aborted !!!

@echo on

:ENDE

@echo off

rem 
rem  �nderungen:
rem  ***********
rem  Datum     Vers.  	Bemerkung                                              	Name
rem 
rem  09.10.00  00.01  	neu erstellt                                            PET
rem  --------------------------------------------------------------------------------
rem  13.02.01  00.02  	Kommandozeilenparameter Auswertung eingebaut		PET
rem  --------------------------------------------------------------------------------
rem  12.03.01  00.03  	Datenmodul mit Sortennamen laden			PET
rem  --------------------------------------------------------------------------------
rem  02.04.01  00.04  	Parameter f�r 6 Aggregate verwalten			PET
rem 
rem 

cls
echo ********************************************
echo * Laden der Maschinen- und Sortenparameter *
echo *  vom Laptop in das Autocolaggregat       *
echo *                                          *
echo * machine- and product-parameter upload    *
echo *    from the laptop to the aggregate      *
echo ********************************************
echo 

set COM=com2
set PORT=2

rem m�gliche Kommandozeilenparameter:
rem  load [COM] [AGGREGAT_NR]
rem        COM         = COM1 / COM2
rem	   AGGREGAT_NR = 1 / 2 / 3 / 4 / 5 / 6

REM testen des ersten Parameters - serielle Schnittstelle
if "%1" == "COM1" GOTO COM1
if "%1" == "COM2" GOTO COM2

rem 1. Kommandozeilenparameter ung�ltig
GOTO FRAGE_COM

:COM1
set COM=com1
GOTO AGGREGAT_NR

:COM2
set COM=com2

:AGGREGAT_NR

REM testen des zweiten Parameters - Nummer des Aggregates?

if "%2" == "1" GOTO AGGR_1
if "%2" == "2" GOTO AGGR_2
if "%2" == "3" GOTO AGGR_3
if "%2" == "4" GOTO AGGR_4
if "%2" == "5" GOTO AGGR_5
if "%2" == "6" GOTO AGGR_6

rem Kommandozeilenparameter ung�ltig
GOTO FRAGE_AGGR_NR

:FRAGE_COM

echo 
echo Choose the transmission port COM1=[1] or COM2=[2] ?
CHOICE /C:12 Soll das Laden ueber COM1=[1] oder COM2=[2] laufen?
IF ERRORLEVEL 2 GOTO WEITER

set COM=com1
set PORT=1

:WEITER

:FRAGE_AGGR_NR
cls

echo In wich aggregate do you want to load the parameter?
CHOICE /C:123456 In welches Aggregat sollen die Parameter geladen werden?
if errorlevel 6 goto AGGR_6
if errorlevel 5 goto AGGR_5
if errorlevel 4 goto AGGR_4
if errorlevel 3 goto AGGR_3
if errorlevel 2 goto AGGR_2
if errorlevel 1 goto AGGR_1

:AGGR_1
echo 
echo Kopieren der Parameter aus dem Verzeichniss fuer Aggregat 1
if not exist Aggregat_1\ma_par.br goto AGGR_1_K
copy Aggregat_1\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggregat_1\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggregat_1\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG
:AGGR_1_K
copy Aggreg~1\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggreg~1\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggreg~1\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG

:AGGR_2
echo 
echo Kopieren der Parameter aus dem Verzeichniss fuer Aggregat 2
if not exist Aggregat_2\ma_par.br goto AGGR_2_K
copy Aggregat_2\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggregat_2\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggregat_2\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG
:AGGR_2_K
copy Aggreg~2\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggreg~2\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggreg~2\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG

:AGGR_3
echo 
echo Kopieren der Parameter aus dem Verzeichniss fuer Aggregat 3
if not exist Aggregat_3\ma_par.br goto AGGR_3_K
copy Aggregat_3\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggregat_3\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggregat_3\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG
:AGGR_3_K
copy Aggreg~3\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggreg~3\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggreg~3\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG

:AGGR_4
echo 
echo Kopieren der Parameter aus dem Verzeichniss fuer Aggregat 4
if not exist Aggregat_4\ma_par.br goto AGGR_4_K
copy Aggregat_4\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggregat_4\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggregat_4\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG
:AGGR_4_K
copy Aggreg~4\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggreg~4\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggreg~4\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG

:AGGR_5
echo 
echo Kopieren der Parameter aus dem Verzeichniss fuer Aggregat 5
if not exist Aggregat_5\ma_par.br goto AGGR_5_K
copy Aggregat_5\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggregat_5\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggregat_5\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG
:AGGR_5_K
copy Aggreg~5\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggreg~5\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggreg~5\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG

:AGGR_6
echo 
echo Kopieren der Parameter aus dem Verzeichniss fuer Aggregat 6
if not exist Aggregat_6\ma_par.br goto AGGR_6_K
copy Aggregat_6\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggregat_6\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggregat_6\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG
:AGGR_6_K
copy Aggreg~6\ma_par.br .\pgm\KRONES\CPU\ma_par.br 
copy Aggreg~6\sort_par.br .\pgm\KRONES\CPU\sort_par.br
copy Aggreg~6\sor_text.br .\pgm\KRONES\CPU\sor_text.br
goto FERTIG

:FERTIG

rem ----------Parameter Datenobjekte laden -------------------------------------------
set ZEIT=15
set ACKN=300

tools\boot DIAG /C%COM% /R%ZEIT% /A%ACKN% /W%ZEIT%
if errorlevel 1 goto ERROR

tools\clear /TFIXRAM /X /C%COM% /W%ZEIT% /R%ZEIT%
if errorlevel 1 goto ERROR

tools\boot HALT /C%COM% /R%ZEIT% /A%ACKN% /W%ZEIT%
if errorlevel 1 goto ERROR

tools\dl pgm\KRONES\CPU\ma_par.br /TFIXRAM /C%COM% /R%ZEIT% /A%ACKN%
if errorlevel 1 goto ERROR

tools\dl pgm\KRONES\CPU\sort_par.br /TFIXRAM /C%COM% /R%ZEIT% /A%ACKN%
if errorlevel 1 goto ERROR

tools\dl pgm\KRONES\CPU\sor_text.br /TFIXRAM /C%COM% /R%ZEIT% /A%ACKN%
if errorlevel 1 goto ERROR

tools\boot COLD /C%COM% /R%ZEIT% /A%ACKN%
if errorlevel 1 goto ERROR

cls
echo -----------------------------------------
echo Uebertragung erfolgreich abgeschlossen!
echo -----------------------------------------
echo   Transmission successfully completed!
echo -----------------------------------------
echo --- KRONES - Perfection in Automation ---

goto ENDE
:ERROR
echo 
echo 
echo !!! Uebertragung fehlerhaft abgebrochen !!!
echo 
echo !!! FAULT - Transmission aborted !!!

@echo on

:ENDE

@echo off

rem 
rem  �nderungen:
rem  ***********
rem  Datum     Vers.  	Bemerkung                                              	Name
rem 
rem  01.01.00  00.00  	neu erstellt                                            PET
rem  06.10.00  00.01  	Betriebsystem in RPS �bertragen				PET
rem  10.10.00  00.02  	Fixram mit Param. immer l�schen, Par_load.bat zum �bertr. PET
rem  18.10.00  00.03  	Modulliste in Datei gespeichert zur schnelleren �bertrg.  PET
rem  24.11.00  00.04  	getrennte Parameter Anzahl Versuche (RETRY) und Wartezeit (ZEIT) PET
rem  13.02.01  00.05  	Kommandozeilenparameter Auswertung eingebaut		PET
rem  28.03.01  00.06    zweiter Kaltstart am Ende eingebaut			PET
rem 

cls
echo ************************************************************
echo * Laden der Programme in das Autocolaggregat ueber COM-1/2 *
echo *   program download to the Autocol aggregate via COM-1/2  *
echo ************************************************************
echo 

rem Initialisierung der Variablen
set COM=com2
set ZEIT=15
set ACKN=300
set RETRY=30

rem m�gliche Kommandozeilenparameter:
rem  load [COM] [FIRMWARE]
rem        COM      = COM1 / COM2
rem	   FIRMWARE = FW_YES / FW_NO

REM testen des ersten Parameters - serielle Schnittstelle
if "%1" == "COM1" GOTO COM1
if "%1" == "COM2" GOTO COM2

rem Kommandozeilenparameter ung�ltig
GOTO FRAGE_COM

:COM1
set COM=com1
GOTO FIRMWARE

:COM2
set COM=com2
GOTO FIRMWARE


:FIRMWARE

REM testen des zweiten Parameters - Laden der Firmware?
if "%2" == "FW_YES" GOTO MIT_FW
if "%2" == "FW_NO" GOTO OHNE_FW

rem Kommandozeilenparameter ung�ltig
GOTO FRAGE_FW


:FRAGE_COM

echo 
echo COMMON RULES
echo If you wish to confirm a question with "YES" please type "J".
echo If you deny a question with "NO", please type "N".
echo 
echo 

echo Choose the transmission port COM1=[1] or COM2=[2] ?
CHOICE /C:12 Soll die Uebertragung ueber COM1=[1] oder COM2=[2] laufen?
IF ERRORLEVEL 2 GOTO WEITER

set COM=com1

:WEITER

cls
echo Are the CAN-ID's on the RPS, the display and on the ACOPOS adjusted correctly
echo according to the CAD-plan? 
echo Are the CAN-links correctly wired and the CAN-bus closing resistors correctly
echo switched? 
echo Is the "RUN" LED on the RPS lighting in green?
echo 
echo Sind die CAN-ID's an RPS, Display und ACOPOS gemaess CAD-Plan eingestellt?
echo Sind die CAN-Verbindungen richtig verdrahtet und Abschlusswiderstaende
echo eingestellt?
echo Die "RUN" -LED an der Steuerung leuchtet gruen?
echo 
echo 
echo Everything ok, continue? Yes=J, No=N
CHOICE /C:JN Alles ok, weiter?
IF ERRORLEVEL 2 GOTO error


:FRAGE_FW

rem --------Betriebssystem in die BuR-Steuerung-----------------------------------------------------------
cls
echo Download of the RPS-operating-system - only if the RPS-controller was replaced. 
echo  
echo Das RPS-Betriebssystem muss nur bei Austausch der Steuerung uebertragen werden.
echo 
echo 
echo Is it intended to download the RPS-operating-system?
CHOICE /C:JN Soll das Betriebssystem in die RPS-Steuerung uebertragen werden?
IF ERRORLEVEL 2 GOTO OHNE_FW

:MIT_FW
cls
echo To download the RPS-operating-system you have to switch the MODE-switch to 0.
echo Afterwards you have to switch off and on the RPS-controller.
echo  
echo Zum Laden des RPS-Betriebssystemes muss der MODE-Schalter auf 0 gedreht
echo werden und die RPS-Steuerung muss Aus- und Eingeschaltet werden!

echo  
echo Is the MODE-switch on the 0-position and was the RPS switched off and on?
CHOICE /C:JN Steht der MODE-Schalter auf 0 und Steuerung Aus- und Eingeschaltet?
IF ERRORLEVEL 2 GOTO error
@echo on
cls

set ZEIT=15
set ACKN=300
set RETRY=30

tools\bootcomm pgm\KRONES\CPU\Cp476v22.s17 /C%COM% /B57600 /R%RETRY% /Vstat

@echo off

cls
echo The MODE-switch has to be switched in the position 4
echo and you have to switch off and on the RPS-controller.
echo  
echo Der MODE-Schalter muss auf 4 gedreht werden
echo und die Steuerung muss Aus- und Eingeschaltet werden!

echo  
echo Is the MODE-switch in position 4, was the RPS switched off/on and is in RUN-Mode now?
CHOICE /C:JN MODE-Schalter wieder auf 4, Steuerung Aus- und Eingeschaltet und auf RUN?
IF ERRORLEVEL 2 GOTO error

:OHNE_FW


cls
rem --------Loeschen des Speichers-----------------------------------------------------------

cls
echo erasing the memory (ROM) ...
echo Loeschen des Speichers (ROM) ...
echo 

set ZEIT=15
set ACKN=300
set RETRY=30

tools\boot DIAG /C%COM% /R%RETRY% /A%ACKN% /W%ZEIT%
if errorlevel 1 goto ERROR

tools\clear /TUSRROM /X /C%COM% /R%RETRY% /A%ACKN% /W%ZEIT%
if errorlevel 1 goto error

tools\boot COLD /C%COM% /R%RETRY% /A%ACKN% /W%ZEIT%
if errorlevel 1 goto ERROR

rem ---------Systemmodule uebertragen----------------------------------------------------------

cls
echo system modules download ...
echo Uebertragen der Systemmodule ...
echo 

tools\dl pgm\KRONES\CPU\sysconf.br /TSYSROM /C%COM% /R%RETRY% /A%ACKN%
if errorlevel 1 goto error

tools\boot COLD /C%COM% /R%RETRY% /A%ACKN% /W%ZEIT%
if errorlevel 1 goto ERROR

tools\dl /FSystem1.txt /TUSRROM /C%COM% /R%RETRY% /A%ACKN%
if errorlevel 1 goto error

tools\boot COLD /C%COM% /W%ZEIT% /A%ACKN% /R%RETRY%
if errorlevel 1 goto error

cls
echo ACOPOS-operating-system download ...
echo Uebertragen des ACOPOS-Betriebssystems ...
echo 

tools\boot COLD /C%COM% /R%RETRY% /A%ACKN% /W%RETRY%
if errorlevel 1 goto error

cls
echo The following operating-system download to the ACOPOS
echo lasts about 2 minutes.
echo 
echo Die folgende Uebertragung des Betriebssystems in den ACOPOS
echo dauert ungefaehr 2 Minuten.
echo 

tools\dl pgm\KRONES\CPU\acp10sys.br /TUSRROM /C%COM% /R%RETRY% /A%ACKN%
if errorlevel 1 goto error

tools\boot COLD /C%COM% /R%RETRY% /A%ACKN% /W%RETRY%
if errorlevel 1 goto error

tools\dl /FSystem2.txt /TUSRROM /C%COM% /R%RETRY% /A%ACKN%
if errorlevel 1 goto error

tools\boot COLD /C%COM% /W%ZEIT% /A%ACKN% /R%RETRY%
if errorlevel 1 goto error

cls
rem --------Loeschen des Speichers-----------------------------------------------------------

cls
echo erasing the memory (FIXRAM) ...
echo Loeschen des Speichers (FIXRAM) ...
echo 

tools\boot DIAG /C%COM% /R%RETRY% /A%ACKN% /W%ZEIT%
if errorlevel 1 goto ERROR

tools\clear /TFIXRAM /X /C%COM% /R%RETRY% /A%ACKN% /W%ZEIT%
if errorlevel 1 goto ERROR
 
tools\boot COLD /C%COM% /R%RETRY% /A%ACKN% /W%ZEIT%
if errorlevel 1 goto ERROR

rem zweiter Kaltstart um Datenmodule im Ram - Speicher zu l�schen
tools\boot COLD /C%COM% /R%RETRY% /A%ACKN% /W%ZEIT%
if errorlevel 1 goto ERROR

rem ----------Anwendermodule uebertragen---------------------------------------------------------
cls
echo user-modules download ...
echo Uebertragen der Anwendermodule ...
echo 

tools\dl /FAnwender.txt /TUSRROM /C%COM% /R%RETRY% /A%ACKN%
if errorlevel 1 goto error

tools\boot HALT /C%COM% /R%RETRY% /A%ACKN% /W%ZEIT_DAIG%
if errorlevel 1 goto error

tools\boot COLD /C%COM% /R%RETRY% /A%ACKN% /W%ZEIT_DAIG%
if errorlevel 1 goto error

tools\boot COLD /C%COM% /R%RETRY% /A%ACKN% /W%ZEIT_DAIG%
if errorlevel 1 goto error

cls
echo 
echo   Programming successfully completed!
echo -----------------------------------------
echo Programmierung erfolgreich abgeschlossen!
echo -----------------------------------------
echo --- KRONES - Perfection in Automation ---
echo 
echo 
echo Start the parameter-download if necessary!
echo Bei Bedarf Parameter-Download starten!



goto ENDE
:error
echo 
echo 
echo !!! Programmierung fehlerhaft abgebrochen !!!
echo 
echo !!! FAULT - programming aborted !!!

:ENDE
@echo on
